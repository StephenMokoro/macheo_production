INSERT INTO `schoolcategories` (`categoryAutoId`, `categoryName`) VALUES
(1, 'Boys & Girls, Day & Boarding'),
(2, 'Boys Day & Boarding'),
(3, 'Girls Day & Boarding'),
(4, 'Boys & Girls Boarding'),
(5, 'Boys & Girls Day'),
(6, 'Boys Day'),
(7, 'Girls Day'),
(8, 'Boys Boarding'),
(9, 'Girls Boarding');