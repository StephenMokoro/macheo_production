-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 23, 2018 at 10:48 AM
-- Server version: 5.6.35-1+deb.sury.org~xenial+0.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `macheo`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `adminAutoId` int(10) NOT NULL,
  `adminStaffId` varchar(15) NOT NULL,
  `adminUserName` varchar(30) NOT NULL,
  `adminId` varchar(30) NOT NULL,
  `adminFname` varchar(30) NOT NULL,
  `adminLname` varchar(30) NOT NULL,
  `adminOtherNames` varchar(50) DEFAULT NULL,
  `adminGender` varchar(15) NOT NULL,
  `adminEmail` varchar(100) NOT NULL,
  `adminPhone1` varchar(30) NOT NULL,
  `adminProfilePhoto` varchar(150) DEFAULT NULL,
  `adminDateAdded` datetime NOT NULL,
  `adminDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `adminActiveStatus` tinyint(1) NOT NULL DEFAULT '1',
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `classAutoId` int(10) NOT NULL,
  `classMenteeId` int(10) NOT NULL,
  `formCode` varchar(10) NOT NULL,
  `classYear` year(4) NOT NULL,
  `menteePromoted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Denote 1 if the mentee is promoted to next class',
  `menteeDropped` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Denote 1 if the mentee drops out of the program while in this class',
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `courseAutoId` int(5) NOT NULL,
  `courseName` varchar(100) NOT NULL,
  `courseCode` varchar(20) NOT NULL,
  `courseInstitutionId` int(5) NOT NULL,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `formCode` varchar(10) NOT NULL,
  `formName` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `generalexams`
--

CREATE TABLE `generalexams` (
  `examAutoId` int(10) NOT NULL,
  `examName` varchar(100) NOT NULL,
  `examTermCode` varchar(10) NOT NULL,
  `examFormCode` varchar(5) NOT NULL,
  `examLock` tinyint(1) NOT NULL DEFAULT '0',
  `examDateCreated` date NOT NULL,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `generalsubjects`
--

CREATE TABLE `generalsubjects` (
  `subjectAutoId` int(10) NOT NULL,
  `subjectName` varchar(30) NOT NULL,
  `subjectCode` varchar(10) NOT NULL,
  `subjectCompulsory` tinyint(1) NOT NULL DEFAULT '0',
  `subjectLocked` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `general_exams_performance`
--

CREATE TABLE `general_exams_performance` (
  `perfAutoId` int(10) NOT NULL,
  `perfMenteeId` int(10) NOT NULL,
  `perfExamId` int(10) NOT NULL,
  `perfSubjectId` int(10) NOT NULL,
  `perfScore` double(5,2) NOT NULL,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `guardians`
--

CREATE TABLE `guardians` (
  `guardianAutoId` int(10) NOT NULL,
  `guardianId` varchar(30) NOT NULL,
  `guardianFname` varchar(30) NOT NULL,
  `guardianLname` varchar(30) NOT NULL,
  `guardianOtherNames` varchar(50) DEFAULT NULL,
  `guardianPhone1` varchar(30) NOT NULL,
  `guardianPhone2` varchar(20) DEFAULT NULL,
  `guardianEmail` varchar(100) DEFAULT NULL,
  `guardianDoB` date DEFAULT NULL,
  `guardianGender` varchar(10) NOT NULL,
  `guardianProfession` varchar(100) DEFAULT NULL,
  `guardianProfilePhoto` varchar(150) DEFAULT NULL,
  `guardianDateAdded` datetime NOT NULL,
  `guardianDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `guardianActiveStatus` tinyint(1) NOT NULL DEFAULT '1',
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `institutions`
--

CREATE TABLE `institutions` (
  `institutionAutoId` int(10) NOT NULL,
  `institutionName` varchar(150) NOT NULL,
  `institutionLocation` varchar(100) DEFAULT NULL,
  `institutionAlias` varchar(30) DEFAULT NULL,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `internattendance`
--

CREATE TABLE `internattendance` (
  `attendanceAutoId` int(10) NOT NULL,
  `attendanceInternId` int(10) NOT NULL,
  `attendanceStatus` varchar(10) NOT NULL,
  `attendanceDate` date NOT NULL,
  `attendance_event_name` varchar(50) NOT NULL,
  `attendanceUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `interns`
--

CREATE TABLE `interns` (
  `internAutoId` int(10) NOT NULL,
  `internStudId` varchar(10) DEFAULT NULL,
  `internFname` varchar(30) NOT NULL,
  `internLname` varchar(30) NOT NULL,
  `internOtherNames` varchar(50) DEFAULT NULL,
  `internGender` varchar(10) NOT NULL,
  `internDoB` date DEFAULT NULL,
  `internPhone` varchar(30) NOT NULL,
  `internEmail` varchar(100) NOT NULL,
  `internCourseId` int(5) DEFAULT NULL,
  `internInstitutionId` int(5) DEFAULT NULL,
  `internYoS` int(1) NOT NULL,
  `internStartDate` date NOT NULL,
  `internDateExpectedOut` date DEFAULT NULL,
  `internDateLeft` date DEFAULT NULL,
  `internPassword` varchar(200) NOT NULL,
  `internProfilePhoto` varchar(50) DEFAULT NULL,
  `internActiveStatus` tinyint(1) NOT NULL DEFAULT '1',
  `internDateAdded` date NOT NULL,
  `internDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `internReasonInactive` varchar(150) DEFAULT NULL,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `macheoexams`
--

CREATE TABLE `macheoexams` (
  `examAutoId` int(10) NOT NULL,
  `examName` varchar(100) NOT NULL,
  `examTermCode` varchar(10) NOT NULL,
  `examFormCode` varchar(5) NOT NULL,
  `examLock` tinyint(1) NOT NULL DEFAULT '0',
  `examDateCreated` date NOT NULL,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `macheosubjects`
--

CREATE TABLE `macheosubjects` (
  `subjectAutoId` int(10) NOT NULL,
  `subjectName` varchar(30) NOT NULL,
  `subjectCode` varchar(10) NOT NULL,
  `subjectCompulsory` tinyint(1) NOT NULL DEFAULT '0',
  `subjectLocked` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `macheo_exams_performance`
--

CREATE TABLE `macheo_exams_performance` (
  `perfAutoId` int(10) NOT NULL,
  `perfMenteeId` int(10) NOT NULL,
  `perfExamId` int(10) NOT NULL,
  `perfSubjectId` int(10) NOT NULL,
  `perfScore` double(5,2) NOT NULL,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menteeattendance`
--

CREATE TABLE `menteeattendance` (
  `attendanceAutoId` int(10) NOT NULL,
  `attendanceMenteeId` int(10) NOT NULL,
  `attendanceStatus` varchar(10) NOT NULL,
  `attendanceDate` date NOT NULL,
  `attendance_event_name` varchar(50) DEFAULT NULL,
  `attendanceUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mentees`
--

CREATE TABLE `mentees` (
  `menteeAutoId` int(10) NOT NULL,
  `menteeFname` varchar(50) NOT NULL,
  `menteeLname` varchar(50) NOT NULL,
  `menteeOtherNames` varchar(50) DEFAULT NULL,
  `menteeGender` varchar(15) NOT NULL,
  `menteePhone` varchar(30) DEFAULT NULL,
  `menteeEmail` varchar(100) DEFAULT NULL,
  `menteeDoB` date DEFAULT NULL,
  `menteeSchoolId` int(10) NOT NULL,
  `menteeGuardianId` int(10) DEFAULT NULL,
  `menteeMentorId` int(10) DEFAULT NULL,
  `menteeDateJoined` date DEFAULT NULL,
  `menteeDateLeft` date DEFAULT NULL,
  `menteeProfilePhoto` varchar(150) DEFAULT NULL,
  `menteeActiveStatus` tinyint(1) NOT NULL DEFAULT '1',
  `menteeDateAdded` datetime NOT NULL,
  `menteeDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `menteeReasonInactive` varchar(150) DEFAULT NULL,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `mentorattendance`
--

CREATE TABLE `mentorattendance` (
  `attendanceAutoId` int(10) NOT NULL,
  `attendanceMentorId` int(10) NOT NULL,
  `attendanceStatus` varchar(10) NOT NULL,
  `attendanceDate` date NOT NULL,
  `attendance_event_name` varchar(50) NOT NULL,
  `attendanceUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mentors`
--

CREATE TABLE `mentors` (
  `mentorAutoId` int(10) NOT NULL,
  `mentorId` varchar(30) DEFAULT NULL,
  `mentorStudId` varchar(30) DEFAULT NULL,
  `mentorStaffId` varchar(20) DEFAULT NULL,
  `mentorFname` varchar(30) NOT NULL,
  `mentorLname` varchar(30) NOT NULL,
  `mentorGender` varchar(10) NOT NULL,
  `mentorDoB` date DEFAULT NULL,
  `mentorOtherNames` varchar(50) DEFAULT NULL,
  `mentorPhone1` varchar(30) NOT NULL,
  `mentorPhone2` varchar(30) DEFAULT NULL,
  `mentorEmail` varchar(100) NOT NULL,
  `mentorInstitutionId` int(10) NOT NULL,
  `mentorYoS` varchar(15) NOT NULL,
  `mentorCourse` int(5) DEFAULT NULL,
  `mentorProfession` varchar(100) DEFAULT NULL,
  `mentorLevel` int(2) NOT NULL DEFAULT '1',
  `mentorProfilePhoto` varchar(150) DEFAULT NULL,
  `mentorPassword` varchar(250) NOT NULL,
  `mentorDateAdded` datetime NOT NULL,
  `mentorDateJoined` date DEFAULT NULL,
  `mentorDateLeft` date DEFAULT NULL,
  `mentorDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mentorActiveStatus` tinyint(1) NOT NULL DEFAULT '1',
  `mentorReasonInactive` varchar(150) DEFAULT NULL,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `mentorship`
--

CREATE TABLE `mentorship` (
  `mentorshipAutoId` int(10) NOT NULL,
  `mentorshipMenteeId` int(10) NOT NULL,
  `mentorshipMentorId` int(10) NOT NULL,
  `mentorshipStartDate` date NOT NULL,
  `mentorshipEndDate` date NOT NULL,
  `mentorshipStatus` tinyint(1) DEFAULT '1',
  `mentorshipUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `schoolcategories`
--

CREATE TABLE `schoolcategories` (
  `categoryAutoId` int(10) NOT NULL,
  `categoryName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE `schools` (
  `schoolAutoId` int(10) NOT NULL,
  `schoolName` varchar(100) NOT NULL,
  `schoolLocation` varchar(100) DEFAULT NULL,
  `schoolCategoryId` int(5) NOT NULL,
  `schoolAlias` varchar(15) NOT NULL,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `selectedgeneralsubjects`
--

CREATE TABLE `selectedgeneralsubjects` (
  `selectAutoId` int(10) NOT NULL,
  `selectSubjectId` int(10) NOT NULL,
  `selectMenteeId` int(10) NOT NULL,
  `selectUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `selectedmacheosubjects`
--

CREATE TABLE `selectedmacheosubjects` (
  `selectAutoId` int(10) NOT NULL,
  `selectSubjectId` int(10) NOT NULL,
  `selectMenteeId` int(10) NOT NULL,
  `selectUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `teacherattendance`
--

CREATE TABLE `teacherattendance` (
  `attendanceAutoId` int(10) NOT NULL,
  `attendanceTeacherId` int(10) NOT NULL,
  `attendanceStatus` varchar(10) NOT NULL,
  `attendanceDate` date NOT NULL,
  `attendance_event_name` varchar(50) NOT NULL,
  `attendanceUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teacherinstitutions`
--

CREATE TABLE `teacherinstitutions` (
  `institutionAutoId` int(10) NOT NULL,
  `institutionName` varchar(100) NOT NULL,
  `institutionLocation` varchar(100) DEFAULT NULL,
  `institutionAlias` varchar(15) NOT NULL,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `teacherAutoId` int(10) NOT NULL,
  `teacherId` varchar(30) NOT NULL,
  `teacherFname` varchar(30) NOT NULL,
  `teacherLname` varchar(30) NOT NULL,
  `teacherOtherNames` varchar(50) DEFAULT NULL,
  `teacherGender` varchar(10) NOT NULL,
  `teacherPhone` varchar(30) NOT NULL,
  `teacherEmail` varchar(100) NOT NULL,
  `teacherSchoolId` int(5) DEFAULT NULL,
  `teacherStartDate` date DEFAULT NULL,
  `teacherDateLeft` int(11) NOT NULL,
  `teacherPassword` varchar(150) NOT NULL,
  `teacherActiveStatus` tinyint(1) NOT NULL DEFAULT '1',
  `teacherReasonInactive` varchar(150) DEFAULT NULL,
  `teacherProfile` varchar(250) DEFAULT NULL,
  `teacherProfilePhoto` varchar(150) DEFAULT NULL,
  `teacherDateAdded` datetime NOT NULL,
  `teacherDateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creating_user_group_id` int(5) NOT NULL,
  `creating_user_id` int(10) NOT NULL,
  `last_update_user_id` int(10) DEFAULT NULL,
  `last_update_user_group_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `user_group_id` int(5) NOT NULL,
  `user_group_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`adminAutoId`),
  ADD UNIQUE KEY `adminStaffId` (`adminStaffId`),
  ADD UNIQUE KEY `adminId` (`adminId`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`classAutoId`),
  ADD KEY `f2_mentee_id_fk` (`classMenteeId`),
  ADD KEY `class_form_code_fk` (`formCode`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`courseAutoId`),
  ADD KEY `course_institution_id_fk` (`courseInstitutionId`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`formCode`);

--
-- Indexes for table `generalexams`
--
ALTER TABLE `generalexams`
  ADD PRIMARY KEY (`examAutoId`),
  ADD KEY `termId` (`examTermCode`),
  ADD KEY `examFormId` (`examFormCode`);

--
-- Indexes for table `generalsubjects`
--
ALTER TABLE `generalsubjects`
  ADD PRIMARY KEY (`subjectAutoId`);

--
-- Indexes for table `general_exams_performance`
--
ALTER TABLE `general_exams_performance`
  ADD PRIMARY KEY (`perfAutoId`),
  ADD KEY `perf_general_mentee_id` (`perfMenteeId`,`perfExamId`,`perfSubjectId`),
  ADD KEY `perf_general_exam_id_fk` (`perfExamId`),
  ADD KEY `perf_general_subject_id_fk` (`perfSubjectId`);

--
-- Indexes for table `guardians`
--
ALTER TABLE `guardians`
  ADD PRIMARY KEY (`guardianAutoId`),
  ADD UNIQUE KEY `guardianPhone` (`guardianPhone1`),
  ADD UNIQUE KEY `guardianEmail` (`guardianEmail`),
  ADD UNIQUE KEY `guardianPhone2` (`guardianPhone2`);

--
-- Indexes for table `institutions`
--
ALTER TABLE `institutions`
  ADD PRIMARY KEY (`institutionAutoId`);

--
-- Indexes for table `internattendance`
--
ALTER TABLE `internattendance`
  ADD PRIMARY KEY (`attendanceAutoId`);

--
-- Indexes for table `interns`
--
ALTER TABLE `interns`
  ADD PRIMARY KEY (`internAutoId`),
  ADD UNIQUE KEY `internEmail` (`internEmail`),
  ADD UNIQUE KEY `internSUID` (`internStudId`),
  ADD KEY `intern_course_id_fk` (`internCourseId`),
  ADD KEY `intern_institution_fk` (`internInstitutionId`);

--
-- Indexes for table `macheoexams`
--
ALTER TABLE `macheoexams`
  ADD PRIMARY KEY (`examAutoId`),
  ADD KEY `termId` (`examTermCode`),
  ADD KEY `examFormId` (`examFormCode`);

--
-- Indexes for table `macheosubjects`
--
ALTER TABLE `macheosubjects`
  ADD PRIMARY KEY (`subjectAutoId`);

--
-- Indexes for table `macheo_exams_performance`
--
ALTER TABLE `macheo_exams_performance`
  ADD PRIMARY KEY (`perfAutoId`),
  ADD KEY `perf_macheo_mentee_id` (`perfMenteeId`,`perfExamId`,`perfSubjectId`),
  ADD KEY `perf_macheo_exam_id_fk` (`perfExamId`),
  ADD KEY `perf_macheo_subject_id_fk` (`perfSubjectId`);

--
-- Indexes for table `menteeattendance`
--
ALTER TABLE `menteeattendance`
  ADD PRIMARY KEY (`attendanceAutoId`),
  ADD KEY `attendance_mentee_id_fk` (`attendanceMenteeId`);

--
-- Indexes for table `mentees`
--
ALTER TABLE `mentees`
  ADD PRIMARY KEY (`menteeAutoId`),
  ADD KEY `studSchoolId` (`menteeSchoolId`),
  ADD KEY `studSchoolId_2` (`menteeSchoolId`,`menteeGuardianId`,`menteeMentorId`),
  ADD KEY `stud_guardian_id_fk` (`menteeGuardianId`),
  ADD KEY `stud_mentor_id_fk` (`menteeMentorId`);

--
-- Indexes for table `mentorattendance`
--
ALTER TABLE `mentorattendance`
  ADD PRIMARY KEY (`attendanceAutoId`);

--
-- Indexes for table `mentors`
--
ALTER TABLE `mentors`
  ADD PRIMARY KEY (`mentorAutoId`),
  ADD UNIQUE KEY `mentorPhone1` (`mentorPhone1`),
  ADD UNIQUE KEY `mentorEmail` (`mentorEmail`),
  ADD UNIQUE KEY `mentorId` (`mentorId`),
  ADD UNIQUE KEY `mentorPhone2` (`mentorPhone2`),
  ADD KEY `mentorInstitution` (`mentorInstitutionId`),
  ADD KEY `mentorYoS_2` (`mentorYoS`),
  ADD KEY `mentor_course_id_fk` (`mentorCourse`);

--
-- Indexes for table `mentorship`
--
ALTER TABLE `mentorship`
  ADD PRIMARY KEY (`mentorshipAutoId`),
  ADD KEY `mentorshipMenteeId` (`mentorshipMenteeId`,`mentorshipMentorId`),
  ADD KEY `mentorship_mentor_id_fk` (`mentorshipMentorId`);

--
-- Indexes for table `schoolcategories`
--
ALTER TABLE `schoolcategories`
  ADD PRIMARY KEY (`categoryAutoId`);

--
-- Indexes for table `schools`
--
ALTER TABLE `schools`
  ADD PRIMARY KEY (`schoolAutoId`),
  ADD UNIQUE KEY `schoolAlias` (`schoolAlias`),
  ADD KEY `schoolCategoryId` (`schoolCategoryId`);

--
-- Indexes for table `selectedgeneralsubjects`
--
ALTER TABLE `selectedgeneralsubjects`
  ADD PRIMARY KEY (`selectAutoId`),
  ADD KEY `selectSubjectId` (`selectSubjectId`,`selectMenteeId`),
  ADD KEY `selected_student_id_fk` (`selectMenteeId`);

--
-- Indexes for table `selectedmacheosubjects`
--
ALTER TABLE `selectedmacheosubjects`
  ADD PRIMARY KEY (`selectAutoId`),
  ADD KEY `selectSubjectId` (`selectSubjectId`,`selectMenteeId`),
  ADD KEY `selected_student_id_fk` (`selectMenteeId`);

--
-- Indexes for table `teacherattendance`
--
ALTER TABLE `teacherattendance`
  ADD PRIMARY KEY (`attendanceAutoId`),
  ADD KEY `attendance_teacher_id_fk` (`attendanceTeacherId`);

--
-- Indexes for table `teacherinstitutions`
--
ALTER TABLE `teacherinstitutions`
  ADD PRIMARY KEY (`institutionAutoId`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`teacherAutoId`),
  ADD UNIQUE KEY `teacherNID` (`teacherId`),
  ADD UNIQUE KEY `teacherPhone` (`teacherPhone`),
  ADD UNIQUE KEY `teacherEmail` (`teacherEmail`),
  ADD KEY `teacher_school_id_fk` (`teacherSchoolId`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`user_group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `adminAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `classAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `courseAutoId` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `generalexams`
--
ALTER TABLE `generalexams`
  MODIFY `examAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `generalsubjects`
--
ALTER TABLE `generalsubjects`
  MODIFY `subjectAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_exams_performance`
--
ALTER TABLE `general_exams_performance`
  MODIFY `perfAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `guardians`
--
ALTER TABLE `guardians`
  MODIFY `guardianAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `institutions`
--
ALTER TABLE `institutions`
  MODIFY `institutionAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `internattendance`
--
ALTER TABLE `internattendance`
  MODIFY `attendanceAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `interns`
--
ALTER TABLE `interns`
  MODIFY `internAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `macheoexams`
--
ALTER TABLE `macheoexams`
  MODIFY `examAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `macheosubjects`
--
ALTER TABLE `macheosubjects`
  MODIFY `subjectAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `macheo_exams_performance`
--
ALTER TABLE `macheo_exams_performance`
  MODIFY `perfAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menteeattendance`
--
ALTER TABLE `menteeattendance`
  MODIFY `attendanceAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mentees`
--
ALTER TABLE `mentees`
  MODIFY `menteeAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mentorattendance`
--
ALTER TABLE `mentorattendance`
  MODIFY `attendanceAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mentors`
--
ALTER TABLE `mentors`
  MODIFY `mentorAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mentorship`
--
ALTER TABLE `mentorship`
  MODIFY `mentorshipAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `schoolcategories`
--
ALTER TABLE `schoolcategories`
  MODIFY `categoryAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `schools`
--
ALTER TABLE `schools`
  MODIFY `schoolAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `selectedgeneralsubjects`
--
ALTER TABLE `selectedgeneralsubjects`
  MODIFY `selectAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `selectedmacheosubjects`
--
ALTER TABLE `selectedmacheosubjects`
  MODIFY `selectAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `teacherattendance`
--
ALTER TABLE `teacherattendance`
  MODIFY `attendanceAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `teacherinstitutions`
--
ALTER TABLE `teacherinstitutions`
  MODIFY `institutionAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `teacherAutoId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `user_group_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `classes`
--
ALTER TABLE `classes`
  ADD CONSTRAINT `class_form_code_fk` FOREIGN KEY (`formCode`) REFERENCES `forms` (`formCode`),
  ADD CONSTRAINT `class_mentee_id_fk` FOREIGN KEY (`classMenteeId`) REFERENCES `mentees` (`menteeAutoId`);

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `course_institution_id_fk` FOREIGN KEY (`courseInstitutionId`) REFERENCES `institutions` (`institutionAutoId`);

--
-- Constraints for table `general_exams_performance`
--
ALTER TABLE `general_exams_performance`
  ADD CONSTRAINT `perf_general_exam_id_fk` FOREIGN KEY (`perfExamId`) REFERENCES `generalexams` (`examAutoId`),
  ADD CONSTRAINT `perf_general_mentee_id_fk` FOREIGN KEY (`perfMenteeId`) REFERENCES `mentees` (`menteeAutoId`),
  ADD CONSTRAINT `perf_general_subject_id_fk` FOREIGN KEY (`perfSubjectId`) REFERENCES `generalsubjects` (`subjectAutoId`);

--
-- Constraints for table `interns`
--
ALTER TABLE `interns`
  ADD CONSTRAINT `intern_course_id_fk` FOREIGN KEY (`internCourseId`) REFERENCES `courses` (`courseAutoId`),
  ADD CONSTRAINT `intern_institution_fk` FOREIGN KEY (`internInstitutionId`) REFERENCES `institutions` (`institutionAutoId`);

--
-- Constraints for table `macheo_exams_performance`
--
ALTER TABLE `macheo_exams_performance`
  ADD CONSTRAINT `perf_macheo_exam_id_fk` FOREIGN KEY (`perfExamId`) REFERENCES `macheoexams` (`examAutoId`),
  ADD CONSTRAINT `perf_macheo_mentee_id_fk` FOREIGN KEY (`perfMenteeId`) REFERENCES `mentees` (`menteeAutoId`),
  ADD CONSTRAINT `perf_macheo_subject_id_fk` FOREIGN KEY (`perfSubjectId`) REFERENCES `macheosubjects` (`subjectAutoId`);

--
-- Constraints for table `menteeattendance`
--
ALTER TABLE `menteeattendance`
  ADD CONSTRAINT `attendance_mentee_id_fk` FOREIGN KEY (`attendanceMenteeId`) REFERENCES `mentees` (`menteeAutoId`);

--
-- Constraints for table `mentees`
--
ALTER TABLE `mentees`
  ADD CONSTRAINT `mentee_guardian_id_fk` FOREIGN KEY (`menteeGuardianId`) REFERENCES `guardians` (`guardianAutoId`),
  ADD CONSTRAINT `mentee_mentor_id_fk` FOREIGN KEY (`menteeMentorId`) REFERENCES `mentors` (`mentorAutoId`),
  ADD CONSTRAINT `mentee_school_id_fk` FOREIGN KEY (`menteeSchoolId`) REFERENCES `schools` (`schoolAutoId`);

--
-- Constraints for table `mentors`
--
ALTER TABLE `mentors`
  ADD CONSTRAINT `mentor_course_id_fk` FOREIGN KEY (`mentorCourse`) REFERENCES `courses` (`courseAutoId`),
  ADD CONSTRAINT `mentor_institution_id_fk` FOREIGN KEY (`mentorInstitutionId`) REFERENCES `institutions` (`institutionAutoId`);

--
-- Constraints for table `mentorship`
--
ALTER TABLE `mentorship`
  ADD CONSTRAINT `mentorship_mentee_id_fk` FOREIGN KEY (`mentorshipMenteeId`) REFERENCES `mentees` (`menteeAutoId`),
  ADD CONSTRAINT `mentorship_mentor_id_fk` FOREIGN KEY (`mentorshipMentorId`) REFERENCES `mentors` (`mentorAutoId`);

--
-- Constraints for table `schools`
--
ALTER TABLE `schools`
  ADD CONSTRAINT `school_category_id_fk` FOREIGN KEY (`schoolCategoryId`) REFERENCES `schoolcategories` (`categoryAutoId`);

--
-- Constraints for table `selectedgeneralsubjects`
--
ALTER TABLE `selectedgeneralsubjects`
  ADD CONSTRAINT `selected_general_mentee_id_fk` FOREIGN KEY (`selectMenteeId`) REFERENCES `mentees` (`menteeAutoId`),
  ADD CONSTRAINT `selected_general_subject_id_fk` FOREIGN KEY (`selectSubjectId`) REFERENCES `generalsubjects` (`subjectAutoId`);

--
-- Constraints for table `selectedmacheosubjects`
--
ALTER TABLE `selectedmacheosubjects`
  ADD CONSTRAINT `selected_macheo_mentee_id_fk` FOREIGN KEY (`selectMenteeId`) REFERENCES `mentees` (`menteeAutoId`),
  ADD CONSTRAINT `selected_macheo_subject_id_fk` FOREIGN KEY (`selectSubjectId`) REFERENCES `macheosubjects` (`subjectAutoId`);

--
-- Constraints for table `teacherattendance`
--
ALTER TABLE `teacherattendance`
  ADD CONSTRAINT `attendance_teacher_id_fk` FOREIGN KEY (`attendanceTeacherId`) REFERENCES `teachers` (`teacherAutoId`);

--
-- Constraints for table `teachers`
--
ALTER TABLE `teachers`
  ADD CONSTRAINT `teacher_school_id_fk` FOREIGN KEY (`teacherSchoolId`) REFERENCES `schools` (`schoolAutoId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
