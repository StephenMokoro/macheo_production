-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 23, 2018 at 10:49 AM
-- Server version: 5.6.35-1+deb.sury.org~xenial+0.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `macheo`
--

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`adminAutoId`, `adminStaffId`, `adminUserName`, `adminId`, `adminFname`, `adminLname`, `adminOtherNames`, `adminGender`, `adminEmail`, `adminPhone1`, `adminProfilePhoto`, `adminDateAdded`, `adminDateUpdated`, `adminActiveStatus`, `creating_user_group_id`, `creating_user_id`, `last_update_user_id`, `last_update_user_group_id`) VALUES
(1, '2902', 'smokoro', '31537790', 'Stephen', 'Mokoro', 'Nyang\'wono', 'Male', 'smokoro@strathmore.edu', '0719508386', NULL, '2018-04-23 00:00:00', '2018-04-23 08:37:14', 1, 1, 1, 1, 1),
(2, '71891', 'oodoyo', '29650723', 'Oliver', 'Yogoh', 'Odoyo', 'Male', 'oodoyo@strathmore.edu', '0707871247', NULL, '2018-04-23 11:18:50', '2018-04-23 08:18:50', 1, 1, 1, NULL, NULL);

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`formCode`, `formName`) VALUES
('FORM2', 'FORM 2'),
('FORM3', 'FORM 3'),
('FORM4', 'FORM 4');

--
-- Dumping data for table `generalsubjects`
--

INSERT INTO `generalsubjects` (`subjectAutoId`, `subjectName`, `subjectCode`, `subjectCompulsory`, `subjectLocked`) VALUES
(1, 'English', 'ENG', 1, 0),
(2, 'Kiswahili', 'KISW', 1, 0),
(3, 'Mathematics', 'MATH', 1, 0),
(4, 'Biology', 'BIO', 0, 0),
(5, 'Chemistry', 'CHEM', 0, 0),
(6, 'Physics', 'PHY', 0, 0),
(7, 'Geography', 'GEOG', 0, 0),
(8, 'History and Government', 'HIST', 0, 0),
(9, 'Christian Religious Education', 'CRE', 0, 0),
(10, 'Islamic Religious Education', 'IRE', 0, 1),
(11, 'Home Science', 'HSC', 0, 1),
(12, 'Art and Design', 'ART', 0, 1),
(13, 'Agriculture', 'AGRIC', 0, 0),
(14, 'Computer Studies', 'COMP', 0, 0),
(15, 'French', 'FRE', 0, 1),
(16, 'German', 'GER', 0, 1),
(17, 'Arabic', 'ARAB', 0, 1),
(18, 'Music', 'MUSIC', 0, 1),
(19, 'Business Studies', 'BST', 0, 0);

--
-- Dumping data for table `macheosubjects`
--

INSERT INTO `macheosubjects` (`subjectAutoId`, `subjectName`, `subjectCode`, `subjectCompulsory`, `subjectLocked`) VALUES
(1, 'English', 'ENG', 1, 0),
(2, 'Kiswahili', 'KISW', 1, 0),
(3, 'Mathematics', 'MATH', 1, 0),
(4, 'Biology', 'BIO', 0, 0),
(5, 'Chemistry', 'CHEM', 0, 0),
(6, 'Physics', 'PHY', 0, 0);

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`user_group_id`, `user_group_name`) VALUES
(1, 'Admin'),
(2, 'Mentor'),
(3, 'Teacher'),
(4, 'Intern');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
