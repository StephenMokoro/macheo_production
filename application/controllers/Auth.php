<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {


    function __construct() 
    {
        parent::__construct();
        $this->load->model("LoginModel", "login");

    }
    public function index()
    {
        $this->load->view('login');
    }
    public function cas_auth(){
        if(!$this->load->is_loaded('session'))
        {
             $this->load->library('cas');
        }else{
            unset($this->_ci_classes['session'], $this->session);
             $this->load->library('cas');
        }
        $this->load->library('cas');
        $this->cas->force_auth();//calling CAS for authentication
        $user = $this->cas->user();//calling the array with username from CAS and assignin it to the variable $user - @smokoro
        //lower case
        $username=strtolower($user->userlogin);

        $result=$this->login->validate_admin($username);
        if($result) 
            {
                //take the returned data and create a session for it (adminName and adminID). 
                foreach ($result as $row)
                { 
                    $fullName=$row->adminFname."&nbsp;".$row->adminLname;
                    $userID=$row->adminAutoId;
                    $userProfile=$row->adminProfilePhoto;
                    $_SESSION['sessdata'] = array('adminName' =>$fullName,'adminId'=>$userID,'adminProfilePhoto'=>$userProfile,'admin_login'=>TRUE,'adminGroupId'=>1); 
                    // $this->load->library('session');
                            
                }
                redirect('admin/dash');
            }else{
                $_SESSION['msg'] = array('error' => "Not a registered macheo user",'success' => "");
                // $this->cas->logout();
                // redirect(base_url(('admin')));
            }


    }
    public function auth()
    {
         //check if cas library is loaded and unload it to allow mnon su mentors to login with ci session
        if(!$this->load->is_loaded('cas'))
        {
             $this->load->library('session');
        }else{
            //remove the cas session handler
            unset($this->_ci_classes['cas'], $this->cas);
             $this->load->library('session');
        }
        
        $username=trim($this->input->post('username',TRUE));
        $password=md5(trim($this->input->post('password',TRUE)));
        $result=$this->login->validate_mentor($username,$password);
        if($result) 
            {
                //take the returned data and create a session for it (adminName and adminID). 
                foreach ($result as $row)
                { 
                    $fullName=$row->mentorFname."&nbsp;".$row->mentorLname;
                    $userID=$row->mentorAutoId;
                    $userProfile=$row->mentorProfilePhoto;

                    $sessdata = array('mentorName' =>$fullName,'mentorId'=>$userID,'mentorProfilePhoto'=>$userProfile,'mentor_login'=>TRUE,'mentorGroupId'=>2); 
                        $this->session->set_userdata($sessdata);
                            
                }
                redirect('mentor/dashboard');
            }else{
                $_SESSION['msg'] = array('error' => "Invalid credentials",'success' => "");
                // $this->cas->logout();
                redirect(base_url(('auth')));
            }

    }
}
