<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Mokoro
 */
//controller to login users
class Login extends CI_Controller
{
    //constructor to initialize variables and load tools
    function __construct() 

    {
        parent::__construct();
        $this->load->model("LoginModel", "login");
        
        // $this->load->library('session');
        // $this->load->model("MainModel", "mainmodel");
    }
public function index()
{
    $this->load->view('login');
}

public function lg() 
{
	//get submitted credentials
	$username=trim($this->input->post('username',TRUE));
    $password=md5(trim($this->input->post('password',TRUE)));
    //try login 
    $admin = $this->login->validate_admin($username, $password);//admin
    $mentor = $this->login->validate_mentor($username, $password);//mentor
		if($admin) 
            {
                //take the returned data and create a session for it (adminName and adminID). 
                foreach ($admin as $row)
                { 
                    $fullName=$row->adminFname."&nbsp;".$row->adminLname;
                    $userID=$row->adminAutoId;
                    $userProfile=$row->adminProfilePhoto;
                    $sessdata=array();
                    $sessdata = array('adminName' =>$fullName,'adminId'=>$userID,'adminProfilePhoto'=>$userProfile,'admin_login'=>TRUE,'adminGroupId'=>1); 
                    $this->session->set_userdata($sessdata);
                            
                }
                redirect('admin/dash');
   		} else if($mentor)
                {
                    foreach ($mentor as $row)
                    { 
                        $fullName=$row->mentorFname."&nbsp;".$row->mentorLname;
                        $userID=$row->mentorAutoId;
                        $sessdata=array();
                        $sessdata = array('mentorName' =>$fullName,'mentorId'=>$userID,'mentor_login'=>TRUE,'mentorGroupId'=>2); 
                        $this->session->set_userdata($sessdata);
                    }
                    redirect('mentor/dashboard');
                } else
                    {
                        
                        $feedback = array('error' => "Wrong credentials. Try again.",'success'=>"");
                        $this->session->set_flashdata('msg',$feedback);
                        redirect(base_url('login'));
	                }
}

}
