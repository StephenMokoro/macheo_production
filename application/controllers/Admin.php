<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @author Mokoro Stephen
*/
class Admin extends CI_Controller
{

    function __construct()
    {
         parent::__construct();
         $this->load->model('MainModel','model');
        if(!$this->load->is_loaded('session'))
        {
             $this->load->library('cas');
        }else{
            unset($this->_ci_classes['session'], $this->session);
             $this->load->library('cas');
        }
    }

    public function index(){
    	$this->load->view('login');
    }
    public function logout()
    {
        $this->cas->logout();
    }

    //admin dashboard
    public function dash()
    {
    	
        if(!empty(array_filter($_SESSION['sessdata'])))
        {
            
            $list['menteescount']=$this->model->menteesCount();
            $list['mentorscount']=$this->model->mentorsCount();
            $list['internscount']=$this->model->internsCount();
            $list['adminscount']=$this->model->adminsCount();
            $list['teacherscount']=$this->model->teachersCount();
            $list['macheoexamscount']=$this->model->macheoExamsCount();
            $list['generalexamscount']=$this->model->generalExamsCount();
            $this->load->view('admin/dashboard',$list);
        }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
                redirect(base_url(('admin')));
        }
    }

	//all active admins
	public function admins()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$admins['admins']=$this->model->activeAdmins();
			$this->load->view('admin/admin',$admins);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//all active mentors
	public function mentors()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['mentors']=$this->model->activeMentors();
			$list['mentorinst']=$this->model->institutions();
			$list['courses']=$this->model->courses();
			$this->load->view('admin/mentors',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	//all active mentees
	public function mentees()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['mentees']=$this->model->activeMentees();
			$list['schools']=$this->model->schools();
			$this->load->view('admin/mentees',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//all active mentees per mentor
	public function mentormentees()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$mentorUID=$this->input->post('mentorUID',TRUE);
			$mentorName=$this->input->post('mentorName',TRUE);
			if(isset($mentorUID) && strlen($mentorUID))
			{
				 $_SESSION['sessdata']=array_merge($_SESSION['sessdata'],array('mentorName'=>$mentorName,'mentorUID'=>$mentorUID));
				 $list['mentees']=$this->model->activeMenteesPerMentor($mentorUID);
				$this->load->view('admin/mentormentees',$list);
			}else{
				 $list['mentees']=$this->model->activeMenteesPerMentor($_SESSION['sessdata']['mentorUID']);
				$this->load->view('admin/mentormentees',$list);
			}

			
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//all active mentees per guardian
	public function guardianmentees()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$guardianUID=$this->input->post('guardianUID',TRUE);
			$list['mentees']=$this->model->activeMenteesPerGuardian($guardianUID);
			$this->load->view('admin/guardianmentees',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//mentee  profile
	public function menteeprofile()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
	        $list['forms']=$this->model->getAllForms();
			$menteeUID=$this->input->post('menteeUID',TRUE);
			$list['mentee_profile']=$this->model->getMentee($menteeUID);

			/////////////////////////////////////////////////////////////////////////////////////////////////

			$list['menteeattendance']=$this->model->menteeAttendanceGroupedPerDatePerMentee($menteeUID);

				foreach($list['forms'] as $key1=>$frm)
				{
					$formCode=$frm['formCode'];

					$list['performattendancedates']=$this->model->menteeAttendanceGroupedPerDatePerFormPerMentee($formCode,$menteeUID);
					

					foreach($list['performattendancedates'] as $key2 => $attendancedate)
					{
						$attendanceDate=$attendancedate['attendanceDate'];

						$list['forms'][$key1]['performattendancedates'][$key2]['attendanceDate']=$attendanceDate;

						$list['forms'][$key1]['performattendancedates'][$key2]['attendanceStatus'] =$this->model->menteesAttendanceStatusPerForm($attendanceDate,$formCode,$menteeUID);
					}
				}



				// $list['menteeattendanceperform']=$this->model->menteeAttendanceGroupedPerDate();
				// foreach($list['menteeattendance'] as $id => $attendance)
				// {
				// 	$attendanceDate=$attendance['attendanceDate'];

				// 	$list['menteeattendance'][$id]['countAll'] =$this->model->countOfMenteesMarkedPresentAbsentOrExcused($attendanceDate);//all

				// 	$list['menteeattendance'][$id]['countPresent'] =$this->model->countOfMenteesPresent($attendanceDate);

				// 	$list['menteeattendance'][$id]['countAbsent'] =$this->model->countOfMenteesAbsent($attendanceDate);
				// 	$list['menteeattendance'][$id]['countExcused'] =$this->model->countOfMenteesExcused($attendanceDate);

				// }

			////////////////////////////////////////////////////////////////////////////////////////////////

			$this->load->view('admin/mentee_profile',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//all active f2 mentees
	public function f2mentees()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	$selected_subjects_table_name="selectedgeneralsubjects";//table to select selected subjects from
        	$performance_table_name="general_exams_performance"; //table to select performance from
			$subjects_table="generalsubjects";//table to select subjects from


			$list['mentees']=$this->model->activeF2Mentees();

			foreach($list['mentees'] as $id => $mentee)//loop and create another array for subjects taken and append it to original array
			{
				$pid=$mentee['menteeAutoId'];//mentee's primary id
				$menteesubs=$this->model->getSubjects($pid,$selected_subjects_table_name,$subjects_table);//list of selected general subjects
				$list['mentees'][$id]['subjects'] =$menteesubs;//append mentee general subjects to mentee array

			}
			$this->load->view('admin/f2mentees',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//all active f3 mentees
	public function f3mentees()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	$selected_subjects_table_name="selectedgeneralsubjects";//table to select selected subjects from
        	$performance_table_name="general_exams_performance"; //table to select performance from
			$subjects_table="generalsubjects";//table to select subjects from


			$list['mentees']=$this->model->activeF3Mentees();

			foreach($list['mentees'] as $id => $mentee)//loop and create another array for subjects taken and append it to original array
			{
				$pid=$mentee['menteeAutoId'];//mentee's primary id
				$menteesubs=$this->model->getSubjects($pid,$selected_subjects_table_name,$subjects_table);//list of selected general subjects
				$list['mentees'][$id]['subjects'] =$menteesubs;//append mentee general subjects to mentee array

			}
			$this->load->view('admin/f3mentees',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//all active f4 mentees
	public function f4mentees()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	$selected_subjects_table_name="selectedgeneralsubjects";//table to select selected subjects from
        	$performance_table_name="general_exams_performance"; //table to select performance from
			$subjects_table="generalsubjects";//table to select subjects from


			$list['mentees']=$this->model->activeF4Mentees();

			foreach($list['mentees'] as $id => $mentee)//loop and create another array for subjects taken and append it to original array
			{
				$pid=$mentee['menteeAutoId'];//mentee's primary id
				$menteesubs=$this->model->getSubjects($pid,$selected_subjects_table_name,$subjects_table);//list of selected general subjects
				$list['mentees'][$id]['subjects'] =$menteesubs;//append mentee general subjects to mentee array

			}
			$this->load->view('admin/f4mentees',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	
	
	
	//Macheo exam registration
	public function newmacheoexam()
    {
        if(!empty(array_filter($_SESSION['sessdata'])))
        {
            $examName=strtoupper($this->input->post('examName'));
            // $examCode=preg_replace('/\s+/', '', strtoupper($this->input->post('examCode')));
	        $termCode=strtoupper($this->input->post('termCode'));
            $formCode=strtoupper($this->input->post('formCode'));
            $dateCreated=date("Y-m-d");

            //record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];
	        
			    	//details of uploaded file
		        $macheoexamInfo=array('examName'=>$examName, 'examTermCode'=>$termCode, 'examFormCode'=>$formCode, 'examDateCreated'=>$dateCreated,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
				$result=$this->model->registerMacheoExams($macheoexamInfo);
				if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "New exam added");
						
			           redirect(base_url(('admin/macheoexams')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "Failed to add exam",'success' => "");
							
			               redirect(base_url(('admin/macheoexams')));
						}
            
         
        }else{
	    	   $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
			   
	           redirect(base_url(('admin')));
	    }
    
    }
	//load general exams page
	public function generalexams()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['exams']=$this->model->generalExams();
			$this->load->view('admin/generalexams',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//General Exam scores 
	public function generalexamscores()
	{

		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	
			$selected_subjects_table_name="selectedgeneralsubjects";//table to select selected subjects from
        	$performance_table_name="general_exams_performance"; //table to select performance from
			$subjects_table="generalsubjects";//table to select subjects from

        	$examFormCode=$this->input->post('examFormCode',TRUE);
			$examAutoId=$this->input->post('examAutoId',TRUE);
			if(isset ($examFormCode) && strlen($examFormCode) && $examAutoId && strlen($examAutoId))
			{
				//keep a session of examAutoId
				 $_SESSION['sessdata']=array_merge($_SESSION['sessdata'],array('examAutoId' =>$examAutoId));


				//get exam particulars
				$this->db->select('*');
				$this->db->from('generalexams');
				$this->db->where('examAutoId',$examAutoId);
				$this->db->limit(1);
				$result=$this->db->get()->result_array();
				foreach($result as $result)
					{
						// $examInfo=$examFormCode.'-'.$result['examTermCode'].'-' .$result['examName'].', '.date('Y', strtotime($result['examDateCreated']));
						$examInfo=$examFormCode.' ' .$result['examName'];
						// set session for form code and exam info shows which exams
						
					    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'],array('examFormCode' =>$examFormCode,'examInfo'=>$examInfo)); 
					    
					}
				
				$list['substaken'] =$this->model->generalSubjects();
				foreach($list['substaken'] as $id=>$sub)
				{
					$subjectId=$sub['subjectAutoId'];
					$list['substaken'][$id]['average']=$this->model->getSubjectAverage($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_A_Plain']=$this->model->getSubject_A_Plain($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_A_Minus']=$this->model->getSubject_A_Minus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_B_Plus']=$this->model->getSubject_B_Plus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_B_Plain']=$this->model->getSubject_B_Plain($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_B_Minus']=$this->model->getSubject_B_Minus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_C_Plus']=$this->model->getSubject_C_Plus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_C_Plain']=$this->model->getSubject_C_Plain($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_C_Minus']=$this->model->getSubject_C_Minus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_D_Plus']=$this->model->getSubject_D_Plus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_D_Plain']=$this->model->getSubject_D_Plain($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_D_Minus']=$this->model->getSubject_D_Minus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_E']=$this->model->getSubject_E($subjectId,$examAutoId,$performance_table_name);

				}

				$list['mentees']=$this->model->activeMenteesPerFormWithAtLeastOneScoreEntry($examFormCode,$examAutoId,$selected_subjects_table_name,$performance_table_name);

				foreach($list['mentees'] as $id2 => $mentee)//loop and create another array for subjects taken and append it to original array
				{
					$pid=$mentee['menteeAutoId'];//mentee's primary id


					$list['mentees'][$id2]['subjects'] =$this->model->getSubjects($pid,$selected_subjects_table_name,$subjects_table);//append mentee general subjects to mentee array
					foreach ($list['mentees'][$id2]['subjects'] as $key => $value) {
						//get mentee subject scores, grades and position in comparison to other students scores
						$subjectId=$value['selectSubjectId'];
						$list['mentees'][$id2]['subjects'][$key]['score']=$this->model->getSubjectScorePerMentee($subjectId,$examAutoId,$pid,$performance_table_name);
						$list['mentees'][$id2]['subjects'][$key]['grade']=$this->model->getSubjectGrade($subjectId,$examAutoId,$pid,$performance_table_name);
						$list['mentees'][$id2]['subjects'][$key]['scoreentries']=$this->model->getSubjectScores($subjectId,$examAutoId,$performance_table_name);
						$list['mentees'][$id2]['subjects'][$key]['countofscoreentries']=$this->model->getSubjectTotalScoreEntries($subjectId,$examAutoId,$performance_table_name);
					}

				}



					$list['gradesArray']=array('average','grade_A_Plain','grade_A_Minus','grade_B_Plus','grade_B_Plain','grade_B_Minus','grade_C_Plus','grade_C_Plain','grade_C_Minus','grade_D_Plus','grade_D_Plain','grade_D_Minus','grade_E');

					$list['gradesShort']=array('AVR','A','A-','B+','B','B-','C+','C','C-','D+','D','D-','E');//grades part on view

					$list['schools']=$this->model->schools();

					foreach($list['schools'] as $schid => $school)
					{
						$list['schools'][$schid]['subjects'] =$this->model->generalSubjects();
						foreach ($list['schools'][$schid]['subjects'] as $key => $subj) 
						{
							$schoolId=$school['schoolAutoId'];//autoId of each school
							$subjectId=$subj['subjectAutoId'];//autoId of each subject

							$list['schools'][$schid]['subjects'][$key]['average']=$this->model->getSubjectAveragePerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_A_Plain']=$this->model->getSubject_A_PlainPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_A_Minus']=$this->model->getSubject_A_MinusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_B_Plus']=$this->model->getSubject_B_PlusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_B_Plain']=$this->model->getSubject_B_PlainPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_B_Minus']=$this->model->getSubject_B_MinusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_C_Plus']=$this->model->getSubject_C_PlusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_C_Plain']=$this->model->getSubject_C_PlainPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_C_Minus']=$this->model->getSubject_C_MinusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_D_Plus']=$this->model->getSubject_D_PlusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_D_Plain']=$this->model->getSubject_D_PlainPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_D_Minus']=$this->model->getSubject_D_MinusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_E']=$this->model->getSubject_EPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
						}
					}


				$this->load->view('admin/exam_scores',$list);
			}else{
					$examFormCode=$_SESSION['sessdata']['examFormCode'];//get form code from session above
	                $examAutoId=$_SESSION['sessdata']['examAutoId'];//get from session above
	                
					$this->db->select('*');
					$this->db->from('generalexams');
					$this->db->where('examAutoId',$examAutoId);
					$this->db->limit(1);
					$result=$this->db->get()->result_array();
					foreach($result as $result)
						{
							// $examInfo=$examFormCode.'-'.$result['examTermCode'].'-' .$result['examName'].', '.date('Y', strtotime($result['examDateCreated']));
							$examInfo=$examFormCode.' ' .$result['examName'];
							// set session for form code and exam info shows which exams
							
						    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array('examFormCode' =>$examFormCode,'examInfo'=>$examInfo)); 
						}
					
					$list['substaken'] =$this->model->generalSubjects();
				foreach($list['substaken'] as $id=>$sub)
				{
					$subjectId=$sub['subjectAutoId'];
					$list['substaken'][$id]['average']=$this->model->getSubjectAverage($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_A_Plain']=$this->model->getSubject_A_Plain($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_A_Minus']=$this->model->getSubject_A_Minus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_B_Plus']=$this->model->getSubject_B_Plus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_B_Plain']=$this->model->getSubject_B_Plain($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_B_Minus']=$this->model->getSubject_B_Minus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_C_Plus']=$this->model->getSubject_C_Plus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_C_Plain']=$this->model->getSubject_C_Plain($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_C_Minus']=$this->model->getSubject_C_Minus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_D_Plus']=$this->model->getSubject_D_Plus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_D_Plain']=$this->model->getSubject_D_Plain($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_D_Minus']=$this->model->getSubject_D_Minus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_E']=$this->model->getSubject_E($subjectId,$examAutoId,$performance_table_name);

				}

				$list['mentees']=$this->model->activeMenteesPerFormWithAtLeastOneScoreEntry($examFormCode,$examAutoId,$selected_subjects_table_name,$performance_table_name);

				foreach($list['mentees'] as $id2 => $mentee)//loop and create another array for subjects taken and append it to original array
				{
					$pid=$mentee['menteeAutoId'];//mentee's primary id


					$list['mentees'][$id2]['subjects'] =$this->model->getSubjects($pid,$selected_subjects_table_name,$subjects_table);//append mentee general subjects to mentee array
					foreach ($list['mentees'][$id2]['subjects'] as $key => $value) {
						//get mentee subject scores, grades and position in comparison to other students scores
						$subjectId=$value['selectSubjectId'];
						$list['mentees'][$id2]['subjects'][$key]['score']=$this->model->getSubjectScorePerMentee($subjectId,$examAutoId,$pid,$performance_table_name);
						$list['mentees'][$id2]['subjects'][$key]['grade']=$this->model->getSubjectGrade($subjectId,$examAutoId,$pid,$performance_table_name);
						$list['mentees'][$id2]['subjects'][$key]['scoreentries']=$this->model->getSubjectScores($subjectId,$examAutoId,$performance_table_name);
						$list['mentees'][$id2]['subjects'][$key]['countofscoreentries']=$this->model->getSubjectTotalScoreEntries($subjectId,$examAutoId,$performance_table_name);
					}

				}



					$list['gradesArray']=array('average','grade_A_Plain','grade_A_Minus','grade_B_Plus','grade_B_Plain','grade_B_Minus','grade_C_Plus','grade_C_Plain','grade_C_Minus','grade_D_Plus','grade_D_Plain','grade_D_Minus','grade_E');

					$list['gradesShort']=array('AVR','A','A-','B+','B','B-','C+','C','C-','D+','D','D-','E');//grades part on view

					$list['schools']=$this->model->schools();

					foreach($list['schools'] as $schid => $school)
					{
						$list['schools'][$schid]['subjects'] =$this->model->generalSubjects();
						foreach ($list['schools'][$schid]['subjects'] as $key => $subj) 
						{
							$schoolId=$school['schoolAutoId'];//autoId of each school
							$subjectId=$subj['subjectAutoId'];//autoId of each subject

							$list['schools'][$schid]['subjects'][$key]['average']=$this->model->getSubjectAveragePerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_A_Plain']=$this->model->getSubject_A_PlainPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_A_Minus']=$this->model->getSubject_A_MinusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_B_Plus']=$this->model->getSubject_B_PlusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_B_Plain']=$this->model->getSubject_B_PlainPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_B_Minus']=$this->model->getSubject_B_MinusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_C_Plus']=$this->model->getSubject_C_PlusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_C_Plain']=$this->model->getSubject_C_PlainPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_C_Minus']=$this->model->getSubject_C_MinusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_D_Plus']=$this->model->getSubject_D_PlusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_D_Plain']=$this->model->getSubject_D_PlainPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_D_Minus']=$this->model->getSubject_D_MinusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
							$list['schools'][$schid]['subjects'][$key]['grade_E']=$this->model->getSubject_EPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name);
						}
					}


				$this->load->view('admin/exam_scores',$list);
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
		
	}
	//General exam registration
	public function newgeneralexam()
    {
        if(!empty(array_filter($_SESSION['sessdata'])))
        {
            $examName=strtoupper($this->input->post('examName'));
            // $examCode=preg_replace('/\s+/', '', strtoupper($this->input->post('examCode')));
	        $termCode=strtoupper($this->input->post('termCode'));
            $formCode=strtoupper($this->input->post('formCode'));
            $dateCreated=date("Y-m-d");

            //record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];
	        
			    	//details of uploaded file
		        $generalexamInfo=array('examName'=>$examName, 'examTermCode'=>$termCode, 'examFormCode'=>$formCode, 'examDateCreated'=>$dateCreated,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
				$result=$this->model->registerGeneralExams($generalexamInfo);
				if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "New exam added");
						
			           redirect(base_url(('admin/generalexams')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "Failed to add exam",'success' => "");
							
			               redirect(base_url(('admin/generalexams')));
						}
            
         
        }else{
	    	   $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
			   
	           redirect(base_url(('admin')));
	    }
    
    }
   //general subjects marks 
	public function generalmarks()
	{

		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$selected_subjects_table_name="selectedgeneralsubjects";//table to select selected subjects from
        	$performance_table_name="general_exams_performance"; //table to select performance from
			$subjects_table="generalsubjects";//table to select subjects from

			$examFormCode=$this->input->post('examFormCode',TRUE);
			$examAutoId=$this->input->post('examAutoId',TRUE);
			if(isset ($examFormCode) && strlen($examFormCode) && isset($examAutoId) && strlen($examAutoId)){
					//get exam particulars
					$this->db->select('*');
					$this->db->from('generalexams');
					$this->db->where('examAutoId',$examAutoId);
					$this->db->limit(1);
					$result=$this->db->get()->result_array();
					foreach($result as $result)
						{
							// $examInfo=$examFormCode.'-'.$result['examTermCode'].'-' .$result['examName'].', '.date('Y', strtotime($result['examDateCreated'])).' General';
							$examInfo=$examFormCode.' ' .$result['examName'];
							// set session for form code and exam info shows which exams
							
						    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'],array('examAutoId'=>$examAutoId,'examFormCode' =>$examFormCode,'examInfo'=>$examInfo)); 
					
							$list['substaken'] =$this->model->generalSubjects();
							
							$list['mentees']=$this->model->activeMenteesPerForm($examFormCode,$examAutoId,$selected_subjects_table_name);


							foreach($list['mentees'] as $id => $mentee)//loop and create another array for subjects taken and append it to original array
							{
								$pid=$mentee['menteeAutoId'];//mentee's primary id
								$menteesubs=$this->model->getSubjectsPerMentee($pid,$examAutoId,$performance_table_name,$selected_subjects_table_name,$subjects_table);//list of selected  general subjects per mentee whose marks have not been recorded as yet
								$list['mentees'][$id]['subjects'] =$menteesubs;//append mentee general subjects to mentee array

							}
							//remove members who have ZERO subjects not having marks i.e all their marks have been recorded
		    				$list['mentees'] = array_filter($list['mentees'], function($item){
						    	return count($item) == count(array_filter($item));
							});
					
							$this->load->view('admin/generalmarksentry',$list);
						}
			}else{
					$examFormCode=$_SESSION['sessdata']['examFormCode'];//get form code from session above
	                $examAutoId=$_SESSION['sessdata']['examAutoId'];//get from session above
					
					$list['substaken'] =$this->model->generalSubjects();
					$list['mentees']=$this->model->activeMenteesPerForm($examFormCode,$examAutoId,$selected_subjects_table_name);

					foreach($list['mentees'] as $id => $mentee)//loop and create another array for subjects taken and append it to original array
					{
						$pid=$mentee['menteeAutoId'];//mentee's primary id
						$menteesubs=$this->model->getSubjectsPerMentee($pid,$examAutoId,$performance_table_name,$selected_subjects_table_name,$subjects_table);//list of selected  general subjects per mentee whose marks have not been recorded as yet
						$list['mentees'][$id]['subjects'] =$menteesubs;//append mentee general subjects to mentee array

					}
					//remove members who have ZERO subjects not having marks i.e all marks have been recorded
    				$list['mentees'] = array_filter($list['mentees'], function($item){
				    	return count($item) == count(array_filter($item));
					});

				$this->load->view('admin/generalmarksentry',$list);
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
		
	}
	//get list of players taking a specific subject
	 public function gensubjectmentees()
        {

           if(!empty(array_filter($_SESSION['sessdata'])))
        	{
        		$performance_table_name="general_exams_performance"; //table to select from
        		$selected_subjects_table_name="selectedgeneralsubjects"; //table to select from
        		$subjects_table="generalsubjects"; //table to select from
                $subjectId=$this->input->post('subjectId');//posted
                $formCode=$_SESSION['sessdata']['examFormCode'];//get form code from previous session
                $examAutoId=$_SESSION['sessdata']['examAutoId'];//get from previous session

                $list =$this->model->activeMenteesPerFormTakingSubjectMarksNotRecorded($subjectId,$formCode,$examAutoId,$performance_table_name,$selected_subjects_table_name,$subjects_table);

                $htmldata='<form role="form" method="post" action="'.base_url('admin/').'newgensubmarks">
                	<table class="table table-bordred table-striped table-hover" id="list" width="100%">
	                   <thead style="background-color:#512E5F;color:#FFFFFF;">
	                        <th class="text-left" >MENTEE</th>
	                        <th class="text-center" id="pid">Mentee PID</th>
	                        <th class="text-center" >SUBJECT</th>
	                        <th class="text-center" >SCORE</th>
	                    </thead>
                    	<tbody>	';
              if(!empty($list))
              {
              	$arrayKey=0;
               foreach ($list as $mentee) 
                    {
                        $htmldata=$htmldata.'
                        		
                        <tr>
                			<td class="text-left col-md-3">'.$mentee['menteeFname']." ".$mentee['menteeLname'].'
                			</td>
                	        <td class="text-center col-md-3"> 
                	        	<input type="text" class="form-control text-center" name="mentee['.$arrayKey.'][menteeId]"'.' style="width:100%!important;" value="'.$mentee['menteeAutoId'].'">
                	        </td>
                	        <td class="text-center  col-md-3">'.$mentee['subjectCode'].'
                	        <input type="text" class="form-control text-center" name="mentee['.$arrayKey.'][subjectId]"'.' style="width:100%!important;display:none;" value="'.$subjectId.'">
                	        </td>
                	        <td class="text-left  col-md-3">
                	        <input type="number" class="form-control text-center" name="mentee['.$arrayKey.'][subjectScore]"'.' style="width:100%!important;" min="0" max="100" >
                            </td>
                     	</tr>';
                     	$arrayKey=$arrayKey+1;
                    }
                  $htmldata=$htmldata.'</tbody></table><button id="submit" class="btn btn-primary" style="margin-top: 10px" >Submit</button></form>
                <script>
                
  					$(document).ready(function () {
  						$("#list").DataTable({responsive:true,"aaSorting": [], scrollCollapse: true,paging:false,columnDefs: [{ orderable: false,targets: [3]}] });});</script>';
                }else{

                $htmldata='No mentees available';
                }
               
                echo json_encode($htmldata);
            }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
    }
    //saving general marks 
	public function newgensubmarks()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
    	{
    		if(isset($_POST['mentee']))
    		{
    			//here, get the posted array, filter/ remove those arrays where at least one element is missing e.g. marks. i.e if some mentees's marks were not entered. The marks fields are not set to required to allow flexibility in marks entries in case they are not available..yet allow you to add by selecting only those whose marks are not entered
    			$filtered_array = array_filter($_POST['mentee'], function($item){
				    return count($item) == count(array_filter($item));
				});

				//record user creating this record
	            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
	            $userId=$_SESSION['sessdata']['adminId'];


				$subjectScores=array();
            	foreach($filtered_array as $record ) /*loop through filtered array and get individual items */
                {
        			$subjectScores[]=array('perfMenteeId'=>$record['menteeId'],'perfExamId	'=>$_SESSION['sessdata']['examAutoId'],'perfSubjectId'=>$record['subjectId'],'perfScore'=>$record['subjectScore'],'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
                }
				$result=$this->model->newGeneralExamScores($subjectScores);
                if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "New subject score(s) added");
						
			           redirect(base_url(('admin/generalmarks')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "Failed to add subject score(s)",'success' => "");
							
			               redirect(base_url(('admin/generalmarks')));
						}
    		}
    	}else{
	            $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
	            redirect(base_url(('admin')));
        }

	}
    //load macheo exams page
	public function macheoexams()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['exams']=$this->model->macheoExams();
			$this->load->view('admin/macheoexams',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//macheo subjects marks 
	public function macheomarks()
	{

		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$selected_subjects_table_name="selectedmacheosubjects";//table to select selected subjects from
        	$performance_table_name="macheo_exams_performance"; //table to select performance from
			$subjects_table="macheosubjects";//table to select subjects from

			$examFormCode=$this->input->post('examFormCode',TRUE);
			$examAutoId=$this->input->post('examAutoId',TRUE);
			if(isset ($examFormCode) && strlen($examFormCode) && isset($examAutoId) && strlen($examAutoId)){
					//get exam particulars
					$this->db->select('*');
					$this->db->from('macheoexams');
					$this->db->where('examAutoId',$examAutoId);
					$this->db->limit(1);
					$result=$this->db->get()->result_array();
					foreach($result as $result)
						{
						$examInfo=$examFormCode.' ' .$result['examName'];

							// $examInfo=$examFormCode.'-'.$result['examTermCode'].'-' .$result['examName'].', '.date('Y', strtotime($result['examDateCreated'])).' Macheo';
							// set session for form code and exam info shows which exams
							
						    $_SESSION['sessdata']= array_merge($_SESSION['sessdata'],array('examAutoId'=>$examAutoId,'examFormCode' =>$examFormCode,'examInfo'=>$examInfo)); 
						    
						}
					
					$list['substaken'] =$this->model->macheoSubjects();
					
					$list['mentees']=$this->model->activeMenteesPerForm($examFormCode,$examAutoId,$selected_subjects_table_name);


					foreach($list['mentees'] as $id => $mentee)//loop and create another array for subjects taken and append it to original array
					{
						$pid=$mentee['menteeAutoId'];//mentee's primary id
						$menteesubs=$this->model->getSubjectsPerMentee($pid,$examAutoId,$performance_table_name,$selected_subjects_table_name,$subjects_table);//list of selected  macheo subjects per mentee whose marks have not been recorded as yet
						$list['mentees'][$id]['subjects'] =$menteesubs;//append mentee macheo subjects to mentee array

					}
					//remove members who have ZERO subjects not having marks
    				$list['mentees'] = array_filter($list['mentees'], function($item){
				    	return count($item) == count(array_filter($item));
					});
					
				$this->load->view('admin/macheomarksentry',$list);
			}else{
					$examFormCode=$_SESSION['sessdata']['examFormCode'];//get form code from session above
	                $examAutoId=$_SESSION['sessdata']['examAutoId'];//get from session above
					
					$list['substaken'] =$this->model->macheoSubjects();
					$list['mentees']=$this->model->activeMenteesPerForm($examFormCode,$examAutoId,$selected_subjects_table_name);

					foreach($list['mentees'] as $id => $mentee)//loop and create another array for subjects taken and append it to original array
					{
						$pid=$mentee['menteeAutoId'];//mentee's primary id
						$menteesubs=$this->model->getSubjectsPerMentee($pid,$examAutoId,$performance_table_name,$selected_subjects_table_name,$subjects_table);//list of selected  macheo subjects per mentee whose marks have not been recorded as yet
						$list['mentees'][$id]['subjects'] =$menteesubs;//append mentee macheo subjects to mentee array

					}
					//remove members who have ZERO subjects not having marks
    				$list['mentees'] = array_filter($list['mentees'], function($item){
				    	return count($item) == count(array_filter($item));
					});

				$this->load->view('admin/macheomarksentry',$list);
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
		
	}
	
	 //saving macheo marks 
	public function newmacheosubmarks()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
    	{
    		if(isset($_POST['mentee']))
    		{
    			//here, get the posted array, filter/ remove those arrays where at least one element is missing e.g. marks. i.e if some mentees's marks were not entered. The marks fields are not set to required to allow flexibility in marks entries in case they are not available..yet allow you to add by selecting only those whose marks are not entered
    			$filtered_array = array_filter($_POST['mentee'], function($item){
				    return count($item) == count(array_filter($item));
				});

				//record user creating this record
	            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
	            $userId=$_SESSION['sessdata']['adminId'];


				$subjectScores=array();
            	foreach($filtered_array as $record ) /*loop through filtered array and get individual items */
                {
        			$subjectScores[]=array('perfMenteeId'=>$record['menteeId'],'perfExamId	'=>$_SESSION['sessdata']['examAutoId'],'perfSubjectId'=>$record['subjectId'],'perfScore'=>$record['subjectScore'],'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
                }
				$result=$this->model->newMacheoExamScores($subjectScores);
                if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "New subject scores added");
						
			           redirect(base_url(('admin/macheomarks')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "Failed to add subject scores",'success' => "");
							
			               redirect(base_url(('admin/macheomarks')));
						}
    		}
    	}else{
	            $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
	            redirect(base_url(('admin')));
        }

	}


















	//Macheo Exam scores 
	public function macheoexamscores()
	{

		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	
			$selected_subjects_table_name="selectedmacheosubjects";//table to select selected subjects from
        	$performance_table_name="macheo_exams_performance"; //table to select performance from
			$subjects_table="macheosubjects";//table to select subjects from

        	$examFormCode=$this->input->post('examFormCode',TRUE);
			$examAutoId=$this->input->post('examAutoId',TRUE);
			if(isset ($examFormCode) && strlen($examFormCode) && $examAutoId && strlen($examAutoId)){
				//get exam particulars
				$this->db->select('*');
				$this->db->from('macheoexams');
				$this->db->where('examAutoId',$examAutoId);
				$this->db->limit(1);
				$result=$this->db->get()->result_array();
				foreach($result as $result)
					{
						$examInfo=$examFormCode.' ' .$result['examName'];
						// $examInfo=$examFormCode.'-'.$result['examTermCode'].'-' .$result['examName'].', '.date('Y', strtotime($result['examDateCreated']));
						// set session for form code and exam info shows which exams
						
					    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'],array('examFormCode' =>$examFormCode,'examInfo'=>$examInfo)); 
					    
					}
				
				$list['substaken'] =$this->model->macheoSubjects();
				foreach($list['substaken'] as $id=>$sub)
				{
					$subjectId=$sub['subjectAutoId'];
					$list['substaken'][$id]['average']=$this->model->getSubjectAverage($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_A_Plain']=$this->model->getSubject_A_Plain($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_A_Minus']=$this->model->getSubject_A_Minus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_B_Plus']=$this->model->getSubject_B_Plus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_B_Plain']=$this->model->getSubject_B_Plain($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_B_Minus']=$this->model->getSubject_B_Minus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_C_Plus']=$this->model->getSubject_C_Plus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_C_Plain']=$this->model->getSubject_C_Plain($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_C_Minus']=$this->model->getSubject_C_Minus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_D_Plus']=$this->model->getSubject_D_Plus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_D_Plain']=$this->model->getSubject_D_Plain($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_D_Minus']=$this->model->getSubject_D_Minus($subjectId,$examAutoId,$performance_table_name);
					$list['substaken'][$id]['grade_E']=$this->model->getSubject_E($subjectId,$examAutoId,$performance_table_name);

				}

				$list['mentees']=$this->model->activeMenteesPerFormWithAtLeastOneScoreEntry($examFormCode,$examAutoId,$selected_subjects_table_name,$performance_table_name);

				foreach($list['mentees'] as $id2 => $mentee)//loop and create another array for subjects taken and append it to original array
				{
					$pid=$mentee['menteeAutoId'];//mentee's primary id


					$list['mentees'][$id2]['subjects'] =$this->model->getSubjects($pid,$selected_subjects_table_name,$subjects_table);//append mentee general subjects to mentee array
					foreach ($list['mentees'][$id2]['subjects'] as $key => $value) {
						//get mentee subject scores, grades and position in comparison to other students scores
						$subjectId=$value['selectSubjectId'];
						$list['mentees'][$id2]['subjects'][$key]['score']=$this->model->getSubjectScorePerMentee($subjectId,$examAutoId,$pid,$performance_table_name);
						$list['mentees'][$id2]['subjects'][$key]['grade']=$this->model->getSubjectGrade($subjectId,$examAutoId,$pid,$performance_table_name);
						$list['mentees'][$id2]['subjects'][$key]['scoreentries']=$this->model->getSubjectScores($subjectId,$examAutoId,$performance_table_name);
						$list['mentees'][$id2]['subjects'][$key]['countofscoreentries']=$this->model->getSubjectTotalScoreEntries($subjectId,$examAutoId,$performance_table_name);
					}

				}

				$this->load->view('admin/exam_scores',$list);
			}else{
					$examFormCode=$_SESSION['sessdata']['examFormCode'];//get form code from session above
	                $examAutoId=$_SESSION['sessdata']['examAutoId'];//get from session above
	                
					$this->db->select('*');
					$this->db->from('generalexams');
					$this->db->where('examAutoId',$examAutoId);
					$this->db->limit(1);
					$result=$this->db->get()->result_array();
					foreach($result as $result)
						{
							$examInfo=$examFormCode.' ' .$result['examName'];

							// $examInfo=$examFormCode.'-'.$result['examTermCode'].'-' .$result['examName'].', '.date('Y', strtotime($result['examDateCreated']));
							// set session for form code and exam info shows which exams
							
						    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array('examFormCode' =>$examFormCode,'examInfo'=>$examInfo)); 
						}
					
					$list['substaken'] =$this->model->generalSubjects();
					foreach($list['substaken'] as $id=>$sub)
					{
						$subjectId=$sub['subjectAutoId'];
						$list['substaken'][$id]['average']=$this->model->getSubjectAverage($subjectId,$examAutoId,$performance_table_name);
						$list['substaken'][$id]['grade_A_Plain']=$this->model->getSubject_A_Plain($subjectId,$examAutoId,$performance_table_name);
						$list['substaken'][$id]['grade_A_Minus']=$this->model->getSubject_A_Minus($subjectId,$examAutoId,$performance_table_name);
						$list['substaken'][$id]['grade_B_Plus']=$this->model->getSubject_B_Plus($subjectId,$examAutoId,$performance_table_name);
						$list['substaken'][$id]['grade_B_Plain']=$this->model->getSubject_B_Plain($subjectId,$examAutoId,$performance_table_name);
						$list['substaken'][$id]['grade_B_Minus']=$this->model->getSubject_B_Minus($subjectId,$examAutoId,$performance_table_name);
						$list['substaken'][$id]['grade_C_Plus']=$this->model->getSubject_C_Plus($subjectId,$examAutoId,$performance_table_name);
						$list['substaken'][$id]['grade_C_Plain']=$this->model->getSubject_C_Plain($subjectId,$examAutoId,$performance_table_name);
						$list['substaken'][$id]['grade_C_Minus']=$this->model->getSubject_C_Minus($subjectId,$examAutoId,$performance_table_name);
						$list['substaken'][$id]['grade_D_Plus']=$this->model->getSubject_D_Plus($subjectId,$examAutoId,$performance_table_name);
						$list['substaken'][$id]['grade_D_Plain']=$this->model->getSubject_D_Plain($subjectId,$examAutoId,$performance_table_name);
						$list['substaken'][$id]['grade_D_Minus']=$this->model->getSubject_D_Minus($subjectId,$examAutoId,$performance_table_name);
						$list['substaken'][$id]['grade_E']=$this->model->getSubject_E($subjectId,$examAutoId,$performance_table_name);

					}

					$list['mentees']=$this->model->activeMenteesPerFormWithAtLeastOneScoreEntry($examFormCode,$examAutoId,$selected_subjects_table_name,$performance_table_name);
					

					foreach($list['mentees'] as $id2 => $mentee)//loop and create another array for subjects taken and append it to original array
					{
						$pid=$mentee['menteeAutoId'];//mentee's primary id


						$list['mentees'][$id2]['subjects'] =$this->model->getSubjects($pid,$selected_subjects_table_name,$subjects_table);//append mentee general subjects to mentee array
						foreach ($list['mentees'][$id2]['subjects'] as $key => $value) {
							//get mentee subject scores, grades and position in comparison to other students scores
							$subjectId=$value['selectSubjectId'];
							$list['mentees'][$id2]['subjects'][$key]['score']=$this->model->getSubjectScorePerMentee($subjectId,$examAutoId,$pid,$performance_table_name);
							$list['mentees'][$id2]['subjects'][$key]['grade']=$this->model->getSubjectGrade($subjectId,$examAutoId,$pid,$performance_table_name);
							$list['mentees'][$id2]['subjects'][$key]['scoreentries']=$this->model->scoreentries($subjectId,$examAutoId,$performance_table_name);
							$list['mentees'][$id2]['subjects'][$key]['countofscoreentries']=$this->model->getSubjectTotalScoreEntries($subjectId,$examAutoId,$performance_table_name);
						}

					}
				$this->load->view('admin/exam_scores',$list);
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
		
	}


	
	
    //get list of players taking a specific subject
	 public function macheosubjectmentees()
        {

           if(!empty(array_filter($_SESSION['sessdata'])))
        	{
        		$performance_table_name="macheo_exams_performance"; //table to select from
        		$selected_subjects_table_name="selectedmacheosubjects"; //table to select from
        		$subjects_table="macheosubjects"; //table to select from
                $subjectId=$this->input->post('subjectId');//posted
                $formCode=$_SESSION['sessdata']['examFormCode'];//get form code from previous session
                $examAutoId=$_SESSION['sessdata']['examAutoId'];//get from previous session

                $list =$this->model->activeMenteesPerFormTakingSubjectMarksNotRecorded($subjectId,$formCode,$examAutoId,$performance_table_name,$selected_subjects_table_name,$subjects_table);

                $htmldata='<form role="form" method="post" action="'.base_url('admin/').'newmacheosubmarks">
                	<table class="table table-bordred table-striped table-hover" id="list" width="100%">
	                   <thead style="background-color:#512E5F;color:#FFFFFF;">
	                        <th class="text-left" >MENTEE</th>
	                        <th class="text-center" id="pid">Mentee PID</th>
	                        <th class="text-center" >SUBJECT</th>
	                        <th class="text-center" >SCORE</th>
	                    </thead>
                    	<tbody>	';
              if(!empty($list))
              {
              	$arrayKey=0;
               foreach ($list as $mentee) 
                    {
                        $htmldata=$htmldata.'
                        		
                        <tr>
                			<td class="text-left col-md-3">'.$mentee['menteeFname']." ".$mentee['menteeLname'].'
                			</td>
                	        <td class="text-center col-md-3"> 
                	        	<input type="text" class="form-control text-center" name="mentee['.$arrayKey.'][menteeId]"'.' style="width:100%!important;" value="'.$mentee['menteeAutoId'].'">
                	        </td>
                	        <td class="text-center  col-md-3">'.$mentee['subjectCode'].'
                	        <input type="text" class="form-control text-center" name="mentee['.$arrayKey.'][subjectId]"'.' style="width:100%!important;display:none;" value="'.$subjectId.'">
                	        </td>
                	        <td class="text-left  col-md-3">
                	        <input type="number" class="form-control text-center" name="mentee['.$arrayKey.'][subjectScore]"'.' style="width:100%!important;" min="0" max="100" >
                            </td>
                     	</tr>';

                     	$arrayKey=$arrayKey+1;
                       

                    }
                  $htmldata=$htmldata.'</tbody></table><button id="submit" class="btn btn-primary" style="margin-top: 10px" >Submit</button></form>
                <script>
                
  					$(document).ready(function () {
  						$("#list").DataTable({responsive:true,"aaSorting": [],scrollY:"60vh", scrollCollapse: true,paging:false,columnDefs: [{ orderable: false,targets: [3]}] });});</script>';
                }else{

                $htmldata='No mentees available';
                }
               
                echo json_encode($htmldata);
            }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
    }
	
	//form 3 marks per mentee
	public function f3marksps()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['mentees']=$this->model->activeF3Mentees();
			// $list['subjects']=$this->model->subjects();
			$this->load->view('admin/f3markspermentee',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//form 4 marks per mentee
	public function f4marksps()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['mentees']=$this->model->activeF4Mentees();
			// $list['subjects']=$this->model->subjects();
			$this->load->view('admin/f4markspermentee',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	//admin profile
	public function adminprofile()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$adminUID=$this->input->post('adminUID',TRUE);
			$list['admin_profile']=$this->model->getAdmin($adminUID);
			$this->load->view('admin/admin_profile',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }	
		}
	//register admin
	public function  newadmin()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$firstName=$this->input->post('firstName',TRUE);
			$lastName=$this->input->post('lastName',TRUE);
			$otherNames=$this->input->post('otherNames',TRUE);
			$userName=$this->input->post('userName',TRUE);
			$gender=$this->input->post('gender');
			$staffId=$this->input->post('staffId',TRUE);
			$nationalId=$this->input->post('nationalId',TRUE);
			$phoneNumber=$this->input->post('phoneNumber',TRUE);
			$officePhoneNumber=$this->input->post('officePhoneNumber',TRUE);
			$emailAddress=$this->input->post('emailAddress',TRUE);
			// $adminPassword=md5($phoneNumber);

		    $adminDateAdded = date("Y-m-d H:i:s"); 


			//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];


			$config['upload_path']          = 'uploads/profile_photos/admins';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		     // $config['max_size']             = 100;
		    $config['max_width']            = "";
		    $config['max_height']           = "";
		    $config['min_width']            = "";
		    $config['min_height']           = "";
		    $config['file_name']			=md5(time().$staffId.$nationalId.$phoneNumber);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

		    $this->load->library('upload', $config);
		     //if no photo uploaded
			  if (empty($_FILES['photo']['name'])) 
			  {
			  	$adminInfo=array('adminFname'=>$firstName,'adminLname'=>$lastName, 'adminOtherNames'=>$otherNames,'adminGender'=>$gender,'adminStaffId'=>$staffId,'adminId'=>$nationalId,'adminPhone1'=>$phoneNumber,'adminEmail'=>$emailAddress,'adminUserName'=>$userName,'adminDateAdded'=>$adminDateAdded,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);

		    	$result=$this->model->registerAdmin($adminInfo);
				if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "New admin added");
						
			           redirect(base_url(('admin/admins')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "Failed to add admin",'success' => "");
							
			               redirect(base_url(('admin/admins')));
						}
			  }else{

				    if ( ! $this->upload->do_upload('photo'))
				    {   
				        $_SESSION['msg'] = array('error' => $this->upload->display_errors(),'success' => "");
						
				       	redirect(base_url(('admin/admins')));
				    }
				    else
				    {
				       $data =$this->upload->data();
				    	//details of uploaded file

				    	$adminInfo=array('adminFname'=>$firstName,'adminLname'=>$lastName, 'adminOtherNames'=>$otherNames,'adminGender'=>$gender,'adminStaffId'=>$staffId,'adminId'=>$nationalId,'adminPhone1'=>$phoneNumber,'adminEmail'=>$emailAddress,'adminUserName'=>$userName,'adminProfilePhoto'=>$config['file_name'].$data['file_ext'],'adminDateAdded'=>$adminDateAdded,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);

				    	$result=$this->model->registerAdmin($adminInfo);
						if($result)
							{
								$_SESSION['msg'] = array('error' => "",'success' => "New admin added");
								
					           redirect(base_url(('admin/admins')));
							}else 
								{
									$_SESSION['msg'] = array('error' => "Failed to add admin",'success' => "");
									
					               redirect(base_url(('admin/admins')));
								}
				    }
				}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//admin edit view page
	public function editadmin()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$adminUID=$this->input->post('adminUID',TRUE);
			$list['admin_profile']=$this->model->getAdmin($adminUID);
			$this->load->view('admin/edit_admin',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//update admin information
	public function  updateadmin()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$adminUID=$this->input->post('adminUID',TRUE);

			//get initial file name..this file should be deleted and a new one inserted on update
			$initFile=$this->input->post('initFile',TRUE);

			$firstName=$this->input->post('firstName',TRUE);
			$lastName=$this->input->post('lastName',TRUE);
			$otherNames=$this->input->post('otherNames',TRUE);
			$userName=$this->input->post('userName',TRUE);
			$gender=$this->input->post('gender');
			$staffId=$this->input->post('staffId',TRUE);
			$nationalId=$this->input->post('nationalId',TRUE);
			$phoneNumber=$this->input->post('phoneNumber',TRUE);
			$emailAddress=$this->input->post('emailAddress',TRUE);

			//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];

			$config['upload_path']          = 'uploads/profile_photos/admins';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		    // $config['max_size']             = 100;
		    // $config['max_width']            = 1024;
		    // $config['max_height']           = 768;
		    $config['max_width']            = "";
		    $config['max_height']           = "";
		    $config['min_width']            = "";
		    $config['min_height']           = "";
		    $config['file_name']			=md5(time().$staffId.$nationalId.$phoneNumber);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

		    $this->load->library('upload', $config);


		    if (empty($_FILES['photo']['name'])) {
		    	$adminInfo=array('adminFname'=>$firstName,'adminLname'=>$lastName, 'adminOtherNames'=>$otherNames,'adminGender'=>$gender,'adminStaffId'=>$staffId,'adminId'=>$nationalId,'adminPhone1'=>$phoneNumber,'adminEmail'=>$emailAddress,'adminUserName'=>$userName,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

				$result=$this->model->updateAdminNoPhoto($adminInfo,$adminUID);
				if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "Admin info updated");
						
			           redirect(base_url(('admin/admins')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "No changes made ",'success' => "");
							
			               redirect(base_url(('admin/admins')));
						}

			}else{ 

					if(!$this->upload->do_upload('photo'))
					{
						
						$adminInfo=array('adminFname'=>$firstName,'adminLname'=>$lastName, 'adminOtherNames'=>$otherNames,'adminGender'=>$gender,'adminStaffId'=>$staffId,'adminId'=>$nationalId,'adminPhone1'=>$phoneNumber,'adminEmail'=>$emailAddress,'adminUserName'=>$userName,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);
						$result=$this->model->updateAdminNoPhoto($adminInfo,$adminUID);
						if($result)
							{
								$_SESSION['msg'] = array('error' => "",'success' => "<span style='color:#FFFF00'>Admin info was updated. </span> However, ".$this->upload->display_errors());
								
					           redirect(base_url(('admin/admins')));
							}else 
								{
									$_SESSION['msg'] = array('error' => "No changes made  and, ".$this->upload->display_errors(),'success' => "");
									
					               redirect(base_url(('admin/admins')));
								}
					}else{

						   	$data =$this->upload->data();//details of uploaded file

							$adminInfo=array('adminFname'=>$firstName,'adminLname'=>$lastName, 'adminOtherNames'=>$otherNames,'adminGender'=>$gender,'adminStaffId'=>$staffId,'adminId'=>$nationalId,'adminPhone1'=>$phoneNumber,'adminEmail'=>$emailAddress,'adminUserName'=>$userName,'adminProfilePhoto'=>$config['file_name'].$data['file_ext'],'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);


							//new session of admin profile photo
		                    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array("adminProfilePhoto"=>$config['file_name'].$data['file_ext']));

							$result=$this->model->updateAdmin($adminInfo,$adminUID,$initFile);
							if($result)
								{
									$_SESSION['msg'] = array('error' => "",'success' => "Admin info updated");
									
						           redirect(base_url(('admin/admins')));
								}else 
									{
										$_SESSION['msg'] = array('error' => "Failed to update ",'success' => "");
										
						               redirect(base_url(('admin/admins')));
									}


					}

			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
		
	}
	public function deladmin()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$adminUID=$this->input->post('adminUID',TRUE);
			$adminName=$this->input->post('adminName',TRUE);

			if(isset($adminUID) && $adminUID !="")
			{
				$_SESSION['msg'] = 

					array('error' => '<span class="fa fa-warning"> Delete everything about <b>'. $adminName.'?</b></span> 

							<form style="display:inline;" name="formDel_'.$adminUID. '" method="post" action="'.base_url('admin/deladminconfirm').'">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                 <label for="adminUID" class="control-label">admin UID*</label>
                                <input class="form-control" name="adminUID" id="adminUID" value="'.$adminUID.'">
                            </div>
                            <button class="btn btn-success btn-s" data-placement="top" data-toggle="tooltip" data-title="Delete admin" title="Delete admin" id="del_'. $adminUID. '" name="del_'. $adminUID.'"  type="submit" >Confirm and Delete</button>
                        </form>
                        ','success' => "");
		            redirect(base_url(('admin/admins')));

			}else{
					$_SESSION['msg'] = array('error' => "Select admin to delete ",'success' => "");
		            redirect(base_url(('admin/admins')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	
	public function deladminconfirm()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$adminUID=$this->input->post('adminUID',TRUE);
			$adminName=$this->input->post('adminName',TRUE);

			if(isset($adminUID) && $adminUID !="")
			{
				$result= $this->model->deleteAdmin($adminUID);
				if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Admin records deleted");
					
		           redirect(base_url(('admin/admins')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Could not delete admin records",'success' => "");
						
		               redirect(base_url(('admin/admins')));
					}
				
		            redirect(base_url(('admin/admins')));

			}else{
					$_SESSION['msg'] = array('error' => "Select admin to delete ",'success' => "");
		            redirect(base_url(('admin/admins')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	public function disableadmin()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$adminUID=$this->input->post('adminUID',TRUE);
			$adminName=$this->input->post('adminName',TRUE);

			if(isset($adminUID) && $adminUID !="")
			{
				$_SESSION['msg'] = 

					array('error' => '<span class="fa fa-warning"> Disable <b>'. $adminName.'?</b></span> 

							<form style="display:inline;" name="formDisable_'.$adminUID. '" method="post" action="'.base_url('admin/disableadminconfirm').'">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                 <label for="adminUID" class="control-label">admin UID*</label>
                                <input class="form-control" name="adminUID" id="adminUID" value="'.$adminUID.'">
                            </div>
                            <button class="btn btn-success btn-s" data-placement="top" data-toggle="tooltip" title="Disable admin" id="disable_'. $adminUID. '" name="disable_'. $adminUID.'"  type="submit" >Confirm and Disable</button>
                        </form>
                        ','success' => "");
		            redirect(base_url(('admin/admins')));

			}else{
					$_SESSION['msg'] = array('error' => "Select admin to disable ",'success' => "");
		            redirect(base_url(('admin/admins')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	public function disableadminconfirm()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$adminUID=$this->input->post('adminUID',TRUE);
			$adminName=$this->input->post('adminName',TRUE);

			if(isset($adminUID) && $adminUID !="")
			{
				$disableInfo=array('adminActiveStatus'=>0);
				$result= $this->model->disableAdmin($adminUID,$disableInfo);
				if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' =>"Admin was disabled.");
					
		           redirect(base_url(('admin/admins')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Could not disable admin",'success' => "");
						
		               redirect(base_url(('admin/admins')));
					}
				
		            redirect(base_url(('admin/admins')));

			}else{
					$_SESSION['msg'] = array('error' => "Select admin to disable",'success' => "");
		            redirect(base_url(('admin/admins')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	

	//Mentor Registration
	public function  newmentor()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$firstName=$this->input->post('firstName');
			$lastName=$this->input->post('lastName');
			$otherNames=$this->input->post('otherNames');
			$gender=$this->input->post('gender');
			$dob=$this->input->post('dob');
			$studentId=$this->input->post('studentId');
			$staffId=$this->input->post('staffId');
			$yearOfStudy=$this->input->post('yearOfStudy');
			$emailAddress=$this->input->post('emailAddress');
			$institutionId=$this->input->post('institutionId');
			
			$course=$this->input->post('courseId');
			if (empty($course)){ 
			    $courseId=NULL;
			}else{
			    $courseId=$course;
			}
			
			$phoneNumber=$this->input->post('phoneNumber');
			$mentorLevel=$this->input->post('mentorLevel');
			$profession=$this->input->post('profession');
			$mentorDateJoined=$this->input->post('joinDate');

			$mentorPassword=md5($phoneNumber);

			$mentorDateAdded=date("Y-m-d H:i:s");

			//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];

			$config['upload_path']          = 'uploads/profile_photos/mentors';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		    // $config['max_size']             = 100;
		    $config['max_width']            = "";
		    $config['max_height']           = "";
		    $config['min_width']            = "";
		    $config['min_height']           = "";
		    $config['file_name']			=md5(time().$dob.$firstName.$lastName);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

		    $this->load->library('upload', $config);

		   	//if no photo uploaded
			  if (empty($_FILES['photo']['name'])) 
			  {
			  	$mentorInfo=array('mentorFname'=>$firstName,'mentorLname'=>$lastName, 'mentorOtherNames'=>$otherNames,'mentorGender'=>$gender,'mentorPhone1'=>$phoneNumber,'mentorEmail'=>$emailAddress,'mentorInstitutionId'=>$institutionId,'mentorCourse'=>$courseId, 'mentorDoB'=>$dob,'mentorStudId'=>$studentId,'mentorStaffId'=>$staffId,'mentorPassword'=>$mentorPassword,'mentorLevel'=>$mentorLevel,'mentorYoS'=>$yearOfStudy,'mentorProfession'=>$profession, 'mentorDateAdded'=>$mentorDateAdded,'mentorDateJoined'=>$mentorDateJoined,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
				$result=$this->model->registerMentor($mentorInfo);
				if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "New mentor added");
						
			           redirect(base_url(('admin/mentors')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "Failed to add mentor",'success' => "");
							
			               redirect(base_url(('admin/mentors')));
						}
			}else{

				    if(!$this->upload->do_upload('photo'))
				    {   
				        $_SESSION['msg'] = array('error' => $this->upload->display_errors(),'success' => "");
						
				       	redirect(base_url(('admin/mentors')));
				    }
				    else
					    {
					       $data =$this->upload->data();
					    	//details of uploaded file
					       $mentorInfo=array('mentorFname'=>$firstName,'mentorLname'=>$lastName, 'mentorOtherNames'=>$otherNames,'mentorGender'=>$gender,'mentorPhone1'=>$phoneNumber,'mentorEmail'=>$emailAddress,'mentorInstitutionId'=>$institutionId,'mentorCourse'=>$courseId, 'mentorDoB'=>$dob,'mentorStudId'=>$studentId,'mentorStaffId'=>$staffId,'mentorPassword'=>$mentorPassword,'mentorLevel'=>$mentorLevel,'mentorYoS'=>$yearOfStudy,'mentorProfession'=>$profession, 'mentorDateAdded'=>$mentorDateAdded,'mentorDateJoined'=>$mentorDateJoined,'mentorProfilePhoto'=>$config['file_name'].$data['file_ext'],'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
							$result=$this->model->registerMentor($mentorInfo);
							if($result)
								{
									$_SESSION['msg'] = array('error' => "",'success' => "New mentor added");
									
						           redirect(base_url(('admin/mentors')));
								}else 
									{
										$_SESSION['msg'] = array('error' => "Failed to add mentor",'success' => "");
										
						               redirect(base_url(('admin/mentors')));
									}
					    }
				}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//mentor profile
	public function mentorprofile()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$mentorUID=$this->input->post('mentorUID',TRUE);
			$list['mentor_profile']=$this->model->getMentor($mentorUID);
			$this->load->view('admin/mentor_profile',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	//Mentor edit view page
	public function editmentor()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$mentorUID=$this->input->post('mentorUID',TRUE);
			$list['mentor_profile']=$this->model->getMentor($mentorUID);
			$list['courses']=$this->model->courses();
			$list['mentorinst']=$this->model->institutions();
			$this->load->view('admin/edit_mentor',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//update mentor information
	public function  updatementor()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$mentorUID=$this->input->post('mentorUID',TRUE);

			//get initial file name..this file should be deleted and a new one inserted on update
			$initFile=$this->input->post('initFile',TRUE);

			$firstName=$this->input->post('firstName');
			$lastName=$this->input->post('lastName');
			$otherNames=$this->input->post('otherNames');
			$gender=$this->input->post('gender');
			$dob=$this->input->post('dob');
			$studentId=$this->input->post('studentId');
			$staffId=$this->input->post('staffId');
			$yearOfStudy=$this->input->post('yearOfStudy');
			$emailAddress=$this->input->post('emailAddress');
			$institutionId=$this->input->post('institutionId');

			$course=$this->input->post('courseId');
			if (empty($course)){ 
			    $courseId=NULL;
			}else{
			    $courseId=$course;
			}
			
			$phoneNumber=$this->input->post('phoneNumber');
			$mentorLevel=$this->input->post('mentorLevel');
			$profession=$this->input->post('profession');
			$mentorDateJoined=$this->input->post('joinDate');

			//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];



			$config['upload_path']          = 'uploads/profile_photos/mentors';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		    // $config['max_size']             = 100;
		    $config['max_width']            = "";
		    $config['max_height']           = "";
		    $config['min_width']            = "";
		    $config['min_height']           = "";
		    $config['file_name']			=md5(time().$dob.$firstName.$lastName);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

		    $this->load->library('upload', $config);


		    if (empty($_FILES['photo']['name'])) {
		    	

		    	 $mentorInfo=array('mentorFname'=>$firstName,'mentorLname'=>$lastName, 'mentorOtherNames'=>$otherNames,'mentorGender'=>$gender,'mentorPhone1'=>$phoneNumber,'mentorEmail'=>$emailAddress,'mentorInstitutionId'=>$institutionId,'mentorCourse'=>$courseId,'mentorDoB'=>$dob,'mentorStudId'=>$studentId,'mentorStaffId'=>$staffId,'mentorYoS'=>$yearOfStudy,'mentorLevel'=>$mentorLevel,'mentorProfession'=>$profession,'mentorDateJoined'=>$mentorDateJoined,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

				$result=$this->model->updateMentorNoPhoto($mentorInfo,$mentorUID);
				if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "Mentor info updated");
						
			           redirect(base_url(('admin/mentors')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "No changes made ",'success' => "");
							
			               redirect(base_url(('admin/mentors')));
						}

			}else{ 

					if(!$this->upload->do_upload('photo'))
					{
						
						$mentorInfo=array('mentorFname'=>$firstName,'mentorLname'=>$lastName, 'mentorOtherNames'=>$otherNames,'mentorGender'=>$gender,'mentorPhone1'=>$phoneNumber,'mentorEmail'=>$emailAddress,'mentorInstitutionId'=>$institutionId,'mentorCourse'=>$courseId,'mentorDoB'=>$dob,'mentorStudId'=>$studentId,'mentorStaffId'=>$staffId,'mentorLevel'=>$mentorLevel,'mentorYoS'=>$yearOfStudy,'mentorProfession'=>$profession,'mentorDateJoined'=>$mentorDateJoined,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

						$result=$this->model->updateMentorNoPhoto($mentorInfo,$mentorUID);
						if($result)
							{
								$_SESSION['msg'] = array('error' => "",'success' => "<span style='color:#FFFF00'>Mentor info was updated. </span> However, ".$this->upload->display_errors());
								
					           redirect(base_url(('admin/mentors')));
							}else 
								{
									$_SESSION['msg'] = array('error' => "No changes made  and, ".$this->upload->display_errors(),'success' => "");
									
					               redirect(base_url(('admin/mentors')));
								}
					}else{

						   	$data =$this->upload->data();//details of uploaded file

						   	$mentorInfo=array('mentorFname'=>$firstName,'mentorLname'=>$lastName, 'mentorOtherNames'=>$otherNames,'mentorGender'=>$gender,'mentorPhone1'=>$phoneNumber,'mentorEmail'=>$emailAddress,'mentorInstitutionId'=>$institutionId,'mentorCourse'=>$courseId,'mentorDoB'=>$dob,'mentorStudId'=>$studentId,'mentorStaffId'=>$staffId,'mentorLevel'=>$mentorLevel,'mentorYoS'=>$yearOfStudy,'mentorProfession'=>$profession,'mentorProfilePhoto'=>$config['file_name'].$data['file_ext'],'mentorDateJoined'=>$mentorDateJoined,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

							$result=$this->model->updateMentor($mentorInfo,$mentorUID,$initFile);
							if($result)
							{
								$_SESSION['msg'] = array('error' => "",'success' => "Mentor info updated");
								
					           redirect(base_url(('admin/mentors')));
							}else 
								{
									$_SESSION['msg'] = array('error' => "Failed to update ",'success' => "");
									
					               redirect(base_url(('admin/mentors')));
								}

					}

			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
		
	}
	public function delmentor()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$mentorUID=$this->input->post('mentorUID',TRUE);
			$mentorName=$this->input->post('mentorName',TRUE);

			if(isset($mentorUID) && $mentorUID !="")
			{
				$_SESSION['msg'] = 

					array('error' => '<span class="fa fa-warning"> Delete everything about <b>'. $mentorName.'?</b></span> 

							<form style="display:inline;" name="formDel_'.$mentorUID. '" method="post" action="'.base_url('admin/delmentorconfirm').'">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                 <label for="mentorUID" class="control-label">mentor UID*</label>
                                <input class="form-control" name="mentorUID" id="mentorUID" value="'.$mentorUID.'">
                            </div>
                            <button class="btn btn-success btn-s" data-placement="top" data-toggle="tooltip" data-title="Delete mentor" title="Delete mentor" id="del_'. $mentorUID. '" name="del_'. $mentorUID.'"  type="submit" >Confirm and Delete</button>
                        </form>
                        ','success' => "");
		            redirect(base_url(('admin/mentors')));

			}else{
					$_SESSION['msg'] = array('error' => "Select mentor to delete ",'success' => "");
		            redirect(base_url(('admin/mentors')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	
	public function delmentorconfirm()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$mentorUID=$this->input->post('mentorUID',TRUE);
			$mentorName=$this->input->post('mentorName',TRUE);

			if(isset($mentorUID) && $mentorUID !="")
			{
				$result= $this->model->deletementor($mentorUID);
				if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Mentor records deleted");
					
		           redirect(base_url(('admin/mentors')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Could not delete mentor records",'success' => "");
						
		               redirect(base_url(('admin/mentors')));
					}
				
		            redirect(base_url(('admin/mentors')));

			}else{
					$_SESSION['msg'] = array('error' => "Select mentor to delete ",'success' => "");
		            redirect(base_url(('admin/mentors')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	public function disablementor()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$mentorUID=$this->input->post('mentorUID',TRUE);
			$mentorName=$this->input->post('mentorName',TRUE);

			if(isset($mentorUID) && $mentorUID !="")
			{
				$_SESSION['msg'] = 

					array('error' => '<span class="fa fa-warning"> Disable <b>'. $mentorName.'?</b></span> 

							<form style="display:inline;" name="formDisable_'.$mentorUID. '" method="post" action="'.base_url('admin/disablementorconfirm').'">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                 <label for="mentorUID" class="control-label">mentor UID*</label>
                                <input class="form-control" name="mentorUID" id="mentorUID" value="'.$mentorUID.'">
                            </div>
                            <button class="btn btn-success btn-s" data-placement="top" data-toggle="tooltip" title="Disable mentor" id="disable_'. $mentorUID. '" name="disable_'. $mentorUID.'"  type="submit" >Confirm and Disable</button>
                        </form>
                        ','success' => "");
		            redirect(base_url(('admin/mentors')));

			}else{
					$_SESSION['msg'] = array('error' => "Select mentor to disable ",'success' => "");
		            redirect(base_url(('admin/mentors')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	public function disablementorconfirm()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$mentorUID=$this->input->post('mentorUID',TRUE);
			$mentorName=$this->input->post('mentorName',TRUE);

			if(isset($mentorUID) && $mentorUID !="")
			{
				$disableInfo=array('mentorActiveStatus'=>0);
				$result= $this->model->disablementor($mentorUID,$disableInfo);
				if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Mentor disabled");
					
		           redirect(base_url(('admin/mentors')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Could not disable mentor",'success' => "");
						
		               redirect(base_url(('admin/mentors')));
					}
				
		            redirect(base_url(('admin/mentors')));

			}else{
					$_SESSION['msg'] = array('error' => "Select mentor to disable ",'success' => "");
		            redirect(base_url(('admin/mentors')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	//Guardian Registration
	public function  newparent()
	{

		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$firstName=$this->input->post('firstName');
			$lastName=$this->input->post('lastName');
			$otherNames=$this->input->post('otherNames');
			$gender=$this->input->post('gender');
			$dob=$this->input->post('dob');

			$mail=$this->input->post('emailAddress');
			if($mail=="")
			{
				$emailAddress=NULL;
			}else{
					$emailAddress=$mail;
			}

			$guardianId=$this->input->post('guardianId');
			$phoneNumber=$this->input->post('phoneNumber');

			$phone=$this->input->post('phoneNumber2');

			if($phone=="")
			{
				$phoneNumber2=NULL;
			}else{
					$phoneNumber2=$phone;
			}

			$guardianProfession=$this->input->post('profession');

			$guardianDateAdded=date("Y-m-d H:i:s");

			//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];


			$config['upload_path']          = 'uploads/profile_photos/guardians';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		    // $config['max_size']             = 100;
		    $config['max_width']            = "";
		    $config['max_height']           = "";
		    $config['min_width']            = "";
		    $config['min_height']           = "";
		    $config['file_name']			=md5(time().$dob.$firstName.$lastName);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

			 $this->load->library('upload', $config);

			 //if no photo uploaded
			  if (empty($_FILES['photo']['name'])) 
			  {
			  	$guardianInfo=array('guardianFname'=>$firstName,'guardianLname'=>$lastName, 'guardianOtherNames'=>$otherNames,'guardianGender'=>$gender,'guardianPhone1'=>$phoneNumber,'guardianPhone2'=>$phoneNumber2,'guardianEmail'=>$emailAddress,'guardianId'=>$guardianId,'guardianDoB'=>$dob,'guardianProfession'=>$guardianProfession,'guardianDateAdded'=>$guardianDateAdded,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);

		    	$result=$this->model->registerGuardian($guardianInfo);
				if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "New parent/guardian added");
						
			           redirect(base_url(('admin/guardians')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "Registration failed",'success' => "");
							
			               redirect(base_url(('admin/guardians')));
						}
			  }else{

				    if (!$this->upload->do_upload('photo'))
				    {   
				        $_SESSION['msg'] = array('error' => $this->upload->display_errors(),'success' => "");
						
				       	redirect(base_url(('admin/guardians')));
				    }else{
			       
				       		$data =$this->upload->data();
				    		//details of uploaded file
				    		$guardianInfo=array('guardianFname'=>$firstName,'guardianLname'=>$lastName, 'guardianOtherNames'=>$otherNames,'guardianGender'=>$gender,'guardianPhone1'=>$phoneNumber,'guardianPhone2'=>$phoneNumber2,'guardianEmail'=>$emailAddress,'guardianId'=>$guardianId,'guardianDoB'=>$dob,'guardianProfession'=>$guardianProfession,'guardianProfilePhoto'=>$config['file_name'].$data['file_ext'],'guardianDateAdded'=>$guardianDateAdded,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);

					    	$result=$this->model->registerGuardian($guardianInfo);
							if($result)
								{
									$_SESSION['msg'] = array('error' => "",'success' => "New parent/guardian added");
									
						           redirect(base_url(('admin/guardians')));
								}else 
									{
										$_SESSION['msg'] = array('error' => "Registration failed",'success' => "");
										
						               redirect(base_url(('admin/guardians')));
									}
						}
		    	}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//admin edit view page
	public function editguardian()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$guardianUID=$this->input->post('guardianUID',TRUE);
			$list['guardian_profile']=$this->model->getGuardian($guardianUID);
			$this->load->view('admin/edit_guardian',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//update parent information
	public function  updateparent()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$guardianUID=$this->input->post('guardianUID',TRUE);

			//get initial file name..this file should be deleted and a new one inserted on update
			$initFile=$this->input->post('initFile',TRUE);

			$firstName=$this->input->post('firstName');
			$lastName=$this->input->post('lastName');
			$otherNames=$this->input->post('otherNames');
			$gender=$this->input->post('gender');
			$dob=$this->input->post('dob');
			$mail=$this->input->post('emailAddress');
			
			$mail=$this->input->post('emailAddress');
			if($mail=="")
			{
				$emailAddress=NULL;
			}else{
					$emailAddress=$mail;
			}

			$guardianId=$this->input->post('guardianId');
			$phoneNumber=$this->input->post('phoneNumber');

			$phone=$this->input->post('phoneNumber2');

			if($phone=="")
			{
				$phoneNumber2=NULL;
			}else{
					$phoneNumber2=$phone;
			}

			$guardianProfession=$this->input->post('profession');

			//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];


			$config['upload_path']          = 'uploads/profile_photos/guardians';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		    // $config['max_size']             = 100;
		    $config['max_width']            = "";
		    $config['max_height']           = "";
		    $config['min_width']            = "";
		    $config['min_height']           = "";
		    $config['file_name']			=md5(time().$dob.$firstName.$lastName);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

			 $this->load->library('upload', $config);

		    if (empty($_FILES['photo']['name'])) {
		    	 $guardianInfo=array('guardianFname'=>$firstName,'guardianLname'=>$lastName, 'guardianOtherNames'=>$otherNames,'guardianGender'=>$gender,'guardianPhone1'=>$phoneNumber,'guardianPhone2'=>$phoneNumber2,'guardianEmail'=>$emailAddress,'guardianId'=>$guardianId,'guardianDoB'=>$dob,'guardianProfession'=>$guardianProfession,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

				$result=$this->model->updateGuardianNoPhoto($guardianUID,$guardianInfo);
				if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Parent/Guardian info updated");
					
		           redirect(base_url(('admin/guardians')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "No changes made ",'success' => "");
						
		               redirect(base_url(('admin/guardians')));
					}

			}else{ 

					if(!$this->upload->do_upload('photo'))
					{
						$guardianInfo=array('guardianFname'=>$firstName,'guardianLname'=>$lastName, 'guardianOtherNames'=>$otherNames,'guardianGender'=>$gender,'guardianPhone1'=>$phoneNumber,'guardianPhone2'=>$phoneNumber2,'guardianEmail'=>$emailAddress,'guardianId'=>$guardianId,'guardianDoB'=>$dob,'guardianProfession'=>$guardianProfession,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

						$result=$this->model->updateGuardianNoPhoto($guardianUID,$guardianInfo);
						if($result)
						{
							$_SESSION['msg'] = array('error' => "",'success' => "<span style='color:#FFFF00'>Guardian info was updated. </span> However, ".$this->upload->display_errors());
							
				           redirect(base_url(('admin/guardians')));
						}else 
							{
								$_SESSION['msg'] = array('error' => "No changes made  and, ".$this->upload->display_errors(),'success' => "");
								
				               redirect(base_url(('admin/guardians')));
							}
					}else{

						   	$data =$this->upload->data();//details of uploaded file

						   	$guardianInfo=array('guardianFname'=>$firstName,'guardianLname'=>$lastName, 'guardianOtherNames'=>$otherNames,'guardianGender'=>$gender,'guardianPhone1'=>$phoneNumber,'guardianPhone2'=>$phoneNumber2,'guardianEmail'=>$emailAddress,'guardianId'=>$guardianId,'guardianDoB'=>$dob,'guardianProfession'=>$guardianProfession,'guardianProfilePhoto'=>$config['file_name'].$data['file_ext'],'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

							$result=$this->model->updateGuardian($guardianUID,$guardianInfo,$initFile);
							if($result)
								{
									$_SESSION['msg'] = array('error' => "",'success' => "Parent/Guardian info updated");
									
						           redirect(base_url(('admin/guardians')));
								}else 
									{
										$_SESSION['msg'] = array('error' => "Failed to update ",'success' => "");
										
						               redirect(base_url(('admin/guardians')));
									}


					}

			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
		
	}
	public function disableguardian()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$guardianUID=$this->input->post('guardianUID',TRUE);
			$guardianName=$this->input->post('guardianName',TRUE);

			if(isset($guardianUID) && $guardianUID !="")
			{
				$_SESSION['msg'] = 

					array('error' => '<span class="fa fa-warning"> Disable <b>'. $guardianName.'?</b></span> 

							<form style="display:inline;" name="formDisable_'.$guardianUID. '" method="post" action="'.base_url('admin/disableguardianconfirm').'">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                 <label for="guardianUID" class="control-label">guardian UID*</label>
                                <input class="form-control" name="guardianUID" id="guardianUID" value="'.$guardianUID.'">
                            </div>
                            <button class="btn btn-success btn-s" data-placement="top" data-toggle="tooltip" title="Disable guardian" id="disable_'. $guardianUID. '" name="disable_'. $guardianUID.'"  type="submit" >Confirm and Disable</button>
                        </form>
                        ','success' => "");
		            redirect(base_url(('admin/guardians')));

			}else{
					$_SESSION['msg'] = array('error' => "Select parent/guardian to disable ",'success' => "");
		            redirect(base_url(('admin/guardians')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	public function disableguardianconfirm()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$guardianUID=$this->input->post('guardianUID',TRUE);
			$guardianName=$this->input->post('guardianName',TRUE);

			if(isset($guardianUID) && $guardianUID !="")
			{
				$disableInfo=array('guardianActiveStatus'=>0);
				$result= $this->model->disableGuardian($guardianUID,$disableInfo);
				if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' =>"Parent/guardian was disabled.");
					
		           redirect(base_url(('admin/guardians')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Could not disable parent/guardian",'success' => "");
						
		               redirect(base_url(('admin/guardians')));
					}
				
		            redirect(base_url(('admin/guardians')));

			}else{
					$_SESSION['msg'] = array('error' => "Select parent/guardian to disable",'success' => "");
		            redirect(base_url(('admin/guardians')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	public function delguardian()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$guardianUID=$this->input->post('guardianUID',TRUE);
			$guardianName=$this->input->post('guardianName',TRUE);

			if(isset($guardianUID) && $guardianUID !="")
			{
				$_SESSION['msg'] = 

					array('error' => '<span class="fa fa-warning"> Delete everything about <b>'. $guardianName.'?</b></span> 

							<form style="display:inline;" name="formDel_'.$guardianUID. '" method="post" action="'.base_url('admin/delguardianconfirm').'">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                 <label for="guardianUID" class="control-label">guardian UID*</label>
                                <input class="form-control" name="guardianUID" id="guardianUID" value="'.$guardianUID.'">
                            </div>
                            <button class="btn btn-success btn-s" data-placement="top" data-toggle="tooltip" data-title="Delete guardian" title="Delete guardian" id="del_'. $guardianUID. '" name="del_'. $guardianUID.'"  type="submit" >Confirm and Delete</button>
                        </form>
                        ','success' => "");
		            redirect(base_url(('admin/guardians')));

			}else{
					$_SESSION['msg'] = array('error' => "Select parent/guardian to delete ",'success' => "");
		            redirect(base_url(('admin/guardians')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	
	public function delguardianconfirm()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$guardianUID=$this->input->post('guardianUID',TRUE);
			$guardianName=$this->input->post('guardianName',TRUE);

			if(isset($guardianUID) && $guardianUID !="")
			{
				$result= $this->model->deleteGuardian($guardianUID);
				if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Guardian records deleted");
					
		           redirect(base_url(('admin/guardians')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Could not delete parent/guardian records",'success' => "");
						
		               redirect(base_url(('admin/guardians')));
					}
				
		            redirect(base_url(('admin/guardians')));

			}else{
					$_SESSION['msg'] = array('error' => "Select parent/guardian to delete ",'success' => "");
		            redirect(base_url(('admin/guardians')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//Parent/Guardian  profile
	public function guardianprofile()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$guardianUID=$this->input->post('guardianUID',TRUE);
			$list['guardian_profile']=$this->model->getGuardian($guardianUID);
			$this->load->view('admin/guardian_profile',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//mentee Registration
	public function  newmentee()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$firstName=$this->input->post('firstName',TRUE);
			$lastName=$this->input->post('lastName',TRUE);
			$otherNames=$this->input->post('otherNames',TRUE);
			$gender=$this->input->post('gender',TRUE);
			$dob=$this->input->post('dob',TRUE);

			$mail=$this->input->post('emailAddress');
			if($mail=="")
			{
				$emailAddress=NULL;
			}else{
					$emailAddress=$mail;
			}

			$phone=$this->input->post('phoneNumber');

			if($phone=="")
			{
				$phoneNumber=NULL;
			}else{
					$phoneNumber=$phone;
			}

			$institutionId=$this->input->post('institutionId',TRUE);
			$formCode=$this->input->post('formCode',TRUE);

			$menteeDateJoined=$this->input->post('joinDate');

			$menteeDateAdded=date("Y-m-d H:i:s");

			$year=date("Y");

			//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];

			$config['upload_path']          = 'uploads/profile_photos/mentees';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		    // $config['max_size']             = 100;
		    $config['max_width']            = "";
		    $config['max_height']           = "";
		    $config['min_width']            = "";
		    $config['min_height']           = "";
		    $config['file_name']			=md5(time().$dob.$firstName.$lastName);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

		    $this->load->library('upload', $config);
	     	//if no photo uploaded
		  	if (empty($_FILES['photo']['name'])) 
		  	{
		  		$menteeInfo=array('menteeFname'=>$firstName,'menteeLname'=>$lastName, 'menteeOtherNames'=>$otherNames,'menteeGender'=>$gender,'menteePhone'=>$phoneNumber,'menteeEmail'=>$emailAddress,'menteeDoB'=>$dob,'menteeSchoolId'=>$institutionId,'menteeDateAdded'=>$menteeDateAdded,'menteeDateJoined'=>$menteeDateJoined,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);

		    	$result=$this->model->registerMentee($menteeInfo);
				if($result)
					{
						$idOfJustInserted=$result;//auto id of the just inserted row. this will be used to assign the mentee into his/her current form

						$formInfo=array('classMenteeId'=>$idOfJustInserted,'classYear'=>$year,'formCode'=>$formCode,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
						
						$formInsertState=$this->model->insertMenteeToRespectiveForm($formInfo);

						if($formInsertState)
						{
							$_SESSION['msg'] = array('error' => "",'success' => "New mentee added");
							
				           redirect(base_url(('admin/mentees')));
				        }else {
					        	$_SESSION['msg'] = array('error' => "Mentee registered with errors. form Not Recorded.",'success' => "");
								
				               redirect(base_url(('admin/mentees')));
				        }
					}else 
						{
							$_SESSION['msg'] = array('error' => "Registration failed",'success' => "");
							
			               redirect(base_url(('admin/mentees')));
						}
			}else{
				    if (!$this->upload->do_upload('photo'))
				    {   
				        $_SESSION['msg'] = array('error' => $this->upload->display_errors(),'success' => "");
						
				       	redirect(base_url(('admin/mentees')));
				    }
				    else{
				       		$data =$this->upload->data();
					    	//details of uploaded file
					    	$menteeInfo=array('menteeFname'=>$firstName,'menteeLname'=>$lastName, 'menteeOtherNames'=>$otherNames,'menteeGender'=>$gender,'menteePhone'=>$phoneNumber,'menteeEmail'=>$emailAddress,'menteeDoB'=>$dob,'menteeSchoolId'=>$institutionId,'menteeProfilePhoto'=>$config['file_name'].$data['file_ext'],'menteeDateJoined'=>$menteeDateJoined,'menteeDateAdded'=>$menteeDateAdded,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);

					    	$result=$this->model->registerMentee($menteeInfo);
							if($result)
								{
									$idOfJustInserted=$result;//auto id of the just inserted row. this will be used to assign the mentee into his/her current form

									$formInfo=array('classMenteeId'=>$idOfJustInserted,'formYear'=>$year,'formCode'=>$formCode);
									
									$formInsertState=$this->model->insertMenteeToRespectiveForm($formInfo);

									if($formInsertState)
									{
										$_SESSION['msg'] = array('error' => "",'success' => "New mentee added");
										
							           redirect(base_url(('admin/mentees')));
							        }else {
								        	$_SESSION['msg'] = array('error' => "Mentee registered with errors. form Not Recorded.",'success' => "");
											
							               redirect(base_url(('admin/mentees')));
							        }
								}else 
									{
										$_SESSION['msg'] = array('error' => "Registration failed",'success' => "");
										
						               redirect(base_url(('admin/mentees')));
									}
				    	}
				}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }	
	}
	//mentee edit view page
	public function editmentee()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$menteeUID=$this->input->post('menteeUID',TRUE);
			$list['mentee_profile']=$this->model->getMentee($menteeUID);
			$list['schools']=$this->model->schools();
			$this->load->view('admin/edit_mentee',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//update mentee information
	public function  updatementee()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$menteeUID=$this->input->post('menteeUID',TRUE);

			//get initial file name..this file should be deleted and a new one inserted on update
			$initFile=$this->input->post('initFile',TRUE);


			$firstName=$this->input->post('firstName',TRUE);
			$lastName=$this->input->post('lastName',TRUE);
			$otherNames=$this->input->post('otherNames',TRUE);
			$gender=$this->input->post('gender',TRUE);
			$dob=$this->input->post('dob',TRUE);

			$mail=$this->input->post('emailAddress');
			if($mail=="")
			{
				$emailAddress=NULL;
			}else{
					$emailAddress=$mail;
			}

			$phone=$this->input->post('phoneNumber');

			if($phone=="")
			{
				$phoneNumber=NULL;
			}else{
					$phoneNumber=$phone;
			}
			
			$institutionId=$this->input->post('institutionId',TRUE);

			$menteeDateJoined=$this->input->post('joinDate');

			//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];


			$config['upload_path']          = 'uploads/profile_photos/mentees';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		      // $config['max_size']             = 100;
		    $config['max_width']            = "";
		    $config['max_height']           = "";
		    $config['min_width']            = "";
		    $config['min_height']           = "";
		    $config['file_name']			=md5(time().$dob.$firstName.$lastName);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

		    $this->load->library('upload', $config);

		    if (empty($_FILES['photo']['name'])) {
		    	$menteeInfo=array('menteeFname'=>$firstName,'menteeLname'=>$lastName, 'menteeOtherNames'=>$otherNames,'menteeGender'=>$gender,'menteePhone'=>$phoneNumber,'menteeEmail'=>$emailAddress,'menteeDoB'=>$dob,'menteeSchoolId'=>$institutionId,'menteeDateJoined'=>$menteeDateJoined,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

				$result=$this->model->updateMenteeNoPhoto($menteeInfo,$menteeUID);
				if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "Mentee info updated");
						
			           redirect(base_url(('admin/mentees')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "No changes made ",'success' => "");
							
			               redirect(base_url(('admin/mentees')));
						}

			}else{ 

					if(!$this->upload->do_upload('photo'))
					{
						
						$menteeInfo=array('menteeFname'=>$firstName,'menteeLname'=>$lastName, 'menteeOtherNames'=>$otherNames,'menteeGender'=>$gender,'menteePhone'=>$phoneNumber,'menteeEmail'=>$emailAddress,'menteeDoB'=>$dob,'menteeSchoolId'=>$institutionId,'menteeDateJoined'=>$menteeDateJoined,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

						$result=$this->model->updateMenteeNoPhoto($menteeInfo,$menteeUID);
						if($result)
							{
								$_SESSION['msg'] = array('error' => "",'success' => "<span style='color:#FFFF00'>Mentee info updated. </span> However, ".$this->upload->display_errors());
								
					           redirect(base_url(('admin/mentees')));
							}else 
								{
									$_SESSION['msg'] = array('error' => "No changes made  and, ".$this->upload->display_errors(),'success' => "");
									
					               redirect(base_url(('admin/mentees')));
								}


						$_SESSION['msg'] = array('error' => $this->upload->display_errors(),'success' => "");
						
				       	redirect(base_url(('admin/mentees')));
					}else{
						   	$data =$this->upload->data();//details of uploaded file

							$menteeInfo=array('menteeFname'=>$firstName,'menteeLname'=>$lastName, 'menteeOtherNames'=>$otherNames,'menteeGender'=>$gender,'menteePhone'=>$phoneNumber,'menteeEmail'=>$emailAddress,'menteeDoB'=>$dob,'menteeSchoolId'=>$institutionId,'menteeProfilePhoto'=>$config['file_name'].$data['file_ext'],'menteeDateJoined'=>$menteeDateJoined,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);
							$result=$this->model->updateMentee($menteeInfo,$menteeUID,$initFile);
							if($result)
								{
									$_SESSION['msg'] = array('error' => "",'success' => "Mentee info updated");
									
						           redirect(base_url(('admin/mentees')));
								}else 
									{
										$_SESSION['msg'] = array('error' => "Failed to update ",'success' => "");
										
						               redirect(base_url(('admin/mentees')));
									}


					}

			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
		
	}

	public function delmentee()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$menteeUID=$this->input->post('menteeUID',TRUE);
			$menteeName=$this->input->post('menteeName',TRUE);

			if(isset($menteeUID) && $menteeUID !="")
			{
				$_SESSION['msg'] = 

					array('error' => '<span class="fa fa-warning"> Delete everything about <b>'. $menteeName.'?</b></span> 

							<form style="display:inline;" name="formDel_'.$menteeUID. '" method="post" action="'.base_url('admin/delmenteeconfirm').'">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                 <label for="menteeUID" class="control-label">Mentee UID*</label>
                                <input class="form-control" name="menteeUID" id="menteeUID" value="'.$menteeUID.'">
                            </div>
                            <button class="btn btn-success btn-s" data-placement="top" data-toggle="tooltip" data-title="Delete Mentee" title="Delete Mentee" id="del_'. $menteeUID. '" name="del_'. $menteeUID.'"  type="submit" >Confirm and Delete</button>
                        </form>
                        ','success' => "");
		            redirect(base_url(('admin/mentees')));

			}else{
					$_SESSION['msg'] = array('error' => "Select mentee to delete ",'success' => "");
		            redirect(base_url(('admin/mentees')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	
	public function delmenteeconfirm()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$menteeUID=$this->input->post('menteeUID',TRUE);
			$menteeName=$this->input->post('menteeName',TRUE);

			if(isset($menteeUID) && $menteeUID !="")
			{
				$result= $this->model->deleteMentee($menteeUID);
				if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Mentee records deleted");
					
		           redirect(base_url(('admin/mentees')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Could not delete mentee records",'success' => "");
						
		               redirect(base_url(('admin/mentees')));
					}
				
		            redirect(base_url(('admin/mentees')));

			}else{
					$_SESSION['msg'] = array('error' => "Select mentee to delete ",'success' => "");
		            redirect(base_url(('admin/mentees')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	public function disablementee()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$menteeUID=$this->input->post('menteeUID',TRUE);
			$menteeName=$this->input->post('menteeName',TRUE);

			if(isset($menteeUID) && $menteeUID !="")
			{
				$_SESSION['msg'] = 

					array('error' => '<span class="fa fa-warning"> Disable <b>'. $menteeName.'?</b></span> 

							<form style="display:inline;" name="formDisable_'.$menteeUID. '" method="post" action="'.base_url('admin/disablementeeconfirm').'">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                 <label for="menteeUID" class="control-label">Mentee UID*</label>
                                <input class="form-control" name="menteeUID" id="menteeUID" value="'.$menteeUID.'">
                            </div>
                            <button class="btn btn-success btn-s" data-placement="top" data-toggle="tooltip" title="Disable Mentee" id="disable_'. $menteeUID. '" name="disable_'. $menteeUID.'"  type="submit" >Confirm and Disable</button>
                        </form>
                        ','success' => "");
		            redirect(base_url(('admin/mentees')));

			}else{
					$_SESSION['msg'] = array('error' => "Select mentee to disable ",'success' => "");
		            redirect(base_url(('admin/mentees')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	public function disablementeeconfirm()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$menteeUID=$this->input->post('menteeUID',TRUE);
			$menteeName=$this->input->post('menteeName',TRUE);

			if(isset($menteeUID) && $menteeUID !="")
			{
				$disableInfo=array('menteeActiveStatus'=>0);
				$result= $this->model->disableMentee($menteeUID,$disableInfo);
				if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Mentee disabled");
					
		           redirect(base_url(('admin/mentees')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Could not disable mentee",'success' => "");
						
		               redirect(base_url(('admin/mentees')));
					}
				
		            redirect(base_url(('admin/mentees')));

			}else{
					$_SESSION['msg'] = array('error' => "Select mentee to disable ",'success' => "");
		            redirect(base_url(('admin/mentees')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//Load Mentor Assignment Page
	public function assignmentor()
	{
		
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	$menteeUID=$this->input->post('menteeUID');

			//check if mentor assigned to mentee
			$this->db->select('*');
		    $this->db->from('mentees');
		    $this->db->where('menteeMentorId',NULL);
		    $this->db->where('menteeAutoId',$menteeUID);
		    $result=$this->db->get();
		    $num=$result->num_rows(); 
		    if($num>0)
		        {
					$list['details']=$this->model->getmentee($menteeUID);
					$list['statement']="Assign Mentor";
					$this->load->view('admin/assignmentor',$list);
				}else
					{
						$list['details']=$this->model->getmentee($menteeUID);
						$list['statement']="Change Mentor";
						$this->load->view('admin/assignmentor',$list);
					}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	//get mentor for autocomplete (only level 3 and above)
	public function getMentor()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$keyword=$this->input->get("q");
			$json = [];
			if(!empty($this->input->get("q"))){
				
	             $this->db->where("(mentorFname LIKE '%".$keyword."%' OR mentorLname LIKE '%".$keyword."%' OR mentorOtherNames LIKE '%".$keyword."%')", NULL, FALSE);
	             $this->db->where('mentorLevel !=', 1);
	             $this->db->where('mentorLevel !=', 2);
				$query = $this->db->select('mentorAutoId as id,CONCAT(mentorFname," ",mentorLname) as text')
							->limit(10)
							->get("mentors");
				$json = $query->result();
			}
			echo json_encode($json);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
		
	}
	//get mentee for autocomplete
	public function getmentee()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$keyword=$this->input->get("q");
			$json = [];
			if(!empty($this->input->get("q"))){
				$this->db->or_like(array('menteeFname' => $keyword, 'menteeLname' => $keyword, 'menteeOtherNames' => $keyword));
				$query = $this->db->select('menteeAutoId as id,CONCAT(menteeFname," ",menteeLname) as text')
							->limit(10)
							->get("mentees");
				$json = $query->result();
			}
			echo json_encode($json);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
		
	}
	//get guardian for autocomplete
	public function getguardian()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$keyword=$this->input->get("q");
			$json = [];
			if(!empty($this->input->get("q"))){
				$this->db->or_like(array('guardianFname' => $keyword, 'guardianLname' => $keyword, 'guardianOtherNames' => $keyword));
				$query = $this->db->select('guardianAutoId as id,CONCAT(guardianFname," ",guardianLname) as text')
							->limit(10)
							->get("guardians");
				$json = $query->result();
			}
			echo json_encode($json);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
		
	}
	//assign mentor to mentee
	public function allocatementor()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$fullName=$this->input->post('fullName');
			$menteeUID=$this->input->post('menteeUID');
			$mentorId=$this->input->post('mentorId');

			//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];

			$details=array('menteeMentorId'=>$mentorId,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);
			$result=$this->model->assignMentor($menteeUID,$details);
			if($result)
				{
					$status=$this->db->query("SELECT mentees.menteeMentorId FROM mentees WHERE mentees.menteeAutoId='$menteeUID' LIMIT 1")->row();//check if mentee assigned or removed
					$mentorAssignState=$status->menteeMentorId;//if this has a value, then a mentor was assigned else, a mentor was removed

					$message="";//display message
					if($mentorAssignState !="")
					{
						$message="Mentor assigned to ".$fullName;
					}else{
						$message="Mentor for ".$fullName." was removed";
					}
					$_SESSION['msg'] = array('error' => "",'success' =>$message);
					
		           redirect(base_url(('admin/mentees')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Mentor for ".$fullName." was not changed.",'success' => "");
						
		               redirect(base_url(('admin/mentees')));
					}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }

	}
	//Load Guardian Assignment Page
	public function assignparent()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$menteeUID=$this->input->post('menteeUID');

			//check if mentor assigned to mentee
			$this->db->select('*');
		    $this->db->from('mentees');
		    $this->db->where('menteeGuardianId',NULL);
		    $this->db->where('menteeAutoId',$menteeUID);
		    $result=$this->db->get();
		    $num=$result->num_rows(); 
		    if($num>0)
		        {
					$list['details']=$this->model->getmentee($menteeUID);
					$list['statement']="Assign Parent/Guardian";
					$this->load->view('admin/assignguardian',$list);
				}else
					{
						$list['details']=$this->model->getmentee($menteeUID);
						$list['statement']="Change Parent/Guardian";
						$this->load->view('admin/assignguardian',$list);
					}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//save new guardian/parent
	public function assignguardian()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$fullName=$this->input->post('fullName');
			$menteeUID=$this->input->post('menteeUID');
			$guardianId=$this->input->post('guardianId');

			//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];


			$details=array('menteeGuardianId'=>$guardianId,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);
			$result=$this->model->assignGuardian($menteeUID,$details);
			if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Parent/Guardian assigned to ".$fullName);
					
		           redirect(base_url(('admin/mentees')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Parent/Guardian for ".$fullName." was not changed.",'success' => "");
						
		               redirect(base_url(('admin/mentees')));
					}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//Mentor Registration
	public function  newintern()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$firstName=$this->input->post('firstName');
			$lastName=$this->input->post('lastName');
			$otherNames=$this->input->post('otherNames');
			$gender=$this->input->post('gender');
			$dob=$this->input->post('dob');
			$studentId=$this->input->post('studentId');
			$yearOfStudy=$this->input->post('yearOfStudy');
			$emailAddress=$this->input->post('emailAddress');
			$institutionId=$this->input->post('institutionId');
			$courseId=$this->input->post('courseId');
			$phoneNumber=$this->input->post('phoneNumber');
			$startDate=$this->input->post('startDate');
			$expectedEndDate=$this->input->post('expectedEndDate');

			$internPassword=md5($phoneNumber);

			$internDateAdded=date("Y-m-d H:i:s");

			//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];

			$config['upload_path']          = 'uploads/profile_photos/interns';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		    // $config['max_size']             = 100;
		    $config['max_width']            = "";
		    $config['max_height']           = "";
		    $config['min_width']            = "";
		    $config['min_height']           = "";
		    $config['file_name']			=md5(time().$dob.$firstName.$lastName);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

		    $this->load->library('upload', $config);
		   	//if no photo uploaded
		  	if (empty($_FILES['photo']['name'])) 
		  	{
		  		$internInfo=array('internFname'=>$firstName,'internLname'=>$lastName, 'internOtherNames'=>$otherNames,'internGender'=>$gender,'internPhone'=>$phoneNumber,'internEmail'=>$emailAddress,'internInstitutionId'=>$institutionId,'internCourseId'=>$courseId, 'internDoB'=>$dob,'internStudId'=>$studentId,'internPassword'=>$internPassword,'internYoS'=>$yearOfStudy, 'internDateAdded'=>$internDateAdded,'internStartDate'=>$startDate,'internDateExpectedOut'=>$expectedEndDate,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
				$result=$this->model->registerIntern($internInfo);
				if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "New intern added");
						
			           redirect(base_url(('admin/interns')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "Failed to add intern",'success' => "");
							
			               redirect(base_url(('admin/interns')));
						}
			}else{

				    if(!$this->upload->do_upload('photo'))
				    {   
				        $_SESSION['msg'] = array('error' => $this->upload->display_errors(),'success' => "");
						
				       	redirect(base_url(('admin/interns')));
				    }
				    else
					    {
					       $data =$this->upload->data();
					    	//details of uploaded file
					       $internInfo=array('internFname'=>$firstName,'internLname'=>$lastName, 'internOtherNames'=>$otherNames,'internGender'=>$gender,'internPhone'=>$phoneNumber,'internEmail'=>$emailAddress,'internInstitutionId'=>$institutionId,'internCourseId'=>$courseId, 'internDoB'=>$dob,'internStudId'=>$studentId,'internPassword'=>$internPassword,'internYoS'=>$yearOfStudy, 'internDateAdded'=>$internDateAdded,'internProfilePhoto'=>$config['file_name'].$data['file_ext'],'internStartDate'=>$startDate,'internDateExpectedOut'=>$expectedEndDate,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
							$result=$this->model->registerIntern($internInfo);
							if($result)
								{
									$_SESSION['msg'] = array('error' => "",'success' => "New intern added");
									
						           redirect(base_url(('admin/interns')));
								}else 
									{
										$_SESSION['msg'] = array('error' => "Failed to add intern",'success' => "");
										
						               redirect(base_url(('admin/interns')));
									}
					    }
				}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//Intern edit view page
	public function editintern()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$internUID=$this->input->post('internUID',TRUE);
			$list['intern_profile']=$this->model->getIntern($internUID);
			$list['courses']=$this->model->courses();
			$list['interninst']=$this->model->institutions();
			$this->load->view('admin/edit_intern',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//update intern information
	public function  updateintern()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$internUID=$this->input->post('internUID',TRUE);

			//get initial file name..this file should be deleted and a new one inserted on update
			$initFile=$this->input->post('initFile',TRUE);

			$firstName=$this->input->post('firstName');
			$lastName=$this->input->post('lastName');
			$otherNames=$this->input->post('otherNames');
			$gender=$this->input->post('gender');
			$dob=$this->input->post('dob');
			$studentId=$this->input->post('studentId');
			$yearOfStudy=$this->input->post('yearOfStudy');
			$emailAddress=$this->input->post('emailAddress');
			$institutionId=$this->input->post('institutionId');
			$courseId=$this->input->post('courseId');
			$startDate=$this->input->post('startDate');
			$expectedEndDate=$this->input->post('expectedEndDate');
			$phoneNumber=$this->input->post('phoneNumber');

			//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];

			$config['upload_path']          = 'uploads/profile_photos/interns';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		    // $config['max_size']             = 100;
		    $config['max_width']            = "";
		    $config['max_height']           = "";
		    $config['min_width']            = "";
		    $config['min_height']           = "";
		    $config['file_name']			=md5(time().$dob.$firstName.$lastName);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

		    $this->load->library('upload', $config);


		    if (empty($_FILES['photo']['name'])) {
		    	

		    	   $internInfo=array('internFname'=>$firstName,'internLname'=>$lastName, 'internOtherNames'=>$otherNames,'internGender'=>$gender,'internPhone'=>$phoneNumber,'internEmail'=>$emailAddress,'internInstitutionId'=>$institutionId,'internCourseId'=>$courseId, 'internDoB'=>$dob,'internStudId'=>$studentId,'internYoS'=>$yearOfStudy,'internStartDate'=>$startDate,'internDateExpectedOut'=>$expectedEndDate,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

				$result=$this->model->updateInternNoPhoto($internInfo,$internUID);
				if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "Intern info updated");
						
			           redirect(base_url(('admin/interns')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "No changes made ",'success' => "");
							
			               redirect(base_url(('admin/interns')));
						}

			}else{ 
					if(!$this->upload->do_upload('photo'))
					{
						$internInfo=array('internFname'=>$firstName,'internLname'=>$lastName, 'internOtherNames'=>$otherNames,'internGender'=>$gender,'internPhone'=>$phoneNumber,'internEmail'=>$emailAddress,'internInstitutionId'=>$institutionId,'internCourseId'=>$courseId, 'internDoB'=>$dob,'internStudId'=>$studentId,'internYoS'=>$yearOfStudy,'internStartDate'=>$startDate,'internDateExpectedOut'=>$expectedEndDate,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

						$result=$this->model->updateInternNoPhoto($internInfo,$internUID);
						if($result)
							{
								$_SESSION['msg'] = array('error' => "",'success' => "<span style='color:#FFFF00'>Intern info was updated. </span> However, ".$this->upload->display_errors());
								
					           redirect(base_url(('admin/interns')));
							}else 
								{
									$_SESSION['msg'] = array('error' => "No changes made  and, ".$this->upload->display_errors(),'success' => "");
									
					               redirect(base_url(('admin/interns')));
								}
					}else{

						   	$data =$this->upload->data();//details of uploaded file

						   	$internInfo=array('internFname'=>$firstName,'internLname'=>$lastName, 'internOtherNames'=>$otherNames,'internGender'=>$gender,'internPhone'=>$phoneNumber,'internEmail'=>$emailAddress,'internInstitutionId'=>$institutionId,'internCourseId'=>$courseId, 'internDoB'=>$dob,'internStudId'=>$studentId,'internYoS'=>$yearOfStudy,'internProfilePhoto'=>$config['file_name'].$data['file_ext'],'internStartDate'=>$startDate,'internDateExpectedOut'=>$expectedEndDate,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

							$result=$this->model->updateIntern($internInfo,$internUID,$initFile);
							if($result)
								{
									$_SESSION['msg'] = array('error' => "",'success' => "Intern info updated");
									
						           redirect(base_url(('admin/interns')));
								}else 
									{
										$_SESSION['msg'] = array('error' => "Failed to update ",'success' => "");
										
						               redirect(base_url(('admin/interns')));
									}


					}

			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
		
	}
	public function delintern()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$internUID=$this->input->post('internUID',TRUE);
			$internName=$this->input->post('internName',TRUE);
			if(isset($internUID) && $internUID !="")
			{
				$_SESSION['msg'] = 

					array('error' => '<span class="fa fa-warning"> Delete everything about <b>'. $internName.'?</b></span> 

							<form style="display:inline;" name="formDel_'.$internUID. '" method="post" action="'.base_url('admin/delinternconfirm').'">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                 <label for="internUID" class="control-label">intern UID*</label>
                                <input class="form-control" name="internUID" id="internUID" value="'.$internUID.'">
                            </div>
                            <button class="btn btn-success btn-s" data-placement="top" data-toggle="tooltip" data-title="Delete intern" title="Delete intern" id="del_'. $internUID. '" name="del_'. $internUID.'"  type="submit" >Confirm and Delete</button>
                        </form>
                        ','success' => "");
		            redirect(base_url(('admin/interns')));

			}else{
					$_SESSION['msg'] = array('error' => "Select intern to delete ",'success' => "");
		            redirect(base_url(('admin/interns')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	
	public function delinternconfirm()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$internUID=$this->input->post('internUID',TRUE);
			$internName=$this->input->post('internName',TRUE);

			if(isset($internUID) && $internUID !="")
			{
				$result= $this->model->deleteIntern($internUID);
				if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Intern records deleted");
					
		           redirect(base_url(('admin/interns')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Could not delete intern records",'success' => "");
						
		               redirect(base_url(('admin/interns')));
					}
				
		            redirect(base_url(('admin/interns')));

			}else{
					$_SESSION['msg'] = array('error' => "Select intern to delete ",'success' => "");
		            redirect(base_url(('admin/interns')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	public function disableintern()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$internUID=$this->input->post('internUID',TRUE);
			$internName=$this->input->post('internName',TRUE);

			if(isset($internUID) && $internUID !="")
			{
				$_SESSION['msg'] = 

					array('error' => '<span class="fa fa-warning"> Disable <b>'. $internName.'?</b></span> 

							<form style="display:inline;" name="formDisable_'.$internUID. '" method="post" action="'.base_url('admin/disableinternconfirm').'">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                 <label for="internUID" class="control-label">intern UID*</label>
                                <input class="form-control" name="internUID" id="internUID" value="'.$internUID.'">
                            </div>
                            <button class="btn btn-success btn-s" data-placement="top" data-toggle="tooltip" title="Disable intern" id="disable_'. $internUID. '" name="disable_'. $internUID.'"  type="submit" >Confirm and Disable</button>
                        </form>
                        ','success' => "");
		            redirect(base_url(('admin/interns')));

			}else{
					$_SESSION['msg'] = array('error' => "Select intern to disable ",'success' => "");
		            redirect(base_url(('admin/interns')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	public function disableinternconfirm()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$internUID=$this->input->post('internUID',TRUE);
			$internName=$this->input->post('internName',TRUE);

			if(isset($internUID) && $internUID !="")
			{
				$disableInfo=array('internActiveStatus'=>0);
				$result= $this->model->disableIntern($internUID,$disableInfo);
				if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "intern disabled");
					
		           redirect(base_url(('admin/interns')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Could not disable intern",'success' => "");
						
		               redirect(base_url(('admin/interns')));
					}
				
		            redirect(base_url(('admin/interns')));

			}else{
					$_SESSION['msg'] = array('error' => "Select intern to disable ",'success' => "");
		            redirect(base_url(('admin/interns')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//Intern profile
	public function internprofile()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$internUID=$this->input->post('internUID',TRUE);
			$list['intern_profile']=$this->model->getIntern($internUID);
			$this->load->view('admin/intern_profile',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	//all active interns
	public function interns()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['interns']=$this->model->activeInterns();
			$list['courses']=$this->model->courses();
			$list['interninst']=$this->model->institutions();
			$this->load->view('admin/interns',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//all active teachers
	public function teachers()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['teacherinst']=$this->model->schools();
			$list['teachers']=$this->model->activeTeachers();
			$this->load->view('admin/teachers',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//teacher profile
	public function teacherprofile()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$teacherUID=$this->input->post('teacherUID',TRUE);
			$list['teacher_profile']=$this->model->getTeacher($teacherUID);
			$this->load->view('admin/teacher_profile',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//teacher edit view page
	public function editteacher()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$teacherUID=$this->input->post('teacherUID',TRUE);
			$list['teacher_profile']=$this->model->getTeacher($teacherUID);
			$list['teacherinst']=$this->model->schools();
			$this->load->view('admin/edit_teacher',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	
	//register teacher
	public function  newteacher()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {

			$firstName=$this->input->post('firstName',TRUE);
			$lastName=$this->input->post('lastName',TRUE);
			$otherNames=$this->input->post('otherNames',TRUE);
			$gender=$this->input->post('gender');
			$emailAddress=$this->input->post('emailAddress',TRUE);
			$schoolId=$this->input->post('schoolId');
			$phoneNumber=$this->input->post('phoneNumber',TRUE);
			$nationalId=$this->input->post('nationalId',TRUE);
			$startDate=$this->input->post('startDate',TRUE);

			$teacherPassword=md5($phoneNumber);

		    $teacherDateAdded = date("Y-m-d H:i:s"); 

		    //record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];


			$config['upload_path']          = 'uploads/profile_photos/teachers';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		     // $config['max_size']             = 100;
		    $config['max_width']            = "";
		    $config['max_height']           = "";
		    $config['min_width']            = "";
		    $config['min_height']           = "";
		    $config['file_name']			=md5(time().$nationalId.$phoneNumber);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

		    $this->load->library('upload', $config);

		    if (empty($_FILES['photo']['name'])) 
		  	{
		  		$teacherInfo=array('teacherFname'=>$firstName,'teacherLname'=>$lastName, 'teacherOtherNames'=>$otherNames,'teacherGender'=>$gender,'teacherEmail'=>$emailAddress,'teacherSchoolId'=>$schoolId,'teacherId'=>$nationalId,'teacherPhone'=>$phoneNumber,'teacherPassword'=>$teacherPassword,'teacherDateAdded'=>$teacherDateAdded,'teacherStartDate'=>$startDate,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);

		    	$result=$this->model->registerTeacher($teacherInfo);
				if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "New teacher added");
						
			           redirect(base_url(('admin/teachers')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "Failed to add teacher",'success' => "");
							
			               redirect(base_url(('admin/teachers')));
						}
			}else{

				    if (!$this->upload->do_upload('photo'))
				    {   
				        $_SESSION['msg'] = array('error' => $this->upload->display_errors(),'success' => "");
						
				       	redirect(base_url(('admin/teachers')));
				    }
				    else
				    {
				       $data =$this->upload->data();
				    	//details of uploaded file

				    	$teacherInfo=array('teacherFname'=>$firstName,'teacherLname'=>$lastName, 'teacherOtherNames'=>$otherNames,'teacherGender'=>$gender,'teacherEmail'=>$emailAddress,'teacherSchoolId'=>$schoolId,'teacherId'=>$nationalId,'teacherPhone'=>$phoneNumber,'teacherPassword'=>$teacherPassword,'teacherProfilePhoto'=>$config['file_name'].$data['file_ext'],'teacherDateAdded'=>$teacherDateAdded,'teacherStartDate'=>$startDate,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);

				    	$result=$this->model->registerTeacher($teacherInfo);
						if($result)
							{
								$_SESSION['msg'] = array('error' => "",'success' => "New teacher added");
								
					           redirect(base_url(('admin/teachers')));
							}else 
								{
									$_SESSION['msg'] = array('error' => "Failed to add teacher",'success' => "");
									
					               redirect(base_url(('admin/teachers')));
								}
				    }
				}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//update teacher information
	public function  updateteacher()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$teacherUID=$this->input->post('teacherUID',TRUE);

			//get initial file name..this file should be deleted and a new one inserted on update
			$initFile=$this->input->post('initFile',TRUE);

			$firstName=$this->input->post('firstName',TRUE);
			$lastName=$this->input->post('lastName',TRUE);
			$otherNames=$this->input->post('otherNames',TRUE);
			$gender=$this->input->post('gender');
			$emailAddress=$this->input->post('emailAddress',TRUE);
			$schoolId=$this->input->post('schoolId');
			$phoneNumber=$this->input->post('phoneNumber',TRUE);
			$nationalId=$this->input->post('nationalId',TRUE);
			$startDate=$this->input->post('startDate',TRUE);

			//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];

			$config['upload_path']          = 'uploads/profile_photos/teachers';
		    $config['allowed_types']='jpg|png|jpeg|JPG|PNG|JPEG';
		    // $config['max_size']             = 100;
		    // $config['max_width']            = 1024;
		    // $config['max_height']           = 768;
		    $config['max_width']            = "";
		    $config['max_height']           = "";
		    $config['min_width']            = "";
		    $config['min_height']           = "";
		    $config['file_name']			=md5(time().$nationalId.$phoneNumber);
		    $config['overwrite']           = true;
		    $config['file_ext_tolower']     = true;

		    $this->load->library('upload', $config);


		    if (empty($_FILES['photo']['name'])) {
		    	$teacherInfo=array('teacherFname'=>$firstName,'teacherLname'=>$lastName, 'teacherOtherNames'=>$otherNames,'teacherGender'=>$gender,'teacherEmail'=>$emailAddress,'teacherSchoolId'=>$schoolId,'teacherId'=>$nationalId,'teacherPhone'=>$phoneNumber,'teacherStartDate'=>$startDate,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

				$result=$this->model->updateTeacherNoPhoto($teacherInfo,$teacherUID);
				if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "Teacher info updated");
						
			           redirect(base_url(('admin/teachers')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "No changes made ",'success' => "");
							
			               redirect(base_url(('admin/teachers')));
						}

			}else{ 

					if(!$this->upload->do_upload('photo'))
					{
						
						$teacherInfo=array('teacherFname'=>$firstName,'teacherLname'=>$lastName, 'teacherOtherNames'=>$otherNames,'teacherGender'=>$gender,'teacherEmail'=>$emailAddress,'teacherSchoolId'=>$schoolId,'teacherId'=>$nationalId,'teacherPhone'=>$phoneNumber,'teacherStartDate'=>$startDate,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

							$result=$this->model->updateTeacherNoPhoto($teacherInfo,$teacherUID);
						if($result)
							{
								$_SESSION['msg'] = array('error' => "",'success' => "<span style='color:#FFFF00'>Teacher info was updated. </span> However, ".$this->upload->display_errors());
								
					           redirect(base_url(('admin/teachers')));
							}else 
								{
									$_SESSION['msg'] = array('error' => "No changes made  and, ".$this->upload->display_errors(),'success' => "");
									
					               redirect(base_url(('admin/teachers')));
								}
					}else{

						   	$data =$this->upload->data();//details of uploaded file

							$teacherInfo=array('teacherFname'=>$firstName,'teacherLname'=>$lastName, 'teacherOtherNames'=>$otherNames,'teacherGender'=>$gender,'teacherEmail'=>$emailAddress,'teacherSchoolId'=>$schoolId,'teacherId'=>$nationalId,'teacherPhone'=>$phoneNumber,'teacherProfilePhoto'=>$config['file_name'].$data['file_ext'],'teacherStartDate'=>$startDate,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

							//new session of teacher profile photo
		                    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array("teacherProfilePhoto"=>$config['file_name'].$data['file_ext']));

							$result=$this->model->updateTeacher($teacherInfo,$teacherUID,$initFile);
							if($result)
								{
									$_SESSION['msg'] = array('error' => "",'success' => "Teacher info updated");
									
						           redirect(base_url(('admin/teachers')));
								}else 
									{
										$_SESSION['msg'] = array('error' => "Failed to update ",'success' => "");
										
						               redirect(base_url(('admin/teachers')));
									}


					}

			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
		
	}
	public function delteacher()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$teacherUID=$this->input->post('teacherUID',TRUE);
			$teacherName=$this->input->post('teacherName',TRUE);

			if(isset($teacherUID) && $teacherUID !="")
			{
				$_SESSION['msg'] = 

					array('error' => '<span class="fa fa-warning"> Delete everything about <b>'. $teacherName.'?</b></span> 

							<form style="display:inline;" name="formDel_'.$teacherUID. '" method="post" action="'.base_url('admin/delteacherconfirm').'">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                 <label for="teacherUID" class="control-label">teacher UID*</label>
                                <input class="form-control" name="teacherUID" id="teacherUID" value="'.$teacherUID.'">
                            </div>
                            <button class="btn btn-success btn-s" data-placement="top" data-toggle="tooltip" data-title="Delete teacher" title="Delete teacher" id="del_'. $teacherUID. '" name="del_'. $teacherUID.'"  type="submit" >Confirm and Delete</button>
                        </form>
                        ','success' => "");
		            redirect(base_url(('admin/teachers')));

			}else{
					$_SESSION['msg'] = array('error' => "Select teacher to delete ",'success' => "");
		            redirect(base_url(('admin/teachers')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	
	public function delteacherconfirm()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$teacherUID=$this->input->post('teacherUID',TRUE);
			$teacherName=$this->input->post('teacherName',TRUE);

			if(isset($teacherUID) && $teacherUID !="")
			{
				$result= $this->model->deleteTeacher($teacherUID);
				if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Teacher records deleted");
					
		           redirect(base_url(('admin/teachers')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Could not delete teacher records",'success' => "");
						
		               redirect(base_url(('admin/teachers')));
					}
				
		            redirect(base_url(('admin/teachers')));

			}else{
					$_SESSION['msg'] = array('error' => "Select teacher to delete ",'success' => "");
		            redirect(base_url(('admin/teachers')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	public function disableteacher()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$teacherUID=$this->input->post('teacherUID',TRUE);
			$teacherName=$this->input->post('teacherName',TRUE);

			if(isset($teacherUID) && $teacherUID !="")
			{
				$_SESSION['msg'] = 

					array('error' => '<span class="fa fa-warning"> Disable <b>'. $teacherName.'?</b></span> 

							<form style="display:inline;" name="formDisable_'.$teacherUID. '" method="post" action="'.base_url('admin/disableteacherconfirm').'">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                 <label for="teacherUID" class="control-label">teacher UID*</label>
                                <input class="form-control" name="teacherUID" id="teacherUID" value="'.$teacherUID.'">
                            </div>
                            <button class="btn btn-success btn-s" data-placement="top" data-toggle="tooltip" title="Disable teacher" id="disable_'. $teacherUID. '" name="disable_'. $teacherUID.'"  type="submit" >Confirm and Disable</button>
                        </form>
                        ','success' => "");
		            redirect(base_url(('admin/teachers')));

			}else{
					$_SESSION['msg'] = array('error' => "Select teacher to disable ",'success' => "");
		            redirect(base_url(('admin/teachers')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	public function disableteacherconfirm()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$teacherUID=$this->input->post('teacherUID',TRUE);
			$teacherName=$this->input->post('teacherName',TRUE);

			if(isset($teacherUID) && $teacherUID !="")
			{
				$disableInfo=array('teacherActiveStatus'=>0);
				$result= $this->model->disableTeacher($teacherUID,$disableInfo);
				if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Teacher disabled");
					
		           redirect(base_url(('admin/teachers')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Could not disable teacher",'success' => "");
						
		               redirect(base_url(('admin/teachers')));
					}
				
		            redirect(base_url(('admin/teachers')));

			}else{
					$_SESSION['msg'] = array('error' => "Select teacher to disable ",'success' => "");
		            redirect(base_url(('admin/teachers')));
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//all active parents/guardians
	public function guardians()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			// $list['categories']=$this->model->parentCategories();
			$list['guardians']=$this->model->activeGuardians();
			$this->load->view('admin/guardians',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//all mentor institutions
	public function institutions()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
		    $list['mentorinst']=$this->model->institutions();
			$list['courses']=$this->model->courses();
			$this->load->view('admin/institutions',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	//all mentee/teacher institutions
	public function schools()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['schools']=$this->model->schools();	
		    $list['schoolscategories']=$this->model->schoolCategories();
			$this->load->view('admin/schools',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//returns information related to a particular school
	public function schoolprofile()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	$schoolUID=$this->input->post('schoolUID',TRUE);
        	$schoolName=$this->input->post('schoolName',TRUE);
        	if(isset($schoolUID)&& strlen($schoolUID))
        	{
        		$_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array('schoolId' =>$schoolUID,'schoolName' =>$schoolName));
				$list['mentees']=$this->model->activeMenteesPerSchool($schoolUID);
				$list['generalexams']=$this->model->generalExams();
				$list['macheoexams']=$this->model->macheoExams();
				$list['forms']=$this->model->getAllForms();



				$list['menteeattendance']=$this->model->menteeAttendanceGroupedPerDatePerSchool($schoolUID);

				foreach($list['forms'] as $key1=>$frm)
				{
					$formCode=$frm['formCode'];

					$list['performattendancedates']=$this->model->menteeAttendanceGroupedPerDatePerForm($formCode);
					

					foreach($list['performattendancedates'] as $key2 => $attendancedate)
					{
						$attendanceDate=$attendancedate['attendanceDate'];

						$list['forms'][$key1]['performattendancedates'][$key2]['attendanceDate']=$attendanceDate;

						$list['forms'][$key1]['performattendancedates'][$key2]['countAll'] =$this->model->countOfMenteesMarkedPresentAbsentOrExcusedPerFormPerSchool($attendanceDate,$formCode,$schoolUID);
						$list['forms'][$key1]['performattendancedates'][$key2]['countPresent'] =$this->model->countOfMenteesPresentPerFormPerDatePerSchool($attendanceDate,$formCode,$schoolUID);
						$list['forms'][$key1]['performattendancedates'][$key2]['countAbsent'] =$this->model->countOfMenteesAbsentPerFormPerDatePerSchool($attendanceDate,$formCode,$schoolUID);
						$list['forms'][$key1]['performattendancedates'][$key2]['countExcused'] =$this->model->countOfMenteesExcusedPerFormPerDatePerSchool($attendanceDate,$formCode,$schoolUID);
					}
				}

				$list['menteeattendanceperform']=$this->model->menteeAttendanceGroupedPerDate();
				foreach($list['menteeattendance'] as $id => $attendance)
				{
					$attendanceDate=$attendance['attendanceDate'];

					$list['menteeattendance'][$id]['countAll'] =$this->model->countOfMenteesMarkedPresentAbsentOrExcusedPerSchool($attendanceDate,$schoolUID);//all

					$list['menteeattendance'][$id]['countPresent'] =$this->model->countOfMenteesPresentPerSchool($attendanceDate,$schoolUID);

					$list['menteeattendance'][$id]['countAbsent'] =$this->model->countOfMenteesAbsentPerSchool($attendanceDate,$schoolUID);
					$list['menteeattendance'][$id]['countExcused'] =$this->model->countOfMenteesExcusedPerSchool($attendanceDate,$schoolUID);

				}


				$this->load->view('admin/schoolprofile',$list);
        	}else{
        			$schoolUID=$_SESSION['sessdata']['schoolId'];
					$list['mentees']=$this->model->activeMenteesPerSchool($schoolUID);
					$list['generalexams']=$this->model->generalExams();
					$list['macheoexams']=$this->model->macheoExams();
					$list['forms']=$this->model->getAllForms();



				$list['menteeattendance']=$this->model->menteeAttendanceGroupedPerDate();

				foreach($list['forms'] as $key1=>$frm)
				{
					$formCode=$frm['formCode'];

					$list['performattendancedates']=$this->model->menteeAttendanceGroupedPerDatePerForm($formCode);
					

					foreach($list['performattendancedates'] as $key2 => $attendancedate)
					{
						$attendanceDate=$attendancedate['attendanceDate'];

						$list['forms'][$key1]['performattendancedates'][$key2]['attendanceDate']=$attendanceDate;

						$list['forms'][$key1]['performattendancedates'][$key2]['countAll'] =$this->model->countOfMenteesMarkedPresentAbsentOrExcusedPerFormPerSchool($attendanceDate,$formCode,$schoolUID);
						$list['forms'][$key1]['performattendancedates'][$key2]['countPresent'] =$this->model->countOfMenteesPresentPerFormPerDatePerSchool($attendanceDate,$formCode,$schoolUID);
						$list['forms'][$key1]['performattendancedates'][$key2]['countAbsent'] =$this->model->countOfMenteesAbsentPerFormPerDatePerSchool($attendanceDate,$formCode,$schoolUID);
						$list['forms'][$key1]['performattendancedates'][$key2]['countExcused'] =$this->model->countOfMenteesExcusedPerFormPerDatePerSchool($attendanceDate,$formCode,$schoolUID);

					}
				}

				$list['menteeattendanceperform']=$this->model->menteeAttendanceGroupedPerDate();
				foreach($list['menteeattendance'] as $id => $attendance)
				{
					$attendanceDate=$attendance['attendanceDate'];

					$list['menteeattendance'][$id]['countAll'] =$this->model->countOfMenteesMarkedPresentAbsentOrExcused($attendanceDate);//all

					$list['menteeattendance'][$id]['countPresent'] =$this->model->countOfMenteesPresent($attendanceDate);

					$list['menteeattendance'][$id]['countAbsent'] =$this->model->countOfMenteesAbsent($attendanceDate);
					$list['menteeattendance'][$id]['countExcused'] =$this->model->countOfMenteesExcused($attendanceDate);

				}


					$this->load->view('admin/schoolprofile',$list);
        	}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	//all mentor courses
	public function courses()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['courses']=$this->model->courses();
			$list['mentorinst']=$this->model->institutions();
			$this->load->view('admin/courses',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	//all general subjects
	public function subjects()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['subjects']=$this->model->generalSubjects();
			$this->load->view('admin/generalsubjects',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	//all macheo subjects
	public function macheosubjects()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['subjects']=$this->model->macheoSubjects();
			$this->load->view('admin/macheosubjects',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	
	//page to add mentees to forms
	public function assignForm()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$this->load->view('admin/assignmenteeform');
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//f2 mentee general subjects selection page
	public function f2subselect()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['mentees']=$this->model->activeGeneralF2UnassignedMentees();
			$list['subjects']=$this->model->selectSubjects();
			$list['form']="Form 2";
			$this->load->view('admin/generalsubjectselection',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//f3 mentee general subjects selection page
	public function f3subselect()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['mentees']=$this->model->activeGeneralF3UnassignedMentees();
			$list['subjects']=$this->model->selectSubjects();
			$list['form']="Form 3";
			$this->load->view('admin/generalsubjectselection',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//f2 mentee general subjects selection page
	public function f4subselect()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['mentees']=$this->model->activeGeneralF4UnassignedMentees();
			$list['subjects']=$this->model->selectSubjects();
			$list['form']="Form 4";
			$this->load->view('admin/generalsubjectselection',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}


//not sure this function is used!!!
	public function gensubs()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
            $list =$this->model->activeGeneralUnassignedMentees();
			$list2 =$this->model->generalSubjects();

			$count=0;

			foreach($list2 as $sub){$count=$count+1;}

            $data = array();
            foreach ($list as $mentee) 
                {
                	$count=$count+1;
                    $fullname=$mentee['menteeFname']. " ". $mentee['menteeLname'];
                    $data['data'][] = array('menteeId' => $mentee['menteeAutoId'],'fullName'=>$fullname);

                    $data['count']=array($count);
                }



            echo json_encode($data);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	//mentee macheo subjects selection page
	public function macheosubselect()
	{

		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['mentees']=$this->model->activeMacheoUnassignedMentees();
			$list['subjects']=$this->model->selectMacheoSubjects();
			$this->load->view('admin/macheosubjectselection',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	
	//all attendance list
	public function menteeattendance()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
	        {

				$list['schools']=$this->model->schools();

	        	$list['forms']=$this->model->getAllForms();
	        	foreach($list['schools'] as $schId=>$school)
	        	{
	        		$schoolUID=$school['schoolAutoId'];
					$list['schools'][$schId]['menteeattendance']=$this->model->menteeAttendanceGroupedPerDatePerSchool($schoolUID);

					foreach($list['schools'][$schId]['menteeattendance'] as $id => $attendance)
					{
						$attendanceDate=$attendance['attendanceDate'];

						$list['schools'][$schId]['menteeattendance'][$id]['countAll'] =$this->model->countOfMenteesMarkedPresentAbsentOrExcusedPerSchool($attendanceDate,$schoolUID);//all

						$list['schools'][$schId]['menteeattendance'][$id]['countPresent'] =$this->model->countOfMenteesPresentPerSchool($attendanceDate,$schoolUID);

						$list['schools'][$schId]['menteeattendance'][$id]['countAbsent'] =$this->model->countOfMenteesAbsentPerSchool($attendanceDate,$schoolUID);
						$list['schools'][$schId]['menteeattendance'][$id]['countExcused'] =$this->model->countOfMenteesExcusedPerSchool($attendanceDate,$schoolUID);

					}

	        	}






				
				// foreach($list['forms'] as $key1=>$frm)
				// {
				// 	$formCode=$frm['formCode'];

				// 	$list['performattendancedates']=$this->model->menteeAttendanceGroupedPerDatePerForm($formCode);
					

				// 	foreach($list['performattendancedates'] as $key2 => $attendancedate)
				// 	{
				// 		$attendanceDate=$attendancedate['attendanceDate'];

				// 		$list['forms'][$key1]['performattendancedates'][$key2]['attendanceDate']=$attendanceDate;

				// 		$list['forms'][$key1]['performattendancedates'][$key2]['countAll'] =$this->model->countOfMenteesMarkedPresentAbsentOrExcusedPerFormPerSchool($attendanceDate,$formCode,$schoolUID);
				// 		$list['forms'][$key1]['performattendancedates'][$key2]['countPresent'] =$this->model->countOfMenteesPresentPerFormPerDatePerSchool($attendanceDate,$formCode,$schoolUID);
				// 		$list['forms'][$key1]['performattendancedates'][$key2]['countAbsent'] =$this->model->countOfMenteesAbsentPerFormPerDatePerSchool($attendanceDate,$formCode,$schoolUID);
				// 		$list['forms'][$key1]['performattendancedates'][$key2]['countExcused'] =$this->model->countOfMenteesExcusedPerFormPerDatePerSchool($attendanceDate,$formCode,$schoolUID);
				// 	}
				// }

				// $list['menteeattendanceperform']=$this->model->menteeAttendanceGroupedPerDate();
				// foreach($list['menteeattendance'] as $id => $attendance)
				// {
				// 	$attendanceDate=$attendance['attendanceDate'];

				// 	$list['menteeattendance'][$id]['countAll'] =$this->model->countOfMenteesMarkedPresentAbsentOrExcusedPerSchool($attendanceDate,$schoolUID);//all

				// 	$list['menteeattendance'][$id]['countPresent'] =$this->model->countOfMenteesPresentPerSchool($attendanceDate,$schoolUID);

				// 	$list['menteeattendance'][$id]['countAbsent'] =$this->model->countOfMenteesAbsentPerSchool($attendanceDate,$schoolUID);
				// 	$list['menteeattendance'][$id]['countExcused'] =$this->model->countOfMenteesExcusedPerSchool($attendanceDate,$schoolUID);

				// }

























				

				//get dates for each form
				$list['mentees']=$this->model->activeMenteesForAttendance();
				$list['menteeattendance']=$this->model->menteeAttendanceGroupedPerDate();

				foreach($list['forms'] as $key1=>$frm)
				{
					$formCode=$frm['formCode'];

					$list['performattendancedates']=$this->model->menteeAttendanceGroupedPerDatePerForm($formCode);
					

					foreach($list['performattendancedates'] as $key2 => $attendancedate)
					{
						$attendanceDate=$attendancedate['attendanceDate'];

						$list['forms'][$key1]['performattendancedates'][$key2]['attendanceDate']=$attendanceDate;

						$list['forms'][$key1]['performattendancedates'][$key2]['countAll'] =$this->model->countOfMenteesMarkedPresentAbsentOrExcusedPerForm($attendanceDate,$formCode);
						$list['forms'][$key1]['performattendancedates'][$key2]['countPresent'] =$this->model->countOfMenteesPresentPerFormPerDate($attendanceDate,$formCode);
						$list['forms'][$key1]['performattendancedates'][$key2]['countAbsent'] =$this->model->countOfMenteesAbsentPerFormPerDate($attendanceDate,$formCode);
						$list['forms'][$key1]['performattendancedates'][$key2]['countExcused'] =$this->model->countOfMenteesExcusedPerFormPerDate($attendanceDate,$formCode);

					}
				}

				$list['menteeattendanceperform']=$this->model->menteeAttendanceGroupedPerDate();
				foreach($list['menteeattendance'] as $id => $attendance)
				{
					$attendanceDate=$attendance['attendanceDate'];

					$list['menteeattendance'][$id]['countAll'] =$this->model->countOfMenteesMarkedPresentAbsentOrExcused($attendanceDate);//all

					$list['menteeattendance'][$id]['countPresent'] =$this->model->countOfMenteesPresent($attendanceDate);

					$list['menteeattendance'][$id]['countAbsent'] =$this->model->countOfMenteesAbsent($attendanceDate);
					$list['menteeattendance'][$id]['countExcused'] =$this->model->countOfMenteesExcused($attendanceDate);

				}
				// print_r($list);
				$this->load->view('admin/menteeattendance',$list);
			}else{
	                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('admin')));
	        }
	}
	public function viewmenteeattendance()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
	    {
			$formCode=$this->input->post('formCode',TRUE);
			$attendanceDate=$this->input->post('attendanceDate',TRUE);
			// $list['attendanceDate']=$attendanceDate;
			$_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array('attendanceDate' =>$attendanceDate));
			$list['mentees']=$this->model->viewMenteeAttendance($formCode,$attendanceDate);
			$this->load->view('admin/menteeattendanceview',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
	        }

	}
	//attendance for all mentees per date
	public function allmenteeattendance()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
	    {

			$attendanceDate=$this->input->post('attendanceDate',TRUE);
			if(isset ($attendanceDate) && strlen($attendanceDate)){
				
				$list['mentees']=$this->model->allMenteeAttendance($attendanceDate);


				
				$_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array('attendanceDate' =>$attendanceDate));


				$this->load->view('admin/menteeattendanceview',$list);
			}else{
					$attendanceDate=$_SESSION['sessdata']['attendanceDate'];
					$list['mentees']=$this->model->allMenteeAttendance($attendanceDate);
					$this->load->view('admin/menteeattendanceview',$list);
			}

		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
	        }

	}
	//get individual mentee attendance for update
	public function getmenteeattendance()
	{
		$recordId=$this->input->post('recordId',TRUE);

        $list =$this->model->getMenteeAttendance($recordId);

        $data = array();

        foreach ($list as $record) 
            {
            	$state=$record['attendanceStatus'];
            	if($state=="PRESENT")
            	{
            		$status='
            			<div class="row setup-content">
            				
                            <div class="col-md-12">
	                            <div class="form-group col-md-12 col-lg-12" >
	                            	<h4>Date: '.date_format(date_create($_SESSION['sessdata']['attendanceDate']),"D j<\s\up>S</\s\up> M, Y").'</h4>
	                            </div>
                            	<div class="form-group col-md-12 col-lg-12" style="display: none;">
                                    <label for="recordId" class="control-label">Record Id <span class="star">*</span></label>
                                    <input type="number" name="recordId" placeholder="" class=" form-control" id="recordId" required="required" maxlength="20" value="'.$record['attendanceAutoId'].'">
                                </div>
                            	<div class="form-group col-md-12 col-lg-12">
                                <label class="">ATTENDANCE <span class="star">*</span></label><br>
                                <label class="radio-inline ">
                                    <input type="radio" name="status" value="PRESENT" required="required" autocomplete="off" checked="checked">PRESENT
                                </label>
                                <label class="radio-inline ">
                                    <input type="radio" name="status" value="ABSENT" required autocomplete="off">ABSENT
                                </label>
                                 <label class="radio-inline ">
                                    <input type="radio" name="status" value="EXCUSED" required autocomplete="off">EXCUSED
                                </label>
                            </div>
                        </div>';
            	}else if($state=="ABSENT")
            	{
            		$status='
            			<div class="row setup-content">
                            <div class="col-md-12">
                            <div class="form-group col-md-12 col-lg-12" >
                            	<h4>Date: '.date_format(date_create($_SESSION['sessdata']['attendanceDate']),"D j<\s\up>S</\s\up> M, Y").'</h4>
                            </div>
                            <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                    <label for="recordId" class="control-label">Record Id <span class="star">*</span></label>
                                    <input type="number" name="recordId" placeholder="" class=" form-control" id="recordId" required="required" maxlength="20" value="'.$record['attendanceAutoId'].'">
                                </div>
                            	<div class="form-group col-md-12 col-lg-12">
                                <label class="">ATTENDANCE <span class="star">*</span></label><br>
                                <label class="radio-inline ">
                                    <input type="radio" name="status" value="PRESENT" required="required" autocomplete="off" >PRESENT
                                </label>
                                <label class="radio-inline ">
                                    <input type="radio" name="status" value="ABSENT" required autocomplete="off" checked="checked">ABSENT
                                </label>
                                 <label class="radio-inline ">
                                    <input type="radio" name="status" value="EXCUSED" required autocomplete="off">EXCUSED
                                </label>
                            </div>
                        </div>';
            	}else if($state=="EXCUSED")
            	{
            		$status='
            			<div class="row setup-content">
                            <div class="col-md-12">
                           	<div class="form-group col-md-12 col-lg-12" >
                            	<h4>Date: '.date_format(date_create($_SESSION['sessdata']['attendanceDate']),"D j<\s\up>S</\s\up> M, Y").'</h4>
                            </div>
                            <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                    <label for="recordId" class="control-label">Record Id <span class="star">*</span></label>
                                    <input type="number" name="recordId" placeholder="" class=" form-control" id="recordId" required="required" maxlength="20" value="'.$record['attendanceAutoId'].'">
                                </div>
                            	<div class="form-group col-md-12 col-lg-12">
                                <label class="">ATTENDANCE <span class="star">*</span></label><br>
                                <label class="radio-inline ">
                                    <input type="radio" name="status" value="PRESENT" required="required" autocomplete="off" >PRESENT
                                </label>
                                <label class="radio-inline ">
                                    <input type="radio" name="status" value="ABSENT" required autocomplete="off" >ABSENT
                                </label>
                                 <label class="radio-inline ">
                                    <input type="radio" name="status" value="EXCUSED" required autocomplete="off" checked="checked">EXCUSED
                                </label>
                            </div>
                        </div>';
            	}
               
                $fullname=$record['menteeFname']. " ". $record['menteeLname'];
                $data= array('recordId' => $record['attendanceAutoId'],'fullName'=>$fullname, 'status'=>$status);
            }

        echo json_encode($data);//data should be a plain array...
    }
    //get individual mentee attendance for delete
	public function getmenteeattendancestate()
	{
		$recordId=$this->input->post('recordId',TRUE);

        $list =$this->model->getMenteeAttendance($recordId);

        $data = array();

        foreach ($list as $record) 
            {
            	$state=$record['attendanceStatus'];
            	if($state=="PRESENT")
            	{
            		$status='
            			<div class="row setup-content">
            				
                            <div class="col-md-12">
                            <div class="form-group col-md-12 col-lg-12" >
                            	<h4>Date: '.date_format(date_create($_SESSION['sessdata']['attendanceDate']),"D j<\s\up>S</\s\up> M, Y").'</h4>
                            </div>
                            	<div class="form-group col-md-12 col-lg-12" style="display: none;">
                                    <label for="recordId" class="control-label">Record Id <span class="star">*</span></label>
                                    <input type="number" name="recordId" placeholder="" class=" form-control" id="recordId" required="required" maxlength="20" value="'.$record['attendanceAutoId'].'">
                                </div>
                            	<div class="form-group col-md-12 col-lg-12">
                                <label class="">CURRENT STATUS : <span style="font-weight:normal;">PRESENT</span></label>
                            </div>
                        </div>';
            	}else if($state=="ABSENT")
            	{
            		$status='
            			<div class="row setup-content">
                            <div class="col-md-12">
                            <div class="form-group col-md-12 col-lg-12" >
                            	<h4>Date: '.date_format(date_create($_SESSION['sessdata']['attendanceDate']),"D j<\s\up>S</\s\up> M, Y").'</h4><br>
                            </div>
                            <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                    <label for="recordId" class="control-label">Record Id <span class="star">*</span></label>
                                    <input type="number" name="recordId" placeholder="" class=" form-control" id="recordId" required="required" maxlength="20" value="'.$record['attendanceAutoId'].'">
                                </div>
                            	<div class="form-group col-md-12 col-lg-12">
                                <label class="">Attendance <span class="star">*</span></label><br>
                                <label class="radio-inline ">
                                    <input type="radio" name="status" value="PRESENT" required="required" autocomplete="off" >PRESENT
                                </label>
                                <label class="radio-inline ">
                                    <input type="radio" name="status" value="ABSENT" required autocomplete="off" checked="checked">ABSENT
                                </label>
                                 <label class="radio-inline ">
                                    <input type="radio" name="status" value="EXCUSED" required autocomplete="off">EXCUSED
                                </label>
                            </div>
                        </div>';
            	}else if($state=="EXCUSED")
            	{
            		$status='
            			<div class="row setup-content">
                            <div class="col-md-12">
                           	<div class="form-group col-md-12 col-lg-12" >
                            	<h4>Date: '.date_format(date_create($_SESSION['sessdata']['attendanceDate']),"D j<\s\up>S</\s\up> M, Y").'</h4><br>
                            </div>
                            <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                    <label for="recordId" class="control-label">Record Id <span class="star">*</span></label>
                                    <input type="number" name="recordId" placeholder="" class=" form-control" id="recordId" required="required" maxlength="20" value="'.$record['attendanceAutoId'].'">
                                </div>
                            	<div class="form-group col-md-12 col-lg-12">
                                <label class="">Attendance <span class="star">*</span></label><br>
                                <label class="radio-inline ">
                                    <input type="radio" name="status" value="PRESENT" required="required" autocomplete="off" >PRESENT
                                </label>
                                <label class="radio-inline ">
                                    <input type="radio" name="status" value="ABSENT" required autocomplete="off" >ABSENT
                                </label>
                                 <label class="radio-inline ">
                                    <input type="radio" name="status" value="EXCUSED" required autocomplete="off" checked="checked">EXCUSED
                                </label>
                            </div>
                        </div>';
            	}
               
                $fullname=$record['menteeFname']. " ". $record['menteeLname'];
                $data= array('recordId' => $record['attendanceAutoId'],'fullName'=>$fullname, 'status'=>$status);
            }

        echo json_encode($data);//data should be a plain array...
    }

    public function delmenteeattendance()
    {
    	$recordId=$this->input->post('recordId',TRUE);
    	$result=$this->model->deleteMenteeAttendanceRecord($recordId);
       if($result)
		{
			$_SESSION['msg'] = array('error' => "",'success' => "Deleted successfully");
			
           redirect(base_url(('admin/allmenteeattendance')));
		}else 
			{
				$_SESSION['msg'] = array('error' => "Could not delete",'success'=>'');
				
               redirect(base_url(('admin/allmenteeattendance')));
			}
	}
	//all active f2 mentees attendance
	public function form2attendance()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
	        {
				$list['mentees']=$this->model->activeF2MenteesForAttendance();
				$list['title']="New FORM 2 Attendance";
				
			    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array('attendanceFormFunction' =>"form2attendance"));
				$this->load->view('admin/newmenteeattendance',$list);
			}else{
	                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('admin')));
	        }
	}
	//all active f3 mentees attendance
	public function form3attendance()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
	        {
				$list['mentees']=$this->model->activeF3MenteesForAttendance();
				$list['title']="New FORM 3 Attendance";
				
			    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array('attendanceFormFunction' =>"form3attendance"));
				$this->load->view('admin/newmenteeattendance',$list);
			}else{
	                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('admin')));
	        }
	}
	//all active f4 mentees attendance
	public function form4attendance()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
	        {
				$list['mentees']=$this->model->activeF4MenteesForAttendance();
				$list['title']="New FORM 4 Attendance";
				
			    $_SESSION['sessdata']=array_merge($_SESSION['sessdata'], array('attendanceFormFunction' =>"form4attendance"));
				$this->load->view('admin/newmenteeattendance',$list);
			}else{
	                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('admin')));
	        }
	}
	//get submited mentee attendance data 
	public function newmenteeattendance()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];


            $attendanceInfo=$this->input->post('mentee[]',TRUE);
			$attendanceDate=$this->input->post('attendanceDate',TRUE);
			$eventName=$this->input->post('eventName',TRUE);
	
			if(!empty(array_filter($attendanceInfo)))
			{
				$filtered_array = array_filter($attendanceInfo, function($item){
			    return count(array_keys($item)) == 2;});

			    if(!empty(array_filter($filtered_array)) && $attendanceDate !="")
			    {
			    	//record user creating this record
		            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
		            $userId=$_SESSION['sessdata']['adminId'];

					$attendanceArray=array();
	            	foreach($filtered_array as $record ) /*loop through filtered array and get individual items */
	                {
	        			$attendanceArray[]=array('attendanceMenteeId'=>$record['menteeId'],'attendanceStatus'=>$record['status'],'attendanceDate'=>$attendanceDate,'attendance_event_name'=>$eventName,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
	                }
					$result=$this->model->menteeAttendance($attendanceArray);
	                if($result)
						{
							$_SESSION['msg'] = array('error' => "",'success' => "Attendance recorded successfully");
							
				           redirect(base_url(('admin/'.$_SESSION['sessdata']['attendanceFormFunction'])));
						}else 
							{
								$_SESSION['msg'] = array('error' => "Failed to add attendance",'success' => "");
								
				               redirect(base_url(('admin/'.$_SESSION['sessdata']['attendanceFormFunction'])));
							}
				}else{
						$_SESSION['msg'] = array('error' => "Date or attendance is missing",'success' => "");
						
				        redirect(base_url(('admin/'.$_SESSION['sessdata']['attendanceFormFunction'])));
				}
			}else{
					$_SESSION['msg'] = array('error' => "No attendance selected",'success' => "");
					
			        redirect(base_url(('admin/'.$_SESSION['sessdata']['attendanceFormFunction'])));
			}
    		
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//updating mentee attendance
	public function updatementeeattendance()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$recordId=$this->input->post('recordId',TRUE);
			$status=$this->input->post('status',TRUE);
			$attendanceInfo=array('attendanceStatus'=>$status,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

			//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];


			$result=$this->model->updateMenteeAttendance($attendanceInfo,$recordId);
			if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Update successful");
					
		           redirect(base_url(('admin/allmenteeattendance')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "No changes made",'success'=>'');
						
		               redirect(base_url(('admin/allmenteeattendance')));
					}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	//all active mentors attendance
	public function mentorattendance()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['mentors']=$this->model->activeMentorsForAttendance();
			$list['mentorattendance']=$this->model->mentorAttendanceGroupedPerDate();
			foreach($list['mentorattendance'] as $id => $attendance)//loop and create another array for subjects taken and append it to original array
			{
				$attendanceDate=$attendance['attendanceDate'];
				$countOfMentorsMarkedPresentAbsentOrExcused=$this->model->countOfMentorsMarkedPresentAbsentOrExcused($attendanceDate);

				$countOfMentorsPresent=$this->model->countOfMentorsPresent($attendanceDate);
				$countOfMentorsAbsent=$this->model->countOfMentorsAbsent($attendanceDate);
				$countOfMentorsExcused=$this->model->countOfMentorsExcused($attendanceDate);

				$list['mentorattendance'][$id]['countAll'] =$countOfMentorsMarkedPresentAbsentOrExcused;

				$list['mentorattendance'][$id]['countPresent'] =$countOfMentorsPresent;
				$list['mentorattendance'][$id]['countAbsent'] =$countOfMentorsAbsent;
				$list['mentorattendance'][$id]['countExcused'] =$countOfMentorsExcused;

			}
			// print_r($list);
			$this->load->view('admin/mentorattendance',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//get submited mentor attendance data from json 
	public function newmentorattendance()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];


            $attendanceInfo=$this->input->post('mentor[]',TRUE);
			$attendanceDate=$this->input->post('attendanceDate',TRUE);
			$eventName=$this->input->post('eventName',TRUE);
	
			if(!empty(array_filter($attendanceInfo)))
			{
				$filtered_array = array_filter($attendanceInfo, function($item){
			    return count(array_keys($item)) == 2;});

			    if(!empty(array_filter($filtered_array)) && $attendanceDate !="")
			    {
			    	//record user creating this record
		            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
		            $userId=$_SESSION['sessdata']['adminId'];

					$attendanceArray=array();
	            	foreach($filtered_array as $record ) /*loop through filtered array and get individual items */
	                {
	        			$attendanceArray[]=array('attendanceMentorId'=>$record['mentorId'],'attendanceStatus'=>$record['status'],'attendanceDate'=>$attendanceDate,'attendance_event_name'=>$eventName,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
	                }
					$result=$this->model->mentorAttendance($attendanceArray);
	                if($result)
						{
							$_SESSION['msg'] = array('error' => "",'success' => "Attendance recorded successfully");
							
				           redirect(base_url(('admin/mentorattendance')));
						}else 
							{
								$_SESSION['msg'] = array('error' => "Failed to add attendance",'success' => "");
								
				               redirect(base_url(('admin/mentorattendance')));
							}
				}else{
						$_SESSION['msg'] = array('error' => "Date or attendance is missing",'success' => "");
						
				        redirect(base_url(('admin/mentorattendance')));
				}
			}else{
					$_SESSION['msg'] = array('error' => "No attendance selected",'success' => "");
					
			        redirect(base_url(('admin/mentorattendance')));
			}
    		
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	//all active teachers attendance
	public function teacherattendance()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$list['teachers']=$this->model->activeTeachersForAttendance();
			$list['teacherattendance']=$this->model->teacherAttendanceGroupedPerDate();
			foreach($list['teacherattendance'] as $id => $attendance)//loop and create another array for subjects taken and append it to original array
			{
				$attendanceDate=$attendance['attendanceDate'];

				$countOfTeachersMarkedPresentAbsentOrExcused=$this->model->countOfTeachersMarkedPresentAbsentOrExcused($attendanceDate);

				$countOfTeachersPresent=$this->model->countOfTeachersPresent($attendanceDate);
				$countOfTeachersAbsent=$this->model->countOfTeachersAbsent($attendanceDate);
				$countOfTeachersExcused=$this->model->countOfTeachersExcused($attendanceDate);

				$list['teacherattendance'][$id]['countAll'] =$countOfTeachersMarkedPresentAbsentOrExcused;

				$list['teacherattendance'][$id]['countPresent'] =$countOfTeachersPresent;
				$list['teacherattendance'][$id]['countAbsent'] =$countOfTeachersAbsent;
				$list['teacherattendance'][$id]['countExcused'] =$countOfTeachersExcused;

			}
			// print_r($list);
			$this->load->view('admin/teacherattendance',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//get submited teacher attendance data from json 
	public function newteacherattendance()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];


            $attendanceInfo=$this->input->post('teacher[]',TRUE);
			$attendanceDate=$this->input->post('attendanceDate',TRUE);
			$eventName=$this->input->post('eventName',TRUE);
	
			if(!empty(array_filter($attendanceInfo)))
			{
				$filtered_array = array_filter($attendanceInfo, function($item){
			    return count(array_keys($item)) == 2;});

			    if(!empty(array_filter($filtered_array)) && $attendanceDate !="")
			    {
			    	//record user creating this record
		            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
		            $userId=$_SESSION['sessdata']['adminId'];

					$attendanceArray=array();
	            	foreach($filtered_array as $record ) /*loop through filtered array and get individual items */
	                {
	        			$attendanceArray[]=array('attendanceTeacherId'=>$record['teacherId'],'attendanceStatus'=>$record['status'],'attendanceDate'=>$attendanceDate,'attendance_event_name'=>$eventName,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
	                }
					$result=$this->model->teacherAttendance($attendanceArray);
	                if($result)
						{
							$_SESSION['msg'] = array('error' => "",'success' => "Attendance recorded successfully");
							
				           redirect(base_url(('admin/teacherattendance')));
						}else 
							{
								$_SESSION['msg'] = array('error' => "Failed to add attendance",'success' => "");
								
				               redirect(base_url(('admin/teacherattendance')));
							}
				}else{
						$_SESSION['msg'] = array('error' => "Date or attendance is missing",'success' => "");
						
				        redirect(base_url(('admin/teacherattendance')));
				}
			}else{
					$_SESSION['msg'] = array('error' => "No attendance selected",'success' => "");
					
			        redirect(base_url(('admin/teacherattendance')));
			}
    		
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//all active interns attendance
	public function internattendance()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
	        {
				$list['interns']=$this->model->activeInternsForAttendance();
				$list['internattendance']=$this->model->internAttendanceGroupedPerDate();
				foreach($list['internattendance'] as $id => $attendance)//loop and create another array for subjects taken and append it to original array
				{
					$attendanceDate=$attendance['attendanceDate'];

					$countOfInternsMarkedPresentAbsentOrExcused=$this->model->countOfInternsMarkedPresentAbsentOrExcused($attendanceDate);

					$countOfInternsPresent=$this->model->countOfInternsPresent($attendanceDate);
					$countOfInternsAbsent=$this->model->countOfInternsAbsent($attendanceDate);
					$countOfInternsExcused=$this->model->countOfInternsExcused($attendanceDate);

					$list['internattendance'][$id]['countAll'] =$countOfInternsMarkedPresentAbsentOrExcused;

					$list['internattendance'][$id]['countPresent'] =$countOfInternsPresent;
					$list['internattendance'][$id]['countAbsent'] =$countOfInternsAbsent;
					$list['internattendance'][$id]['countExcused'] =$countOfInternsExcused;

				}
				// print_r($list);
				$this->load->view('admin/internattendance',$list);
			}else{
	                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('admin')));
	        }
	}
	//get submited intern attendance data from json 
	public function newinternattendance()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];


            $attendanceInfo=$this->input->post('intern[]',TRUE);
			$attendanceDate=$this->input->post('attendanceDate',TRUE);
			$eventName=$this->input->post('eventName',TRUE);
	
			if(!empty(array_filter($attendanceInfo)))
			{
				$filtered_array = array_filter($attendanceInfo, function($item){
			    return count(array_keys($item)) == 2;});

			    if(!empty(array_filter($filtered_array)) && $attendanceDate !="")
			    {
			    	//record user creating this record
		            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
		            $userId=$_SESSION['sessdata']['adminId'];

					$attendanceArray=array();
	            	foreach($filtered_array as $record ) /*loop through filtered array and get individual items */
	                {
	        			$attendanceArray[]=array('attendanceInternId'=>$record['internId'],'attendanceStatus'=>$record['status'],'attendanceDate'=>$attendanceDate,'attendance_event_name'=>$eventName,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
	                }
					$result=$this->model->internAttendance($attendanceArray);
	                if($result)
						{
							$_SESSION['msg'] = array('error' => "",'success' => "Attendance recorded successfully");
							
				           redirect(base_url(('admin/internattendance')));
						}else 
							{
								$_SESSION['msg'] = array('error' => "Failed to add attendance",'success' => "");
								
				               redirect(base_url(('admin/internattendance')));
							}
				}else{
						$_SESSION['msg'] = array('error' => "Date or attendance is missing",'success' => "");
						
				        redirect(base_url(('admin/internattendance')));
				}
			}else{
					$_SESSION['msg'] = array('error' => "No attendance selected",'success' => "");
					
			        redirect(base_url(('admin/internattendance')));
			}
    		
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	//save selected general subjects
	public function selectedsubjects()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];

			$data=$this->input->post('selected[]');/*attendance is submitted as a json array*/
		    foreach($_POST as $selectedSubjects )/*loop through and create an array of the data*/ 
		    {
	        	if(is_array($selectedSubjects)) /*if is array created*/
	            {
	            	/*
	                - loop through $selectedSubjects array and get individual items
	                - Then create an array of the subjects to be inserted as a batch */
	                
	            	$selectedGeneralSubjects=array();//array to hold selected general subjects
	            	$subjectMenteeIds=array();//array to hold all menteeids
	            	foreach($selectedSubjects as $record ) /*loop through and get individual items*/
	                    {
	                    	//array of info to insert for general subjects
	            			$selectedGeneralSubjects[]=array('selectMenteeId'=>$record['selectMenteeId'],'selectSubjectId'=>$record['selectSubjectId'],'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);

	            			$subjectMenteeIds[]=array('selectMenteeId'=>$record['selectMenteeId']);
	                    }
	                
	                $groupedMenteeIds=array();
	                foreach ($subjectMenteeIds as $element) 
	                {
	                	//get uniquely the menteeIds
					    $groupedMenteeIds[$element['selectMenteeId']][] = $element;
					}
					//foreach unique menteeId, auto assign the default subjects ie english, kiswahili and math: ids 1,2,3 respectively
					foreach($groupedMenteeIds as $menteeId=>$mentee)
					{
						//add default subjects to the general subjects info for insertion array above
	            		$selectedGeneralSubjects[]=array('selectMenteeId'=>$menteeId,'selectSubjectId'=>'1','creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
	            		$selectedGeneralSubjects[]=array('selectMenteeId'=>$menteeId,'selectSubjectId'=>'2','creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
	            		$selectedGeneralSubjects[]=array('selectMenteeId'=>$menteeId,'selectSubjectId'=>'3','creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);

					}

	                $macheoSubjects=array('1','2','3','4','5','6');//macheo subjects IDs
	                // $macheoSubjects=array('ENG','KISW','MATH','BIO','PHY','CHEM'); in this order

	                $selectedmacheosubjects=array();//create array to hold subjects selected that are also in macheo subjects above
	                foreach($selectedGeneralSubjects as $record )
	                    {
	                    	$recordSubject=array($record['selectSubjectId']);

	            			foreach(array_intersect($recordSubject,$macheoSubjects ) as $array)
	            			{
	            				$selectedmacheosubjects[]=array('selectMenteeId'=>$record['selectMenteeId'],'selectSubjectId'=>$array,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
	            			}//get the selected subjects the macheo subjects, create an array
	                    }
		                // insert selected subjects into general subjects as batch in model
	                	$result=$this->model->selectedGeneralSubjects($selectedGeneralSubjects);
	                    if($result)
	                		{
	    						 //insert of the selected macheo subjects into macheo subjects as batch in model
	                			$result2=$this->model->selectedMacheoSubjects($selectedmacheosubjects);
	                			if($result2)
	                			{
	            					$message['successful'][]=  "General Subjects Successfully Added";
	                       
	                            	echo json_encode($message);//return success 
	                			}else{
	                					$message['halfsuccess'][]=  "Subjects Successfully Added but Macheo subjects not recorded Successfully";
	                           
	                                	echo json_encode($message);//return success 
	                				}
	                                
	                        }else
	                            {
	                                $message['error'][]=  "An error occurred";
	                           
	                                echo json_encode($message);//return no changes made
	                            }


				}else{
						$message['empty'][]=  "Not an array";
                        echo json_encode($message);//return no changes made
					}
			}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}

	//get list of general subjects taken by mentee
	public function generalsubslist()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$pid=$this->input->post('pid',TRUE);
			$list =$this->model->getGeneralSubjects($pid);
			$data = array();
			foreach ($list as $subject) 
			    {
			        $data[]=  $subject['subjectCode'];
			    }

			echo json_encode($data);//data should be a plain array...
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	//get macheo subjects for marks entry
	public function getgeneralsubs()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$pid=$this->input->post('pid',TRUE);
			$list =$this->model->getGeneralSubjects($pid);
			$data = array();
			foreach ($list as $subject) 
			    {
			        $data[]= '
			        <div form="form-group col-md-12 col-lg-12"><label form="control-label">'.$subject['subjectCode'].'*</label> 
			        <input type="number" name="'.$subject['subjectCode'].'" form=" form-control" required="required" min="0"  max="100" step="0.5" style="margin-bottom: 10px;float:right!important;">

			        <input type="number" name="hidden_'.$subject['subjectCode'].'" form=" form-control" required="required" min="0"  max="100" step="0.5" style="margin-bottom: 10px;display:none!important;" value="'.$subject['selectSubjectId'].'">


			        </div>';
			    }

			echo json_encode($data);//data should be a plain array...
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}


	//download admin profile photo
 	public function download_adminphoto($filename = NULL) 
    {
	    	if(!empty(array_filter($_SESSION['sessdata'])))
        {
	      // load download helder
	        $this->load->helper('download');
	        // read file contents
	        $data = file_get_contents('uploads/profile_photos/admins/'.$filename);
	        force_download($filename, $data);
	    }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
    }

	//download mentor profile photo
 	public function download_mentorphoto($filename = NULL) 
    {
    	if(!empty(array_filter($_SESSION['sessdata'])))
        {
	      // load download helder
	        $this->load->helper('download');
	        // read file contents
	        $data = file_get_contents('uploads/profile_photos/mentors/'.$filename);
	        force_download($filename, $data);
	    }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
    }
	//download mentee profile photo
	public function download_menteephoto($filename = NULL) 
    {
	    if(!empty(array_filter($_SESSION['sessdata'])))
        {
	      // load download helder
	        $this->load->helper('download');
	        // read file contents
	        $data = file_get_contents('uploads/profile_photos/mentees/'.$filename);
	        force_download($filename, $data);
	    }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
    }
	//download intern profile photo
	public function download_internphoto($filename = NULL) 
    {
    	if(!empty(array_filter($_SESSION['sessdata'])))
        {
	      // load download helder
	        $this->load->helper('download');
	        // read file contents
	        $data = file_get_contents('uploads/profile_photos/interns/'.$filename);
	        force_download($filename, $data);
	    }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
    }
	//download teacher profile photo
	public function download_teacherphoto($filename = NULL) 
    {
    	if(!empty(array_filter($_SESSION['sessdata'])))
        {
	      // load download helder
	        $this->load->helper('download');
	        // read file contents
	        $data = file_get_contents('uploads/profile_photos/teachers/'.$filename);
	        force_download($filename, $data);
	    }else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
    }
	    
	//Institution Registration
	public function newinstitutions()
    {
    	if(!empty(array_filter($_SESSION['sessdata'])))
        {
	       	$institutionName=$this->input->post('institutionName');
		   	$institutionLocation=$this->input->post('institutionLocation');
		   	$institutionAlias=$this->input->post('institutionAlias');

		   	//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];

	       	$institutionInfo=array('institutionName'=>$institutionName,'institutionLocation'=>$institutionLocation,'institutionAlias'=>$institutionAlias,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
			$result=$this->model->registerInstitution($institutionInfo);
			if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "New institution added");
					
		           redirect(base_url(('admin/institutions')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Failed to add institution",'success' => "");
						
		               redirect(base_url(('admin/institutions')));
					}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
    }
	//Institution edit view page
	public function editinstitution()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	
			$institutionUID=$this->input->post('institutionUID',TRUE);
			$list['institution_profile']=$this->model->getInstitution($institutionUID);
			$list['mentorinst']=$this->model->activeMentors();
			$this->load->view('admin/edit_institution',$list);
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
	}
	
	//update intern information
	public function  updateinstitution()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	//record user updating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];

			$institutionUID=$this->input->post('institutionUID',TRUE);
			//get initial file name..this file should be deleted and a new one inserted on update
			$initFile=$this->input->post('initFile',TRUE);
			$institutionName=$this->input->post('institutionName',TRUE);
			$institutionLocation=$this->input->post('institutionLocation',TRUE);
			$institutionAlias=$this->input->post('institutionAlias',TRUE);
				
	    	$institutionInfo=array('institutionName'=>$institutionName,'institutionLocation'=>$institutionLocation, 'institutionAlias'=>$institutionAlias,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

			$result=$this->model->updateInstitutionNoPhoto($institutionInfo,$institutionUID);
			if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Institution info updated");
					
		           redirect(base_url(('admin/institutions')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "No changes made ",'success' => "");
						
		               redirect(base_url(('admin/institutions')));
					}
				
		}else{
	            $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
	            redirect(base_url(('admin')));
	    }
						

	}
	//School registration
	public function newschools()
    {
    	if(!empty(array_filter($_SESSION['sessdata'])))
       	{
       		//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];

	        $schoolName=$this->input->post('schoolName');
	        $schoolLocation=$this->input->post('schoolLocation');
	        $schoolAlias=$this->input->post('schoolAlias');
	        $schoolCategory=$this->input->post('schoolCategory');
		        
		        
		    //details of uploaded file
	       $schoolInfo=array('schoolName'=>$schoolName,'schoolLocation'=>$schoolLocation, 'schoolAlias'=>$schoolAlias, 'schoolCategoryId'=>$schoolCategory,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
			$result=$this->model->registerSchool($schoolInfo);
			if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "New School added");
					
		           redirect(base_url(('admin/schools')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "Failed to add school",'success' => "");
						
		               redirect(base_url(('admin/schools')));
					}
		}else{
                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
                redirect(base_url(('admin')));
        }
    }
	//school edit view page
	public function editschool()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$schoolUID=$this->input->post('schoolUID',TRUE);
			$list['school_profile']=$this->model->getSchool($schoolUID);
			$list['schoolscategories']=$this->model->schoolCategories();
			$this->load->view('admin/edit_school',$list);
		}else{
	                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('admin')));
	       	 }
	}
	   //update school information
	public function  updateschool()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
        	//record user updating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];


			$schoolUID=$this->input->post('schoolUID',TRUE);

			//get initial file name..this file should be deleted and a new one inserted on update
			$initFile=$this->input->post('initFile',TRUE);

			$schoolName=$this->input->post('schoolName',TRUE);
			$schoolLocation=$this->input->post('schoolLocation',TRUE);
			$schoolAlias=$this->input->post('schoolAlias',TRUE);
		    $schoolCategory=$this->input->post('schoolCategory', TRUE);
			
		    
	    	$schoolInfo=array('schoolName'=>$schoolName,'schoolLocation'=>$schoolLocation, 'schoolAlias'=>$schoolAlias, 'schoolCategoryId'=>$schoolCategory,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

			$result=$this->model->updateSchoolNoPhoto($schoolInfo,$schoolUID);
			if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "School info updated");
					
		           redirect(base_url(('admin/schools')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "No changes made ",'success' => "");
						
		               redirect(base_url(('admin/schools')));
					}

			
			}else{
	                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('admin')));
	        }		

		}
	//courses registration
	public function newcourse()
    {
    	if(!empty(array_filter($_SESSION['sessdata'])))
       	{
       		//record user creating this record
            $userGroupId=$_SESSION['sessdata']['adminGroupId'];
            $userId=$_SESSION['sessdata']['adminId'];

	        $courseInstitutionId=$this->input->post('courseInstitutionId');
	        $courseName=$this->input->post('courseName');
	        $courseCode=$this->input->post('courseCode');
	        
			    	//details of uploaded file
		       $courseInfo=array('courseInstitutionId'=>$courseInstitutionId,'courseName'=>$courseName, 'courseCode'=>$courseCode,'creating_user_group_id'=>$userGroupId,'creating_user_id'=>$userId);
				$result=$this->model->registerCourse($courseInfo);
				if($result)
					{
						$_SESSION['msg'] = array('error' => "",'success' => "New course added");
						
			           redirect(base_url(('admin/courses')));
					}else 
						{
							$_SESSION['msg'] = array('error' => "Failed to add course",'success' => "");
							
			               redirect(base_url(('admin/courses')));
						}
		}else{
	            $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
		        
	            redirect(base_url(('admin')));
		      }
	    }
	//course edit view page
	public function editcourse()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$courseUID=$this->input->post('courseUID',TRUE);
			$list['course_profile']=$this->model->getCourse($courseUID);
			$list['mentorinst']=$this->model->institutions();
			$this->load->view('admin/edit_courses',$list);
		}else{
	                $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
			        
	                redirect(base_url(('admin')));
	    }
	}
	   //update school information
	public function  updatecourse()
	{
		if(!empty(array_filter($_SESSION['sessdata'])))
        {
			$courseUID=$this->input->post('courseUID',TRUE);

			//get initial file name..this file should be deleted and a new one inserted on update
			$initFile=$this->input->post('initFile',TRUE);

			$courseInstitutionId=$this->input->post('courseInstitutionId',TRUE);
			$courseName=$this->input->post('courseName',TRUE);
			$courseCode=$this->input->post('courseCode',TRUE);
		    	
		    
	    	$courseInfo=array('courseInstitutionId'=>$courseInstitutionId,'courseName'=>$courseName, 'courseCode'=>$courseCode,'last_update_user_group_id'=>$userGroupId,'last_update_user_id'=>$userId);

			$result=$this->model->updateCourse($courseInfo,$courseUID);
			if($result)
				{
					$_SESSION['msg'] = array('error' => "",'success' => "Course info updated");
					
		           redirect(base_url(('admin/courses')));
				}else 
					{
						$_SESSION['msg'] = array('error' => "No changes made ",'success' => "");
						
		               redirect(base_url(('admin/courses')));
					}
		}else{
	    	   $_SESSION['msg'] = array('error' => "Your session has expired. Login",'success' => "");
			   
	           redirect(base_url(('admin')));
	    }	

	}
        
}//close class
