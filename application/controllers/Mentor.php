<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @author Mokoro Stephen
*/
class Mentor extends CI_Controller
{

    function __construct()
    {
         parent::__construct();
         $this->load->model('MentorModel','model');

         //check if cas library is loaded and unload it to allow mnon su mentors to login with ci session
         if(!$this->load->is_loaded('cas'))
	        {
	             $this->load->library('session');
	        }else{
	            unset($this->_ci_classes['cas'], $this->session);
	             $this->load->library('session');
	        }
    }

    public function index()
    {
        $this->load->view('login');
    }
    public function logout()
    {
        $array_items=array('mentorName','mentorId','mentorProfilePhoto','mentor_login'); 
        $this->session->unset_userdata($array_items);
        $this->load->view('login');
    }
    public function dashboard()
    {
        if($this->session->userdata('mentor_login'))
        {
            $mentorId=$this->session->userdata('mentorId');
            $list['menteescount']=$this->model->myMenteesCount($mentorId);
            /*$list['mentorscount']=$this->model->mentorsCount();
            $list['internscount']=$this->model->internsCount();
            $list['adminscount']=$this->model->adminsCount();
            $list['teacherscount']=$this->model->teachersCount();
            $list['macheoexamscount']=$this->model->macheoExamsCount();
            $list['generalexamscount']=$this->model->generalExamsCount();*/
            $this->load->view('mentor/dashboard',$list);
        }else{
                $feedback = array('error' => "Your session has expired. Login",'success' => "");
		        $this->session->set_flashdata('msg',$feedback);
                redirect(base_url(('mentor')));
        }
    }
    public function mentees()
	{
		if($this->session->userdata('mentor_login'))
        {
			$mentorId=$this->session->userdata('mentorId');
			$list['mentees']=$this->model->mentorMentees($mentorId);
			$list['schools']=$this->model->schools();
			$this->load->view('mentor/mentees',$list);
		}else{
                $feedback = array('error' => "Your session has expired. Login",'success' => "");
		        $this->session->set_flashdata('msg',$feedback);
                redirect(base_url(('mentor')));
        }
	}
	//mentee  profile
	public function menteeprofile()
	{
		if($this->session->userdata('mentor_login'))
        {
			$menteeUID=$this->input->post('menteeUID',TRUE);
			$list['mentee_profile']=$this->model->getMentee($menteeUID);
			$this->load->view('mentor/mentee_profile',$list);
		}else{
                $feedback = array('error' => "Your session has expired. Login",'success' => "");
		        $this->session->set_flashdata('msg',$feedback);
                redirect(base_url(('mentor')));
        }
	}
	public function mentorship()
	{
		if($this->session->userdata('mentor_login'))
		{
			$mentorId=$this->session->userdata('mentorId');
			$list['mentees']=$this->model->mentorMentees($mentorId);
			foreach($list['mentees'] as $id => $mentee)//loop and create another array for subjects taken and append it to original array
			{
				$pid=$mentee['menteeAutoId'];//mentee's primary id
				$menteesession=$this->model->menteeSessions($pid);//list of selected general subjects
				$list['mentees'][$id]['sessions'] =$menteesession;//append mentee general subjects to mentee array

			}
			
			$this->load->view('mentor/mentorship',$list);
		}else{
                $feedback = array('error' => "Your session has expired. Login",'success' => "");
		        $this->session->set_flashdata('msg',$feedback);
                redirect(base_url(('mentor')));
			}		
	}
	//New mentee session registration
	public function newmenteesession()
    {
    	if($this->session->userdata('mentor_login'))
       	{
			$mentorId=$this->session->userdata('mentorId');
	        $menteeId=$this->input->post('menteeId');
	        $sessionDate=$this->input->post('sessionDate');
	        $sessionTime=$this->input->post('sessionTime');
			$sessionTopic=$this->input->post('sessionTopic');
			$sessionDescription=$this->input->post('sessionDescription');
	        
			    	//details of uploaded file
		       $menteesessionInfo=array('mentorId'=>$mentorId, 'menteeId'=>$menteeId,'sessionDate'=>$sessionDate, 'sessionTime'=>$sessionTime,'sessionTopic'=>$sessionTopic, 'sessionDescription'=>$sessionDescription);
				$result=$this->model->registerMenteeSession($menteesessionInfo);
				if($result)
					{
						$feedback = array('error' => "",'success' => "New session added");
						$this->session->set_flashdata('msg',$feedback);
			           redirect(base_url(('mentor/mentorship')));
					}else 
						{
							$feedback = array('error' => "Failed to add session",'success' => "");
							$this->session->set_flashdata('msg',$feedback);
			               redirect(base_url(('mentor/mentorship')));
						}
		}else{
	            $feedback = array('error' => "Your session has expired. Login",'success' => "");
		        $this->session->set_flashdata('msg',$feedback);
	            redirect(base_url(('mentor')));
		      }
	    }
	//attendance records
	public function attendance()
	{
		if($this->session->userdata('mentor_login'))
		{
			$mentorId=$this->session->userdata('mentorId');
			$list['mentees']=$this->model->mentorMentees($mentorId);
			foreach($list['mentees'] as $id => $mentee)
			{
				$pid=$mentee['menteeAutoId'];//mentee's primary id
				$menteeattendance=$this->model->menteeAtt($pid);
				$list['mentees'][$id]['attendance'] =$menteeattendance;//append mentee general subjects to mentee array

			}
			
			$this->load->view('mentor/attendance',$list);
		}else{
                $feedback = array('error' => "Your session has expired. Login",'success' => "");
		        $this->session->set_flashdata('msg',$feedback);
                redirect(base_url(('mentor')));
			}		
	}
//
	public function performance()
	{
		if($this->session->userdata('mentor_login'))
		{
			$mentorId=$this->session->userdata('mentorId');
			$list['mentees']=$this->model->mentorMentees($mentorId);
			foreach($list['mentees'] as $id => $mentee)
			{
				/*$form=$mentee['menteeAutoId'];*///mentee's primary id
				$menteeperformance=$this->model->exams();
				$list['mentees'][$id]['performance'] =$menteeperformance;

			}
			
			$this->load->view('mentor/performance',$list);
		}else{
                $feedback = array('error' => "Your session has expired. Login",'success' => "");
		        $this->session->set_flashdata('msg',$feedback);
                redirect(base_url(('mentor')));
			}		
	}
	
}
