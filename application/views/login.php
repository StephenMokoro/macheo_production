<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Macheo Management System</title>
    <!-- Logo -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon" />
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/bootstrap/bootstrap-4.0.0/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link href="<?php echo base_url();?>assets/css/login.css" rel="stylesheet">
</head>
  <body id="body">
    <div class="overlay"></div>
    <div class="login" id="card">
      <div class="front signin_form"> 
        <!-- <p>Login Your Account</p> -->
        <?php if(isset($_SESSION['msg']))
        {
          $msg = $_SESSION['msg'];
          $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
          <div class="messagebox alert alert-danger" style="display: block">
            <button type="button" class="close" data-dismiss="alert">*</button>
            <div class="cs-text">
                <i class="fa fa-close"></i>
                <strong><span>';echo $msg['error']; echo '</span></strong>
            </div> 
          </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
          <div class="messagebox alert alert-success" style="display: block">
            <button type="button" class="close" data-dismiss="alert">*</button>
            <div class="cs-text">
                <i class="fa fa-check-circle-o"></i>
                <strong><span>';echo $msg['success'];echo '</span></strong>
            </div> 
            </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
          <form class="login-form" method="POST" action="<?php echo base_url(); ?>auth/auth">
              <div class="form-group">
                  <div class="input-group">
                      <input type="text" class="form-control" placeholder="Username" name="username" required="required">
                  </div>
              </div>
              <div class="form-group">
                  <div class="input-group">
                      <input type="password" class="form-control" placeholder="Password" name="password" required="required">
                  </div>
              </div>
              <div class="form-group sign-btn">
                  <input type="submit" class="btn btn-submit" value="Sign In" style="width: 100%;">
              </div>
              <div class="text-left">
                  <a  href="<?php echo base_url();?>auth/cas_auth" class="btn btn-default " style=";background-color: rgba(0,46,102,.8);color: #FFFFFF; font-weight: bolder;">SU Login</a>
              </div>
          </form>
        </div>
      </div>
      <div class="container">
      <div class="row">
        <header>
          <div id="topbar" class="fixed-top  clearHeader">
              <div class="col-sm-12" >
                <div class="col-sm-12" >
                  <nav class="navbar navbar-expand-lg navbar-dark" >
                    <div style="min-height:0;transform:skewX(-8deg);transform-origin:top right;">
                      <img src="<?php echo base_url();?>assets/img/logo.png" class="mt-2" style="min-height:0;transform:skewX(8deg);transform-origin:top right">
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent" style="font-weight: bolder;">
                    <!-- <ul class="navbar-nav ml-auto mr-3">
                      <li class="nav-item active">
                      <a class="nav-link text-uppercase" href="#">Home <span class="sr-only">(current)</span></a>
                      </li>
                      <li class="nav-item">
                      <a class="nav-link text-uppercase" href="#">About</a>
                      </li>
                      <li class="nav-item">
                      <a class="nav-link text-uppercase" href="#">Blog</a>
                      </li>
                      <li class="nav-item">
                      <a class="nav-link text-uppercase" href="#">Contact us</a>
                      </li>
                      <li class="nav-item">
                      <a class="nav-link text-uppercase" href="#">Projects</a>
                      </li>
                    </ul> -->
                    </div>
                  </nav>
                </div>
              </div>
            </div>
        </header>
      </div>
    </div>
    <div class="masthead">
      <div class="masthead-bg"></div>
      <div class="container h-100" >
        <div class="row h-100">
          <div class="col-12 my-auto">
            <div class="masthead-content text-white py-5 py-md-0">
              <h2 class="mb-3">Macheo Management System</h2>
              <p class="mb-2">An Information Management System for Macheo Program - Strathmore Community Service Center -</strong> Designed and Developed by Student Researchers at Phenom Research Lab, Strathmore University</p>
                  <button class="btn btn-secondary" type="button">Visit Phenom Research Lab Website</button>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="social-icons">
      <ul class="list-unstyled text-center mb-0">
        <li class="list-unstyled-item">
          <a href="https://twitter.com/suresearchclub">
            <i class="fa fa-twitter"></i>
          </a>
        </li>
        <li class="list-unstyled-item">
          <a href="https://www.facebook.com/strathmoreresearchclub/">
            <i class="fa fa-facebook"></i>
          </a>
        </li>
        <li class="list-unstyled-item">
          <a href="#">
            <i class="fa fa-instagram"></i>
          </a>
        </li>
      </ul>
    </div>
    <div class="navbar-fixed-bottom" id="footer">
        <div class="row">
            <div class="col-md-12 widget" style="background-color: maroon; margin-left: 10px; color: white;"><span class="pull-left" > © 2018 Strathmore University</span><span class="pull-right" >Developed at Phenom Research Lab | <span style="color:white">Ingenious minds</span></span>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/bootstrap-4.0.0/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url();?>assets/vendor/vide/jquery.vide.min.js"></script>

    <!-- Custom scripts for this template -->
  <script>
  (function($) {
  "use strict"; // Start of use strict
  // Vide - Video Background Settings
  $('body').vide({
      // mp4: "mp4/bg.mp4",
      poster: "<?php echo base_url();?>assets/img/bg-mobile-fallback.jpg"
    }, {
      posterType: 'jpg'
    });
    })(jQuery); // End of use strict
    </script>
  </body>

</html>
