<!DOCTYPE html>
<html>

<head>
	<title>Macheo | Mentorship</title>
	<?php $this->load->view('headerlinks/headerlinks.php'); ?>
	<script src="<?php echo base_url();?>assets/jquery/dist/jquery.min.js"></script>
</head>

<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
	<div class="wrapper">
		<?php $this->load->view('mentor/mentornav.php'); ?>
		<!--navigation -->
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="row" style="margin-bottom: -15px;">
					<div class="col-lg-12 ">
						<h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Mentorship</h4>
						<div class="pull-right">
							<span data-placement="top" data-toggle="tooltip" title="Refresh">
                    <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>
							&nbsp;Refresh</button>
							</span>
							<span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-xs" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>
							&nbsp;Print All</a>
							</span>
						</div>
					</div>
					<!-- /.col-lg-12 -->
				</div>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-body">
								<div class="box box-solid collapsed-box" style="background:lightgrey">
									<div class="box-header">
										<h3 class="box-title" style="color: #21618C;">New Session</h3>
										<div class="box-tools pull-right">
											<button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
											<!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
										</div>
									</div>
									<div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
										<?php echo form_open_multipart('mentor/newmenteesession',array('id' => 'menteesession_registration','method'=>'post'));?>
										<div class="row setup-content">
											<div class="col-xs-12">
												<div class="col-md-6">
													<div class="form-group col-md-12 col-lg-12">
														<label for="menteeId" class="control-label"> Mentee <span class="star">*</span></label>
														<select type="text" name="menteeId" class=" form-control" id="menteeId" required="required">
															<option value="">--Select Mentee--</option>
															<?php  foreach($mentees as $mentee){ 
                                                ?>
															<option value=<?php echo '"'.$mentee[ 'menteeAutoId']. '"';?>>
																<?php  echo $mentee['menteeFname']." ".$mentee['menteeLname'];}?>
															</option>
														</select>
													</div>
													
												</div>
												<div class="col-md-6">
													<div class="form-group col-md-6 col-lg-6">
														<label for="sessionDate" class="control-label">Date <span class="star">*</span></label>
														<div class="form-group">
															<div class='input-group date' id='sessionDate'>
																<input type='text' class="form-control" readonly="true" name="sessionDate"/>
																<span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
															
																</span>
															</div>
														</div>
													</div>
													<div class="form-group col-md-6 col-lg-6">
														<label for="sessionTime" class="control-label">Time<span class="star">*</span></label>
														<input type="time" name="sessionTime" class=" form-control" id="sessionTime" required="required"/>
													</div>
												</div>
												<div class="form-group col-md-12 col-lg-12 ">
														<label for="sessionTopic" class="control-label">Topic/Activity<span class="star">*</span></label>
														<input type="text" required class="form-control" name="sessionTopic"/>
													</div>
													<div class="form-group col-md-12 col-lg-12 ">
														<label for="sessionDescription" class="control-label">Description<span class="star">*</span></label>
														<input type="text" required class="form-control" name="sessionDescription"/>
													</div>
												<div class="col-md-12">
													<div class="form-group col-md-6 col-lg-6">
														<input type="submit" class="btn btn-primary" value="Submit">
														<input type="reset" class="btn btn-default" value="Reset">
													</div>
												</div>
											</div>
											<!--/.col-xs-12-->
										</div>
										<!--/.setup-content-->
										<?php echo form_close();?>
									</div>
									<!-- /.box-body -->
								</div>
								<!-- /.box -->
								<?php $msg = $this->session->flashdata('msg');
                $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-close"></i>
                            <strong><span>';echo $msg['error']; echo '</span></strong>
                        </div> 
                </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                <div class="messagebox alert alert-success" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-check-circle-o"></i>
                            <strong><span>';echo $msg['success'];echo '</span></strong>
                        </div> 
                </div>';}?>
								<table class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="mentorshiplist">
									<thead>
										<tr style="background: #FFFFFF;color: #000000 ;">
											<th class="text-center pull-left">Sort Mentees &nbsp;&nbsp; </th>
										</tr>
									</thead>
									<tbody style="color: #17202A  ;">
										<?php  foreach($mentees as $mentee){ 
                           ?>

										<tr>
											<td>
												<div class="box box-solid collapsed-box" id="selector" style="margin-bottom: 0px!important;padding-bottom: 0px!important;background-color: #CACFD2;">
													<div class="box-header">
														<h3 class="box-title">
															<?php  echo $mentee['menteeFname']." ".$mentee['menteeLname']; ?>
														</h3>
														<div class="box-tools pull-right clicked">
															<button class="btn btn-default btn-sm " data-widget="collapse" value=<?php echo '"'. $mentee[ 'menteeAutoId']. '"'; ?> id="sessions"><i class="fa fa-plus"></i></button>
															<!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
														</div>
													</div>
													<?php $tableId="mentorship_".$mentee['menteeAutoId'];?>
													<div style="display: none;background-color: #FFFFFF;color: #000000;" class="box-body">
														<table class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="<?php echo $tableId;?>">
															<thead>
																<tr style="background: #2E4053;color: #F7F9F9  ;">
																	<th class="text-center">Date last Seen</th>
																	<th class="text-center">Time</th>
																	<th class="text-center">Topic</th>
																	<th class="text-center"><i class="fa fa-cog fa-spin"></i>
																	</th>
																</tr>
															</thead>
															<tbody style="color: #17202A;">
																<?php  foreach($mentee['sessions'] as $sess){?>
																<tr>
																	<td class="text-center">
																		<?php  echo $sess['sessionDate']; ?>
																	</td>
																	<td class="text-center">
																		<?php  echo $sess['sessionTime']; ?>
																	</td>
																	<td class="text-center">
																		<?php  echo $sess['sessionTopic']; ?>
																	</td>

																	<td class="text-center">
																		<form style="display:inline;" name=<?php echo '"formMore_'. $mentee[ 'menteeAutoId']. '"'; ?> method="post" action="
																			<?php echo base_url('mentor/mentorshipdetails');?>">
																			<div class="form-group col-md-12 col-lg-12" style="display:none">
																				<label for="menteeUID" class="control-label">Mentee UID*</label>
																				<input required="required" class="form-control" name="menteeUID" id="menteeUID" placeholder="101" value="<?php echo $mentee['menteeAutoId']; ?>">
																			</div>
																			<button class="btn btn-default btn-xs" data-placement="top" data-toggle="tooltip" data-title="View More" title="View More" id=<?php echo '"more_'. $mentee[ 'menteeAutoId']. '"'; ?> name=<?php echo '"more_'. $mentee['menteeAutoId'].'"';  ?>  type="submit" ><i class="fa fa-eye"></i></button>
																		</form>
																	</td>
																</tr> 
																<?php } ?>
															</tbody>
														</table>
														<?php  echo  "<script>
                        $(document).ready(function () { 
                             //datatable initialization
                            $('#";echo $tableId."').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 20, 50, 100, 200, -1], [10, 20, 50, 100, 200, 'All']],'aaSorting':[],
                                 'aoColumnDefs': [{ 'aTargets': [0],'bSortable':false, 'orderable': false},{'aTargets': [1], 'orderable': false}] }); 
                        });//close document.ready

                        </script>";?>
													</div>
													<!-- /.box-body -->
												</div>
												<!-- /.box -->
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>

								<!-- /.table-responsive -->
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<?php $this->load->view('footer');?>

		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
	<script>
		// Limit scope pollution from any deprecated API
		( function () {

			var matched, browser;

			// Use of jQuery.browser is frowned upon.
			// More details: http://api.jquery.com/jQuery.browser
			// jQuery.uaMatch maintained for back-compat
			jQuery.uaMatch = function ( ua ) {
				ua = ua.toLowerCase();

				var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
					/(webkit)[ \/]([\w.]+)/.exec( ua ) ||
					/(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
					/(msie) ([\w.]+)/.exec( ua ) ||
					ua.indexOf( "compatible" ) < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) || [];

				return {
					browser: match[ 1 ] || "",
					version: match[ 2 ] || "0"
				};
			};

			matched = jQuery.uaMatch( navigator.userAgent );
			browser = {};

			if ( matched.browser ) {
				browser[ matched.browser ] = true;
				browser.version = matched.version;
			}

			// Chrome is Webkit, but Webkit is also Safari.
			if ( browser.chrome ) {
				browser.webkit = true;
			} else if ( browser.webkit ) {
				browser.safari = true;
			}

			jQuery.browser = browser;

			jQuery.sub = function () {
				function jQuerySub( selector, context ) {
					return new jQuerySub.fn.init( selector, context );
				}
				jQuery.extend( true, jQuerySub, this );
				jQuerySub.superclass = this;
				jQuerySub.fn = jQuerySub.prototype = this();
				jQuerySub.fn.constructor = jQuerySub;
				jQuerySub.sub = this.sub;
				jQuerySub.fn.init = function init( selector, context ) {
					if ( context && context instanceof jQuery && !( context instanceof jQuerySub ) ) {
						context = jQuerySub( context );
					}

					return jQuery.fn.init.call( this, selector, context, rootjQuerySub );
				};
				jQuerySub.fn.init.prototype = jQuerySub.fn;
				var rootjQuerySub = jQuerySub( document );
				return jQuerySub;
			};

		} )();
	</script>
	<script>
		$( document ).ready( function () {
			//datatable initialization
			var table = $( '#mentorshiplist' ).DataTable( {
				responsive: true,
				"iDisplayLength": 10,
				"lengthMenu": [
					[ 10, 25, 50, 100, 200, -1 ],
					[ 10, 25, 50, 100, 200, "All" ]
				],
				"aaSorting": []
			} );

			var submitBtn = $( 'input[type="submit"]' );
			// allWells.show();
			submitBtn.click( function () {
				var curStep = $( this ).closest( ".setup-content" ),
					curStepBtn = curStep.attr( "id" ),
					curInputs = curStep.find( "input,select" ),
					isValid = true;
				$( ".form-group" ).removeClass( "has-error" );
				for ( var i = 0; i < curInputs.length; i++ ) {
					if ( !curInputs[ i ].validity.valid ) {
						isValid = false;
						$( curInputs[ i ] ).closest( ".form-group" ).addClass( "has-error" );
					}
				}
				if ( isValid )
					nextStepWizard.removeAttr( 'disabled' ).trigger( 'click' );
			} );
		} );
		$( function () {

			$( '#sessionDate' ).datepicker( {
				format: "yyyy-mm-dd",
				minDate: new Date(),
				todayHighlight: true
			} );
		} );
		//to refresh the page
		$( "#refresh" ).click( function ( event ) {
			window.setTimeout( function () {
				location.reload()
			}, 1 )

		} );
	</script>
</body>

</html>