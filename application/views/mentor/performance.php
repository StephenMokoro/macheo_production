<!DOCTYPE html>
<html>

<head>
	<title>Macheo | Performance</title>
	<?php $this->load->view('headerlinks/headerlinks.php'); ?>
	<script src="<?php echo base_url();?>assets/jquery/dist/jquery.min.js"></script>

</head>

<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
	<div class="wrapper">
		<?php $this->load->view('mentor/mentornav.php'); ?>
		<!--navigation -->
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="row" style="margin-bottom: -15px;">
					<div class="col-lg-12 ">
						<h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Performance</h4>
						<div class="pull-right">
							<span data-placement="top" data-toggle="tooltip" title="Refresh">
                    <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>
							&nbsp;Refresh</button>
							</span>
							<span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-xs" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>
							&nbsp;Print All</a>
							</span>
						</div>
					</div>
					<!-- /.col-lg-12 -->
				</div>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-body">
								<!-- /.box -->
								<?php $msg = $this->session->flashdata('msg');
                $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-close"></i>
                            <strong><span>';echo $msg['error']; echo '</span></strong>
                        </div> 
                </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                <div class="messagebox alert alert-success" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-check-circle-o"></i>
                            <strong><span>';echo $msg['success'];echo '</span></strong>
                        </div> 
                </div>';}?>
								<table class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="attendancelist">
									<thead>
										<tr style="background: #FFFFFF;color: #000000 ;">
											<th class="text-center pull-left">Sort Mentees &nbsp;&nbsp; </th>
										</tr>
									</thead>
									<tbody style="color: #17202A  ;">
										<?php  foreach($mentees as $mentee){ 
                           ?>

										<tr>
											<td>
												<div class="box box-solid collapsed-box" id="selector" style="margin-bottom: 0px!important;padding-bottom: 0px!important;background-color: #CACFD2;">
													<div class="box-header">
														<h3 class="box-title">
															<?php  echo $mentee['menteeFname']." ".$mentee['menteeLname']; ?>
														</h3>
														<div class="box-tools pull-right clicked">
															<button class="btn btn-default btn-sm " data-widget="collapse" value=<?php echo '"'. $mentee[ 'menteeAutoId']. '"'; ?> id="performance"><i class="fa fa-plus"></i></button>
															<!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
														</div>
													</div>
													<?php $tableId="performance_".$mentee['menteeAutoId'];?>
													<div style="display: none;background-color: #FFFFFF;color: #000000;" class="box-body">
														<table class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="<?php echo $tableId;?>">
															<thead>
																<tr style="background: #2E4053;color: #F7F9F9  ;">
																	<th class="text-center">Exam</th>
																	<th class="text-center">Form</th>
																	<th class="text-center">Term</th>
																	<th class="text-center"></th>
																</tr>
															</thead>
															<tbody style="color: #17202A;">
																<?php foreach($mentee['performance'] as $perf){?>
																<tr>
																	<td class="text-center">
																		<?php  echo $perf['examName'];  ?>
																	</td>
																	<td class="text-center">
																		<?php  echo $perf['examFormCode']; ?>
																	</td>
																	<td class="text-center">
																		<?php  echo $perf['examTermCode']; ?>
																	</td>
																	<td class="text-center">
                                                    <button class="btn btn-default btn-xs" data-placement="top" data-toggle="tooltip" data-title="View More" title="View More" id=<?php echo '"more_'. $perf[ 'examAutoId']. '"'; ?> name=<?php echo '"more_'. $perf['examAutoId'].'"';  ?>  type="submit" ><i class="fa fa-eye"></i></button>
                                            </td>
																</tr> 
																<?php } ?>
															</tbody>
														</table>
														<?php  echo  "<script>
                        $(document).ready(function () { 
                             //datatable initialization
                            $('#";echo $tableId."').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 20, 50, 100, 200, -1], [10, 20, 50, 100, 200, 'All']],'aaSorting':[],
                                 'aoColumnDefs': [{ 'aTargets': [0],'bSortable':false, 'orderable': false},{'aTargets': [1], 'orderable': false}] }); 
                        });//close document.ready

                        </script>";?>
													</div>
													<!-- /.box-body -->
												</div>
												<!-- /.box -->
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>

								<!-- /.table-responsive -->
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<?php $this->load->view('footer');?>

		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
	<script>
		// Limit scope pollution from any deprecated API
		( function () {

			var matched, browser;

			// Use of jQuery.browser is frowned upon.
			// More details: http://api.jquery.com/jQuery.browser
			// jQuery.uaMatch maintained for back-compat
			jQuery.uaMatch = function ( ua ) {
				ua = ua.toLowerCase();

				var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
					/(webkit)[ \/]([\w.]+)/.exec( ua ) ||
					/(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
					/(msie) ([\w.]+)/.exec( ua ) ||
					ua.indexOf( "compatible" ) < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) || [];

				return {
					browser: match[ 1 ] || "",
					version: match[ 2 ] || "0"
				};
			};

			matched = jQuery.uaMatch( navigator.userAgent );
			browser = {};

			if ( matched.browser ) {
				browser[ matched.browser ] = true;
				browser.version = matched.version;
			}

			// Chrome is Webkit, but Webkit is also Safari.
			if ( browser.chrome ) {
				browser.webkit = true;
			} else if ( browser.webkit ) {
				browser.safari = true;
			}

			jQuery.browser = browser;

			jQuery.sub = function () {
				function jQuerySub( selector, context ) {
					return new jQuerySub.fn.init( selector, context );
				}
				jQuery.extend( true, jQuerySub, this );
				jQuerySub.superclass = this;
				jQuerySub.fn = jQuerySub.prototype = this();
				jQuerySub.fn.constructor = jQuerySub;
				jQuerySub.sub = this.sub;
				jQuerySub.fn.init = function init( selector, context ) {
					if ( context && context instanceof jQuery && !( context instanceof jQuerySub ) ) {
						context = jQuerySub( context );
					}

					return jQuery.fn.init.call( this, selector, context, rootjQuerySub );
				};
				jQuerySub.fn.init.prototype = jQuerySub.fn;

				var rootjQuerySub = jQuerySub( document );
				return jQuerySub;
			};

		} )();
	</script>
	<script>
		$( document ).ready( function () {
			//datatable initialization
			var table = $( '#attendancelist' ).DataTable( {
				responsive: true,
				"iDisplayLength": 10,
				"lengthMenu": [
					[ 10, 25, 50, 100, 200, -1 ],
					[ 10, 25, 50, 100, 200, "All" ]
				],
				"aaSorting": []
			} );

			var submitBtn = $( 'input[type="submit"]' );
			// allWells.show();
			submitBtn.click( function () {
				var curStep = $( this ).closest( ".setup-content" ),
					curStepBtn = curStep.attr( "id" ),
					curInputs = curStep.find( "input,select" ),
					isValid = true;
				$( ".form-group" ).removeClass( "has-error" );
				for ( var i = 0; i < curInputs.length; i++ ) {
					if ( !curInputs[ i ].validity.valid ) {
						isValid = false;
						$( curInputs[ i ] ).closest( ".form-group" ).addClass( "has-error" );
					}
				}
				if ( isValid )
					nextStepWizard.removeAttr( 'disabled' ).trigger( 'click' );
			} );
		} );
		//to refresh the page
		$( "#refresh" ).click( function ( event ) {
			window.setTimeout( function () {
				location.reload()
			}, 1 )

		} );
	</script>
</body>

</html>