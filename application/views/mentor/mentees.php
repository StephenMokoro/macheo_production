<!DOCTYPE html>
<html>

<head>
    <title>Macheo | Mentees</title>
    <?php $this->load->view('headerlinks/headerlinks.php'); ?>
    <style>
        @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,700,300);

    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
    <div class="wrapper">
        <?php $this->load->view('mentor/mentornav.php'); ?>
        <!--navigation -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="row" style="margin-bottom: -10px;">
                    <div class="col-lg-12 ">
                        <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Mentees</h4>
                        <div class="pull-right">
                            <span data-placement="top" data-toggle="tooltip" title="Refresh">
                    <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                            </span>
                            <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-xs" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>&nbsp;Print All</a>
                            </span>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <!-- /.box -->
                                <?php $msg = $this->session->flashdata('msg');
                                $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                                <div class="messagebox alert alert-danger" style="display: block">
                                        <button type="button" class="close" data-dismiss="alert">*</button>
                                        <div class="cs-text">
                                            <i class="fa fa-close"></i>
                                            <strong><span>';echo $msg['error']; echo '</span></strong>
                                        </div> 
                                </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                                <div class="messagebox alert alert-success" style="display: block">
                                        <button type="button" class="close" data-dismiss="alert">*</button>
                                        <div class="cs-text">
                                            <i class="fa fa-check-circle-o"></i>
                                            <strong><span>';echo $msg['success'];echo '</span></strong>
                                        </div> 
                                </div>';}?>
                                <table class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="menteeslist">
                                    <thead>
                                        <tr style="background: #2E4053;color: #F7F9F9  ;">
                                            <th class="text-left">MENTEE</th>
                                           	<th class="text-center">SCHOOL</th>
                                            <th class="text-center">FORM</th>
                                            <th class="text-center"><i class="fa fa-cog fa-spin"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody style="color: #17202A;">
                                        <?php  foreach($mentees as $mentee){?>
                                        <tr>
                                            <?php $photo=$mentee['menteeProfilePhoto']; if($photo==""){$profile="defaultimage.png";}else{$profile=$mentee['menteeProfilePhoto'];}?>
                                            <td class="text-left"><img src="<?php echo base_url();echo 'uploads/profile_photos/mentees/'.$profile?>" width="25" height="25" class="img-circle" alt="User Image">
                                                <?php  echo $mentee['menteeFname']. " ".$mentee['menteeLname']; ?>
                                            </td>

                                            <td class="text-center">
                                                <?php  echo $mentee['schoolAlias']; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php  echo $mentee['formCode']; ?>
                                            </td>
                                            <td class="text-center">
                                                <form style="display:inline;" name=<?php echo '"formMore_'. $mentee[ 'menteeAutoId']. '"'; ?> method="post" action="
                                                    <?php echo base_url('mentor/menteeprofile');?>">
                                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                        <label for="menteeUID" class="control-label">Mentee UID*</label>
                                                        <input required="required" class="form-control" name="menteeUID" id="menteeUID" placeholder="101" value="<?php echo $mentee['menteeAutoId']; ?>">
                                                    </div>
                                                    <button class="btn btn-default btn-xs" data-placement="top" data-toggle="tooltip" data-title="View More" title="View More" id=<?php echo '"more_'. $mentee[ 'menteeAutoId']. '"'; ?> name=<?php echo '"more_'. $mentee['menteeAutoId'].'"';  ?>  type="submit" ><i class="fa fa-eye"></i></button>
												</form>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <!-- /.table-responsive -->

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('footer');?>
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
    <script>
        // Limit scope pollution from any deprecated API
        (function() {

            var matched, browser;

            // Use of jQuery.browser is frowned upon.
            // More details: http://api.jquery.com/jQuery.browser
            // jQuery.uaMatch maintained for back-compat
            jQuery.uaMatch = function(ua) {
                ua = ua.toLowerCase();

                var match = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
                    /(webkit)[ \/]([\w.]+)/.exec(ua) ||
                    /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
                    /(msie) ([\w.]+)/.exec(ua) ||
                    ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) || [];

                return {
                    browser: match[1] || "",
                    version: match[2] || "0"
                };
            };

            matched = jQuery.uaMatch(navigator.userAgent);
            browser = {};

            if (matched.browser) {
                browser[matched.browser] = true;
                browser.version = matched.version;
            }

            // Chrome is Webkit, but Webkit is also Safari.
            if (browser.chrome) {
                browser.webkit = true;
            } else if (browser.webkit) {
                browser.safari = true;
            }

            jQuery.browser = browser;

            jQuery.sub = function() {
                function jQuerySub(selector, context) {
                    return new jQuerySub.fn.init(selector, context);
                }
                jQuery.extend(true, jQuerySub, this);
                jQuerySub.superclass = this;
                jQuerySub.fn = jQuerySub.prototype = this();
                jQuerySub.fn.constructor = jQuerySub;
                jQuerySub.sub = this.sub;
                jQuerySub.fn.init = function init(selector, context) {
                    if (context && context instanceof jQuery && !(context instanceof jQuerySub)) {
                        context = jQuerySub(context);
                    }

                    return jQuery.fn.init.call(this, selector, context, rootjQuerySub);
                };
                jQuerySub.fn.init.prototype = jQuerySub.fn;
                var rootjQuerySub = jQuerySub(document);
                return jQuerySub;
            };

        })();

    </script>
    <script>
        $(document).ready(function() {
            //datatable initialization
            var table = $('#menteeslist').DataTable({
                responsive: true,
                "iDisplayLength": 10,
                "lengthMenu": [
                    [10, 25, 50, 100, 200, -1],
                    [10, 25, 50, 100, 200, "All"]
                ],
                columnDefs: [{
                    orderable: false,
                    targets: [4]
                }, {
                    sWidth: '25%',
                    targets: [4]
                }],
                "aaSorting": []



            });
            var submitBtn = $('input[type="submit"]');
            // allWells.show();
            submitBtn.click(function() {
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    curInputs = curStep.find("input,select"),
                    isValid = true;
                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }
                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });


            var brand = document.getElementById('photo-id');
            brand.className = 'attachment_upload';
            brand.onchange = function() {
                document.getElementById('fakeUploadLogo').value = this.value.substring(12);
            };

            // Source: http://stackoverflow.com/a/4459419/6396981
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('.img-preview').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#photo-id").change(function() {
                readURL(this);
            });
            //datepicker
            $('#joinDate').datepicker({
                format: "yyyy-mm-dd",
                minDate: new Date(),
                todayHighlight: true
            });
            $('#dob').datepicker({
                format: "yyyy-mm-dd",
                todayHighlight: true
            });
        });
        //to refresh the page
        $("#refresh").click(function(event) {
            window.setTimeout(function() {
                location.reload()
            }, 1)

        });

    </script>
</body>

</html>
