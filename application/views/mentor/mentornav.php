<?php $photo=$this->session->userdata('mentorProfilePhoto'); if($photo==""){$ppic="defaultimage.png";}else{$ppic=$this->session->userdata('mentorProfilePhoto');}?>

<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b class="fa fa-expand" data-toggle="push-menu"></b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><small><b> MACHEO</b></span></small>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <style>
            #sidebar-toggle:hover {
                background-color: #222d32;
            }

        </style>
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button" id="sidebar-toggle">
            <span class="sr-only">Toggle navigation</span><span style="text-transform:uppercase;font-weight: bolder;color: #FFFF00;"><i class="fa fa-sun-o fa-spin" style="color: #FFFF00;"></i></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
      <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img  src="<?php echo base_url();echo 'uploads/profile_photos/mentors/'.$ppic?>" width="30" height="30"  class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata('mentorName');?></span>
            </a>
                    <ul class="dropdown-menu">
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo base_url('mentor/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
            </ul>
        </div>
    </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left ">
                <img src="<?php echo base_url();echo 'uploads/profile_photos/mentors/'.$ppic?>" style="width:40px;height:40px;" class="img-circle img-responsive" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>
                    <?php echo $this->session->userdata('mentorName');?>
                </p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="<?php echo base_url();?>mentor/dashboard" "><i class= "fa fa-dashboard text-aqua " ></i> <span>Dashboard</span></a>
        </li>
        <li>
          <a href="<?php echo base_url();?>mentor/mentees" ">
                <i class="fa fa-users "></i> <span> My Mentees</span>
               </a>
            </li>

            <li>
                <a href="<?php echo base_url();?>mentor/mentorship">
                <i class="fa fa-signal"></i> <span>Mentorship</span>
            </a>
            </li>

            <li>
                <a href="<?php echo base_url();?>mentor/performance">
                <i class="fa fa-book"></i> <span>Performance</span>
            </a>
            </li>
            <li>
                <a href="<?php echo base_url();?>mentor/attendance">
                <i class="fa fa-bookmark-o"></i> <span>Attendance</span>
            </a>
                <li class="header" style="background-color: #008080;color: #FFFFFF;">2017 MACHEO</li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
