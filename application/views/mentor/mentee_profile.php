<!DOCTYPE html>
<html>
<head>
   <title>Macheo | Mentee Profile</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('mentor/mentornav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row" style="margin-bottom: -15px;">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Mentee Profile</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
             <?php foreach($mentee_profile as $profile){ 
                $photo=$profile['menteeProfilePhoto']; if($photo==""){$ppic="defaultimage.png";}else{$ppic=$profile['menteeProfilePhoto'];}?>
                    <a href="<?php echo base_url();?>admin/download_menteephoto/<?php echo $ppic;?>">
                      <div class="col-md-3" style="text-align: center;margin-right: auto">
                        <div class="col-md-12" style="display: inline-block;text-align: center">
                            <img src="<?php echo base_url();echo 'uploads/profile_photos/mentees/'.$ppic?>" alt="PPIC" class="img-rounded img-responsive" />
                            <b><p style="color: #000000;"><br><?php echo $profile['menteeFname']." ".$profile['menteeLname'];?></p></b>
                        </div>
                    </div></a>
                    <div class="col-md-9 text-center">
                            <div class="col-md-6" style="text-align: left">
                                <blockquote >
                                     <p><i class="fa fa-user-circle text-primary fa-1x"></i> <?php echo date_format(date_create($profile['menteeDoB']),"j<\s\up>S</\s\up> M, Y");?>  <span><small style="display: inline">D.O.B </small></cite></span> </p>

                                     <p><i class="fa fa-venus-mars text-success fa-1x"></i> <?php echo $profile['menteeGender'];?>  <span><cite title="when <?php echo $profile['menteeGender'];?> "><small style="display: inline">Gender </small></cite></span> </p>

                                      <p><i class="fa fa-phone text-primary fa-1x"></i> <?php echo $profile['menteePhone'];?>  <span><cite title="<?php echo $profile['menteeFname'];?>'s phone number"><small style="display: inline">Cell Phone  </small></cite></span> </p>

                                      <p><i class="fa fa-envelope text-success fa-1x"></i> <?php echo $profile['menteeEmail'];?>  <span><cite title="<?php echo $profile['menteeFname'];?>'s Email Address"><small style="display: inline">Email  </small></cite></span> </p>

                               
                                   <?php $currentClass=$profile['formCode'];
                                    if($currentClass==""){ $display=" <b class='text-warning'>N/A</b>";}else{$display=$profile['formCode'];}?>

                                     <p><i class="fa fa-line-chart text-primary fa-1x"> </i> <?php echo $display;?>  <span><cite title="<?php echo $profile['menteeFname'];?>'s Current Form"><small style="display: inline">Current Form  </small></cite></span> </p>

                                     <p><i class="fa fa-institution text-success fa-1x"> </i> <?php echo $profile['schoolAlias'];?>  <span><cite title="<?php echo $profile['menteeFname'];?>'s Current School"><small style="display: inline">Current School  </small></cite></span> </p>

                                     <?php  $guardian=$profile['menteeGuardianId'];
                                     if($guardian==""){ $parent="<b class='text-warning'>N/A</b>";}else{$parent=$profile['guardianFname']." ".$profile['guardianLname'];}?>
                                     <p><i class="fa fa-user-circle text-default fa-1x"> </i>  <?php echo $parent;?>  <span><cite title="<?php echo $profile['menteeFname'];?>'s Parent/Guardian"><small style="display: inline"> Parent  </small></cite></span> </p>
                                </blockquote>
                            <!-- <hr> -->
                            </div>
                    </div>
                    <div class="col-md-12 text-left">
                          <blockquote >
                            <p><b>Call Parent/Guardian on: </b> <?php ?>  <?php echo $profile['guardianPhone1'];?> <i>/</i> <?php echo $profile['guardianPhone2'];?>
                        </blockquote>
                    </div>
                <?php }?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
    // Limit scope pollution from any deprecated API
(function() {

    var matched, browser;

// Use of jQuery.browser is frowned upon.
// More details: http://api.jquery.com/jQuery.browser
// jQuery.uaMatch maintained for back-compat
    jQuery.uaMatch = function( ua ) {
        ua = ua.toLowerCase();

        var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
            /(msie) ([\w.]+)/.exec( ua ) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
            [];

        return {
            browser: match[ 1 ] || "",
            version: match[ 2 ] || "0"
        };
    };

    matched = jQuery.uaMatch( navigator.userAgent );
    browser = {};

    if ( matched.browser ) {
        browser[ matched.browser ] = true;
        browser.version = matched.version;
    }

// Chrome is Webkit, but Webkit is also Safari.
    if ( browser.chrome ) {
        browser.webkit = true;
    } else if ( browser.webkit ) {
        browser.safari = true;
    }

    jQuery.browser = browser;

    jQuery.sub = function() {
        function jQuerySub( selector, context ) {
            return new jQuerySub.fn.init( selector, context );
        }
        jQuery.extend( true, jQuerySub, this );
        jQuerySub.superclass = this;
        jQuerySub.fn = jQuerySub.prototype = this();
        jQuerySub.fn.constructor = jQuerySub;
        jQuerySub.sub = this.sub;
        jQuerySub.fn.init = function init( selector, context ) {
            if ( context && context instanceof jQuery && !(context instanceof jQuerySub) ) {
                context = jQuerySub( context );
            }

            return jQuery.fn.init.call( this, selector, context, rootjQuerySub );
        };
        jQuerySub.fn.init.prototype = jQuerySub.fn;
        var rootjQuerySub = jQuerySub(document);
        return jQuerySub;
    };

})();</script>

</body>
</html>
