<!DOCTYPE html>
<html>

<head>

	<title>Macheo | Dashboard</title>
	<?php $this->load->view('headerlinks/headerlinks.php'); ?>

</head>

<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;">
	<div class="wrapper">
		<?php $this->load->view('mentor/mentornav.php'); ?>
		<!--navigation -->
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="row" style="margin-bottom: -15px;">
					<div class="col-lg-12 ">
						<h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Home</h4>
						<div class="pull-right">
							<span data-placement="top" data-toggle="tooltip" title="Refresh">
                    <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>
							&nbsp;Refresh</button>
							</span>
							<span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-xs" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>
							&nbsp;Print All</a>
							</span>
						</div>
					</div>
					<!-- /.col-lg-12 -->
				</div>
			</section>
			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-body">
								<!-- Small boxes (Stat box) -->
								<div class="row">
									<div class="col-lg-3 col-xs-6">
										<!-- small box -->
										<div class="small-box" style="background-color: #85929E; color: #FFFFFF;">
											<div class="inner">
												<h3 id="mentees">
													<?php  echo $menteescount; ?>
												</h3>
												<p>My Mentees</p>
											</div>
											<div class="icon">
												<i class="fa fa-child text-deafult"></i>
											</div>
											<a href="<?php echo base_url();?>mentor/mentees" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
									<!-- /.row -->
								</div>
								<!-- /.box-body -->
							</div>
							<!-- /.box -->
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<?php $this->load->view('footer');?>
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
	<script>
		// Limit scope pollution from any deprecated API
		( function () {

			var matched, browser;

			// Use of jQuery.browser is frowned upon.
			// More details: http://api.jquery.com/jQuery.browser
			// jQuery.uaMatch maintained for back-compat
			jQuery.uaMatch = function ( ua ) {
				ua = ua.toLowerCase();

				var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
					/(webkit)[ \/]([\w.]+)/.exec( ua ) ||
					/(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
					/(msie) ([\w.]+)/.exec( ua ) ||
					ua.indexOf( "compatible" ) < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) || [];

				return {
					browser: match[ 1 ] || "",
					version: match[ 2 ] || "0"
				};
			};

			matched = jQuery.uaMatch( navigator.userAgent );
			browser = {};

			if ( matched.browser ) {
				browser[ matched.browser ] = true;
				browser.version = matched.version;
			}

			// Chrome is Webkit, but Webkit is also Safari.
			if ( browser.chrome ) {
				browser.webkit = true;
			} else if ( browser.webkit ) {
				browser.safari = true;
			}

			jQuery.browser = browser;

			jQuery.sub = function () {
				function jQuerySub( selector, context ) {
					return new jQuerySub.fn.init( selector, context );
				}
				jQuery.extend( true, jQuerySub, this );
				jQuerySub.superclass = this;
				jQuerySub.fn = jQuerySub.prototype = this();
				jQuerySub.fn.constructor = jQuerySub;
				jQuerySub.sub = this.sub;
				jQuerySub.fn.init = function init( selector, context ) {
					if ( context && context instanceof jQuery && !( context instanceof jQuerySub ) ) {
						context = jQuerySub( context );
					}

					return jQuery.fn.init.call( this, selector, context, rootjQuerySub );
				};
				jQuerySub.fn.init.prototype = jQuerySub.fn;
				var rootjQuerySub = jQuerySub( document );
				return jQuerySub;
			};

		} )();

		$( document ).ready( function () {
			//datatable initialization
			$( '#studentslist' ).DataTable( {
				responsive: true,
				"iDisplayLength": 10,
				"lengthMenu": [
					[ 10, 25, 50, 100, 200, -1 ],
					[ 10, 25, 50, 100, 200, "All" ]
				],
				columnDefs: [ {
					orderable: false,
					targets: [ 3 ]
				} ],
				"aaSorting": []
			} );

			$.ajax( {
				type: "post",
				url: "<?php echo base_url(); ?>mentor/menteescount",
				data: {},
				dataType: 'json',
				success: function ( data ) {
					$( '#mentees' ).text( data );
				}
			} );
			// $.ajax({type:"post", url: "<?php //echo base_url(); ?>admin/mentorscount",data:{},
			//   dataType:'json',success:function(data){ $('#mentors').text(data); }});
			//  $.ajax({type:"post", url: "<?php //echo base_url(); ?>admin/internscount",data:{},
			//   dataType:'json',success:function(data){ $('#interns').text(data); }});
			//  $.ajax({type:"post", url: "<?php //echo base_url(); ?>admin/adminscount",data:{},
			//   dataType:'json',success:function(data){ $('#admins').text(data); }});
			//  $.ajax({type:"post", url: "<?php //echo base_url(); ?>admin/teacherscount",data:{},
			//   dataType:'json',success:function(data){ $('#teachers').text(data); }});
			//   $.ajax({type:"post", url: "<?php //echo base_url(); ?>admin/macheoexcount",data:{},
			//   dataType:'json',success:function(data){ $('#macheoexams').text(data); }});
			//   $.ajax({type:"post", url: "<?php //echo base_url(); ?>admin/generalexcount",data:{},
			//   dataType:'json',success:function(data){ $('#generalexams').text(data); }});
			// $.ajax({type:"post", url: "<?php //echo base_url(); ?>admin/attendancecount",data:{},
			// dataType:'json',success:function(data){ $('#attendance').text(data); }});


		} );
		//to refresh the page
		$( "#refresh" ).click( function ( event ) {
			window.setTimeout( function () {
				location.reload()
			}, 1 )

		} );
	</script>

</body>

</html>