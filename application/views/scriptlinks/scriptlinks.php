
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url();?>assets/jquery/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);

</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/bootstrap/bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/moment/min/moment.min.js"></script><!--must come before datepickers-->

<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/jquery/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/adminLTE-2.4.2/js/adminlte.min.js"></script>
<script src="<?php echo base_url(); ?>assets/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/datatables/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/datatables/js/dataTables.bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/bootstrap/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>

<!-- custom script -->
<script src="<?php echo base_url();?>assets/customjs/custom.js"></script>
<!-- date-picker -->
<script src="<?php echo base_url(); ?>assets/bootstrap/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/bootstrap-datepicker/datetimepicker/js/bootstrap-datetimepicker.js"></script>

<script src="<?php echo base_url();?>assets/bootstrap/bootstrap-datepicker/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<script src="<?php echo base_url(); ?>assets/datatables/js/dataTables.buttons.min.js"> </script>
<script src="<?php echo base_url(); ?>assets/datatables/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url(); ?>assets/datatables/js/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/datatables/js/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/datatables/js/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/datatables/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/datatables/js/buttons.print.min.js"></script>
