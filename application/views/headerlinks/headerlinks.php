<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon" />
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/bootstrap-3.3.7/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/adminLTE-2.4.2/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/adminLTE-2.4.2/css/skins/_all-skins.min.css">
<!-- DataTables CSS -->
<link href="<?php echo base_url();?>assets/datatables/css/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="<?php echo base_url();?>assets/datatables/css/dataTables.responsive.css" rel="stylesheet">
<!-- custom css -->
<link href="<?php echo base_url(); ?>assets/css/general.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url();?>assets/bootstrap/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">

<link href="<?php echo base_url();?>assets/bootstrap/bootstrap-datepicker/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">

 <link href="<?php echo base_url(); ?>assets/css/collapsible.css" rel="stylesheet" type="text/css" />

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/jquery/jquery-3.2.1/dist/jquery.min.js"></script>
