<!DOCTYPE html>
<html>

<head>
    <title>Macheo | Edit Parent/Guardian Profile</title>
    <?php $this->load->view('headerlinks/headerlinks.php'); ?>
    <style>
        @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,700,300);
        body {
            /*font: 12px 'Open Sans';*/
        }

    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
    <div class="wrapper">
        <?php $this->load->view('admin/adminnav.php'); ?>
        <!--navigation -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="row">
                    <div class="col-lg-12 ">
                        <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Edit Parent/Guardian Profile</h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <?php if(isset($_SESSION['msg']))
                                  {
                                    $msg = $_SESSION['msg'];
                                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                                    <div class="messagebox alert alert-danger" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <i class="fa fa-close"></i>
                                          <strong><span>';echo $msg['error']; echo '</span></strong>
                                      </div> 
                                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                                    <div class="messagebox alert alert-success" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <i class="fa fa-check-circle-o"></i>
                                          <strong><span>';echo $msg['success'];echo '</span></strong>
                                      </div> 
                                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                                <?php foreach($guardian_profile as $profile){
                        echo form_open_multipart('admin/updateparent',array('id' => 'guardian_update','method'=>'post','name'=>'guardianupdate'));

                         $photo=$profile['guardianProfilePhoto']; if($photo==""){$ppic="defaultimage.png";}else{$ppic=$profile['guardianProfilePhoto'];}
                         ?>
                                <div class="row setup-content">
                                    <div class="col-xs-12">
                                        <div class="col-md-6">
                                            <div class="col-md-9 col-md-offset-3">
                                                <div class="form-group">
                                                    <div class="main-img-preview">
                                                        <img class="thumbnail img-preview " src="<?php echo base_url();echo 'uploads/profile_photos/guardians/'.$ppic?>" alt="Parent Photo" width="210" height="230">
                                                    </div>
                                                    <!-- <p class="help-block">* Upload mentee passport photo.</p> -->
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-md-offset-4">
                                                <div class="input-group">
                                                    <input id="fakeUploadLogo" class="form-control fake-shadow" disabled="disabled" style="display: none; " required="required">
                                                    <div class="input-group-btn">
                                                        <div class="fileUpload btn btn-default fake-shadow">
                                                            <span><i class="fa fa-upload"></i> Upload Photo</span>
                                                            <input id="photo-id" name="photo" type="file" class="attachment_upload">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                                <label for="initFile" class="control-label">Initial File <span class="star">*</span></label>
                                                <input type="text" name="initFile" placeholder="" class=" form-control" id="initFile" value=<?php echo '"'.$profile[ 'guardianProfilePhoto']. '"';?> >
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                                <label for="guardianUID" class="control-label">Guardian UID <span class="star">*</span></label>
                                                <input type="number" name="guardianUID" placeholder="" class=" form-control" id="guardianUID" required="required" maxlength="20" value=<?php echo '"'.$profile[ 'guardianAutoId']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="firstName" class="control-label">First Name <span class="star">*</span></label>
                                                <input type="text" name="firstName" placeholder="" class=" form-control" id="firstName" required="required" maxlength="20" value=<?php echo '"'.$profile[ 'guardianFname']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="lastName" class="control-label">Last Name <span class="star">*</span></label>
                                                <input type="text" name="lastName" placeholder="" class=" form-control" id="lastName" required="required" maxlength="20" value=<?php echo '"'.$profile[ 'guardianLname']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="otherNames" class="control-label">Other Names</label>
                                                <input type="text" name="otherNames" placeholder="" class=" form-control" id="otherNames" maxlength="20" value=<?php echo '"'.$profile[ 'guardianOtherNames']. '"';?>>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label class="">Gender <span class="star">*</span></label><br>
                                                <?php $guardian_gender= $profile['guardianGender'];  if ($guardian_gender=='Female'){?>
                                                <?php  echo '<label class="radio-inline ">
                                                <input type="radio" name="gender" id="female" value="Female" autocomplete="off" required="required" checked="true">Female
                                            </label>
                                             <label class="radio-inline ">
                                                <input type="radio" name="gender" id="male" value="Male" autocomplete="off" required="required">Male
                                            </label>';}else if ($guardian_gender=="Male"){echo '
                                            <label class="radio-inline ">
                                                <input type="radio" name="gender" id="female" value="Female" autocomplete="off" required="required" >Female
                                            </label>
                                             <label class="radio-inline ">
                                                <input type="radio" name="gender" id="male" value="Male" autocomplete="off" required="required" checked="true">Male
                                            </label>';}?>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="dob" class="control-label">Date of Birth <span class="star">*</span></label>
                                                <input type="text" name="dob" placeholder="" class=" form-control" id="dob" required="required" placeholder="YYYY-MM-DD" data-mask="9999-99-99" value=<?php echo '"'.$profile[ 'guardianDoB']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="phoneNumber" class="control-label"> Current Phone No. <span class="star">*</span> </label>
                                                <input type="text" name="phoneNumber" placeholder="" class=" form-control" id="phoneNumber" data-mask="0799999999" required="required" value=<?php echo '"'.$profile[ 'guardianPhone1']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="phoneNumber2" class="control-label"> Alternative Phone No.</label>
                                                <input type="text" name="phoneNumber2" class=" form-control" id="phoneNumber2" data-mask="0799999999" value=<?php echo '"'.$profile[ 'guardianPhone2']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="guardianId" class="control-label"> National ID <span class="star">*</span></label>
                                                <input type="text" name="guardianId" class=" form-control" id="guardianId" required="required" value=<?php echo '"'.$profile[ 'guardianId']. '"';?>>
                                            </div>

                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="emailAddress" class="control-label"> Email Address </label>
                                                <input type="email" name="emailAddress" placeholder="" class=" form-control" id="emailAddress" value=<?php echo '"'.$profile[ 'guardianEmail']. '"';?>>
                                            </div>

                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="profession" class="control-label"> Profession </label>
                                                <input type="text" name="profession" placeholder="" class=" form-control" id="profession" value=<?php echo '"'.$profile[ 'guardianProfession']. '"';?>>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group col-md-6 col-lg-6">
                                                <input type="submit" class="btn btn-warning" value="Update">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/.col-xs-12-->
                                </div>
                                <!--/.setup-content-->
                                <?php echo form_close();?>
                                <?php }?>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('footer');?>
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
    <script>
        $(document).ready(function() {
            var submitBtn = $('input[type="submit"]');
            // allWells.show();
            submitBtn.click(function() {
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    curInputs = curStep.find("input,select"),
                    isValid = true;
                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }
                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });


            var brand = document.getElementById('photo-id');
            brand.className = 'attachment_upload';
            brand.onchange = function() {
                document.getElementById('fakeUploadLogo').value = this.value.substring(12);
            };

            // Source: http://stackoverflow.com/a/4459419/6396981
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('.img-preview').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#photo-id").change(function() {
                readURL(this);
            });
        });
        //to refresh the page
        $("#refresh").click(function(event) {
            window.setTimeout(function() {
                location.reload()
            }, 1)

        });

    </script>
</body>

</html>
