<!DOCTYPE html>
<html>
<head>
    <title>Macheo | Mentees</title>
    <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <!--  <style type="text/css">
        table.table ul.dropdown-menu{
    position:relative;
    float:none;
    max-width:160px;
}
    </style> -->
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
    <div class="wrapper">
        <?php $this->load->view('admin/adminnav.php'); ?>
        <!--navigation -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="row">
                    <div class="col-lg-12 ">
                        <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Mentees</h4>
                        <div class="pull-right">
                            <span data-placement="top" data-toggle="tooltip" title="Refresh">
                    <button class="btn btn-s" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                            </span>
                            <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-s" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>&nbsp;Print All</a>
                            </span>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <?php if(isset($_SESSION['msg']))
                                  {
                                    $msg = $_SESSION['msg'];
                                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                                    <div class="messagebox alert alert-danger" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <strong><span>';echo $msg['error']; echo '</span></strong>
                                      </div> 
                                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                                    <div class="messagebox alert alert-success" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <strong><span>';echo $msg['success'];echo '</span></strong>
                                      </div> 
                                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                                <table class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="menteeslist">
                                    <thead>
                                        <tr style="background: #2E4053;color: #F7F9F9;">
                                            <th class="text-left">MENTEE</th>
                                            <th class="text-center">SCHOOL</th>
                                            <th class="text-center">FORM</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody style="color: #17202A;">
                                        <?php  foreach($mentees as $mentee){?>
                                        <tr>
                                            <?php $photo=$mentee['menteeProfilePhoto']; if($photo==""){$profile="defaultimage.png";}else{$profile=$mentee['menteeProfilePhoto'];}?>
                                            <td class="text-left"><img src="<?php echo base_url();echo 'uploads/profile_photos/mentees/'.$profile?>" width="25" height="25" class="img-circle" alt="User Image">
                                                <?php  echo $mentee['menteeFname']. " ".$mentee['menteeLname']; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php  echo $mentee['schoolAlias']; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php  echo $mentee['formCode']; ?>
                                            </td>
                                            <td class="text-center">
                                               <!--  <div class="hidden-sm">
                                                 <div class="btn-group " >
                                                     <button class="btn btn-default">Action</button> <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li>
                                                            <a href="#">Action</a>
                                                        </li>
                                                        <li class="disabled">
                                                            <a href="#">Another action</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Something else here</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div> -->
                                                <form style="display:inline;" name="form_<?php echo $mentee['menteeAutoId'];?> " method="post" action="#">
                                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                        <label for="menteeUID" class="control-label">Mentee ID*</label>
                                                        <input required="required" class="form-control" name="menteeUID" id="menteeUID" placeholder="101" value="<?php echo $mentee['menteeAutoId']; ?>">
                                                    </div>
                                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                         <label for="menteeName" class="control-label">Mentee Name*</label>
                                                        <input required="required" class="form-control" name="menteeName" id="menteeName" placeholder="101" value="<?php echo $mentee['menteeFname']." ".$mentee['menteeLname']; ?>">
                                                    </div>
                                                    <div class="input-group" style="padding: 0px!important;">
                                                        <span class="input-group-addon" style="padding: 0px!important;margin: 0px!important;border: 0px!important">
                                                            <button class="btn btn-primary" >Go!</button> 
                                                        </span>
                                                        <select class="form-control" style="padding: 0px!important;margin: 0px!important;border-radius: 5px!important;width: 80px !important;font-family: 'FontAwesome',serif;" id="action_select" onchange="actionBase(this.parentNode.parentNode, this.options[this.selectedIndex].value, '<?php echo base_url();?>admin/')">
                                                            <option value="#">Action</option>
                                                            <option value="menteeprofile">&#xf06e; Profile</option>
                                                            <option value="editmentee">&#xf044; Edit</option>
                                                            <option value="disablementee">&#xf05e; Disable</option>
                                                            <option value="delmentee">&#xf1f8; Delete</option>
                                                        </select>
                                                    </div>
                                            </form>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <!-- /.table-responsive -->

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('footer');?>
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
    
    <script>
     function actionBase(form,value,url){
            //set the action 
            form.setAttribute('action',url+value);
        }
       
        $(document).ready(function() {
            //datatable initialization
        $('#menteeslist').DataTable({'iDisplayLength': 50,'lengthMenu': [[50, 100, 500, 200, -1], [50, 100, 500, 'All']],columnDefs: [{ orderable: false,targets: [3]}],"aaSorting": [],dom: 'lBfrtip', 
        buttons: [{extend: 'print',exportOptions: {columns:[0,1,2]   } },{extend: 'excel',exportOptions: {columns:[0,1,2]   } },{extend: 'pdf',exportOptions: {columns:[0,1,2]   } }]
        // buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],exportOptions: {columns:[0,1,2,3]}
        });
    });
        //to refresh the page
        $("#refresh").click(function(event) {
            window.setTimeout(function() {
                location.reload()
            }, 1)

        });
       

    </script>
</body>

</html>
