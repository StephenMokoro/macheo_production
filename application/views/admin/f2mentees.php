<!DOCTYPE html>
<html>
<head>
  <title>Macheo | FORM 2 Mentees</title>
  <?php $this->load->view('headerlinks/headerlinks.php'); ?>
  <link href="<?php echo base_url(); ?>assets/css/collapsible.css" rel="stylesheet" type="text/css" />
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav.php'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                          <div class="col-lg-12" style="margin-top: 0px;">
                        <h4 class="pull-left">Forms <span class="fa fa-angle-right"></span> Form 2 <span class="fa fa-angle-right"></span> Mentees</h4>
                        <div class="pull-right">
                        </div> 
                    </div>
                    <!-- /.col-lg-12 -->
                          <?php if(isset($_SESSION['msg']))
                            {
                              $msg = $_SESSION['msg'];
                              $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                              <div class="messagebox alert alert-danger" style="display: block">
                                <button type="button" class="close" data-dismiss="alert">*</button>
                                <div class="cs-text">
                                    <strong><span>';echo $msg['error']; echo '</span></strong>
                                </div> 
                              </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                              <div class="messagebox alert alert-success" style="display: block">
                                <button type="button" class="close" data-dismiss="alert">*</button>
                                <div class="cs-text">
                                    <strong><span>';echo $msg['success'];echo '</span></strong>
                                </div> 
                                </div>';
                              } $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                              <table  class="table display responsive nowrap" cellspacing="0" width="100%" id="menteeslist"  >
                                <thead>
                                    <tr style="background: #2E4053;color: #F7F9F9  ;" hidden="true">
                                        <th class="text-center pull-left" >Sort Mentees &nbsp;&nbsp; </th>
                                     </tr>
                                </thead>
                                <tbody style="color: #17202A ;">
                                    <?php  foreach($mentees as $mentee){?>
                                   <tr >
                                      <td style="margin: 0px!important;padding: 0px!important;border: none;">
                                        <div class="panel panel-default">
                                          <div class="panel-heading" role="tab" id="mentee<?php echo $mentee['menteeAutoId'];?>">
                                              <h4 class="panel-title">
                                                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $mentee['menteeAutoId'];?>" aria-expanded="false" aria-controls="collapseTwo" style="font-weight: normal;">
                                                      <?php echo $mentee['menteeFname']." ".$mentee['menteeLname']; ?>
                                                  </a>
                                              </h4>
                                          </div>
                                          <div id="collapse_<?php echo $mentee['menteeAutoId'];?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="mentee<?php echo $mentee['menteeAutoId'];?>">
                                            <div class="panel-body">
                                              <?php $tableId="table_".$mentee['menteeAutoId'];?>
                                              <form method="post" action="<?php echo base_url(); ?>admin/newgensubmarks">
                                                <div class="form-group col-md-4 col-lg-4" style="display: none;">
                                                   <input type="number" class=" form-control text-center" required="required" name="menteeId" value="<?php  echo $mentee['menteeAutoId']; ?>">
                                                </div>
                                                <div class="row">
                                                  <table class="table table-bordred table-striped table-hover" id="list">
                                                     <thead style="background-color:#512E5F;color:#FFFFFF;">
                                                         
                                                      </thead>
                                                      <tbody>
                                                         
                                                      </tbody>
                                                  </table>
                                                </div>
                                              </form>
                                            </div>
                                          </div>
                                      </div>
                                    </td>
                                  </tr>
                                  <?php }?>
                                </tbody>
                              </table>
                        </div>
                    </div><!--- END COL -->     
                </div><!--- END ROW -->       
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
       $('#menteeslist').dataTable({'responsive': true,'iDisplayLength': 50,'lengthMenu': [[50, 200, 500, -1], [50, 200, 500, 'All']],'aaSorting': [] }); 
});

//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
