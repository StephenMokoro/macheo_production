<!DOCTYPE html>
<html>
<head>
   <title>Macheo | Mentee Profile</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
<style>
    .tabtop li a{font-family:'Lato', sans-serif;font-weight:700;color:#1b2834;border-radius:0px;margin-right:22.008px;border:0px solid #ebebeb !important;background-color: #D7DBDD;}
    .tabtop .active a:before{content:"♦";position:absolute;top:40px;left:110px;color:#e31837;font-size:30px;}
    .tabtop li a:hover{color:#000 !important;text-decoration:none;background-color: #ECF0F1;}
    .tabtop .active a:hover{color:#fff !important;}
    .tabtop .active a{background-color:#e31837 !important;color:#FFF !important;}
    .margin-tops{margin-top:30px;}
    .tabtop li a:last-child{padding:22px 22px;}
    .thbada{padding:10px 28px !important;}
    .margin-tops4{margin-top:0px;}
    .tabsetting{border-top:0px solid #ebebeb;padding-top:6px;}
    .heading-container p{font-family:'Lato', sans-serif;text-align:center;font-size:16px !important;text-transform:uppercase;}

    .tab-content{
      border: 0px;
    }
    .circle{border-radius: 5px !important;}
  #sect option {
      margin: 40px;
      background: #fff;
      color: #000;
      /*text-shadow: 0 1px 0 rgba(0, 0, 0, 0.4);*/
  }

</style>
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Mentee Profile</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
             <?php foreach($mentee_profile as $profile){ 
                $photo=$profile['menteeProfilePhoto']; if($photo==""){$ppic="defaultimage.png";}else{$ppic=$profile['menteeProfilePhoto'];}?>
                    <a href="<?php echo base_url();?>admin/download_menteephoto/<?php echo $ppic;?>">
                      <div class="col-md-3" style="text-align: center;margin-right: auto">
                        <div class="col-md-12" style="display: inline-block;text-align: center">
                            <img src="<?php echo base_url();echo 'uploads/profile_photos/mentees/'.$ppic?>" alt="PPIC" class="img-rounded img-responsive" />
                            <b><p style="color: #000000;"><br><?php echo $profile['menteeFname']." ".$profile['menteeLname'];?></p></b>
                        </div>
                    </div></a>
                    <div class="col-md-9 text-center">
                            <div class="col-md-6" style="text-align: left">
                                <blockquote >
                                     <p><i class="fa fa-user-circle text-primary fa-1x"></i> <?php echo date_format(date_create($profile['menteeDoB']),"j<\s\up>S</\s\up> M, Y");?>  <span><small style="display: inline">D.O.B </small></cite></span> </p>

                                     <p><i class="fa fa-venus-mars text-success fa-1x"></i> <?php echo $profile['menteeGender'];?>  <span><cite title="when <?php echo $profile['menteeGender'];?> "><small style="display: inline">Gender </small></cite></span> </p>

                                      <p><i class="fa fa-phone text-primary fa-1x"></i> <?php echo $profile['menteePhone'];?>  <span><cite title="<?php echo $profile['menteeFname'];?>'s phone number"><small style="display: inline">Cell Phone  </small></cite></span> </p>

                                      <p><i class="fa fa-envelope text-success fa-1x"></i> <?php echo $profile['menteeEmail'];?>  <span><cite title="<?php echo $profile['menteeFname'];?>'s Email Address"><small style="display: inline">Email  </small></cite></span> </p>

                               
                                   <?php $currentClass=$profile['formCode'];
                                    if($currentClass==""){ $display=" <b class='text-warning'>N/A</b>";}else{$display=$profile['formCode'];}?>

                                     <p><i class="fa fa-line-chart text-primary fa-1x"> </i> <?php echo $display;?>  <span><cite title="<?php echo $profile['menteeFname'];?>'s Current Form"><small style="display: inline">Current Form  </small></cite></span> </p>

                                     <p><i class="fa fa-institution text-success fa-1x"> </i> <?php echo $profile['schoolAlias'];?>  <span><cite title="<?php echo $profile['menteeFname'];?>'s Current School"><small style="display: inline">Current School  </small></cite></span> </p>

                                     <?php  $guardian=$profile['menteeGuardianId'];
                                     if($guardian==""){ $parent="<b class='text-warning'>N/A</b>";}else{$parent=$profile['guardianFname']." ".$profile['guardianLname'];}?>
                                     <p><i class="fa fa-user-circle text-default fa-1x"> </i>  <?php echo $parent;?>  <span><cite title="<?php echo $profile['menteeFname'];?>'s Parent/Guardian"><small style="display: inline"> Parent  </small></cite></span> </p>

                                     <?php  $mentor=$profile['menteeMentorId'];
                                     if($mentor==""){ $mentor_="<b class='text-warning'>N/A</b>";}else{$mentor_=$profile['mentorFname']." ".$profile['mentorLname'];}?>
                                     <p><i class="fa fa-mortar-board text-default fa-1x"> </i>  <?php echo $mentor_;?>  <span><cite title="<?php echo $profile['menteeFname'];?>'s Mentor"><small style="display: inline"> Mentor  </small></cite></span> </p>  
                                </blockquote>
                            <!-- <hr> -->
                            </div>
                    </div>
                    <div class="col-md-12 text-left">
                          <blockquote >
                            <p><b>Call Parent/Guardian on: </b> <?php ?>  <?php echo $profile['guardianPhone1'];?> <i>/</i> <?php echo $profile['guardianPhone2'];?>
                        </blockquote>
                    </div>
                <?php }?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="clearfix"></div>
          <div class="tabbable-panel margin-tops4">
            <div class="hidden-md hidden-lg" style="margin-right: 15px;">
              <div class="form-group">
                <!-- <label for="sect">Select list:</label> -->
                <select class="form-control" id="sect" style="background: #85929E; color: #fff; height: 45px;">
                  <option value="tab_default_1">General Attendance</option>
                  <option value="tab_default_2">Form Attendance</option>
                  <option value="tab_default_3">Exams</option>
                  <option value="tab_default_4">Analysis</option>
                </select>
              </div>
            </div>
            
            <div class="tabbable-line">
                <ul class="nav nav-tabs tabtop  tabsetting hidden-xs hidden-sm" style="border-bottom: none;">
                  <li class="active col-md-3 text-center"> <a href="#tab_default_1" data-toggle="tab" class="circle"> General Attendance</a> </li>
                  <li class="col-md-3 text-center"> <a href="#tab_default_2" data-toggle="tab" class="circle"> Form Attendance </a> </li>
                  <li class="col-md-3 text-center"> <a href="#tab_default_3" data-toggle="tab" class="circle"> Exams </a> </li> 
                  <li class="col-md-3 text-center"> <a href="#tab_default_4" data-toggle="tab" class="circle"> Analysis</a> </li>
                  <!-- <li> <a href="#tab_default_5" data-toggle="tab" class="thbada"> Amazon Product Listing </a> </li> -->
                </ul>
              
                <div class="tab-content margin-tops" id="myTabs">
                    <div class="tab-pane active fade in" id="tab_default_1">
                      <div class="col-md-12">
                          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                               <table  class="table table-striped table-bordered table-hover display responsive" cellspacing="0" width="100%" id="attendancelist">
                                  <thead>
                                      <tr style="background: ;color: #000000;">
                                          <th class="text-left">Record Date</th>
                                          <th class="text-left">Status</th>
                                       </tr>
                                  </thead>
                                  <tbody >
                                     <?php foreach($menteeattendance as $attendance){?>
                                      <tr>
                                          <td class="text-left"><?php  echo date_format(date_create($attendance['attendanceDate']),"D j<\s\up>S</\s\up> M, Y"); ?>
                                          </td>
                                          <td class="text-left">
                                           <?php echo $attendance['attendanceStatus'];?>
                                          </td>
                                            <?php  ?>
                                      </tr>
                                      <?php } ?>
                                  </tbody>
                              </table>
                              <!-- /.table-responsive -->
                          </div>
                      </div>
                    </div>
                    <div class="tab-pane fade" id="tab_default_2">
                      <div class="col-md-12">
                          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php foreach ($forms as $form) {?>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="<?php echo strtolower($form['formCode']);?>">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo strtolower($form['formCode']);?>" aria-expanded="false" aria-controls="collapseTwo">
                                                <?php echo $form['formName'];?> Attendance
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?php echo strtolower($form['formCode']);?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="<?php echo strtolower($form['formCode']);?>">
                                        <div class="panel-body">
                                            <br><br>
                                            <?php $tableId3="table_".strtolower($form['formCode']);?>
                                            <table  class="table table-striped table-bordered table-hover display responsive" cellspacing="0" width="100%" id="<?php echo $tableId3;?>">
                                                <thead>
                                                    <tr style="background: ;color: #000000;">
                                                        <th class="text-left">Record Date</th>
                                                        <th class="text-center">Status</th>
                                                     </tr>
                                                </thead>
                                                <tbody >
                                                   <?php if(isset($form['performattendancedates'])){foreach($form['performattendancedates'] as $attendance){?>
                                                    <tr>
                                                        <td class="text-left"><?php  echo date_format(date_create($attendance['attendanceDate']),"D j<\s\up>S</\s\up> M, Y"); ?>
                                                        </td>
                                                        <td class="text-center">
                                                         <?php echo $attendance['attendanceStatus'];?>
                                                        </td>
                                                          <?php  ?>
                                                    </tr>
                                                    <?php }} ?>
                                                </tbody>
                                            </table>
                                            <!-- /.table-responsive -->
                                            <?php  echo  "<script>
                                                $(document).ready(function () { 
                                                    $('#";echo $tableId3."').dataTable({'responsive': true,'iDisplayLength': 50,'lengthMenu': [[50, 200, 500, -1], [50, 200, 500, 'All']],responsive:true, 'aoColumnDefs': [{ 'aTargets': [1], 'orderable': false}],'aaSorting': [],dom: 'lBfrtip', buttons: [{extend: 'print',title:'',exportOptions: {columns:[0,1]   } },{extend: 'excel',title:'',exportOptions: {columns:[0,1]   } },{extend: 'pdf',title:'',exportOptions: {columns:[0,1]   } }] }); 
                                                });//close document.ready

                                            </script>";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                          </div>
                      </div><!--- END COL -->     
                    </div>
                     <!-- /////////// -->
                    <div class="tab-pane fade" id="tab_default_3">
                      <div class="col-md-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            
                          </div>
                        </div><!--- END COL -->     
                      </div>
                      <div class="tab-pane fade" id="tab_default_4">
                        <div class="col-md-12">
                            
                        </div>
                      </div>
                    <!-- <div class="tab-pane fade" id="tab_default_5">
                      <div class="col-md-12">
                          
                       </div>
                    </div> -->
                </div>
              </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
  $('#attendancelist').dataTable({'responsive': true,'iDisplayLength': 50,'lengthMenu': [[50, 200, 500, -1], [50, 200, 500, 'All']],responsive:true, 'aoColumnDefs': [{ 'aTargets': [1], 'orderable': false}],'aaSorting': [],dom: 'lBfrtip', buttons: [{extend: 'print',title:'',exportOptions: {columns:[0,1]   } },{extend: 'excel',title:'',exportOptions: {columns:[0,1]   } },{extend: 'pdf',title:'',exportOptions: {columns:[0,1]   } }] });
</script>
</body>
</html>
