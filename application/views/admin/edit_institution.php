<!DOCTYPE html>
<html>

<head>
    <title>Macheo | Edit Institution Profile</title>
    <?php $this->load->view('headerlinks/headerlinks.php'); ?>
    <style>
        @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,700,300);
        body {
            /*font: 12px 'Open Sans';*/
        }

    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
    <div class="wrapper">
        <?php $this->load->view('admin/adminnav.php'); ?>
        <!--navigation -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="row">
                    <div class="col-lg-12 ">
                        <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Edit Institution Profile</h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <?php if(isset($_SESSION['msg']))
                                  {
                                    $msg = $_SESSION['msg'];
                                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                                    <div class="messagebox alert alert-danger" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <i class="fa fa-close"></i>
                                          <strong><span>';echo $msg['error']; echo '</span></strong>
                                      </div> 
                                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                                    <div class="messagebox alert alert-success" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <i class="fa fa-check-circle-o"></i>
                                          <strong><span>';echo $msg['success'];echo '</span></strong>
                                      </div> 
                                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                                <?php foreach($institution_profile as $profile){
                  echo form_open_multipart('admin/updateinstitution',array('id' => 'institution_update','method'=>'post','name'=>'institutionupdate'));
                    }
                   ?>
                                <div class="row setup-content">
                                    <div class="col-xs-12">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                                <label for="institutionUID" class="control-label">Institution UID <span class="star">*</span></label>
                                                <input type="number" name="institutionUID" placeholder="" class=" form-control" id="institutionUID" required="required" maxlength="20" value=<?php echo '"'.$profile[ 'institutionAutoId']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="institutionName" class="control-label">Institution Name <span class="star">*</span></label>
                                                <input type="text" name="institutionName" placeholder="" class=" form-control" id="institutionName" required="required" maxlength="100" value=<?php echo '"'.$profile[ 'institutionName']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="institutionLocation" class="control-label">Institution Location <span class="star">*</span></label>
                                                <input type="text" name="institutionLocation" placeholder="" class=" form-control" id="institutionLocation" required="required" maxlength="100" value=<?php echo '"'.$profile[ 'institutionLocation']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="institutionAlias" class="control-label">Institution Alias</label>
                                                <input type="text" name="institutionAlias" placeholder="" class=" form-control" id="institutionAlias" maxlength="20" value=<?php echo '"'.$profile[ 'institutionAlias']. '"';?>>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group col-md-6 col-lg-6">
                                                    <input type="submit" class="btn btn-warning" value="Update">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/.col-xs-12-->
                                </div>
                                <!--/.setup-content-->
                                <?php echo form_close();?>
                                <?php ?>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('footer');?>
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
    <script>
        $(document).ready(function() {
            var submitBtn = $('input[type="submit"]');
            // allWells.show();
            submitBtn.click(function() {
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    curInputs = curStep.find("input,select"),
                    isValid = true;
                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }
                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });


            var brand = document.getElementById('photo-id');
            brand.className = 'attachment_upload';
            brand.onchange = function() {
                document.getElementById('fakeUploadLogo').value = this.value.substring(12);
            };

            // Source: http://stackoverflow.com/a/4459419/6396981
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('.img-preview').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#photo-id").change(function() {
                readURL(this);
            });
        });
        //to refresh the page
        $("#refresh").click(function(event) {
            window.setTimeout(function() {
                location.reload()
            }, 1)

        });

    </script>
</body>

</html>
