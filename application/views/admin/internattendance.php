<!DOCTYPE html>
<html>
<head>
  <title>Macheo | Attendance</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav.php'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="row">
          <div class="col-lg-12 ">
              <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Interns Attendance</h4>
              <div class="pull-right">
                <span data-placement="top" data-toggle="tooltip" title="Refresh">
                    <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                </span>
                <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-xs" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>&nbsp;Print All</a>
                </span>
              </div> 
          </div>
          <!-- /.col-lg-12 -->
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
              <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <div class="box box-solid collapsed-box" style="background:lightgrey">
                    <div class="box-header">
                        <h3 class="box-title" style="color: #21618C;" >New Attendance</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                        </div>
                    </div>
                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                        <?php echo form_open('admin/newinternattendance',array('id' => 'newinternattendance','method'=>'post'));?>
                      
                        <div class="row setup-content">
                            <div class="col-xs-6 col-md-6">
                                <div class="form-group col-md-12 col-lg-12">
                                    <label for="attendanceDate" class="control-label">Attendance Date</label>
                                    <div class="form-group">
                                        <div class='input-group date' id='attendanceDate' >
                                            <input type='text' class="form-control" readonly="true" name="attendanceDate" style="background-color: #FFFFFF;" />
                                            <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group col-md-12 col-lg-12">
                                    <label for="eventName" class="control-label">Event<span class="star">*</span></label>
                                    <input type="text" name="eventName" placeholder="" class=" form-control" id="eventName" required="required" maxlength="50">
                                </div>
                            </div>
                        </div>
                         
                        <table  class="table table-striped table-bordered table-hover display responsive" cellspacing="0" width="100%" id="attendancelist">
                          <thead>
                              <tr style="background: #2E4053;color: #F7F9F9;">
                                  <th class="text-left">intern Name</th>
                                  <th class="text-center">PRESENT</th>
                                  <th class="text-center">ABSENT</th>
                                  <th class="text-center">EXCUSED</th>
                               </tr>
                          </thead>
                          <tbody>
                             <?php $count=0; foreach($interns as $intern){?>
                              <tr>
                                  <td class="text-left">
                                    <?php  echo $intern['internFname']. " ".$intern['internLname'].'
                                    <input type="number" class="form-control text-center" name="intern['.$count.'][internId]" style="width:100%!important;display: none;" value="'.$intern['internAutoId'].'">
                                  </td>

                                  <td class="text-center"><span class="text-success">PRESENT <input  type="radio" name="intern['.$count.'][status]" value="PRESENT" ></span> &nbsp;&nbsp;</td>

                                  <td class="text-center"><span class="text-danger">ABSENT <input type="radio" name="intern['.$count.'][status]" value="ABSENT"></span>&nbsp;&nbsp;</td>

                                  <td class="text-center"><span class="text-info">EXCUSED <input type="radio" name="intern['.$count.'][status]" value="EXCUSED"></span> &nbsp;&nbsp;</td>';?>
                              </tr>
                              <?php $count=$count +1;} ?>
                          </tbody>
                      </table>
                      <br>
                      <input type="submit" class="btn btn-primary" name="submit" value="Submit" id="submit">
                      <?php echo form_close(); ?>
                      <!-- /.table-responsive -->
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
               <table  class="table table-striped table-bordered table-hover display responsive" cellspacing="0" width="100%" id="internslist">
                  <thead>
                      <tr style="background: #2E4053;color: #F7F9F9;">
                          <th class="text-left">Record Date</th>
                          <th class="text-center">Present</th>
                          <th class="text-center">Absent</th>
                          <th class="text-center">Excused</th>
                          <th class="text-center">Total</th>
                          <th class="text-center"><i class="fa fa-cog"></i></th>
                       </tr>
                  </thead>
                  <tbody >
                     <?php foreach($internattendance as $attendance){ 
                         ?>
                      <tr>
                          <td class="text-left"><?php  echo date_format(date_create($attendance['attendanceDate']),"D j<\s\up>S</\s\up> M, Y"); ?></td>
                          <td class="text-center"><?php echo $attendance['countPresent']; echo " (".number_format(($attendance['countPresent']/$attendance['countAll'])*100, 1 )."%)"; ?></td>

                          <td class="text-center"><span class="text-danger"><?php echo $attendance['countAbsent']; echo " (".number_format(($attendance['countAbsent']/$attendance['countAll'])*100, 1 )."%)";?></td>

                          <td class="text-center"><span class="text-warning"><?php echo $attendance['countExcused']; echo " (".number_format(($attendance['countExcused']/$attendance['countAll'])*100, 1 )."%)";?></td>
                          <td class="text-center"><span class="text-info"><?php echo $attendance['countAll']; ?></td>

                          <td class="text-center"><i class="fa fa-eye"> View</td>
                      </tr>
                      <?php } ?>
                  </tbody>
              </table>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>

<script>
$(document).ready(function () {
    //datatable initialization
    $('#attendancelist').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],"aoColumnDefs": [{ "aTargets": [1,2,3], "orderable": false},{ "aTargets":1,responsivePriority:1}] });
    $('#internslist').dataTable({responsive:true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],"aoColumnDefs": [{ "aTargets": [5], "orderable": false},{ "aTargets":1,responsivePriority:1}] });
    $(function() {$('#attendanceDate').datepicker({format: "yyyy-mm-dd", minDate: new Date(), todayHighlight: true });});
});
//to refresh the page
$("#refresh").click( function(event){window.setTimeout(function(){location.reload()},1)});
</script>
</body>
</html>
