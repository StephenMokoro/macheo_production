<!DOCTYPE html>
<html>
<head>
  <title>Macheo | Exam Information</title>
  <?php $this->load->view('headerlinks/headerlinks.php'); ?>
  <style>
    .tabtop li a{font-family:'Lato', sans-serif;font-weight:700;color:#1b2834;border-radius:0px;margin-right:22.008px;border:0px solid #ebebeb !important;background-color: #D7DBDD;}
    .tabtop .active a:before{content:"♦";position:absolute;top:40px;left:110px;color:#e31837;font-size:30px;}
    .tabtop li a:hover{color:#000 !important;text-decoration:none;background-color: #ECF0F1;}
    .tabtop .active a:hover{color:#fff !important;}
    .tabtop .active a{background-color:#e31837 !important;color:#FFF !important;}
    .margin-tops{margin-top:30px;}
    .tabtop li a:last-child{padding:22px 22px;}
    .thbada{padding:10px 28px !important;}
    .margin-tops4{margin-top:20px;}
    .tabsetting{border-top:0px solid #ebebeb;padding-top:6px;}
    .heading-container p{font-family:'Lato', sans-serif;text-align:center;font-size:16px !important;text-transform:uppercase;}

    .tab-content{
      border: 0px;
    }
    .circle{border-radius: 5px !important;}
  #sect option {
      margin: 40px;
      background: #fff;
      color: #000;
      /*text-shadow: 0 1px 0 rgba(0, 0, 0, 0.4);*/
  }

</style>
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav.php'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
              <div class="row">
                <div class="col-md-12" style="margin-top: 0px;">
                    <h4 class="pull-left">Extras <span class="fa fa-angle-right"></span> Schools <span class="fa fa-angle-right"></span> Info</h4>
                    <div class="pull-right">
                    </div> 
                </div>
              </div><!--/.row-->
              <div class="row">
                <div class="col-sm-12">
                    
        <div class="clearfix"></div>
        <div class="tabbable-panel margin-tops4">
          <div class="hidden-md hidden-lg" style="margin-right: 15px;">
            <div class="form-group">
              <!-- <label for="sect">Select list:</label> -->
              <select class="form-control" id="sect" style="background: #85929E; color: #fff; height: 45px;">
                <option value="tab_default_1">Per Subject</option>
                  <option value="tab_default_2">Per Mentee</option>
                  <option value="tab_default_3">Per School</option>
                  <option value="tab_default_4">Analysis</option>
              </select>
            </div>
          </div>
          
          <div class="tabbable-line">
              <ul class="nav nav-tabs tabtop  tabsetting hidden-xs hidden-sm" style="border-bottom: none;">
                <li class="active col-md-3 text-center"> <a href="#tab_default_1" data-toggle="tab" class="circle"> Per Subject </a> </li>
                <li class="col-md-3 text-center"> <a href="#tab_default_2" data-toggle="tab" class="circle"> Per Mentee </a> </li>
                <li class="col-md-3 text-center"> <a href="#tab_default_3" data-toggle="tab" class="circle"> Per School </a> </li> 
                <li class="col-md-3 text-center"> <a href="#tab_default_4" data-toggle="tab" class="circle"> Analysis</a> </li>
                <!-- <li> <a href="#tab_default_5" data-toggle="tab" class="thbada"> Amazon Product Listing </a> </li> -->
              </ul>
            
              <div class="tab-content margin-tops" id="myTabs">
                  <div class="tab-pane active fade in" id="tab_default_1">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                           <table  class="table table-striped table-bordered table-hover display responsive" cellspacing="0" width="100%" id="subjectGrades">
                                <thead>
                                    <tr style="background: ;color: #000000;">
                                        <th class="text-left" >SUB</th>
                                        <th class="text-center" >AVR</th>
                                        <th class="text-center" >A</th>
                                        <th class="text-center" >A-</th>
                                        <th class="text-center" >B+</th>
                                        <th class="text-center" >B</th>
                                        <th class="text-center" >B-</th>
                                        <th class="text-center" >C+</th>
                                        <th class="text-center" >C</th>
                                        <th class="text-center" >C-</th>
                                        <th class="text-center" >D+</th>
                                        <th class="text-center" >D</th>
                                        <th class="text-center" >D-</th>
                                        <th class="text-center" >E</th>
                                    </tr>
                                </thead>
                                <tbody >
                                     <?php foreach($substaken as $substaken){?>
                                   <tr>
                                      <td class="text-left"> <?php  echo $substaken['subjectCode'];?></td>
                                      <td class="text-center" >
                                        <?php  echo $substaken['average'];?>
                                      </td>
                                      <td class="text-center" >
                                        <?php  echo $substaken['grade_A_Plain'];?>
                                      </td>
                                       <td class="text-center" >
                                        <?php  echo $substaken['grade_A_Minus'];?>
                                      </td>
                                       <td class="text-center" >
                                        <?php  echo $substaken['grade_B_Plus'];?>
                                      </td>
                                       <td class="text-center" >
                                        <?php  echo $substaken['grade_B_Plain'];?>
                                      </td>
                                       <td class="text-center" >
                                        <?php  echo $substaken['grade_B_Minus'];?>
                                      </td>
                                       <td class="text-center" >
                                        <?php  echo $substaken['grade_C_Plus'];?>
                                      </td>
                                       <td class="text-center" >
                                        <?php  echo $substaken['grade_C_Plain'];?>
                                      </td>
                                       <td class="text-center" >
                                        <?php  echo $substaken['grade_C_Minus'];?>
                                      </td>
                                       <td class="text-center" >
                                        <?php  echo $substaken['grade_D_Plus'];?>
                                      </td>
                                       <td class="text-center" >
                                        <?php  echo $substaken['grade_D_Plain'];?>
                                      </td>
                                       <td class="text-center" >
                                        <?php  echo $substaken['grade_D_Minus'];?>
                                      </td>
                                       <td class="text-center" >
                                        <?php  echo $substaken['grade_E'];?>
                                      </td>
                                  </tr>
                                  <?php } ?>
                                </tbody>
                            </table><!-- /.table-responsive -->
                        </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab_default_2">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                          <table  class="table display responsive nowrap" cellspacing="0" width="100%" id="menteeslist"  >
                            <thead>
                                <tr style="background: #2E4053;color: #F7F9F9  ;" hidden="true">
                                    <th class="text-center pull-left" >Sort Mentees &nbsp;&nbsp; </th>
                                 </tr>
                            </thead>
                            <tbody style="color: #17202A ;">
                                <?php  foreach($mentees as $mentee){?>
                               <tr >
                                  <td style="margin: 0px!important;padding: 0px!important;border: none;">
                                    <div class="panel panel-default">
                                      <div class="panel-heading" role="tab" id="mentee<?php echo $mentee['menteeAutoId'];?>">
                                          <h4 class="panel-title">
                                              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $mentee['menteeAutoId'];?>" aria-expanded="false" aria-controls="collapseTwo" style="font-weight: normal;">
                                                  <?php echo $mentee['menteeFname']." ".$mentee['menteeLname']; ?>
                                              </a>
                                          </h4>
                                      </div>
                                      <div id="collapse_<?php echo $mentee['menteeAutoId'];?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="mentee<?php echo $mentee['menteeAutoId'];?>">
                                        <div class="panel-body">
                                          <?php $tableId="table_".$mentee['menteeAutoId'];?>
                                            <div class="form-group col-md-4 col-lg-4" style="display: none;">
                                               <input type="number" class=" form-control text-center" required="required" name="menteeId" value="<?php  echo $mentee['menteeAutoId']; ?>">
                                            </div>
                                            <div class="row">
                                               <table class="table table-bordred table-striped table-hover" cellspacing="0" width="100%" style="background-color: #A2D9CE;" id="<?php echo $tableId;?>">
                                                 <thead style="background-color: #154360;color: #FFFFFF;">
                                                      <th class="text-left">SUB</th>
                                                      <th class="text-center" >SCORE</th>
                                                      <th class="text-center" >GRADE</th>
                                                      <th class="text-center" >POS</th>
                                                  </thead>
                                                  <tbody>
                                                       <?php foreach($mentee['subjects'] as $subject){?>
                                                          <tr>
                                                              <td class="text-left" style="width: 20%;"> <?php  echo $subject['subjectCode'];?></td>
                                                              <td class="text-center" >
                                                                  <?php  echo $subject['score'];?>
                                                              </td>
                                                              <td class="text-center" >
                                                                  <?php  echo $subject['grade'];?>
                                                              </td>
                                                              <td class="text-center" >

                                                                  <?php 
                                                                  foreach($subject['scoreentries'] as $key=>$array)
                                                                    {
                                                                     if(in_array($subject['score'],$array))
                                                                     {
                                                                      $position=$key+1;
                                                                       echo $position.$subject['countofscoreentries'];
                                                                       break;
                                                                     }
                                                                    }?>
                                                              </td>
                                                          </tr>
                                                      <?php } ?>
                                                  </tbody>
                                              </table>
                                              <?php  echo  "<script>
                                              $(document).ready(function () { 
                                                  $('#";echo $tableId."').dataTable({'responsive': true,paging:false,'aaSorting': [],dom: 'lBfrtip', buttons: [{extend: 'print',title:''},{extend: 'excel',title:'' },{extend: 'pdf',title:''}] }); 
                                              });//close document.ready

                                            </script>";?>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                                </td>
                              </tr>
                              <?php }?>
                            </tbody>
                          </table>
                        </div>
                    </div><!--- END COL -->     
                  </div>
                  <div class="tab-pane fade" id="tab_default_3">
                    <div class="col-md-12">
                      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                          <table  class="table display responsive nowrap" cellspacing="0" width="100%" id="schoolslist"  >
                            <thead>
                                <tr style="background: #2E4053;color: #F7F9F9  ;" hidden="true">
                                    <th class="text-center pull-left" >Sort Schools &nbsp;&nbsp; </th>
                                 </tr>
                            </thead>
                            <tbody style="color: #17202A ;">
                                <?php  foreach($schools as $school){?>
                               <tr >
                                  <td style="margin: 0px!important;padding: 0px!important;border: none;">
                                    <div class="panel panel-default">
                                      <div class="panel-heading" role="tab" id="school<?php echo $school['schoolAutoId'];?>">
                                          <h4 class="panel-title">
                                              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $school['schoolAutoId'];?>" aria-expanded="false" aria-controls="collapseTwo" style="font-weight: normal;">
                                                  <?php echo $school['schoolName']; ?>
                                              </a>
                                          </h4>
                                      </div>
                                      <div id="collapse_<?php echo $school['schoolAutoId'];?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="school<?php echo $school['schoolAutoId'];?>">
                                        <div class="panel-body">
                                          <?php $tableId2="schtable_".$school['schoolAutoId'];?>
                                            <table class="table table-bordred table-striped table-hover" cellspacing="0" width="100%" style="background-color: #A2D9CE;" id="<?php echo $tableId2;?>">
                                                 <thead style="background-color: #154360;color: #FFFFFF;">
                                                  <th class="text-center"></th>
                                                   <?php foreach($school['subjects'] as $subject){?>
                                                      <th class="text-center">
                                                        <?php echo $subject['subjectCode']; ?>
                                                      </th>
                                                    <?php } ?>
                                                  </thead>
                                                  <tbody>
                                                    <?php $counter=0; ?>
                                                    <?php foreach($gradesArray as $grade){?>
                                                        <tr>
                                                            <td class="text-center">
                                                                <b><?php  echo $gradesShort[$counter];?></b>
                                                            </td>
                                                            <?php foreach($school['subjects'] as $subject){?>
                                                                <td class="text-center">
                                                                  <?php  echo $subject[$grade];?>
                                                                </td>
                                                            <?php } ?>
                                                        </tr>
                                                    <?php $counter=$counter+1; } ?>
                                                       
                                                  </tbody>
                                              </table>
                                              <?php  echo  "<script>
                                              $(document).ready(function () { 
                                                  $('#";echo $tableId2."').dataTable({'responsive': true,paging:false,'aaSorting': [],dom: 'lBfrtip', buttons: [{extend: 'print',title:''},{extend: 'excel',title:'' },{extend: 'pdf',title:''}] }); 
                                              });//close document.ready

                                            </script>";?>
                                        </div>
                                      </div>
                                  </div>
                                </td>
                              </tr>
                              <?php }?>
                            </tbody>
                          </table>
                        </div>
                      </div><!--- END COL -->     
                    </div>
                    <div class="tab-pane fade" id="tab_default_4">
                      <div class="col-md-12">
                          
                      </div>
                    </div>
                  <!-- <div class="tab-pane fade" id="tab_default_5">
                    <div class="col-md-12">
                        
                     </div>
                  </div> -->
              </div>
            </div>
        </div>

                </div>
            </div>


            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div> <!-- /.col -->
      </div><!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
  (function () {var previous; $("#sect").on('focus', function () {/*Store the current value on focus and on change*/ previous = this.value;}).change(function() {/* Remove class "active" and "in" from previous to make it inactive*/ $("#"+previous).removeClass('active in'); /* Make sure the previous value is updated*/previous = this.value;/*add class "active" and "in" to the current to make it active */$("#"+previous).addClass('active in');});})();
$(document).ready(function () {
       $('#subjectGrades').dataTable({'responsive': true,paging:false,'aaSorting': [],dom: 'lBfrtip', buttons: [{extend: 'print',title:''},{extend: 'excel',title:'' },{extend: 'pdf',title:''}]  });
       $('#menteeslist').dataTable({'responsive': true,'iDisplayLength': 50,'lengthMenu': [[50, 200, 500, -1], [50, 200, 500, 'All']],'aaSorting': [] }); 
       $('#schoolslist').dataTable({'responsive': true,'iDisplayLength': 50,'lengthMenu': [[50, 200, 500, -1], [50, 200, 500, 'All']],'aaSorting': [] }); 
});

//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
