<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Macheo - Add Form 2 Student</title>
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('admin/adminnav.php'); ?><!--navigation -->
    <div id="page-wrapper">
        <div class="row ">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-right"></span> Add Student to Form</h4>
                <hr>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <br><br>
        <form role="form" method="post" action="<?php echo base_url(); ?>admin/addf2stud">
            <div class="row setup-content" >
                <div class="col-xs-12">
                    <div class="col-md-12">
                        <div class="form-group col-md-12 col-lg-12">
                            <label for="formId" class="control-label green">Form</label>
                            <select type="text" name="formId" placeholder="" class="form-control" id="formId" required="true">
                                <option value="">--Select Form--</option>
                                <option value="2">Form 2</option>
                                <option value="3">Form 3</option>
                                <option value="4">Form 4</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12 col-lg-12">
                            <label for="studentId" class="control-label green">Student</label>
                            <select type="text" name="studentId" placeholder="" class="form-control" id="studentId" required="true"></select>
                        </div>
                        <div class="form-group col-md-12 col-lg-12">
                        <div class="modal-header"></div>
                            <br>
                            <input type="submit" class="btn btn-primary" value="Add to Form">
                            <input type="reset" class="btn btn-default" value="Reset">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script src="<?php echo base_url();?>assets/select2/js/select2.min.js"></script>
<script>
$(document).ready(function () {
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

//autocomplete for mentor to visit
      $('#studentId').select2({
        placeholder: '--- Select Student ---',
        ajax: {
          url: "<?php echo base_url('admin/getStudent'); ?>",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });

});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
