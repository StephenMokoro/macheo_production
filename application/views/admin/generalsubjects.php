<!DOCTYPE html>
<html>

<head>

    <title>Macheo | General Subjects</title>
    <?php $this->load->view('headerlinks/headerlinks.php'); ?>

</head>

<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;">
    <div class="wrapper">
        <?php $this->load->view('admin/adminnav.php'); ?>
        <!--navigation -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="row">
                    <div class="col-lg-12 ">
                        <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> General Subjects</h4>
                        <div class="pull-right">
                            <span data-placement="top" data-toggle="tooltip" title="Add Subject">
                    <button class="btn btn-info btn-xs" data-title="Add Subject" data-toggle="modal" data-target="#subject-registration-modal" ><span class="fa fa-plus-circle"></span>&nbsp;SUBJECT</button>
                            </span>
                            <span data-placement="top" data-toggle="tooltip" title="Refresh">
                    <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                            </span>
                            <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-xs" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>&nbsp;Print All</a>
                            </span>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <?php if(isset($_SESSION['msg']))
                                  {
                                    $msg = $_SESSION['msg'];
                                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                                    <div class="messagebox alert alert-danger" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <i class="fa fa-close"></i>
                                          <strong><span>';echo $msg['error']; echo '</span></strong>
                                      </div> 
                                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                                    <div class="messagebox alert alert-success" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <i class="fa fa-check-circle-o"></i>
                                          <strong><span>';echo $msg['success'];echo '</span></strong>
                                      </div> 
                                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                                <table class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="subjects">
                                    <thead>
                                        <tr style="background: #2E4053;color: #F7F9F9;">
                                            <th class="text-center">Subject Name</th>
                                            <th class="text-center">Subject Code</th>
                                            <th class="text-center"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php  foreach($subjects as $subject){?>
                                        <tr>
                                            <td class="text-left">
                                                <?php  echo $subject['subjectName'];?>
                                            </td>
                                            <td class="text-center">
                                                <?php  echo $subject['subjectCode'];  ?>
                                            </td>
                                            <td class="text-center">
                                                <button class="btn btn-danger btn-xs" data-title="Disable" id=<?php echo '"disable_'. $subject[ 'subjectAutoId']. '"'; ?> name=<?php echo '"disable_'. $subject['subjectAutoId'].'"';?> value=<?php echo '"'.$subject['subjectAutoId'].'"';  ?> onclick="disstudId(this);"><i class="fa fa-lock"></i> Lock Subject
                                                </button>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <!-- /.table-responsive -->

                                <!-- subject-registration-modal -->
                                <div id="subject-registration-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-full">
                                        <div class="modal-content">
                                            <div class="modal-header" style="border-bottom: none!important;margin-bottom: -20px">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="full-width-modalLabel"><strong style="color: darkred">Subject Registration</strong></h4>
                                            </div>
                                            <div class="modal-body">
                                                <form role="form" id="institution_registration" method="post" action="<?php echo base_url(); ?>admin/rteacher">
                                                    <div class="row setup-content">
                                                        <div class="col-xs-12">
                                                            <div class="col-md-12">
                                                                <div class="form-group col-md-6 col-lg-6">
                                                                    <label for="subjectName" class="control-label">Subject Name <span class="star">*</span></label>
                                                                    <input type="text" name="subjectName" class=" form-control" id="subjectName" required="required">
                                                                </div>
                                                                <div class="form-group col-md-6 col-lg-6">
                                                                    <label for="subjectCode" class="control-label">Subject Code <span class="star">*</span></label>
                                                                    <input type="text" name="subjectCode" class=" form-control" id="subjectCode" required="required">
                                                                </div>
                                                                <div class="form-group col-md-12 col-lg-12">
                                                                    <div class="modal-header"></div>
                                                                    <br>
                                                                    <input type="submit" class="btn btn-primary" value="Submit">
                                                                    <input type="reset" class="btn btn-default" value="Reset">
                                                                    <input type="button" class="btn btn-danger pull-right" data-dismiss="modal" value="Close">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.Subject-registration-modal -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('footer');?>
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
    <script>
        $(document).ready(function() {
            //datatable initialization
            $('#subjects').DataTable({responsive: true,paging:false,columnDefs: [{orderable: false,targets: [2]}],"aaSorting": []
            });
            var submitBtn = $('input[type="submit"]');
            // allWells.show();
            submitBtn.click(function() {
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    curInputs = curStep.find("input,select"),
                    isValid = true;
                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }
                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });
        });
        //to refresh the page
        $("#refresh").click(function(event) {
            window.setTimeout(function() {
                location.reload()
            }, 1)

        });

    </script>
</body>

</html>
