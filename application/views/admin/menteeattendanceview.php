<!DOCTYPE html>
<html>

<head>
    <title>Macheo | Mentees</title>
    <?php $this->load->view('headerlinks/headerlinks.php'); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
    <div class="wrapper">
        <?php $this->load->view('admin/adminnav.php'); ?>
        <!--navigation -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="row">
                    <div class="col-lg-12 ">
                        <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span>  Mentees' Attendance: <?php echo date_format(date_create($_SESSION['sessdata']['attendanceDate']),"D j<\s\up>S</\s\up> M, Y"); ?> </h4>
                        <div class="pull-right">
                            <span data-placement="top" data-toggle="tooltip" title="Refresh">
                    <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                            </span>
                            <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-xs" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>&nbsp;Print All</a>
                            </span>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <?php if(isset($_SESSION['msg']))
                                  {
                                    $msg = $_SESSION['msg'];
                                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                                    <div class="messagebox alert alert-danger" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <i class="fa fa-close"></i>
                                          <strong><span>';echo $msg['error']; echo '</span></strong>
                                      </div> 
                                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                                    <div class="messagebox alert alert-success" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <i class="fa fa-check-circle-o"></i>
                                          <strong><span>';echo $msg['success'];echo '</span></strong>
                                      </div> 
                                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                                <table class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="menteeslist">
                                    <thead>
                                        <tr style="background: #2E4053;color: #F7F9F9;">
                                            <th class="text-left">Mentee</th>
                                            <th class="text-center">School</th>
                                            <th class="text-left">Status</th>
                                            <th class="text-center"><i class="fa fa-cog fa-spin"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody style="color: #17202A;">
                                        <?php  foreach($mentees as $mentee){?>
                                        <tr>
                                            <?php $photo=$mentee['menteeProfilePhoto']; if($photo==""){$profile="defaultimage.png";}else{$profile=$mentee['menteeProfilePhoto'];}?>
                                            <td class="text-left"><img src="<?php echo base_url();echo 'uploads/profile_photos/mentees/'.$profile?>" width="25" height="25" class="img-circle" alt="User Image">
                                                <?php  echo $mentee['menteeFname']. " ".$mentee['menteeLname']; ?>
                                            </td>
                                             <td class="text-center">
                                                <?php  echo $mentee['schoolAlias']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php  echo $mentee['attendanceStatus']; ?>
                                            </td>
                                            <td class="text-center">

                                                <button class="btn btn-default btn-s" data-placement="top" data-toggle="tooltip" data-title="Edit Record" title="Edit Record" id=<?php echo '"edit_'. $mentee[ 'attendanceAutoId']. '"'; ?> name=<?php echo '"edit_'. $mentee['attendanceAutoId'].'"';  ?> value=<?php echo '"'. $mentee['attendanceAutoId'].'"';  ?> onclick="editattendance(this);"><i class="fa fa-edit"> Edit</i> </button>

                                             <button class="btn btn-danger btn-s" data-placement="top" data-toggle="tooltip" data-title="Delete Record" title="Delete Record" id=<?php echo '"del_'. $mentee[ 'attendanceAutoId']. '"'; ?> name=<?php echo '"del_'. $mentee['attendanceAutoId'].'"';  ?> value=<?php echo '"'. $mentee['attendanceAutoId'].'"';  ?> onclick="delattendance(this);"><i class="fa fa-trash"> Del</i> </button>

                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <!-- /.table-responsive -->
                                <div class="modal fade" id="updateAttendance">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <form method="post" action="<?php echo base_url(); ?>admin/updatementeeattendance">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="menteeName"></h4>
                                          </div>
                                          <div class="modal-body" id="updatebody">
                                            
                                            
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary  pull-left">Save changes</button>
                                          </div>
                                    </form>
                                    </div>
                                    <!-- /.modal-content -->
                                  </div>
                                  <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                <div class="modal fade" id="delAttendance">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <form method="post" action="<?php echo base_url(); ?>admin/delmenteeattendance">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="menteeName"></h4>
                                          </div>
                                          <div class="modal-body" id="delbody">
                                            
                                            
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-danger  pull-left">Confirm Delete</button>
                                          </div>
                                    </form>
                                    </div>
                                    <!-- /.modal-content -->
                                  </div>
                                  <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('footer');?>
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
    <script>
    $(document).ready(function() {
    //datatable initialization
    $('#menteeslist').DataTable({responsive: true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],responsive:true, columnDefs: [{ orderable: false,targets: [3]}], "aaSorting": []});

    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

});
function editattendance(objButton)
{
    var recordId=objButton.value;
     $.ajax({type:"post", url: "<?php echo base_url(); ?>admin/getmenteeattendance",data:{ recordId:recordId},dataType:'json',success:function(data){$('#updateAttendance #menteeName').text(data.fullName);$('#updateAttendance #updatebody').html(data.status);$('#updateAttendance').modal('toggle');} });
}
function delattendance(objButton)
{
    var recordId=objButton.value;
     $.ajax({type:"post", url: "<?php echo base_url(); ?>admin/getmenteeattendancestate",data:{ recordId:recordId},dataType:'json',success:function(data){$('#delAttendance #menteeName').text(data.fullName);$('#delAttendance #delbody').html(data.status);$('#delAttendance').modal('toggle');} });
}

    </script>
</body>

</html>
