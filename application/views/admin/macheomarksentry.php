<!DOCTYPE html>
<html>
<head>
  <title>Macheo | <?php echo $_SESSION['sessdata']['examFormCode'];?>Marks Entry</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?>
  <link href="<?php echo base_url(); ?>assets/css/collapsible.css" rel="stylesheet" type="text/css" />
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav.php'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="row">
          <div class="col-lg-12 ">
              <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> <?php echo ' <span style="color:#4A235A;"> '.$_SESSION['sessdata']['examInfo'].' </span>';?> Marks Entry</h4>
              <div class="pull-right">
                <span data-placement="top" data-toggle="tooltip" title="Refresh">
                    <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                </span>
                <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-xs" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>&nbsp;Print All</a>
                </span>
              </div> 
          </div>
          <!-- /.col-lg-12 -->
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
                <div class="row">               
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                           Marks Per Subject
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body">
                                    <div class="col-md-12" id="generalsubjects">
                                        <?php foreach($substaken as $subject){?>
                                         <div class="form-group col-md-2 col-lg-2">
                                            <input type="radio" name="subjectId" required="required" value="<?php echo $subject['subjectAutoId'];?>" autocomplete="off"> &nbsp;&nbsp;<label> <?php echo $subject['subjectCode'];?> <span class="star">*</span></label>
                                         </div>
                                         <?php }?>
                                    </div><!--/.col-md-12-->
                                    <div class="modal-header"></div>
                                    <div class="row" id="subjectmentees"></div>
                                  </div><!-- /.panel-body -->
                                </div><!-- /.panel-collapse -->
                              </div><!-- /.panel-default -->
                               <?php if(isset($_SESSION['msg']))
                                {
                                  $msg = $_SESSION['msg'];
                                  $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                                  <div class="messagebox alert alert-danger" style="display: block">
                                    <button type="button" class="close" data-dismiss="alert">*</button>
                                    <div class="cs-text">
                                        <strong><span>';echo $msg['error']; echo '</span></strong>
                                    </div> 
                                  </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                                  <div class="messagebox alert alert-success" style="display: block">
                                    <button type="button" class="close" data-dismiss="alert">*</button>
                                    <div class="cs-text">
                                        <strong><span>';echo $msg['success'];echo '</span></strong>
                                    </div> 
                                    </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                              <table  class="table display responsive nowrap" cellspacing="0" width="100%" id="menteeslist"  >
                                <thead>
                                    <tr style="background: #2E4053;color: #F7F9F9  ;" hidden="true">
                                        <th class="text-center pull-left" >Sort Mentees &nbsp;&nbsp; </th>
                                     </tr>
                                </thead>
                                <tbody style="color: #17202A ;">
                                    <?php  foreach($mentees as $mentee){?>
                                   <tr >
                                      <td style="margin: 0px!important;padding: 0px!important;border: none;">
                                        <div class="panel panel-default">
                                          <div class="panel-heading" role="tab" id="mentee<?php echo $mentee['menteeAutoId'];?>">
                                              <h4 class="panel-title">
                                                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $mentee['menteeAutoId'];?>" aria-expanded="false" aria-controls="collapseTwo" style="font-weight: normal;">
                                                      <?php echo $mentee['menteeFname']." ".$mentee['menteeLname']; ?>
                                                  </a>
                                              </h4>
                                          </div>
                                          <div id="collapse_<?php echo $mentee['menteeAutoId'];?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="mentee<?php echo $mentee['menteeAutoId'];?>">
                                            <div class="panel-body">
                                              <?php $tableId="table_".$mentee['menteeAutoId'];?>
                                              <form method="post" action="<?php echo base_url(); ?>admin/newgensubmarks">
                                                <div class="form-group col-md-4 col-lg-4" style="display: none;">
                                                   <input type="number" class=" form-control text-center" required="required" name="menteeId" value="<?php  echo $mentee['menteeAutoId']; ?>">
                                                </div>
                                                <div class="row">
                                                  <table class="table table-bordred table-striped table-hover" id="list">
                                                     <thead style="background-color:#512E5F;color:#FFFFFF;">
                                                          <th class="col-md-2 col-lg-2" style="padding-left: 20px;">SUBJECT</th>
                                                          <th class="text-left" id="pid">Mentee Id</th>
                                                          <th class="col-md-10 col-lg-10 " style="padding-left: 40px;">SCORE</th>
                                                      </thead>
                                                      <tbody>
                                                         <?php 
                                                          $arrayKey=0;
                                                          foreach($mentee['subjects'] as $subject){?>
                                                            <tr  >
                                                                <td class="text-left col-md-2" style="padding-left: 20px;"> <?php  echo $subject['subjectCode'];?></td>
                                                                <td class="text-left col-md-7" hidden="true"> 
                                                                  <?php echo '<input type="text" class="form-control text-center" name="mentee['.$arrayKey.'][menteeId]"'.' style="width:100%!important;" value="'.$mentee['menteeAutoId'].'">

                                                                   <input type="text" class="form-control text-center" name="mentee['.$arrayKey.'][subjectId]"'.' style="width:100%!important;display:none;" value="'.$subject['subjectAutoId'].'">
                                                                '; ?>
                                                                </td>
                                                                <td class=" tex-left col-md-3" style="padding-left: 20px;">
                                                                   <?php echo '<input type="number" class="form-control text-left" name="mentee['.$arrayKey.'][subjectScore]"'.' style="width:80%;" min="0" max="100" >'; ?>
                                                                </td>
                                                            </tr>

                                                        <?php $arrayKey=$arrayKey+1; } ?>
                                                      </tbody>
                                                  </table>
                                                </div>
                                                <button id="submit" class="btn btn-primary" style="margin-top: 10px" >Submit</button>
                                              </form>
                                            </div>
                                          </div>
                                      </div>
                                    </td>
                                  </tr>
                                  <?php }?>
                                </tbody>
                              </table>
                        </div>
                    </div><!--- END COL -->     
                </div><!--- END ROW -->       
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
       $('#menteeslist').dataTable({'responsive': true,'iDisplayLength': 50,'lengthMenu': [[50, 200, 500, -1], [50, 200, 500, 'All']],'aaSorting': [] }); 

   $(function(){
    var subjectId=$('input[name=subjectId]:checked').val();
    $('input:radio').change(function(){var subjectId=$('input[name=subjectId]:checked').val();$.ajax({type:"post", url: "<?php echo base_url(); ?>admin/gensubjectmentees",data:{subjectId:subjectId},dataType:'json',success:function(data){$("#subjectmentees").html(data);}  }); });
    })
});

//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
