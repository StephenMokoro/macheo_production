<!DOCTYPE html>
<html>
<head>
 
  <title>Macheo | Dashboard</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?>
  
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav.php'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
     <section class="content-header">
      <div class="row">
          <div class="col-lg-12 ">
              <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Home</h4>
              <div class="pull-right">
                <span data-placement="top" data-toggle="tooltip" title="Refresh">
                    <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                </span>
                <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-xs" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>&nbsp;Print All</a>
                </span>
              </div> 
          </div>
          <!-- /.col-lg-12 -->
      </div>
    </section>
     <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
             <!-- Small boxes (Stat box) -->
              <div class="row">
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box btn-primary" style="background-color: #008080; color: #FFFFFF;">
                    <div class="inner">
                      <h3 id="mentees" class="counter"> <?php  echo $menteescount; ?></h3>
                      <p>Current Mentees</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-child" style="color:#D7DBDD; "></i>
                    </div>
                    <a href="<?php echo base_url();?>admin/mentees" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box" style="background-color:#283747;color: #FFFFFF;">
                    <div class="inner">
                      <h3 id="mentors" class="counter"><?php  echo $mentorscount; ?></h3>
                      <p>Current Mentors</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-star-half-o" style="color: #717D7E;"></i>
                    </div>
                    <a href="<?php echo base_url();?>admin/mentors" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box" style="background-color: #641E16;color: #FFFFFF;">
                    <div class="inner">
                      <h3 id="interns" class="counter"><?php  echo $internscount; ?></h3>
                      <p>Current Interns</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-group" style="color: #717D7E;"></i>
                    </div>
                    <a href="<?php echo base_url();?>admin/interns" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box" style="background-color: #0B5345;color: #FFFFFF;">
                    <div class="inner">
                      <h3 id="admins" class="counter"><?php  echo $adminscount; ?></h3>
                      <p>Active Admins</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-user-secret" style="color: #D7DBDD;"></i>
                    </div>
                    <a href="<?php echo base_url();?>admin/admins" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                 <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box" style="background-color: #7D6608;color: #FFFFFF;">
                    <div class="inner">
                      <h3 id="teachers" class="counter"><?php  echo $teacherscount; ?></h3>
                      <p>Teachers</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-user" style="color: #717D7E;"></i>
                    </div>
                    <a href="<?php echo base_url();?>admin/teachers" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box" style="background-color: #B2BABB;color: #273746;">
                    <div class="inner">
                      <h3 id="macheoexams" class="counter"><?php  echo $macheoexamscount; ?></h3>
                      <p>Macheo Exams</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-pie-chart" style="color: #2E4053;"></i>
                    </div>
                    <a href="<?php echo base_url();?>admin/macheoexams" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                 <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box" style="background-color: #1B4F72;color: #FFFFFF;">
                    <div class="inner">
                      <h3 id="generalexams" class="counter"><?php  echo $generalexamscount; ?></h3>
                      <p>General Exams</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-pie-chart" style="color: #D7DBDD;"></i>
                    </div>
                    <a href="<?php echo base_url();?>admin/generalexams" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                 <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box" style="background-color: #138D75;color: #FFFFFF;">
                    <div class="inner">
                      <h3 id="attendance" class="counter">_</h3>
                      <p>Attendance Records</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-group" style="color: #D7DBDD;"></i>
                    </div>
                    <a href="#" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script src="<?php echo base_url();?>assets/jquery/jquery-1.10.2/jquery.min.js"></script> 
<script src="<?php echo base_url();?>assets/waypoints/2.0.3/waypoints.min.js"></script>
<script src="<?php echo base_url();?>assets/counterup/jquery.counterup.min.js"></script>

<script>

$(document).ready(function () {
     //counter on dashboard
     $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
