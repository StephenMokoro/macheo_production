<!DOCTYPE html>
<html>
<head>
    <title>Macheo | Institutions</title>
    <?php $this->load->view('headerlinks/headerlinks.php'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
    <div class="wrapper">
        <?php $this->load->view('admin/adminnav.php'); ?>
        <!--navigation -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="row">
                    <div class="col-lg-12 ">
                        <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span>Mentor Institutions</h4>
                        <div class="pull-right">
                            <span data-placement="top" data-toggle="tooltip" title="Refresh">
                    <button class="btn btn-s" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                            </span>
                            <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-s" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>&nbsp;Print All</a>
                            </span>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <div class="box box-solid collapsed-box" style="background:lightgrey">
                                    <div class="box-header">
                                        <h3 class="box-title" style="color: #21618C;">New Institution</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                                        </div>
                                    </div>
                                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                                        <?php echo form_open_multipart('admin/newinstitutions',array('id' => 'institution_registration','method'=>'post'));?>
                                        <div class="row setup-content">
                                            <div class="col-xs-12">
                                                <div class="col-md-6">
                                                    <div class="form-group col-md-12 col-lg-12">
                                                        <label for="institutionName" class="control-label">Institution Name <span class="star">*</span></label>
                                                        <input type="text" name="institutionName" placeholder="" class=" form-control" id="institutionName" required="required" maxlength="50">
                                                    </div>
                                                    <div class="form-group col-md-12 col-lg-12">
                                                        <label for="institutionLocation" class="control-label">Institution Location <span class="star">*</span></label>
                                                        <input type="text" name="institutionLocation" placeholder="" class=" form-control" id="institutionLocation" required="required" maxlength="50">
                                                    </div>
                                                    <div class="form-group col-md-12 col-lg-12">
                                                        <label for="institutionAlias" class="control-label">Institution Alias</label>
                                                        <input type="text" name="institutionAlias" placeholder="" class=" form-control" id="institutionAlias" maxlength="10">
                                                    </div>
                                                    <div class="form-group col-md-6 col-lg-6">
                                                        <input type="submit" class="btn btn-primary" value="Submit">
                                                        <input type="reset" class="btn btn-default" value="Reset">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.col-xs-12-->
                                        </div>
                                        <!--/.setup-content-->
                                        <?php echo form_close();?>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->

                                <?php if(isset($_SESSION['msg']))
                                  {
                                    $msg = $_SESSION['msg'];
                                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                                    <div class="messagebox alert alert-danger" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <i class="fa fa-close"></i>
                                          <strong><span>';echo $msg['error']; echo '</span></strong>
                                      </div> 
                                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                                    <div class="messagebox alert alert-success" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <i class="fa fa-check-circle-o"></i>
                                          <strong><span>';echo $msg['success'];echo '</span></strong>
                                      </div> 
                                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                                <table class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="institutionlist">
                                    <thead>
                                        <tr style="background: #2E4053;color: #F7F9F9;">
                                            <th class="text-center">Institution Name</th>
                                            <th class="text-center">Institution Location</th>
                                            <th class="text-center">Institution Alias</th>
                                            <th class="text-center"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php  foreach($mentorinst as $mentorinst){ ?>
                                        <tr>
                                            <td class="text-left">
                                                <?php  echo $mentorinst['institutionName'];?>
                                            </td>
                                            <td class="text-center">
                                                <?php  echo $mentorinst['institutionLocation'];  ?>
                                            </td>
                                            <td class="text-center">
                                                <?php  echo $mentorinst['institutionAlias']; ?>
                                            </td>
                                            <td class="text-center">

                                                <form style="display:inline;" name=<?php echo '"formEdit_'. $mentorinst[ 'institutionAutoId']. '"'; ?> method="post" action="
                                                    <?php echo base_url('admin/editinstitution');?>">
                                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                        <label for="institutionUID" class="control-label">Staff ID*</label>
                                                        <input required="required" class="form-control" name="institutionUID" id="institutionUID" placeholder="101" value="<?php echo $mentorinst['institutionAutoId']; ?>">
                                                    </div>
                                                    <button class="btn btn-primary btn-s" data-title="Edit" id=<?php echo '"edit_'. $mentorinst[ 'institutionAutoId']. '"'; ?> name=<?php echo '"edit_'. $mentorinst['institutionAutoId'].'"';  ?>  type="submit"><i class="fa fa-edit"></i> Edit</button>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('footer');?>

        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
    <script>
        $(document).ready(function() {
            //datatable initialization
            $('#institutionlist').DataTable({responsive: true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],columnDefs: [{orderable: false,targets: [3]
                }],"aaSorting": []});

            var submitBtn = $('input[type="submit"]');
            // allWells.show();
            submitBtn.click(function() {
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    curInputs = curStep.find("input,select"),
                    isValid = true;
                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }
                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });
        });
        //to refresh the page
        $("#refresh").click(function(event) {
            window.setTimeout(function() {
                location.reload()
            }, 1)

        });

    </script>
</body>

</html>
