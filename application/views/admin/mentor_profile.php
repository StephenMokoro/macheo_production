<!DOCTYPE html>
<html>
<head>
   <title>Macheo | Mentor Profile</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Mentor Profile</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
             <?php foreach($mentor_profile as $profile){ 
                $photo=$profile['mentorProfilePhoto']; if($photo==""){$ppic="defaultimage.png";}else{$ppic=$profile['mentorProfilePhoto'];}?>
                     <a href="<?php echo base_url();?>admin/download_mentorphoto/<?php echo $ppic;?>">
                      <div class="col-md-3" style="text-align: center;margin-right: auto">
                        <div class="col-md-12" style="display: inline-block;text-align: center">
                            <img src="<?php echo base_url();echo 'uploads/profile_photos/mentors/'.$ppic?>" alt="Mentor Photo" class="img-rounded img-responsive" />
                            <b><p style="color: #000000;"><?php echo $profile['mentorFname']." ".$profile['mentorLname'];?></p></b>
                        </div>
                    </div></a>
                    <div class="col-md-9 text-center">
                            <div class="col-md-6" style="text-align: left">
                                <blockquote >
                                     <p><i class="fa fa-user-circle text-primary fa-1x"></i> <?php echo date_format(date_create($profile['mentorDoB']),"j<\s\up>S</\s\up> M, Y");?>  <span><small style="display: inline">Date of Birth </small></cite></span> </p>

                                     <p><i class="fa fa-venus-mars text-success fa-1x"></i> <?php echo $profile['mentorGender'];?>  <span><cite title="Gender <?php echo $profile['mentorGender'];?> "><small style="display: inline">Gender </small></cite></span> </p>

                                      <p><i class="fa fa-phone text-primary fa-1x"></i> <?php echo $profile['mentorPhone1'];?>  <span><cite title="<?php echo $profile['mentorFname'];?>'s phone number"><small style="display: inline">Cell Phone  </small></cite></span> </p>

                                       <p><i class="fa fa-phone text-default fa-1x"></i> <?php echo $profile['mentorPhone2'];?>  <span><cite title="<?php echo $profile['mentorFname'];?>'s alternative number"><small style="display: inline">Alt. Cell Phone  </small></cite></span> </p>

                                      <p><i class="fa fa-envelope text-success fa-1x"></i> <?php echo $profile['mentorEmail'];?>  <span><cite title="<?php echo $profile['mentorFname'];?>'s Email Address"><small style="display: inline">Email  </small></cite></span> </p>

                                       <p><i class="fa fa-level-up text-default fa-1x"></i> Level <?php echo $profile['mentorLevel'];?>  <span><cite title="<?php echo $profile['mentorFname'];?>'s Mentoring level"><small style="display: inline">Mentoring Level  </small></cite></span> </p>

                                       

                                       
                                </blockquote>
                            </div>
                            <div class="col-md-6" style="text-align: left">
                                <blockquote >
                                    <p><i class="fa fa-institution text-default fa-1x"></i> <?php echo $profile['institutionAlias'];?>  <span><small style="display: inline">Institution</small></cite></span> </p>
                                    <p><i class="fa fa-info-circle text-default fa-1x"></i> <?php echo $profile['mentorProfession'];?>  <span><cite title="<?php echo $profile['mentorFname'];?>'s Profession"><small style="display: inline">Profession  </small></cite></span> </p>

                                     <p><i class="fa fa-book text-default fa-1x"></i> <?php echo $profile['courseCode'];?>  <span><cite title="<?php echo $profile['mentorFname'];?>'s Course"><small style="display: inline">Course  </small></cite></span> </p>

                                     <p><i class="fa fa-id-card text-default fa-1x"></i> <?php echo $profile['mentorStudId'];?>  <span><small style="display: inline">Student Id </small></cite></span> </p>
                                     <p><i class="fa fa-mortar-board text-default fa-1x"></i> <?php echo $profile['mentorYoS'];?>  <span><cite title="<?php echo $profile['mentorFname'];?>'s Year of Study"><small style="display: inline">Year of Study  </small></cite></span> </p>


                                     <p><i class="fa fa-id-badge text-default fa-1x"></i> <?php echo $profile['mentorStaffId'];?>  <span><cite title="Staff Id "><small style="display: inline">Staff Id </small></cite></span> </p>

                                     
                                     
                                </blockquote>
                            </div>
                    </div>
                <?php }?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
</body>
</html>
