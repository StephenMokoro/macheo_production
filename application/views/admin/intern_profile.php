<!DOCTYPE html>
<html>
<head>
   <title>Macheo | Intern Profile</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Intern Profile</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
             <?php foreach($intern_profile as $profile){ 
                $photo=$profile['internProfilePhoto']; if($photo==""){$ppic="defaultimage.png";}else{$ppic=$profile['internProfilePhoto'];}?>
                     <a href="<?php echo base_url();?>admin/download_internphoto/<?php echo $ppic;?>">
                      <div class="col-md-3" style="text-align: center;margin-right: auto">
                        <div class="col-md-12" style="display: inline-block;text-align: center">
                            <img src="<?php echo base_url();echo 'uploads/profile_photos/interns/'.$ppic?>" alt="Mentor Photo" class="img-rounded img-responsive" />
                            <b><p style="color: #000000;"><?php echo $profile['internFname']." ".$profile['internLname'];?></p></b>
                        </div>
                    </div></a>
                    <div class="col-md-9 text-center">
                            <div class="col-md-6" style="text-align: left">
                                <blockquote >
                                     <p><i class="fa fa-user-circle text-primary fa-1x"></i> <?php echo date_format(date_create($profile['internDoB']),"j<\s\up>S</\s\up> M, Y");?>  <span><small style="display: inline">Date of Birth </small></cite></span> </p>

                                     <p><i class="fa fa-venus-mars text-success fa-1x"></i> <?php echo $profile['internGender'];?>  <span><cite title="when <?php echo $profile['internGender'];?> "><small style="display: inline">Gender </small></cite></span> </p>
                                      <p><i class="fa fa-phone text-primary fa-1x"></i> <?php echo $profile['internPhone'];?>  <span><cite title="<?php echo $profile['internFname'];?>'s phone number"><small style="display: inline">Cell Phone  </small></cite></span> </p>

                                      <p><i class="fa fa-envelope text-success fa-1x"></i> <?php echo $profile['internEmail'];?>  <span><cite title="<?php echo $profile['internFname'];?>'s Email Address"><small style="display: inline">Email  </small></cite></span> </p>
                                       
                                </blockquote>
                            </div>
                            <div class="col-md-6" style="text-align: left">
                                <blockquote >
                                  <p><i class="fa fa-institution text-default fa-1x"></i> <?php echo $profile['institutionAlias'];?>  <span><small style="display: inline">Institution</small></cite></span> </p>
                                     <p><i class="fa fa-book text-default fa-1x"></i> <?php echo $profile['courseCode'];?>  <span><cite title="<?php echo $profile['internFname'];?>'s Course"><small style="display: inline">Course  </small></cite></span> </p>

                                     <p><i class="fa fa-id-badge text-default fa-1x"></i> <?php echo $profile['internStudId'];?>  <span><small style="display: inline">Student Id </small></cite></span> </p>
                                     <p><i class="fa fa-mortar-board text-default fa-1x"></i> <?php echo $profile['internYoS'];?>  <span><cite title="<?php echo $profile['internFname'];?>'s Year of Study"><small style="display: inline">Year of Study  </small></cite></span> </p>

                                </blockquote>
                            </div>
                    </div>
                <?php }?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>

</body>
</html>
