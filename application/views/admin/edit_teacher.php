<!DOCTYPE html>
<html>
<head>
  <title>Macheo | Edit Teacher Profile</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?>
 </style> 
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav.php'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="row">
          <div class="col-lg-12 ">
              <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Edit Teacher Profile</h4>
          </div>
          <!-- /.col-lg-12 -->
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
              <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                 <?php foreach($teacher_profile as $profile){
                  echo form_open_multipart('admin/updateteacher',array('id' => 'teacher_update','method'=>'post','name'=>'teacherupdate'));

                   $photo=$profile['teacherProfilePhoto']; if($photo==""){$ppic="defaultimage.png";}else{$ppic=$profile['teacherProfilePhoto'];}
                   ?>
                            <div class="row setup-content" >
                                <div class="col-xs-12">
                                    <div class="col-md-6">
                                      <div class="col-md-9 col-md-offset-3">
                                          <div class="form-group">
                                            <div class="main-img-preview">
                                              <img class="thumbnail img-preview " src="<?php echo base_url();echo 'uploads/profile_photos/teachers/'.$ppic?>" alt="PPIC" width="210" height="230">
                                            </div>
                                            <!-- <p class="help-block">* Upload mentee passport photo.</p> -->
                                          </div>
                                      </div>
                                      <div class="col-md-6 col-md-offset-4">
                                        <div class="input-group">
                                          <input id="fakeUploadLogo" class="form-control fake-shadow"  disabled="disabled" style="display: none;">
                                          <div class="input-group-btn">
                                            <div class="fileUpload btn btn-default fake-shadow">
                                             <span><i class="fa fa-upload"></i> Upload Photo</span>
                                              <input id="photo-id" name="photo" type="file" class="attachment_upload">
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                        <label for="initFile" class="control-label">Initial File <span class="star">*</span></label>
                                        <input type="text" name="initFile" placeholder="" class=" form-control" id="initFile" value=<?php echo '"'.$profile['teacherProfilePhoto'].'"';?> >
                                      </div>
                                      <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                        <label for="teacherUID" class="control-label">Admin UID <span class="star">*</span></label>
                                        <input type="number" name="teacherUID" placeholder="" class=" form-control" id="teacherUID" required="required" maxlength="20" value=<?php echo '"'.$profile['teacherAutoId'].'"';?>>
                                      </div>
                                       <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                          <label for="initFile" class="control-label">Initial File <span class="star">*</span></label>
                                          <input type="text" name="initFile" placeholder="" class=" form-control" id="initFile" value=<?php echo '"'.$profile['teacherProfilePhoto'].'"';?> >
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="firstName" class="control-label">First Name <span class="star">*</span></label>
                                          <input type="text" name="firstName" placeholder="" class=" form-control" id="firstName" required="required" maxlength="20" value=<?php echo '"'.$profile['teacherFname'].'"';?>>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="lastName" class="control-label">Last Name <span class="star">*</span></label>
                                          <input type="text" name="lastName" placeholder="" class=" form-control" id="lastName" required="required" maxlength="20" value=<?php echo '"'.$profile['teacherLname'].'"';?>>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                            <label for="nationalId" class="control-label">National ID/Passport
                                            <input type="text" name="nationalId" class=" form-control" id="nationalId" value=<?php echo '"'.$profile['teacherId'].'"';?>>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        
                                        
                                        <div class="form-group col-md-12 col-lg-12">
                                          <label for="otherNames" class="control-label">Other Names</label>
                                          <input type="text" name="otherNames" placeholder="" class=" form-control" id="otherNames" maxlength="20" value=<?php echo '"'.$profile['teacherOtherNames'].'"';?>>
                                        </div>
                                        <div class="form-group col-md-6 col-lg-6">
                                            <label class="">Gender <span class="star">*</span></label><br>
                                        <?php $teacher_gender= $profile['teacherGender'];  if ($teacher_gender=='Female'){?>
                                            <?php  echo '<label class="radio-inline ">
                                                <input type="radio" name="gender" id="female" value="Female" autocomplete="off" required="required" checked="true">Female
                                            </label>

                                             <label class="radio-inline ">
                                                <input type="radio" name="gender" id="female" value="Female" autocomplete="off" required="required">Male
                                            </label>';}else if ($teacher_gender=="Male"){echo '
                                            <label class="radio-inline ">
                                                <input type="radio" name="gender" id="female" value="Female" autocomplete="off" required="required" >Female
                                            </label>

                                             <label class="radio-inline ">
                                                <input type="radio" name="gender" id="female" value="Female" autocomplete="off" required="required" checked="true">Male
                                            </label>';}?>
                                        </div>
                                         <div class="form-group col-md-12 col-lg-12">
                                              <label for="phoneNumber" class="control-label"> Current Phone No.<span class="star">*</span></label>
                                              <input type="text" name="phoneNumber" class=" form-control" id="phoneNumber" required="required"  data-mask="0799999999" required="required" value=<?php echo '"'.$profile['teacherPhone'].'"';?>>
                                          </div>
                                          <div class="form-group col-md-12 col-lg-12">
                                          <label for="emailAddress" class="control-label"> Email Address </label>
                                          <input type="email" name="emailAddress" placeholder="" class=" form-control" id="emailAddress" value=<?php echo '"'.$profile['teacherEmail'].'"';?>>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                            <label for="schoolId" class="control-label"> School <span class="star">*</span></label>
                                            <select type="text" name="schoolId" class=" form-control" id="schoolId" required="required">
                                            <option value=<?php  echo '"'.$profile['teacherSchoolId'].'"';?>><?php  echo $profile['schoolName'];?></option>
                                          <?php  foreach($teacherinst as $inst){ 
                                                ?>
                                            <option value=<?php  echo '"'.$inst['schoolAutoId'].'"';?>><?php  echo $inst['schoolName'];}?></option>
                                            </select>
                                          </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                            <label for="startDate" class="control-label">Start Date</label>
                                            <div class="form-group">
                                                <div class='input-group date' id='startDate'>
                                                    <input type='text' class="form-control" readonly="true" name="startDate" value=<?php echo '"'.$profile['teacherStartDate'].'"';?> />
                                                    <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-6">
                                        
                                        <div class="form-group col-md-6 col-lg-6">
                                          <input type="submit" class="btn btn-warning" value="Update">
                                        </div>
                                    </div>
                                </div><!--/.col-xs-12-->
                            </div><!--/.setup-content-->
                        <?php echo form_close();?>
                      <?php }?>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
    // Limit scope pollution from any deprecated API
(function() {

    var matched, browser;

// Use of jQuery.browser is frowned upon.
// More details: http://api.jquery.com/jQuery.browser
// jQuery.uaMatch maintained for back-compat
    jQuery.uaMatch = function( ua ) {
        ua = ua.toLowerCase();

        var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
            /(msie) ([\w.]+)/.exec( ua ) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
            [];

        return {
            browser: match[ 1 ] || "",
            version: match[ 2 ] || "0"
        };
    };

    matched = jQuery.uaMatch( navigator.userAgent );
    browser = {};

    if ( matched.browser ) {
        browser[ matched.browser ] = true;
        browser.version = matched.version;
    }

// Chrome is Webkit, but Webkit is also Safari.
    if ( browser.chrome ) {
        browser.webkit = true;
    } else if ( browser.webkit ) {
        browser.safari = true;
    }

    jQuery.browser = browser;

    jQuery.sub = function() {
        function jQuerySub( selector, context ) {
            return new jQuerySub.fn.init( selector, context );
        }
        jQuery.extend( true, jQuerySub, this );
        jQuerySub.superclass = this;
        jQuerySub.fn = jQuerySub.prototype = this();
        jQuerySub.fn.constructor = jQuerySub;
        jQuerySub.sub = this.sub;
        jQuerySub.fn.init = function init( selector, context ) {
            if ( context && context instanceof jQuery && !(context instanceof jQuerySub) ) {
                context = jQuerySub( context );
            }

            return jQuery.fn.init.call( this, selector, context, rootjQuerySub );
        };
        jQuerySub.fn.init.prototype = jQuerySub.fn;
        var rootjQuerySub = jQuerySub(document);
        return jQuerySub;
    };

})();</script>
<script>
$(document).ready(function () {
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });


    var brand = document.getElementById('photo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#photo-id").change(function() {
        readURL(this);
    });
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
