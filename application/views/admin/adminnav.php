<?php $photo=$_SESSION['sessdata']['adminProfilePhoto']; if($photo==""){$ppic="defaultimage.png";}else{$ppic=$_SESSION['sessdata']['adminProfilePhoto'];}?>

<header class="main-header" >
    <!-- Logo -->
    <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b class="fa fa-expand" data-toggle="push-menu"></b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><small><b> MACHEO</b></span></small>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top"  style="background-color: #1a2226;">
        <style>
            #sidebar-toggle:hover {
                background-color: #222d32;
            }

        </style>
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button" id="sidebar-toggle">
            <span class="sr-only">Toggle navigation</span><span style="text-transform:uppercase;font-weight: bolder;color: #FFFF00;"><i class="fa fa-sun-o fa-spin" style="color: #FFFF00;"></i></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img  src="<?php echo base_url();echo 'uploads/profile_photos/admins/'.$ppic?>" width="30" height="30"  class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['sessdata']['adminName'];?></span>
            </a>
                    <ul class="dropdown-menu">
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo base_url('admin/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
            </ul>
        </div>
    </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left ">
                <img src="<?php echo base_url();echo 'uploads/profile_photos/admins/'.$ppic?>" style="width:40px;height:40px;" class="img-circle img-responsive" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>
                    <?php echo $_SESSION['sessdata']['adminName'];?>
                </p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="<?php echo base_url();?>admin/dash" "><i class="fa fa-dashboard text-aqua "></i> <span>Dashboard</span></a>
        </li>
        <li class="treeview ">
          <a href="# ">
            <i class="fa fa-users "></i> <span>Users</span>
            <span class="pull-right-container ">
              <i class="fa fa-angle-left pull-right "></i>
            </span>
          </a>
          <ul class="treeview-menu ">
            <li>
                <a href="<?php echo base_url();?>admin/admins"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Admins</a>
            </li>
            <li>
                <a href="<?php echo base_url();?>admin/mentors"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Mentors</a>
            </li>
            <li>
                <a href="<?php echo base_url();?>admin/mentees"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Mentees</a>
            </li>
            <li>
                <a href="<?php echo base_url();?>admin/guardians"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Guardians</a>
            </li>
            <li>
                <a href="<?php echo base_url();?>admin/interns"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Interns</a>
            </li>
            <li>
                <a href="<?php echo base_url();?>admin/teachers"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Teachers</a>
            </li>
        </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-server"></i> <span>Forms</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <!-- <li><a href="<?php echo base_url();?>admin/formallocation"><i class="fa fa-caret-right"></i> Allocations</a></li> -->
                <li class="treeview">
                    <a href="#"><i class="fa fa-caret-right"></i>Form 2
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url();?>admin/f2mentees"><i class="fa fa-caret-right"></i> Mentees</a></li>
                        <li><a href="<?php echo base_url();?>admin/f2subselect"><i class="fa fa-caret-right"></i> Subject Selection</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-caret-right"></i>Form 3
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?php echo base_url();?>admin/f3mentees"><i class="fa fa-caret-right"></i> Mentees</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>admin/f3subselect"><i class="fa fa-caret-right"></i> Subject Selection</a>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-caret-right"></i>Form 4
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?php echo base_url();?>admin/f4mentees"><i class="fa fa-caret-right"></i> Mentees</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>admin/f4subselect"><i class="fa fa-caret-right"></i> Subject Selection</a>
                        </li>
                        
                    </ul>
                </li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-bars"></i> <span>Extras</span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="<?php echo base_url();?>admin/institutions"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Institutions</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>admin/schools"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Schools</a>
                </li>

                <li>
                    <a href="<?php echo base_url();?>admin/courses"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Courses</a>
                </li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-certificate"></i> <span>Examinations</span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="<?php echo base_url();?>admin/generalexams"><i class="fa fa-caret-right fa-fw"></i>&nbsp; General Exams</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>admin/macheoexams"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Macheo Exams</a>
                </li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-bookmark-o"></i> <span>Attendance</span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="<?php echo base_url();?>admin/menteeattendance"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Mentees</a>
                    <a href="<?php echo base_url();?>admin/mentorattendance"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Mentors</a>
                    <a href="<?php echo base_url();?>admin/teacherattendance"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Teachers</a>
                    <a href="<?php echo base_url();?>admin/internattendance"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Interns</a>
                </li>
            </ul>
        </li>
        <li class="header" style="background-color: #008080;color: #FFFFFF;">&copy; 2018 STRATHMORE UNIVERSITY</li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
