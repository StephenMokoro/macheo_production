<!DOCTYPE html>
<html>
<head>
    <title>Macheo | Macheo Exams</title>
    <?php $this->load->view('headerlinks/headerlinks.php'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;">
    <div class="wrapper">
        <?php $this->load->view('admin/adminnav.php'); ?>
        <!--navigation -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="row">
                    <div class="col-lg-12 ">
                        <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Macheo Exams</h4>
                        <div class="pull-right">
                            <span data-placement="top" data-toggle="tooltip" title="Refresh">
                    <button class="btn btn-s" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                            </span>
                            <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-s" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>&nbsp;Print All</a>
                            </span>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <div class="box box-solid collapsed-box" style="background:lightgrey">
                                    <div class="box-header">
                                        <h3 class="box-title" style="color: #21618C;">New Macheo Exam</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                                        </div>
                                    </div>
                                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                                        <?php echo form_open_multipart('admin/newmacheoexam',array('id' => 'macheoexam_registration','method'=>'post'));?>
                                        <div class="row setup-content">
                                            <div class="col-xs-12">
                                                <div class="col-md-12">
                                                    <div class="form-group col-md-6 col-lg-6">
                                                        <label for="examName" class="control-label">Exam Name <span class="star">*</span></label>
                                                        <input type="text" name="examName" class=" form-control" id="examName" required="required">
                                                    </div>
                                                    <div class="form-group col-md-6 col-lg-6">
                                                        <label for="termCode" class="control-label"> Term <span class="star">*</span></label>
                                                        <select type="text" name="termCode" class=" form-control" id="termCode" required="required">
                                                          <option value="">--Select Term--</option>
                                                          <option value="TERM1">Term 1</option>
                                                          <option value="TERM2">Term 2</option>
                                                          <option value="TERM3">Term 3</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6 col-lg-6">
                                                        <label for="formCode" class="control-label"> Form <span class="star">*</span></label>
                                                        <select type="text" name="formCode" class=" form-control" id="formCode" required="required">
                                                          <option value="">--Select Form--</option>
                                                          <option value="FORM2">Form 2</option>
                                                          <option value="FORM3">Form 3</option>
                                                          <option value="FORM4">Form 4</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-12 col-lg-12">
                                                        <input type="submit" class="btn btn-primary" value="Submit">
                                                        <input type="reset" class="btn btn-default" value="Reset">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.col-xs-12-->
                                        </div>
                                        <!--/.setup-content-->
                                        <?php echo form_close();?>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                
                                <?php if(isset($_SESSION['msg']))
                                  {
                                    $msg = $_SESSION['msg'];
                                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                                    <div class="messagebox alert alert-danger" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <i class="fa fa-close"></i>
                                          <strong><span>';echo $msg['error']; echo '</span></strong>
                                      </div> 
                                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                                    <div class="messagebox alert alert-success" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <i class="fa fa-check-circle-o"></i>
                                          <strong><span>';echo $msg['success'];echo '</span></strong>
                                      </div> 
                                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                                <table class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="examslist">
                                    <thead>
                                        <tr style="color: #000000;">
                                            <th class="text-center">FORM</th>
                                            <th class="text-center">TERM</th>
                                            <th class="text-center">ADDED</th>
                                            <th class="text-center">Exam Name</th>
                                            <th class="text-center"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php  foreach($exams as $exam){ 
                           ?>
                                        <tr>
                                            <td class="text-center">
                                                <?php  echo $exam['examFormCode'];?>
                                            </td>
                                            <td class="text-center">
                                                <?php  echo $exam['examTermCode'];?>
                                            </td>
                                            <td class="text-center">
                                                <?php echo date_format(date_create($exam['examDateCreated']),"D j<\s\up>S</\s\up> M, Y");?>
                                            </td>
                                            <td class="text-center">
                                                <?php echo $exam['examName'];?>
                                            </td>
                                            <td class="text-center">

                                                 <form style="display:inline;" name="form_<?php echo $exam['examAutoId'];?> " method="post" action="#">
                                                   <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                        <label for="examAutoId" class="control-label">Exam Id*</label>
                                                        <input required="required" class="form-control" name="examAutoId" id="examAutoId" placeholder="101" value="<?php echo $exam['examAutoId']; ?>">
                                                    </div>
                                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                        <label for="examFormCode" class="control-label">Form Code*</label>
                                                        <input required="required" class="form-control" name="examFormCode" id="examFormCode" placeholder="101" value="<?php echo $exam['examFormCode']; ?>">
                                                    </div>
                                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                        <label for="examTermCode" class="control-label">Term Code*</label>
                                                        <input required="required" class="form-control" name="examTermCode" id="examTermCode" placeholder="101" value="<?php echo $exam['examTermCode']; ?>">
                                                    </div>
                                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                        <label for="examDateCreated" class="control-label">Year*</label>
                                                        <input required="required" class="form-control" name="examDateCreated" id="examDateCreated" placeholder="101" value="<?php echo $exam['examDateCreated'] ?>">
                                                    </div>
                                                    <div class="input-group" style="padding: 0px!important;">
                                                        <span class="input-group-addon" style="padding: 0px!important;margin: 0px!important;border: 0px!important">
                                                            <button class="btn btn-primary" >Go!</button> 
                                                        </span>
                                                        <select class="form-control" style="padding: 0px!important;margin: 0px!important;border-radius: 5px!important;width: 80px !important;font-family: 'FontAwesome',serif;" id="action_select" onchange="actionBase(this.parentNode.parentNode, this.options[this.selectedIndex].value, '<?php echo base_url();?>admin/')">
                                                            <option value="#" >Action</option>
                                                            <option value="macheoexamscores">&#xf06e; View</option>
                                                            <option value="macheomarks">&#xf055; Marks</option>
                                                            <!-- <option value="#">&#xf044; Edit</option>
                                                            <option value="#">&#xf1f8; Delete</option> -->
                                                        </select>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('footer');?>
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
    <script>
       function actionBase(form,value,url){
            //set the action 
            form.setAttribute('action',url+value);
        }
        $(document).ready(function() {
            //datatable initialization
          $('#examslist').DataTable({'iDisplayLength': 50,'lengthMenu': [[50, 200, 500, -1], [50, 200, 500, 'All']],columnDefs: [{ orderable: false,targets: [4]}],dom: 'lBfrtip', 
                buttons: [{extend: 'print',exportOptions: {columns:[0,1,2,3]   } },{extend: 'excel',exportOptions: {columns:[0,1,2,3]   } },{extend: 'pdf',exportOptions: {columns:[0,1,2,3]   } }]
                // buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],exportOptions: {columns:[0,1,2,3]}
            });
            var submitBtn = $('input[type="submit"]');
            // allWells.show();
            submitBtn.click(function() {
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    curInputs = curStep.find("input,select"),
                    isValid = true;
                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }
                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });
        });
        //to refresh the page
        $("#refresh").click(function(event) {
            window.setTimeout(function() {
                location.reload()
            }, 1)

        });

    </script>
</body>

</html>
