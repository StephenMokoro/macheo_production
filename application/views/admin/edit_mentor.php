<!DOCTYPE html>
<html>

<head>
    <title>Macheo | Edit Mentor Profile</title>
    <?php $this->load->view('headerlinks/headerlinks.php'); ?>
    <style>
        @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,700,300);
        body {
            /*font: 12px 'Open Sans';*/
        }

    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
    <div class="wrapper">
        <?php $this->load->view('admin/adminnav.php'); ?>
        <!--navigation -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="row">
                    <div class="col-lg-12 ">
                        <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Edit Mentor Profile</h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <?php if(isset($_SESSION['msg']))
                                  {
                                    $msg = $_SESSION['msg'];
                                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                                    <div class="messagebox alert alert-danger" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <i class="fa fa-close"></i>
                                          <strong><span>';echo $msg['error']; echo '</span></strong>
                                      </div> 
                                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                                    <div class="messagebox alert alert-success" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <i class="fa fa-check-circle-o"></i>
                                          <strong><span>';echo $msg['success'];echo '</span></strong>
                                      </div> 
                                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                      
                                <?php foreach($mentor_profile as $profile){
                  echo form_open_multipart('admin/updatementor',array('id' => 'mentor_update','method'=>'post','name'=>'mentorupdate'));

                   $photo=$profile['mentorProfilePhoto']; if($photo==""){$ppic="defaultimage.png";}else{$ppic=$profile['mentorProfilePhoto'];}
                   ?>
                                <div class="row setup-content">
                                    <div class="col-xs-12">
                                        <div class="col-md-6">
                                            <div class="col-md-9 col-md-offset-3">
                                                <div class="form-group">
                                                    <div class="main-img-preview">
                                                        <img class="thumbnail img-preview " src="<?php echo base_url();echo 'uploads/profile_photos/mentors/'.$ppic?>" alt="PPIC" width="210" height="230">
                                                    </div>
                                                    <!-- <p class="help-block">* Upload mentee passport photo.</p> -->
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-md-offset-4">
                                                <div class="input-group">
                                                    <input id="fakeUploadLogo" class="form-control fake-shadow" disabled="disabled" style="display: none;" >
                                                    <div class="input-group-btn">
                                                        <div class="fileUpload btn btn-default fake-shadow">
                                                            <span><i class="fa fa-upload"></i> Upload Photo</span>
                                                            <input id="photo-id" name="photo" type="file" class="attachment_upload">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                                <label for="initFile" class="control-label">Initial File <span class="star">*</span></label>
                                                <input type="text" name="initFile" placeholder="" class=" form-control" id="initFile" value=<?php echo '"'.$profile[ 'mentorProfilePhoto']. '"';?> >
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12" style="display: none;">
                                                <label for="mentorUID" class="control-label">Mentor UID <span class="star">*</span></label>
                                                <input type="number" name="mentorUID" placeholder="" class=" form-control" id="mentorUID" required="required" maxlength="20" value=<?php echo '"'.$profile[ 'mentorAutoId']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="firstName" class="control-label">First Name <span class="star">*</span></label>
                                                <input type="text" name="firstName" placeholder="" class=" form-control" id="firstName" required="required" maxlength="20" value=<?php echo '"'.$profile[ 'mentorFname']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="lastName" class="control-label">Last Name <span class="star">*</span></label>
                                                <input type="text" name="lastName" placeholder="" class=" form-control" id="lastName" required="required" maxlength="20" value=<?php echo '"'.$profile[ 'mentorLname']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="otherNames" class="control-label">Other Names</label>
                                                <input type="text" name="otherNames" placeholder="" class=" form-control" id="otherNames" maxlength="20" value=<?php echo '"'.$profile[ 'mentorOtherNames']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label class="">Gender <span class="star">*</span></label><br>
                                                <?php $mentor_gender= $profile['mentorGender'];  if ($mentor_gender=='Female'){?>
                                                <?php  echo '<label class="radio-inline ">
                                                <input type="radio" name="gender" id="female" value="Female" autocomplete="off" required="required" checked="true">Female
                                            </label>

                                             <label class="radio-inline ">
                                                <input type="radio" name="gender" id="male" value="Male" autocomplete="off" required="required">Male
                                            </label>';}else if ($mentor_gender=="Male"){echo '
                                            <label class="radio-inline ">
                                                <input type="radio" name="gender" id="female" value="Female" autocomplete="off" required="required" >Female
                                            </label>

                                             <label class="radio-inline ">
                                                <input type="radio" name="gender" id="male" value="Male" autocomplete="off" required="required" checked="true">Male
                                            </label>';}?>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="dob" class="control-label">Date of Birth </label>
                                                <input type="text" name="dob" placeholder="" class=" form-control" id="dob" placeholder="YYYY-MM-DD" data-mask="9999-99-99" value=<?php echo '"'.$profile[ 'mentorDoB']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="phoneNumber" class="control-label"> Current Phone No. <span class="star">*</span> </label>
                                                <input type="text" name="phoneNumber" placeholder="" class=" form-control" id="phoneNumber" data-mask="0799999999" required="required" value=<?php echo '"'.$profile[ 'mentorPhone1']. '"';?>>
                                            </div>
                                        </div>
                                        <div class="col-md-6">

                                            
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="yearOfStudy" class="control-label">Year of Study <span class="star">*</span></label>
                                                <select type="text" name="yearOfStudy" class=" form-control" id="yearOfStudy" required="required">
                                                    <option value=<?php echo '"'.$profile['mentorYoS'].'"';?>><?php echo $profile['mentorYoS'];?></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="SU Alumni">SU Alumni</option>
                                                    <option value="SU Staff">SU Staff</option>
                                                    <option value="Other">Other</option>
                                            </select>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="studentId" class="control-label">Student ID </label>
                                                <input type="text" name="studentId" placeholder="" class=" form-control" id="studentId" maxlength="20" value=<?php echo '"'.$profile[ 'mentorStudId']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="staffId" class="control-label">Staff Id </label>
                                                <input type="text" name="staffId" placeholder="" class=" form-control" id="staffId" maxlength="20" value=<?php echo '"'.$profile[ 'mentorStaffId']. '"';?>>
                                            </div>

                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="emailAddress" class="control-label"> Email Address </label>
                                                <input type="email" name="emailAddress" placeholder="" class=" form-control" id="emailAddress" value=<?php echo '"'.$profile[ 'mentorEmail']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="institutionId" class="control-label"> Institution <span class="star">*</span></label>
                                                <select type="text" name="institutionId" class=" form-control" id="institutionId" required="required">
                                            <option value=<?php  echo '"'.$profile['mentorInstitutionId'].'"';?>><?php  echo $profile['institutionName'];?></option>
                                          <?php  foreach($mentorinst as $inst){ 
                                                ?>
                                            <option value=<?php  echo '"'.$inst['institutionAutoId'].'"';?>><?php  echo $inst['institutionName'];}?></option>
                                            </select>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="courseId" class="control-label"> Course </label>
                                                <select type="text" name="courseId" class=" form-control" id="courseId">
                                              <?php $cours= $profile['mentorCourse'];if($cours==""){ echo '<option value="">--Select Course--';}else{echo ' <option value='.'"'.$profile['mentorCourse'].'">'.$profile['courseCode'].'</option>';}?>
                                          <?php  foreach($courses as $course){ 
                                                ?>
                                            <option value=<?php  echo '"'.$course['courseAutoId'].'"';?>><?php  echo $course['courseCode'];}?></option>
                                            </select>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="mentorLevel" class="control-label">Mentoring Level <span class="star">*</span></label>
                                                <select type="text" name="mentorLevel" class=" form-control" id="mentorLevel" required="required">
                                                    <option value=<?php  echo '"'.$profile['mentorLevel'].'"';?>>Level <?php  echo $profile['mentorLevel'];?></option>
                                                    <option value="1">Level 1</option>
                                                    <option value="2">Level 2</option>
                                                    <option value="3">Level 3</option>
                                                    <option value="4">Level 4</option>
                                            </select>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="profession" class="control-label"> Profession </label>
                                                <input type="text" name="profession" placeholder="" class=" form-control" id="profession" value=<?php echo '"'.$profile[ 'mentorProfession']. '"';?>>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                                <label for="joinDate" class="control-label">Join Date</label>
                                                <div class="form-group">
                                                    <div class='input-group date' id='joinDate'>
                                                        <input type='text' class="form-control" readonly="true" name="joinDate" value=<?php echo '"'.$profile[ 'mentorDateJoined']. '"';?> />
                                                        <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group col-md-6 col-lg-6">
                                                <input type="submit" class="btn btn-warning" value="Update">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/.col-xs-12-->
                                </div>
                                <!--/.setup-content-->
                                <?php echo form_close();?>
                                <?php }?>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('footer');?>
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
    <script>
        $(document).ready(function() {
            var submitBtn = $('input[type="submit"]');
            // allWells.show();
            submitBtn.click(function() {
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    curInputs = curStep.find("input,select"),
                    isValid = true;
                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }
                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });


            var brand = document.getElementById('photo-id');
            brand.className = 'attachment_upload';
            brand.onchange = function() {
                document.getElementById('fakeUploadLogo').value = this.value.substring(12);
            };

            // Source: http://stackoverflow.com/a/4459419/6396981
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('.img-preview').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#photo-id").change(function() {
                readURL(this);
            });

            $(function() {

            $('#joinDate').datepicker({
                format: "yyyy-mm-dd",
                minDate: new Date(),
                todayHighlight: true
            });
            $('#dob').datepicker({
                format: "yyyy-mm-dd",
                minDate: new Date(),
                todayHighlight: true
            });

        });
    });
        //to refresh the page
        $("#refresh").click(function(event) {
            window.setTimeout(function() {
                location.reload()
            }, 1)

        });

    </script>
</body>

</html>
