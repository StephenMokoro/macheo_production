<!DOCTYPE html>
<html>
<head>
    <title>Macheo | Institution Profile</title>
    <?php $this->load->view('headerlinks/headerlinks.php'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;">
    <div class="wrapper">
        <?php $this->load->view('admin/adminnav'); ?>
        <!--navigation -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="row">
                    <div class="col-lg-12 ">
                        <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Institution Profile</h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <?php foreach($institution_profile as $profile){ ?>
                            <b><p style="color: #000000;"><?php echo $profile['institutionName'];?></p>
                                </b>
                            </div>
                        </div>
                        </a>
                        <div class="col-md-9 text-center">
                            <div class="col-md-6" style="text-align: left">
                                <blockquote>
                                    <!--<p><i class="fa fa-user-circle text-primary fa-1x"></i>
                                         <span><small style="display: inline">Date of Birth </small></cite></span> </p>-->

                                    <p><i class="fa fa-venus-mars text-success fa-1x"></i>
                                        <?php echo $profile['institutionName'];?> <span><cite title="when <?php echo $profile['institutionName'];?> "><small style="display: inline">Institution Name </small></cite></span> </p>
                                    <p><i class="fa fa-phone text-primary fa-1x"></i>
                                        <?php echo $profile['institutionLocation'];?> <span><cite title="<?php echo $profile['institutionLocation'];?>></cite><small style="display: inline">Institution Location </small></cite></span> </p>

                                    <p><i class="fa fa-envelope text-success fa-1x"></i>
                                        <?php echo $profile['institutionAlias'];?> <span><cite title="<?php echo $profile['institutionAlias'];?>><small style="display: inline">Institution Alias </small></cite></span> </p>

                                </blockquote>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php $this->load->view('footer');?>
    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
  </body>

</html>
