<!DOCTYPE html>
<html>
<head>
  <title>Macheo | School Information</title>
  <?php $this->load->view('headerlinks/headerlinks.php'); ?>
  <style>
    .tabtop li a{font-family:'Lato', sans-serif;font-weight:700;color:#1b2834;border-radius:0px;margin-right:22.008px;border:0px solid #ebebeb !important;background-color: #D7DBDD;}
    .tabtop .active a:before{content:"♦";position:absolute;top:40px;left:110px;color:#e31837;font-size:30px;}
    .tabtop li a:hover{color:#000 !important;text-decoration:none;background-color: #ECF0F1;}
    .tabtop .active a:hover{color:#fff !important;}
    .tabtop .active a{background-color:#e31837 !important;color:#FFF !important;}
    .margin-tops{margin-top:30px;}
    .tabtop li a:last-child{padding:22px 22px;}
    .thbada{padding:10px 28px !important;}
    .margin-tops4{margin-top:20px;}
    .tabsetting{border-top:0px solid #ebebeb;padding-top:6px;}
    .heading-container p{font-family:'Lato', sans-serif;text-align:center;font-size:16px !important;text-transform:uppercase;}

    .tab-content{
      border: 0px;
    }
    .circle{border-radius: 5px !important;}
  #sel1 option {
      margin: 40px;
      background: #fff;
      color: #000;
      /*text-shadow: 0 1px 0 rgba(0, 0, 0, 0.4);*/
  }

</style>
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav.php'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
              <div class="row">
                <div class="col-md-12" style="margin-top: 0px;">
                    <h4 class="pull-left">Extras <span class="fa fa-angle-right"></span> Schools <span class="fa fa-angle-right"></span> Info <span class="fa fa-angle-right"></span> Sch: <?php echo $_SESSION['sessdata']['schoolName']; ?></h4>
                    <div class="pull-right">
                    </div> 
                </div>
              </div><!--/.row-->
              <div class="row">
                <div class="col-sm-12">
                    
        <div class="clearfix"></div>
        <div class="tabbable-panel margin-tops4">
          <div class="hidden-md hidden-lg" style="margin-right: 15px;">
            <div class="form-group">
              <!-- <label for="sel1">Select list:</label> -->
              <select class="form-control" id="sel1" style="background: #85929E; color: #fff; height: 45px;">
                <option value="tab_default_1">Mentees</option>
                  <option value="tab_default_2">Macheo Exams</option>
                  <option value="tab_default_3">General Exams</option>
                  <option value="tab_default_4">Attendance</option>
              </select>
            </div>
          </div>
          
          <div class="tabbable-line">
              <ul class="nav nav-tabs tabtop  tabsetting hidden-xs hidden-sm" style="border-bottom: none;">
                <li class="active col-md-3 text-center"> <a href="#tab_default_1" data-toggle="tab" class="circle"> Mentees </a> </li>
                <li class="col-md-3 text-center"> <a href="#tab_default_2" data-toggle="tab" class="circle"> Macheo Exams </a> </li>
                <li class="col-md-3 text-center"> <a href="#tab_default_3" data-toggle="tab" class="circle"> General Exams </a> </li>
                <li class="col-md-3 text-center"> <a href="#tab_default_4" data-toggle="tab" class="circle"> Attendance </a> </li> 
                
                <!-- <li> <a href="#tab_default_5" data-toggle="tab" class="thbada"> Amazon Product Listing </a> </li> -->
              </ul>
            
              <div class="tab-content margin-tops" id="myTabs">
                  <div class="tab-pane active fade in" id="tab_default_1">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                          <table  class="table display responsive nowrap" cellspacing="0" width="100%" id="menteeslist"  >
                            <thead>
                                <tr style="background: #2E4053;color: #F7F9F9  ;" hidden="true">
                                    <th class="text-center pull-left" >Sort Mentees &nbsp;&nbsp; </th>
                                 </tr>
                            </thead>
                            <tbody style="color: #17202A ;">
                                <?php  foreach($mentees as $mentee){?>
                               <tr >
                                  <td style="margin: 0px!important;padding: 0px!important;border: none;">
                                    <div class="panel panel-default">
                                      <div class="panel-heading" role="tab" id="mentee<?php echo $mentee['menteeAutoId'];?>">
                                          <h4 class="panel-title">
                                              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $mentee['menteeAutoId'];?>" aria-expanded="false" aria-controls="collapseTwo" style="font-weight: normal;">
                                                  <?php echo $mentee['menteeFname']." ".$mentee['menteeLname']; ?>
                                              </a>
                                          </h4>
                                      </div>
                                      <div id="collapse_<?php echo $mentee['menteeAutoId'];?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="mentee<?php echo $mentee['menteeAutoId'];?>">
                                        <div class="panel-body">
                                          <?php $tableId="table_".$mentee['menteeAutoId'];?>
                                          <form method="post" action="<?php echo base_url(); ?>admin/newgensubmarks">
                                            <div class="form-group col-md-4 col-lg-4" style="display: none;">
                                               <input type="number" class=" form-control text-center" required="required" name="menteeId" value="<?php  echo $mentee['menteeAutoId']; ?>">
                                            </div>
                                            <div class="row">
                                              <table class="table table-bordred table-striped table-hover" id="list">
                                                 <thead style="background-color:#512E5F;color:#FFFFFF;">
                                                     
                                                  </thead>
                                                  <tbody>
                                                     
                                                  </tbody>
                                              </table>
                                            </div>
                                          </form>
                                        </div>
                                      </div>
                                  </div>
                                </td>
                              </tr>
                              <?php }?>
                            </tbody>
                          </table>
                        </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab_default_2">
                    <div class="col-md-12">
                      <table class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="macheoexams">
                        <thead>
                            <tr style="color: #000000;">
                                <th class="text-center">FORM</th>
                                <th class="text-center">TERM</th>
                                <th class="text-center">ADDED</th>
                                <th class="text-center">Exam Name</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  foreach($macheoexams as $exam){?>
                            <tr>
                                <td class="text-center">
                                    <?php  echo $exam['examFormCode'];?>
                                </td>
                                <td class="text-center">
                                    <?php  echo $exam['examTermCode'];?>
                                </td>
                                <td class="text-center">
                                    <?php echo date_format(date_create($exam['examDateCreated']),"D j<\s\up>S</\s\up> M, Y");?>
                                </td>
                                <td class="text-center">
                                    <?php echo $exam['examName'];?>
                                </td>
                                <td class="text-center">
                                     <form style="display:inline;" name="form_<?php echo $exam['examAutoId'];?> " method="post" action="#">
                                       <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="examAutoId" class="control-label">Exam Id*</label>
                                            <input required="required" class="form-control" name="examAutoId" id="examAutoId" placeholder="101" value="<?php echo $exam['examAutoId']; ?>">
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="examFormCode" class="control-label">Form Code*</label>
                                            <input required="required" class="form-control" name="examFormCode" id="examFormCode" placeholder="101" value="<?php echo $exam['examFormCode']; ?>">
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="examTermCode" class="control-label">Term Code*</label>
                                            <input required="required" class="form-control" name="examTermCode" id="examTermCode" placeholder="101" value="<?php echo $exam['examTermCode']; ?>">
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="examDateCreated" class="control-label">Year*</label>
                                            <input required="required" class="form-control" name="examDateCreated" id="examDateCreated" placeholder="101" value="<?php echo $exam['examDateCreated']; ?>">
                                        </div>
                                        <div class="input-group" style="padding: 0px!important;">
                                            <span class="input-group-addon" style="padding: 0px!important;margin: 0px!important;border: 0px!important">
                                                <button class="btn btn-primary" >Go!</button> 
                                            </span>
                                            <select class="form-control" style="padding: 0px!important;margin: 0px!important;border-radius: 5px!important;width: 80px !important;font-family: 'FontAwesome',serif;" id="action_select" onchange="actionBase(this.parentNode.parentNode, this.options[this.selectedIndex].value, '<?php echo base_url();?>admin/')">
                                                <option value="#" >Action</option>
                                                <option value="macheoexamscores">&#xf06e; View</option>
                                                <option value="macheomarks">&#xf055; Marks</option>
                                                <!-- <option value="#">&#xf044; Edit</option>
                                                <option value="#">&#xf1f8; Delete</option> -->
                                            </select>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                      </table>
                      <!-- /.table-responsive -->
                    </div><!--- END COL -->     
                  </div>
                  <div class="tab-pane fade" id="tab_default_3">
                    <div class="col-md-12">
                      <table class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="generalexams">
                        <thead>
                            <tr style="color: #000000;">
                                <th class="text-center">FORM</th>
                                <th class="text-center">TERM</th>
                                <th class="text-center">ADDED</th>
                                <th class="text-center">Exam Name</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  foreach($generalexams as $exam){?>
                            <tr>
                                <td class="text-center">
                                    <?php  echo $exam['examFormCode'];?>
                                </td>
                                <td class="text-center">
                                    <?php  echo $exam['examTermCode'];?>
                                </td>
                                <td class="text-center">
                                    <?php echo date_format(date_create($exam['examDateCreated']),"D j<\s\up>S</\s\up> M, Y");?>
                                </td>
                                <td class="text-center">
                                    <?php echo $exam['examName'];?>
                                </td>
                                <td class="text-center">
                                     <form style="display:inline;" name="form_<?php echo $exam['examAutoId'];?> " method="post" action="#">
                                       <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="examAutoId" class="control-label">Exam Id*</label>
                                            <input required="required" class="form-control" name="examAutoId" id="examAutoId" placeholder="101" value="<?php echo $exam['examAutoId']; ?>">
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="examFormCode" class="control-label">Form Code*</label>
                                            <input required="required" class="form-control" name="examFormCode" id="examFormCode" placeholder="101" value="<?php echo $exam['examFormCode']; ?>">
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="examTermCode" class="control-label">Term Code*</label>
                                            <input required="required" class="form-control" name="examTermCode" id="examTermCode" placeholder="101" value="<?php echo $exam['examTermCode']; ?>">
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="examDateCreated" class="control-label">Year*</label>
                                            <input required="required" class="form-control" name="examDateCreated" id="examDateCreated" placeholder="101" value="<?php echo $exam['examDateCreated']; ?>">
                                        </div>
                                        <div class="input-group" style="padding: 0px!important;">
                                            <span class="input-group-addon" style="padding: 0px!important;margin: 0px!important;border: 0px!important">
                                                <button class="btn btn-primary" >Go!</button> 
                                            </span>
                                            <select class="form-control" style="padding: 0px!important;margin: 0px!important;border-radius: 5px!important;width: 80px !important;font-family: 'FontAwesome',serif;" id="action_select" onchange="actionBase(this.parentNode.parentNode, this.options[this.selectedIndex].value, '<?php echo base_url();?>admin/')">
                                                <option value="#" >Action</option>
                                                <option value="generalexamscores">&#xf06e; View</option>
                                                <option value="generalmarks">&#xf055; Marks</option>
                                                <!-- <option value="#">&#xf044; Edit</option>
                                                <option value="#">&#xf1f8; Delete</option> -->
                                            </select>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                      </table>
                      <!-- /.table-responsive -->
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab_default_4">
                      <div class="col-md-12">
                          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                              <div class="panel panel-default">
                                  <div class="panel-heading" role="tab" id="headingOne">
                                      <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                             All Attendance Records 
                                          </a>
                                      </h4>
                                  </div>
                                  <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                      <div class="panel-body">
                                          <table  class="table table-striped table-bordered table-hover display responsive" cellspacing="0" width="100%" id="attendancelist">
                                              <thead>
                                                  <tr style="background: ;color: #000000;">
                                                      <th class="text-left">Record Date</th>
                                                      <th class="text-center">Present</th>
                                                      <th class="text-center">Absent</th>
                                                      <th class="text-center">Excused</th>
                                                      <th class="text-center">Total</th>
                                                      <th class="text-center"><i class="fa fa-cog"></i></th>
                                                   </tr>
                                              </thead>
                                              <tbody >
                                                 <?php $counter=1; foreach($menteeattendance as $attendance){ 
                                                     ?>
                                                  <tr>
                                                       <?php $countall=$attendance['countAll'];?>
                                                      <td class="text-left"><?php  echo date_format(date_create($attendance['attendanceDate']),"D j<\s\up>S</\s\up> M, Y"); ?>
                                                      </td>
                                                      <td class="text-center">
                                                        <?php if($countall==0){echo "x÷0";}else{
                                                        echo $attendance['countPresent']; echo " (".number_format(($attendance['countPresent']/$countall)*100, 1 )."%)"; }?>
                                                      </td>
                                                      <td class="text-center">
                                                        <?php if($countall==0){echo "x÷0";}else{
                                                        echo $attendance['countAbsent']; echo " (".number_format(($attendance['countAbsent']/$countall)*100, 1 )."%)"; }?>
                                                      </td>
                                                      <td class="text-center">
                                                        <?php if($countall==0){echo "x÷0";}else{
                                                        echo $attendance['countExcused']; echo " (".number_format(($attendance['countExcused']/$countall)*100, 1 )."%)"; }?>
                                                      </td>
                                                      <td class="text-center">
                                                        <?php echo $countall;?>
                                                      </td>
                                                      <td class="text-center"> 
                                                        <form style="display:inline;" name=<?php echo '"formView_'.$counter.'"'; ?> method="post" action="<?php echo base_url('admin/allmenteeattendance');?>">
                                                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                                <label for="attendanceDate" class="control-label">Attendance Date*</label>
                                                                <input required="required" class="form-control" name="attendanceDate" id="attendanceDate" placeholder="" value="<?php echo $attendance['attendanceDate']; ?>">
                                                            </div>
                                                            <button class="btn btn-default btn-s" data-title="View Attendance" id=<?php echo '"view_'. $counter.'"'; ?> name=<?php echo '"view_'. $counter.'"';  ?>  type="submit"><i class="fa fa-eye"></i> View</button>
                                                        </form>

                                                      </td>
                                                  </tr>
                                                  <?php $counter=$counter +1; } ?>
                                              </tbody>
                                          </table>
                                          <!-- /.table-responsive -->
                                      </div>
                                  </div>
                              </div>
                              <?php foreach ($forms as $form) {?>
                              <div class="panel panel-default">
                                  <div class="panel-heading" role="tab" id="<?php echo strtolower($form['formCode']);?>">
                                      <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo strtolower($form['formCode']);?>" aria-expanded="false" aria-controls="collapseTwo">
                                              <?php echo $form['formName'];?> Attendance
                                          </a>
                                      </h4>
                                  </div>
                                  <div id="collapse<?php echo strtolower($form['formCode']);?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="<?php echo strtolower($form['formCode']);?>">
                                      <div class="panel-body">
                                          <form style="display:inline;" name=<?php echo '"formNewAtt_'. $form['formCode'].'"';  ?> method="post" action="<?php echo base_url('admin/'.strtolower($form['formCode']).'attendance');?>" >
                                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                <label for="formCode" class="control-label">Form Code <span class="star">*</span></label>
                                                <input required="required" class="form-control" name="formCode" id="formCode" placeholder="" value="<?php echo $form['formCode'];?>">
                                            </div>
                                            <button class="btn btn-primary btn-s" data-title="New Attendance" id=<?php echo '"newAtt_'. $form['formCode'].'"';  ?> name=<?php echo '""newAtt_'. $form['formCode'].'"';  ?>  type="submit"><span class="fa fa-plus-circle" ></span> New </button>
                                          </form>
                                          <br><br>
                                          <?php $tableId="table_".strtolower($form['formCode']);?>
                                          <table  class="table table-striped table-bordered table-hover display responsive" cellspacing="0" width="100%" id="<?php echo $tableId;?>">
                                              <thead>
                                                  <tr style="background: ;color: #000000;">
                                                      <th class="text-left">Record Date</th>
                                                      <th class="text-center">Present</th>
                                                      <th class="text-center">Absent</th>
                                                      <th class="text-center">Excused</th>
                                                      <th class="text-center">Total</th>
                                                      <th class="text-center"><i class="fa fa-cog"></i></th>
                                                   </tr>
                                              </thead>
                                              <tbody >
                                                 <?php if(isset($form['performattendancedates'])){foreach($form['performattendancedates'] as $attendanceperclass){?>
                                                    <?php $countall=$attendanceperclass['countAll'];?> 
                                                  <tr>
                                                      <td class="text-left"><?php  echo date_format(date_create($attendanceperclass['attendanceDate']),"D j<\s\up>S</\s\up> M, Y"); ?>
                                                      </td>
                                                      <td class="text-center">
                                                        <?php if($countall==0){echo "No entry";}else{
                                                        echo $attendanceperclass['countPresent']; echo " (".number_format(($attendanceperclass['countPresent']/$attendanceperclass['countAll'])*100, 1 )."%)"; 
                                                        }?>
                                                      </td>
                                                      <td class="text-center">
                                                        <?php if($countall==0){echo "No entry";}else{
                                                         echo $attendanceperclass['countAbsent']; echo " (".number_format(($attendanceperclass['countAbsent']/$attendanceperclass['countAll'])*100, 1 )."%)"; 
                                                         }?>
                                                      </td>
                                                      <td class="text-center">
                                                        <?php if($countall==0){echo "No entry";}else{
                                                         echo $attendanceperclass['countExcused']; echo " (".number_format(($attendanceperclass['countExcused']/$attendanceperclass['countAll'])*100, 1 )."%)";
                                                         }?>
                                                      </td>
                                                      <td class="text-center">
                                                        <?php if($countall==0){echo "No entry";}else{ echo $attendanceperclass['countAll'];}?>
                                                      </td>
                                                      <td class="text-center">
                                                         <form style="display:inline;" name=<?php echo '"formView_'. strtolower($form[ 'formCode']). '"'; ?> method="post" action="<?php echo base_url('admin/viewmenteeattendance');?>">
                                                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                                <label for="formCode" class="control-label">Form Code*</label>
                                                                <input required="required" class="form-control" name="formCode" id="formCode" placeholder="" value="<?php echo $form['formCode']; ?>">
                                                            </div>
                                                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                                <label for="attendanceDate" class="control-label">Attendance Date*</label>
                                                                <input required="required" class="form-control" name="attendanceDate" id="attendanceDate" placeholder="" value="<?php echo $attendanceperclass['attendanceDate']; ?>">
                                                            </div>
                                                            <button class="btn btn-default btn-s" data-title="View Attendance" id=<?php echo '"view_'. strtolower($form[ 'formCode']). '"'; ?> name=<?php echo '"view_'. strtolower($form[ 'formCode']).'"';  ?>  type="submit"><i class="fa fa-eye"></i> View</button>
                                                        </form>

                                                      </td>
                                                        <?php  ?>
                                                  </tr>
                                                  <?php }} ?>
                                              </tbody>
                                          </table>
                                          <!-- /.table-responsive -->
                                          <?php  echo  "<script>
                                              $(document).ready(function () { 
                                                  $('#";echo $tableId."').dataTable({'responsive': true,'iDisplayLength': 50,'lengthMenu': [[50, 200, 500, -1], [50, 200, 500, 'All']],responsive:true, 'aoColumnDefs': [{ 'aTargets': [5], 'orderable': false}],'aaSorting': [],dom: 'lBfrtip', buttons: [{extend: 'print',title:'',exportOptions: {columns:[0,1,2,3,4]   } },{extend: 'excel',title:'',exportOptions: {columns:[0,1,2,3,4]   } },{extend: 'pdf',title:'',exportOptions: {columns:[0,1,2,3,4]   } }] }); 
                                              });//close document.ready

                                          </script>";?>
                                      </div>
                                  </div>
                              </div>
                              
                              <?php }?>
                              
                          </div>
                      </div><!--- END COL -->   
                    </div>
                </div>
                  <div class="tab-pane fade" id="tab_default_5">
                    <div class="col-md-12">
                        
                     </div>
                  </div>
              </div>
            </div>
        </div>

                </div>
            </div>


            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div> <!-- /.col -->
      </div><!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
  (function () {var previous; $("#sel1").on('focus', function () {/*Store the current value on focus and on change*/ previous = this.value;}).change(function() {/* Remove class "active" and "in" from previous to make it inactive*/ $("#"+previous).removeClass('active in'); /* Make sure the previous value is updated*/previous = this.value;/*add class "active" and "in" to the current to make it active */$("#"+previous).addClass('active in');});})();
$(document).ready(function () {
       $('#menteeslist').dataTable({'responsive': true,'iDisplayLength': 50,'lengthMenu': [[50, 200, 500, -1], [50, 200, 500, 'All']],'aaSorting': [] }); 
        $('#macheoexams').DataTable({'iDisplayLength': 50,'lengthMenu': [[50, 200, 500, -1], [50, 200, 500, 'All']],columnDefs: [{ orderable: false,targets: [4]}],dom: 'lBfrtip', 
                buttons: [{extend: 'print',title:'',exportOptions: {columns:[0,1,2,3]   } },{extend: 'excel',title:'',exportOptions: {columns:[0,1,2,3]   } },{extend: 'pdf',title:'',exportOptions: {columns:[0,1,2,3]   } }]
                // buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],exportOptions: {columns:[0,1,2,3]}
            });
        $('#generalexams').DataTable({'iDisplayLength': 50,'lengthMenu': [[50, 200, 500, -1], [50, 200, 500, 'All']],columnDefs: [{ orderable: false,targets: [4]}],dom: 'lBfrtip', 
                buttons: [{extend: 'print',title:'',exportOptions: {columns:[0,1,2,3]   } },{extend: 'excel',title:'',exportOptions: {columns:[0,1,2,3]   } },{extend: 'pdf',title:'',exportOptions: {columns:[0,1,2,3]   } }]
                // buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],exportOptions: {columns:[0,1,2,3]}
            });
        $('#attendancelist').dataTable({'responsive': true,'iDisplayLength': 10,'lengthMenu': [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],responsive:true, 'aoColumnDefs': [{ 'aTargets': [5], 'orderable': false}],'aaSorting': [],dom: 'lBfrtip', 
        buttons: [{extend: 'print',title:'',exportOptions: {columns:[0,1,2,3,4]   } },{extend: 'excel',title:'',exportOptions: {columns:[0,1,2,3,4]   } },{extend: 'pdf',title:'',exportOptions: {columns:[0,1,2,3,4]   } }] });
});

//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
