<!DOCTYPE html>
<html>

<head>
    <title>Macheo | Parents/Guardians</title>
    <?php $this->load->view('headerlinks/headerlinks.php'); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
    <div class="wrapper">
        <?php $this->load->view('admin/adminnav.php'); ?>
        <!--navigation -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="row">
                    <div class="col-lg-12 ">
                        <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Parents/Guardians</h4>
                        <div class="pull-right">
                            <span data-placement="top" data-toggle="tooltip" title="Refresh">
                    <button class="btn btn-s" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                            </span>
                            <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-s" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>&nbsp;Print All</a>
                            </span>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <div class="box box-solid collapsed-box" style="background:lightgrey">
                                    <div class="box-header">
                                        <h3 class="box-title" style="color: #21618C;">New Parent/Guardian</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                            <!-- <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button> -->
                                        </div>
                                    </div>
                                    <div style="display: none;background-color: #FFFFFF;color: #000000;border-bottom: 2px solid;border-color: #979A9A;" class="box-body">
                                        <?php echo form_open_multipart('admin/newparent',array('id' => 'guardian_registration','method'=>'post'));?>
                                        <div class="row setup-content">
                                            <div class="col-xs-12">
                                                <div class="col-md-6">
                                                    <div class="col-md-9 col-md-offset-3">
                                                        <div class="form-group">
                                                            <div class="main-img-preview">
                                                                <img class="thumbnail img-preview" src="<?php echo base_url();?>assets/img/person.png" title="Mentor Photo" width="210" height="230">
                                                            </div>
                                                            <!-- <p class="help-block">* Upload mentee passport photo.</p> -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-md-offset-4">
                                                        <div class="input-group">
                                                            <input id="fakeUploadLogo" class="form-control fake-shadow" disabled="disabled" style="display: none; ">
                                                            <div class="input-group-btn">
                                                                <div class="fileUpload btn btn-default fake-shadow">
                                                                    <span><i class="fa fa-upload"></i> Upload Photo</span>
                                                                    <input id="photo-id" name="photo" type="file" class="attachment_upload">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12 col-lg-12">
                                                        <label for="firstName" class="control-label">First Name <span class="star">*</span></label>
                                                        <input type="text" name="firstName" placeholder="" class=" form-control" id="firstName" required="required" maxlength="20">
                                                    </div>
                                                    <div class="form-group col-md-12 col-lg-12">
                                                        <label for="lastName" class="control-label">Last Name <span class="star">*</span></label>
                                                        <input type="text" name="lastName" placeholder="" class=" form-control" id="lastName" required="required" maxlength="20">
                                                    </div>
                                                    <div class="form-group col-md-12 col-lg-12">
                                                        <label for="otherNames" class="control-label">Other Names</label>
                                                        <input type="text" name="otherNames" placeholder="" class=" form-control" id="otherNames" maxlength="20">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group col-md-6 col-lg-6">
                                                        <label class="">Gender <span class="star">*</span></label><br>
                                                        <label class="radio-inline ">
                                                <input type="radio" name="gender" id="female" value="Female" autocomplete="off" required="required">Female
                                            </label>

                                                        <label class="radio-inline ">
                                                <input type="radio" name="gender" id="male" value="Male" autocomplete="off" required="required">Male
                                            </label>
                                                    </div>
                                                    <div class="form-group col-md-12 col-lg-12">
                                                        <label for="dob" class="control-label">Date of Birth</label>
                                                        <div class="form-group">
                                                            <div class='input-group date' id='dob'>
                                                                <input type='text' class="form-control" readonly="true" name="dob" />
                                                                <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12 col-lg-12">
                                                        <label for="phoneNumber" class="control-label"> Current Phone No. <span class="star">*</span></label>
                                                        <input type="text" name="phoneNumber" class=" form-control" id="phoneNumber" required="required" data-mask="0799999999">
                                                    </div>
                                                    <div class="form-group col-md-12 col-lg-12">
                                                        <label for="phoneNumber2" class="control-label"> Alternative Phone No.</label>
                                                        <input type="text" name="phoneNumber2" class=" form-control" id="phoneNumber2" data-mask="0799999999">
                                                    </div>
                                                    <div class="form-group col-md-12 col-lg-12">
                                                        <label for="guardianId" class="control-label"> National ID <span class="star">*</span></label>
                                                        <input type="number" name="guardianId" class=" form-control" id="guardianId" required="required">
                                                    </div>

                                                    <div class="form-group col-md-12 col-lg-12">
                                                        <label for="emailAddress" class="control-label"> Email Address </label>
                                                        <input type="email" name="emailAddress" placeholder="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class=" form-control" id="emailAddress">
                                                    </div>

                                                    <div class="form-group col-md-12 col-lg-12">
                                                        <label for="profession" class="control-label"> Profession </label>
                                                        <input type="text" name="profession" placeholder="" class=" form-control" id="profession">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group col-md-6 col-lg-6">
                                                        <input type="submit" class="btn btn-primary" value="Submit">
                                                        <input type="reset" class="btn btn-default" value="Reset">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.col-xs-12-->
                                        </div>
                                        <!--/.setup-content-->
                                        <?php echo form_close();?>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->

                                <?php if(isset($_SESSION['msg']))
                                  {
                                    $msg = $_SESSION['msg'];
                                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                                    <div class="messagebox alert alert-danger" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <strong><span>';echo $msg['error']; echo '</span></strong>
                                      </div> 
                                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                                    <div class="messagebox alert alert-success" style="display: block">
                                      <button type="button" class="close" data-dismiss="alert">*</button>
                                      <div class="cs-text">
                                          <strong><span>';echo $msg['success'];echo '</span></strong>
                                      </div> 
                                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                                <table class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="guardianslist">
                                    <thead>
                                        <tr style="background: #2E4053;color: #F7F9F9  ;">
                                            <th class="text-left">Name</th>
                                            <th class="text-center">Phone Number</th>
                                            <th class="text-center">Gender</th>
                                            <th class="text-center">Students</th>
                                            <th class="text-center"></th>
                                        </tr>
                                    </thead>
                                    <tbody style="color: #17202A;">
                                        <?php  foreach($guardians as $guardian){ ?>
                                        <tr>
                                            <?php $photo=$guardian['guardianProfilePhoto']; if($photo==""){$profile="defaultimage.png";}else{$profile=$guardian['guardianProfilePhoto'];}?>
                                            <td class="text-left"><img src="<?php echo base_url();echo 'uploads/profile_photos/guardians/'.$profile?>" width="25" height="25" class="img-circle" alt="Mentor Photo">
                                                <?php  echo $guardian['guardianFname']. " ".$guardian['guardianLname']; ?> </td>
                                            <td class="text-center">
                                                <?php  echo $guardian['guardianPhone1'];  ?>
                                            </td>
                                            <td class="text-center">
                                                <?php  echo $guardian['guardianGender']; ?>
                                            </td>
                                             <td class="text-center">
                                                <?php echo $guardian['mentees']; ?> 
                                            </td>
                                            <td class="text-center">
                                                <form style="display:inline;" name="form_<?php echo $guardian['guardianAutoId'];?> " method="post" action="#">
                                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                        <label for="guardianUID" class="control-label">guardian ID*</label>
                                                        <input required="required" class="form-control" name="guardianUID" id="guardianUID" placeholder="101" value="<?php echo $guardian['guardianAutoId']; ?>">
                                                    </div>
                                                    <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                         <label for="guardianName" class="control-label">guardian Name*</label>
                                                        <input required="required" class="form-control" name="guardianName" id="guardianName" placeholder="101" value="<?php echo $guardian['guardianFname']." ".$guardian['guardianLname']; ?>">
                                                    </div>
                                                    <div class="input-group" style="padding: 0px!important;">
                                                        <span class="input-group-addon" style="padding: 0px!important;margin: 0px!important;border: 0px!important">
                                                            <button class="btn btn-primary" >Go!</button> 
                                                        </span>
                                                        <select class="form-control" style="padding: 0px!important;margin: 0px!important;border-radius: 5px!important;width: 80px !important;font-family: 'FontAwesome',serif;" id="action_select" onchange="actionBase(this.parentNode.parentNode, this.options[this.selectedIndex].value, '<?php echo base_url();?>admin/')">
                                                            <option value="#">Action</option>
                                                            <option value="guardianprofile">&#xf06e; Profile</option>
                                                            <option value="editguardian">&#xf044; Edit</option>
                                                            <option value="disableguardian">&#xf05e; Disable</option>
                                                            <option value="delguardian">&#xf1f8; Delete</option>
                                                            <option value="guardianmentees">&#xf0c0; Students</option>
                                                        </select>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('footer');?>
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
    <script>
        function actionBase(form,value,url){
            //set the action 
            form.setAttribute('action',url+value);
        }
        $(document).ready(function() {
            //datatable initialization
          $('#guardianslist').DataTable({'iDisplayLength': 50,'lengthMenu': [[50, 200, 500, -1], [50, 200, 500, 'All']],columnDefs: [{ orderable: false,targets: [4]}],dom: 'lBfrtip', 
                buttons: [{extend: 'print',exportOptions: {columns:[0,1,2,3]   } },{extend: 'excel',exportOptions: {columns:[0,1,2,3]   } },{extend: 'pdf',exportOptions: {columns:[0,1,2,3]   } }]
                // buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],exportOptions: {columns:[0,1,2,3]}
            });
            var submitBtn = $('input[type="submit"]');
            // allWells.show();
            submitBtn.click(function() {
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    curInputs = curStep.find("input,select"),
                    isValid = true;
                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }
                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });

            //image upload
            var brand = document.getElementById('photo-id');
            brand.className = 'attachment_upload';
            brand.onchange = function() {
                document.getElementById('fakeUploadLogo').value = this.value.substring(12);
            };

            // Source: http://stackoverflow.com/a/4459419/6396981
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('.img-preview').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#photo-id").change(function() {
                readURL(this);
            });
            //datepicker
            $('#dob').datepicker({
                format: "yyyy-mm-dd",
                todayHighlight: true
            });
        });
        //to refresh the page
        $("#refresh").click(function(event) {
            window.setTimeout(function() {
                location.reload()
            }, 1)

        });

    </script>
</body>

</html>
