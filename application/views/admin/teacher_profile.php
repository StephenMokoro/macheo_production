<!DOCTYPE html>
<html>
<head>
   <title>Macheo | Teacher Profile</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Teacher Profile</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
             <?php foreach($teacher_profile as $profile){ 
                 $photo=$profile['teacherProfilePhoto']; if($photo==""){$ppic="defaultimage.png";}else{$ppic=$profile['teacherProfilePhoto'];}?>
                    <a href="<?php echo base_url();?>admin/download_teacherphoto/<?php echo $ppic;?>">
                      <div class="col-md-3" style="text-align: center;margin-right: auto">
                        <div class="col-md-12" style="display: inline-block;text-align: center">
                            <img src="<?php echo base_url();echo 'uploads/profile_photos/teachers/'.$ppic?>" alt="PPIC" class="img-rounded img-responsive" />
                            <b><p style="color: #000000;"><?php echo $profile['teacherFname']." ".$profile['teacherLname'];?></p></b>
                        </div>
                    </div></a>
                    <div class="col-md-9 text-center">
                            <div class="col-md-12" style="text-align: left">
                                <blockquote >
                                     <p><i class="fa fa-calendar-o text-primary fa-1x"></i> <?php echo date_format(date_create($profile['teacherDateAdded']),"j<\s\up>S</\s\up> M, Y");?>  <span><small style="display: inline">Date Registered </small></cite></span> </p>

                                     <p><i class="fa fa-venus-mars text-success fa-1x"></i> <?php echo $profile['teacherGender'];?>  <span><cite title=" <?php echo $profile['teacherGender'];?> "><small style="display: inline">Gender </small></cite></span> </p>

                                      <p><i class="fa fa-phone text-primary fa-1x"></i> <?php echo $profile['teacherPhone'];?>  <span><cite title="<?php echo $profile['teacherFname'];?>'s phone number"><small style="display: inline">Cell Phone  </small></cite></span> </p>

                                      <p><i class="fa fa-envelope text-success fa-1x"></i> <?php echo $profile['teacherEmail'];?>  <span><cite title="<?php echo $profile['teacherFname'];?>'s Email Address"><small style="display: inline">Email  </small></cite></span> </p>

                                      <p><i class="fa fa-id-card text-default fa-1x"></i> <?php echo $profile['teacherId'];?>  <span><cite title="<?php echo $profile['teacherFname'];?>'s Identification"><small style="display: inline">ID/Passport  </small></cite></span> </p>

                                      <p><i class="fa fa-institution text-default fa-1x"></i> <?php echo $profile['schoolAlias'];?>  <span><cite title="<?php echo $profile['teacherFname'];?>'s School"><small style="display: inline">School </small></cite></span> </p>
                                </blockquote>
                            </div>
                    </div>
                <?php }?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
</body>
</html>
