<!DOCTYPE html>
<html>
<head>
  <title>Macheo | Assign Guardian</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?>
<link href="<?php echo base_url(); ?>assets/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<style> @media only screen and (min-width : 767px) and (max-width : 768px) {#guardianId{max-width: 100%!important;}}</style>
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav.php'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
     <section class="content-header">
      <div class="row">
          <div class="col-lg-12 ">
              <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> <?php  echo $statement;?></h4>
              <div class="pull-right">
                  <span data-placement="top" data-toggle="tooltip" title="Refresh">
                      <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                  </span>
                  <span data-placement="top" data-toggle="tooltip" title="Print All">
                      <a class="btn btn-xs" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>&nbsp;Print All</a>
                  </span>
              </div> 
          </div>
          <!-- /.col-lg-12 -->
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
                 <form role="form" method="post" action="<?php echo base_url(); ?>admin/assignguardian">
                    <?php  foreach($details as $info){ ?>
                    <div class="row setup-content" >
                        <div class="col-xs-12">
                            <div class="col-md-12">
                               <div class="form-group col-md-6 col-lg-6 " hidden="true">
                                    <label for="menteeUID" class="control-label">Mentee UID</label>
                                    <input type="text" name="menteeUID" placeholder="Guest Auto Id" class="form-control" id="menteeUID" required="required" value=<?php echo '"'.$info['menteeAutoId'].'"';   ?>>
                                </div>
                               <div class="form-group col-md-12 col-lg-12 ">
                                  <label for="fullName" class="control-label">Mentee Name: <span style="font-weight: normal;"><?php echo $info['menteeFname']." ".$info['menteeLname']; ?></span></label>
                                  <input type="text" name="fullName"  class="form-control" id="fullName" required="required" value=<?php echo '"'.$info['menteeFname']." ".$info['menteeLname'].'"';   }?> name="fullName" readonly="true" style="display: none;">
                              </div>
                                <div class="form-group col-md-12 col-lg-12">
                                    <label for="guardianId" class="control-label green">Select Guardian</label>
                                    <select type="text" name="guardianId" placeholder="" class="form-control" id="guardianId"></select>
                                </div>
                                <div class="form-group col-md-12 col-lg-12">
                                <div class="modal-header"></div>
                                    <br>
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                    <input type="reset" class="btn btn-default" value="Reset">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script src="<?php echo base_url();?>assets/select2/js/select2.min.js"></script>
<script>
  $(document).ready(function () {
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
//autocomplete for guardian to visit
      $('#guardianId').select2({
        placeholder: '--- Select Parent/Guardian ---',
        ajax: {
          url: "<?php echo base_url('admin/getGuardian'); ?>",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });


});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
