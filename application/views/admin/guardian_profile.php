<!DOCTYPE html>
<html>
<head>
   <title>Macheo | Parent/Guardian Profile</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?> 
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 ">
                <h4><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> Parent/Guardian Profile</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body"  >
             <?php foreach($guardian_profile as $profile){ ?>
                      <?php $photo=$profile['guardianProfilePhoto']; if($photo==""){$ppic="defaultimage.png";}else{$ppic=$profile['guardianProfilePhoto'];}?>
                           <a href="<?php echo base_url();?>admin/download_parentphoto/<?php echo $ppic;?>">
                            <div class="col-md-3" style="text-align: center;margin-right: auto">
                              <div class="col-md-12" style="display: inline-block;text-align: center">
                                  <img src="<?php echo base_url();echo 'uploads/profile_photos/guardians/'.$ppic?>" alt="Parent Photo" class="img-rounded img-responsive" />
                                  <b><p style="color: #000000;"><?php echo $profile['guardianFname']." ".$profile['guardianLname'];?></p></b>
                              </div>
                          </div></a>
                            <div class="col-md-9" style="text-align: left">
                                <blockquote >
                                  <?php $dob=strtotime($profile['guardianDoB']);if($dob==""){$age='<span class="fa fa-angle-left" </span><i> D.O.B not specified </i><span class="fa fa-angle-right" </span>';}else{$age=(date('Y-m-d')-strtotime($profile['guardianDoB']))." Years";}?>
                                     <p><i class="fa fa-user-circle text-primary fa-1x"></i> <?php echo $age;?>   <span><small style="display: inline">Age </small></cite></span> </p>

                                     <p><i class="fa fa-venus-mars text-success fa-1x"></i> <?php echo $profile['guardianGender'];?>  <span><cite title="when <?php echo $profile['guardianGender'];?> "><small style="display: inline">Gender </small></cite></span> </p>

                                      <p><i class="fa fa-phone text-primary fa-1x"></i> <?php echo $profile['guardianPhone1'];?>  <span><cite title="<?php echo $profile['guardianFname'];?>'s phone number"><small style="display: inline">Cell Phone  </small></cite></span> </p>

                                      <p><i class="fa fa-envelope text-success fa-1x"></i> <?php echo $profile['guardianEmail'];?>  <span><cite title="<?php echo $profile['guardianFname'];?>'s Email Address"><small style="display: inline">Email  </small></cite></span> </p>

                                       <p><i class="fa fa-info-circle text-default fa-1x"></i> <?php echo $profile['guardianProfession'];?>  <span><cite title="<?php echo $profile['guardianFname'];?>'s Profession"><small style="display: inline">Profession  </small></cite></span> </p>

                                       <p><i class="fa fa-users text-default fa-1x"></i> <?php echo $profile['mentees'];?> Student(s) <span><cite title="<?php echo $profile['guardianFname'];?>'s Children in Macheo Program" ><small style="display: inline">Active Macheo Mentees  </small></cite></span> </p>


                                </blockquote>
                            </div>
                           
                    </div>
                <?php }?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
</body>
</html>
