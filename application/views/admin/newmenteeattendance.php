<!DOCTYPE html>
<html>
<head>
  <title>Macheo | Attendance</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav.php'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="row" style="margin-bottom: -15px;">
          <div class="col-lg-12 ">
              <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span> <?php echo $title; ?></h4>
              <div class="pull-right">
                <span data-placement="top" data-toggle="tooltip" title="Refresh">
                    <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                </span>
                <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-xs" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>&nbsp;Print All</a>
                </span>
              </div> 
          </div>
          <!-- /.col-lg-12 -->
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
              <?php if(isset($_SESSION['msg']))
                  {
                    $msg = $_SESSION['msg'];
                    $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                    <div class="messagebox alert alert-danger" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-close"></i>
                          <strong><span>';echo $msg['error']; echo '</span></strong>
                      </div> 
                    </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                    <div class="messagebox alert alert-success" style="display: block">
                      <button type="button" class="close" data-dismiss="alert">*</button>
                      <div class="cs-text">
                          <i class="fa fa-check-circle-o"></i>
                          <strong><span>';echo $msg['success'];echo '</span></strong>
                      </div> 
                      </div>';} $_SESSION['msg'] =array('error'=>'','success'=>'');}else{ echo '<div></div>';}?>
                <?php echo form_open('admin/newmenteeattendance',array('id' => 'newmenteeattendance','method'=>'post'));?>
                <div class="row setup-content">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group col-md-12 col-lg-12">
                            <label for="attendanceDate" class="control-label">Attendance Date</label>
                            <div class="form-group">
                                <div class='input-group date' id='attendanceDate' >
                                    <input type='text' class="form-control" readonly="true" name="attendanceDate" style="background-color: #FFFFFF;" />
                                    <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group col-md-12 col-lg-12">
                            <label for="eventName" class="control-label">Event<span class="star">*</span></label>
                            <input type="text" name="eventName" placeholder="" class=" form-control" id="eventName" required="required" maxlength="50">
                        </div>
                    </div>
                </div>

                <table  class="table table-striped table-bordered table-hover display responsive" cellspacing="0" width="100%" id="menteeslist">
                  <thead>
                      <tr style="background: #2E4053;color: #F7F9F9;">
                        <th class="text-left">Mentee Name</th>
                        <th class="text-center">PRESENT</th>
                        <th class="text-center">ABSENT</th>
                        <th class="text-center">EXCUSED</th>
                      </tr>
                  </thead>
                  <tbody>
                     <?php $count=0; foreach($mentees as $mentee){?>
                      <tr>
                          <td class="text-left">
                            <?php  echo $mentee['menteeFname']. " ".$mentee['menteeLname'].'
                            <input type="number" class="form-control text-center" name="mentee['.$count.'][menteeId]" style="width:100%!important;display: none;" value="'.$mentee['menteeAutoId'].'">
                          </td>

                          <td class="text-center"><span class="text-success">PRESENT <input  type="radio" name="mentee['.$count.'][status]" value="PRESENT" ></span> &nbsp;&nbsp;</td>

                          <td class="text-center"><span class="text-danger">ABSENT <input type="radio" name="mentee['.$count.'][status]" value="ABSENT"></span>&nbsp;&nbsp;</td>

                          <td class="text-center"><span class="text-info">EXCUSED <input type="radio" name="mentee['.$count.'][status]" value="EXCUSED"></span> &nbsp;&nbsp;</td>';?>
                      </tr>
                      <?php $count=$count +1;} ?>
                  </tbody>
              </table>
               <div class="col-md-9 col-md-offset-3 navbar-fixed-bottom text-center">
                    <!-- <div class="modal-header"></div> -->
                        <button id="submit" class="btn btn-primary " style="margin-top: 10px;margin-bottom:20px;width: 100px;opacity: 0.7;" >Submit</button>
                    </div>
                </div>
              <?php echo form_close(); ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    //datatable initialization
     $('#menteeslist').dataTable({responsive:true,paging:false, "aoColumnDefs": [{ "aTargets": [2,3], "orderable": false}] });
      
    // date
      $(function() {$('#attendanceDate').datepicker({format: "yyyy-mm-dd", minDate: new Date(), todayHighlight: true });});
});

//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
