<!DOCTYPE html>
<html>
<head>
  <title>Macheo | Subject Selection</title>
<?php $this->load->view('headerlinks/headerlinks.php'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;">
<div class="wrapper">
<?php $this->load->view('admin/adminnav.php'); ?><!--navigation -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="row">
          <div class="col-lg-12 ">
              <h4 class="pull-left"><b>Dashboard</b> <span class="fa fa-angle-double-right"></span>  <b style="color: #008080;"><?php echo $form; ?> Subjects Selection</b></h4>
              <div class="pull-right">
                <span data-placement="top" data-toggle="tooltip" title="Refresh">
                    <button class="btn btn-xs" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
                </span>
                <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-xs" data-title="Print All" type="button" href="#"><span class="fa fa-print"></span>&nbsp;Print All</a>
                </span>
              </div> 
          </div>
          <!-- /.col-lg-12 -->
      </div>
    </section> 
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box" >
            <div class="box-body">
                <div class="col-md-12 col-lg-12" style="margin-top: 10px;">
                    <div class="messagebox alert alert-success" style="display: none;" id="success">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-check"></i>
                            <strong><span>Subjects Recorded</span></strong>
                        </div> 
                    </div>
                    <div class="messagebox alert alert-info" style="display: none;" id="isEmpty">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-info-circle"></i>
                            <strong><span>No subjects selected</span></strong>
                        </div> 
                    </div>
                </div>
                 
                  <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="list"  >
                    <thead>
                        <tr style="background: #2E4053;color: #F7F9F9;">
                                    
                            <th class="text-left">Mentee Name</th>
                            <th class="text-center" hidden="true">Mentee PID</th>
                            <?php  foreach($subjects as $sub){ ?>
                            <th class="text-center"><?php  echo $sub['subjectCode'];}?></th>
                         </tr>
                    </thead>
                    <tbody >
                        <?php  foreach($mentees as $mentee){ ?>
                        <tr>
                            <td class="text-left"><?php  echo $mentee['menteeFname']. " ".$mentee['menteeLname']; ?></td>
                            <td class="text-center" hidden="true"><?php  echo $mentee['menteeAutoId']; ?></td>
                            <?php  foreach($subjects as $sub){ ?>
                                <td class="text-center"><input type="checkbox" name=<?php echo '"subj_'. $sub['subjectAutoId'].'"';  ?> value=<?php echo '"'.$sub['subjectAutoId'].'"';  ?>><?php }?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
                <div class="col-md-9 col-md-offset-3 navbar-fixed-bottom text-center">
                    <!-- <div class="modal-header"></div> -->
                        <button id="submit" class="btn btn-primary " style="margin-top: 10px;margin-bottom:20px;width: 100px;opacity: 0.7;" >Submit</button>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('footer');?>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    //datatable initialization
     $('#list').DataTable({responsive:true,paging:false,"language": {"emptyTable": "All mentees have selected subjects"},"bInfo" : false
   });

    $( "#submit").on('click', function()
        {
            var selected = [];
            $('#list input:checkbox:checked').each(function() {
               var $item = $(this);
                selected.push({
                    // id: $item.attr("id"),
                   selectMenteeId: $('td:nth-child(2)', $(this).parents('tr')).text(),
                     selectSubjectId: $item.val()
                });
            });
            // alert(JSON.stringify(selected));
               $.ajax(//ajax script to post the data without page refresh
                {
                    type:"post",
                    url: "<?php echo base_url();?>admin/selectedsubjects",
                    dataType: "json",//note the contentType defintion
                    data: {selected:selected},
                    
                    success:function(data)
                    {
                        console.log(data);
                        if(data.successful) {
                              $("#success ").fadeTo(2000, 500).fadeOut("slow");
                              // window.setTimeout(function(){location.reload()},5000);

                            $("#list").load(window.location + " #list");//reload table on success
                            state="false";
                            }else if(data.empty){
                                $("#isEmpty").fadeTo(2000, 500).fadeOut("slow");
                            }
                    }
                });
             
        });
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
