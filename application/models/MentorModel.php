<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Mentormodel extends CI_Model
{
function __construct()
{
     parent::__construct();
     $this->load->database();
}
//list of all mentee/teacher schools
public function schools()
{
    $this->db->select('*');
    $this->db->from('schools');
    $result=$this->db->get()->result_array();
    return $result;
}
    //mentor mentees
public function mentorMentees($mentorId)
{
    $this->db->select('mentee.*,mship.*,mentor.mentorFname,mentor.mentorLname,sch.*,class.*');
    $this->db->from('mentees mentee');
    $this->db->join('mentorship mship', 'mentee.menteeAutoId=mship.mentorshipMenteeId','left');
    $this->db->join('schools  sch', 'sch.schoolAutoId=menteeSchoolId','left');
    $this->db->join('classes class', 'mentee.menteeAutoId=class.classMenteeId','left');
    $this->db->join('mentors mentor', 'mentor.mentorAutoId=mentee.menteeMentorId','left');
    $this->db->where('menteeMentorId',$mentorId);
	$this->db->where('mentee.menteeActiveStatus',1);
    $this->db->order_by('mentee.menteeFname');
    $result=$this->db->get()->result_array();
    return $result;
}
//get specific mentee
public function getMentee($menteeUID)
{
    $this->db->select('mentee.*,mship.*,sch.*,class.*,guardian.guardianFname,guardian.guardianLname,guardian.guardianPhone1,guardian.guardianPhone2,mentor.mentorFname,mentor.mentorLname');
    $this->db->from('mentees mentee');
    $this->db->join('mentorship mship', 'mentee.menteeAutoId=mship.mentorshipMenteeId','left');
    $this->db->join('schools  sch', 'sch.schoolAutoId=menteeSchoolId','left');
    $this->db->join('classes class', 'mentee.menteeAutoId=class.classMenteeId AND class.classYear='.date('Y'),'left');
    $this->db->join('mentors mentor', 'mentor.mentorAutoId=mentee.menteeMentorId','left');
    $this->db->join('guardians guardian','guardian.guardianAutoId=mentee.menteeGuardianId','left');
    $this->db->where('mentee.menteeAutoId',$menteeUID);
    // $this->db->where('classes.classYear',date('Y'));
    $result=$this->db->get()->result_array();
    return $result;
}
//count a specific mentors mentees
public function myMenteesCount($mentorId)
{
    $this->db->select('*');
    $this->db->from('mentees');
	$this->db->where('menteeMentorId',$mentorId);
    $this->db->where('menteeActiveStatus',1);
    $result=$this->db->get();
    if ( $result->num_rows() > 0 )
        {
            return $result->num_rows();
        }else
            {
                return 0;
            }
}
//mentees sessions    
public function registerMenteeSession($menteesessionInfo)
{
	if($this->db->insert('mentorship2',$menteesessionInfo))
        {
            return true;
        }
         else
            {
                return false;
            }
}
    //metorship sessions
 public function menteeSessions($pid)//$menteeAutoId)
{
    $this->db->select('*');
    $this->db->from('mentorship2');
    $this->db->where('menteeId', $pid);
    $result=$this->db->get()->result_array();
    return $result;
}
//attendance
public function menteeAtt($pid)//$menteeAutoId)
{
    $this->db->select('*');
    $this->db->from('menteeattendance');
    $this->db->where('attendanceMenteeId', $pid);
    $result=$this->db->get()->result_array();
    return $result;
}
public function exams()
{
	$this->db->select('*');
 	$this->db->from('generalexams');
	$result=$this->db->get()->result_array();
    return $result;;
}
	
}