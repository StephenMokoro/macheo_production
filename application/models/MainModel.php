<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @author Mokoro Stephen
*/
class MainModel extends CI_Model
{
    function __construct()
    {
         parent::__construct();
         $this->load->database();
    }

    //active administrators
    public function activeAdmins()
    {
        $this->db->select('*');
        $this->db->from('admins');
        $this->db->where('adminActiveStatus',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //get specific admin
    public function getAdmin($adminUID)
    {
        $this->db->select('*');
        $this->db->from('admins');
        $this->db->where('adminAutoId',$adminUID);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //Admin update -No new profile photo uploaded or uploaded photo has errors
    public function updateAdminNoPhoto($adminInfo,$adminUID)
    {
        $this->db->where('adminAutoId',$adminUID);
        $this->db->update('admins',$adminInfo);
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }
    }
    //admin update - new profile photo uploaded without errors 
    public function updateAdmin($adminInfo,$adminUID,$initFile)
    {
            $this->db->where('adminAutoId',$adminUID);
            $this->db->update('admins',$adminInfo);
            unlink("uploads/profile_photos/admins/".$initFile);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }

      //delete admin 
    public function deleteAdmin($adminUID)
    {
        
                
        //delete from class
        $this->db->where('adminAutoId',$adminUID);
        $query=$this->db->delete('admins');
        $affected=$this->db->affected_rows();
        if($affected>0)
            {
                return true;

            }else
                {
                     return false;
                }
    }
     public function disableAdmin($adminUID,$disableInfo)
    {
            $this->db->where('adminAutoId',$adminUID);
            $this->db->update('admins',$disableInfo);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //active mentors
    public function activeMentors()
    {
        $this->db->select('mentor.*,inst.*, COUNT(mentee.menteeMentorId) as mentees');
        $this->db->from('mentors mentor, institutions inst');
        $this->db->join('mentees mentee', 'mentor.mentorAutoId=mentee.menteeMentorId', 'left');
        $this->db->where('mentor.mentorInstitutionId=inst.institutionAutoId');
        $this->db->where('mentor.mentorActiveStatus',1);
        $this->db->group_by('mentor.mentorAutoId');
        $result=$this->db->get()->result_array();
        return $result;

    }
    //all active mentees
    public function activeMentorsForAttendance()
    {
            //get Ids of mentors whose attendance date is equovalent today's 
            $this->db->select('attendance.attendanceMentorId');
            $this->db->from('mentorattendance attendance');
            $this->db->where('attendance.attendanceDate ',date('Y-m-d'));
            $result=$this->db->get_compiled_select();

            $this->db->select('*');
            $this->db->from('mentors');
            $this->db->where("`mentorAutoId` NOT IN ($result)", NULL, FALSE);//select only mentors whose ids dont appear on the above query
            $this->db->where('mentorActiveStatus',1);
            $this->db->order_by('mentorFname');
            $result2=$this->db->get()->result_array();
            return $result2;
    }
    //get specific mentor
    public function getMentor($mentorUID)
    {
        $this->db->select('mentor.*,inst.*,course.*');
        $this->db->from('mentors mentor');
        $this->db->join('institutions inst','mentor.mentorInstitutionId=inst.institutionAutoId');
        $this->db->join('courses course','course.courseAutoId=mentor.mentorCourse','left');
        $this->db->where('mentor.mentorAutoId',$mentorUID);
        $result=$this->db->get()->result_array();
        return $result;
    }


    //Mentor update -No new profile photo uploaded or uploaded photo has errors
    public function updateMentorNoPhoto($mentorInfo,$mentorUID)
    {
        $this->db->where('mentorAutoId',$mentorUID);
        $this->db->update('mentors',$mentorInfo);
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }
    }
    //Mentor update - new profile photo uploaded without errors 
    public function updateMentor($mentorInfo,$mentorUID,$initFile)
    {
            $this->db->where('mentorAutoId',$mentorUID);
            $this->db->update('mentors',$mentorInfo);
            unlink("uploads/profile_photos/mentors/".$initFile);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
     //delete mentor 
    public function deletementor($mentorUID)
    {
        
                
        //delete from class
        $this->db->where('mentorAutoId',$mentorUID);
        $query=$this->db->delete('mentors');
        $affected=$this->db->affected_rows();
        if($affected>0)
            {
                return true;

            }else
                {
                     return false;
                }
    }
     public function disablementor($mentorUID,$disableInfo)
    {
            $this->db->where('mentorAutoId',$mentorUID);
            $this->db->update('mentors',$disableInfo);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //list of all institutions
    public function institutions()
    {
        $this->db->select('*');
        $this->db->from('institutions');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //all active mentees
    public function activeMentees()
    {
        $this->db->select('mentee.*,mentor.mentorFname,mentor.mentorLname,sch.*,class.*');
        $this->db->from('mentees mentee');
        // $this->db->join('mentorship mship', 'mentee.menteeAutoId=mship.mentorshipMenteeId','left');
        $this->db->join('schools  sch', 'sch.schoolAutoId=mentee.menteeSchoolId');
        $this->db->join('classes class', 'mentee.menteeAutoId=class.classMenteeId');
        $this->db->join('mentors mentor', 'mentor.mentorAutoId=mentee.menteeMentorId','left');
        $this->db->where('mentee.menteeActiveStatus',1);
        $this->db->order_by('mentee.menteeFname');
        $result=$this->db->get()->result_array();
        return $result;
    }

    public function activeMenteesPerSchool($schoolUID)
    {
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId',$schoolUID);
        $result2=$this->db->get_compiled_select();


        $this->db->select('mentee.menteeFname,mentee.menteeLname,mentee.menteeAutoId,mentor.mentorFname,mentor.mentorLname,sch.*,class.*');
        $this->db->from('mentees mentee');
        $this->db->join('schools  sch', 'sch.schoolAutoId=mentee.menteeSchoolId');
        $this->db->join('classes class', 'mentee.menteeAutoId=class.classMenteeId');
        $this->db->where("mentee.menteeAutoId IN ($result2)", NULL, FALSE);//select only mentees whose ids appear on the above query
        $this->db->join('mentors mentor', 'mentor.mentorAutoId=mentee.menteeMentorId','left');
        $this->db->where('mentee.menteeActiveStatus',1);
        $this->db->order_by('mentee.menteeFname');
        $result=$this->db->get()->result_array();
        return $result;

    }
     //all active mentees per mentor
    public function activeMenteesPerMentor($mentorUID)
    {
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeMentorId',$mentorUID);
        $result2=$this->db->get_compiled_select();


        $this->db->select('mentee.menteeFname,mentee.menteeLname,mentee.menteeAutoId,mentee.menteeProfilePhoto, sch.*,class.*');
        $this->db->from('mentees mentee');
        $this->db->join('mentorship mship', 'mentee.menteeAutoId=mship.mentorshipMenteeId','left');
        $this->db->join('schools  sch', 'sch.schoolAutoId=mentee.menteeSchoolId','left');
        $this->db->join('classes class', 'mentee.menteeAutoId=class.classMenteeId','left');
        $this->db->where("mentee.menteeAutoId IN ($result2)", NULL, FALSE);//select only mentees whose ids appear on the above query
        $this->db->where('mentee.menteeActiveStatus',1);
        $this->db->order_by('mentee.menteeFname');
        $result=$this->db->get()->result_array();
        return $result;
    }
     //all active mentees per mentor
    public function activeMenteesPerGuardian($guardianUID)
    {
         $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeGuardianId',$guardianUID);
        $result2=$this->db->get_compiled_select();


        $this->db->select('mentee.menteeFname,mentee.menteeLname,mentee.menteeAutoId,mentee.menteeProfilePhoto, sch.*,class.*');
        $this->db->from('mentees mentee');
        $this->db->join('mentorship mship', 'mentee.menteeAutoId=mship.mentorshipMenteeId','left');
        $this->db->join('schools  sch', 'sch.schoolAutoId=mentee.menteeSchoolId','left');
        $this->db->join('classes class', 'mentee.menteeAutoId=class.classMenteeId','left');
        $this->db->where("mentee.menteeAutoId IN ($result2)", NULL, FALSE);//select only mentees whose ids appear on the above query
        $this->db->where('mentee.menteeActiveStatus',1);
        $this->db->order_by('mentee.menteeFname');
        $result=$this->db->get()->result_array();
        return $result;
    }

    //update mentee attendance
    public function updateMenteeAttendance($attendanceInfo,$recordId)
    {
            $this->db->where('attendanceAutoId',$recordId);
            $this->db->update('menteeattendance',$attendanceInfo);
            $affected=$this->db->affected_rows();
            if($affected>0)
            {
                return true;

            }else
                {
                    return false;
                }
    }

    //all active mentees
    public function activeMenteesForAttendance()
    {

            $this->db->select('attendance.attendanceMenteeId');
            $this->db->from('menteeattendance attendance');
            $this->db->where('attendance.attendanceDate ',date('Y-m-d'));
            $result=$this->db->get_compiled_select();

            $this->db->select('*');
            $this->db->from('mentees');
            $this->db->where("`menteeAutoId` NOT IN ($result)", NULL, FALSE);
            $this->db->where('menteeActiveStatus',1);
            $this->db->order_by('menteeFname');
            $result2=$this->db->get()->result_array();
            return $result2;
    }
    //all active f2 mentees
    public function activeF2MenteesForAttendance()
    {

            $this->db->select('attendance.attendanceMenteeId');
            $this->db->from('menteeattendance attendance');
            $this->db->where('attendance.attendanceDate ',date('Y-m-d'));
            $result=$this->db->get_compiled_select();

            $this->db->select('classMenteeId');
            $this->db->from('classes');
            $this->db->where('formCode',"FORM2");
            $result2=$this->db->get_compiled_select();//ids of f2 mentees

            $this->db->select('*');
            $this->db->from('mentees');
            $this->db->where("`menteeAutoId` NOT IN ($result)", NULL, FALSE);
            $this->db->where("`menteeAutoId` IN ($result2)", NULL, FALSE);
            $this->db->where('menteeActiveStatus',1);
            $this->db->order_by('menteeFname');
            $result3=$this->db->get()->result_array();
            return $result3;
    }
    //all active f3 mentees
    public function activeF3MenteesForAttendance()
    {
            $this->db->select('attendance.attendanceMenteeId');
            $this->db->from('menteeattendance attendance');
            $this->db->where('attendance.attendanceDate ',date('Y-m-d'));
            $result=$this->db->get_compiled_select();

            $this->db->select('classMenteeId');
            $this->db->from('classes');
            $this->db->where('formCode',"FORM3");
            $result2=$this->db->get_compiled_select();//ids of f3 mentees

            $this->db->select('*');
            $this->db->from('mentees');
            $this->db->where("`menteeAutoId` NOT IN ($result)", NULL, FALSE);
            $this->db->where("`menteeAutoId` IN ($result2)", NULL, FALSE);
            $this->db->where('menteeActiveStatus',1);
            $this->db->order_by('menteeFname');
            $result3=$this->db->get()->result_array();
            return $result3;
    }
    //all active f4 mentees
    public function activeF4MenteesForAttendance()
    {

            $this->db->select('attendance.attendanceMenteeId');
            $this->db->from('menteeattendance attendance');
            $this->db->where('attendance.attendanceDate ',date('Y-m-d'));
            $result=$this->db->get_compiled_select();

            $this->db->select('classMenteeId');
            $this->db->from('classes');
            $this->db->where('formCode',"FORM4");
            $result2=$this->db->get_compiled_select();//ids of f2 mentees

            $this->db->select('*');
            $this->db->from('mentees');
            $this->db->where("`menteeAutoId` NOT IN ($result)", NULL, FALSE);
            $this->db->where("`menteeAutoId` IN ($result2)", NULL, FALSE);
            $this->db->where('menteeActiveStatus',1);
            $this->db->order_by('menteeFname');
            $result3=$this->db->get()->result_array();
            return $result3;
    }
    //all active mentees not assigned general subjects
    public function activeGeneralF2UnassignedMentees()
    {
        $this->db->select('*');
        $this->db->from('mentees');
        $this->db->where('menteeAutoId IN (select classMenteeId from classes WHERE formCode="FORM2")',NULL,FALSE);
        $this->db->where('menteeAutoId NOT IN (select selectMenteeId from selectedgeneralsubjects)',NULL,FALSE);
        $this->db->where('menteeActiveStatus',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //all active f3 mentees not assigned general subjects
    public function activeGeneralF3UnassignedMentees()
    {
        $this->db->select('*');
        $this->db->from('mentees');
        $this->db->where('menteeAutoId IN (select classMenteeId from classes WHERE formCode="FORM3")',NULL,FALSE);
        $this->db->where('menteeAutoId NOT IN (select selectMenteeId from selectedgeneralsubjects)',NULL,FALSE);
        $this->db->where('menteeActiveStatus',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //all active f4 mentees not assigned general subjects
    public function activeGeneralF4UnassignedMentees()
    {
        $this->db->select('*');
        $this->db->from('mentees');
        $this->db->where('menteeAutoId IN (select classMenteeId from classes WHERE formCode="FORM4")',NULL,FALSE);
        $this->db->where('menteeAutoId NOT IN (select selectMenteeId from selectedgeneralsubjects)',NULL,FALSE);
        $this->db->where('menteeActiveStatus',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //all active mentees not assigned general subjects
    public function activeMacheoUnassignedMentees()
    {
        $this->db->select('*');
        $this->db->from('mentees');
        $this->db->where('menteeAutoId NOT IN (select selectMenteeId from selectedmacheosubjects)',NULL,FALSE);
        $this->db->where('menteeActiveStatus',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //all active class 2 mentees
    public function activeF2Mentees()
    {
        $this->db->select('mentee.*,mship.*,mentor.mentorFname,mentor.mentorLname,sch.*,class.*');
        $this->db->from('mentees mentee');
        $this->db->join('mentorship mship', 'mentee.menteeAutoId=mship.mentorshipMenteeId','left');
        $this->db->join('schools  sch', 'sch.schoolAutoId=menteeSchoolId','left');
        $this->db->join('classes class', 'mentee.menteeAutoId=class.classMenteeId','left');
        $this->db->join('mentors mentor', 'mentor.mentorAutoId=mentee.menteeMentorId','left');
        $this->db->where('mentee.menteeActiveStatus',1);
        $this->db->order_by('mentee.menteeFname');

        $this->db->where('class.formCode',"FORM2");
        $this->db->where('mentee.menteeActiveStatus',1);
        $this->db->order_by('mentee.menteeFname');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //all active mentees per class
    public function activeMenteesPerForm($examFormCode,$examAutoId,$selected_subjects_table_name)
    {
         //select distinct mentee ids in selected subjects
        $this->db->distinct('selectMenteeId');
        $this->db->select('selectMenteeId');
        $this->db->from($selected_subjects_table_name);
        $result1=$this->db->get_compiled_select();

        $this->db->select('mentee.menteeFname,mentee.menteeLname,mentee.menteeAutoId,class.classAutoId,class.classMenteeId,class.formCode');
        $this->db->from('mentees mentee');
        $this->db->join('classes class', 'mentee.menteeAutoId=class.classMenteeId');
        $this->db->where("mentee.menteeAutoId IN ($result1)", NULL, FALSE);//select only mentees whose ids appear on $result1 in the above query
        $this->db->where('class.formCode',$examFormCode);
        $this->db->where('mentee.menteeActiveStatus',1);
        $this->db->order_by('mentee.menteeFname');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //all active mentees per class
    public function activeMenteesPerFormWithAtLeastOneScoreEntry($examFormCode,$examAutoId,$selected_subjects_table_name,$performance_table_name)
    {
         //select distinct mentee ids in selected subjects
        $this->db->distinct('selectMenteeId');
        $this->db->select('selectMenteeId');
        $this->db->from($selected_subjects_table_name);
        $result1=$this->db->get_compiled_select();

         //select distinct mentee ids with at least a score recorded for the exam
        $this->db->distinct('perfMenteeId');
        $this->db->select('perfMenteeId');
        $this->db->from($performance_table_name);
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where("`perfMenteeId` IN ($result1)", NULL, FALSE);//select only mentees whose ids appear on $result1 in the above query
        $result2=$this->db->get_compiled_select();

        $this->db->select('mentee.menteeFname,mentee.menteeLname,mentee.menteeAutoId,class.classAutoId,class.classMenteeId,class.formCode');
        $this->db->from('mentees mentee');
        $this->db->join('classes class', 'mentee.menteeAutoId=class.classMenteeId');
        $this->db->where("mentee.menteeAutoId IN ($result2)", NULL, FALSE);//select only mentees whose ids appear on $result1 in the above query
        $this->db->where('class.formCode',$examFormCode);
        $this->db->where('mentee.menteeActiveStatus',1);
        $this->db->order_by('mentee.menteeFname');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //all active mentees per class taking subject
    public function activeMenteesPerFormTakingSubjectMarksNotRecorded($subjectId,$formCode,$examAutoId,$performance_table_name,$selected_subjects_table_name,$subjects_table)
    {
        //first, select ids of active mentees belonging to the form and whose marks have been recorded for the specific exam and subject
        $this->db->select('mentee.menteeAutoId');
        $this->db->from('mentees mentee');
        $this->db->join('classes class', 'mentee.menteeAutoId=class.classMenteeId');
        $this->db->join($performance_table_name.' perf', 'perf.perfMenteeId=mentee.menteeAutoId');
        $this->db->where('perf.perfExamId',$examAutoId);
        $this->db->where('perf.perfSubjectId',$subjectId);
        $this->db->where('class.formCode',$formCode);
        $result1=$this->db->get_compiled_select();

        $this->db->select('mentee.*,subs.*');
        $this->db->from('mentees mentee');
        $this->db->join($selected_subjects_table_name.' selectsubs', 'mentee.menteeAutoId=selectsubs.selectMenteeId AND selectsubs.selectSubjectId='.$subjectId);
        $this->db->join($subjects_table.' subs', 'subs.subjectAutoId=selectsubs.selectSubjectId');
        $this->db->where("mentee.menteeAutoId NOT IN ($result1)", NULL, FALSE);//select only mentees whose ids do not appear on the above query
        $this->db->join('classes class', 'mentee.menteeAutoId=class.classMenteeId');
        $this->db->where('class.formCode',$formCode);//only those belongin to the particular class
        $this->db->where('mentee.menteeActiveStatus',1);
        $this->db->order_by('mentee.menteeFname');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //General subject score
    public function getSubjectScorePerMentee($subjectId,$examAutoId,$pid,$performance_table_name)
    {
        $this->db->select('perfScore');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfMenteeId',$pid);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            $row = $query->row(); 
            return $row->perfScore;
        }else{
            return "-";
        }
    }

    //count of  scores per  subject in an exam
    public function getSubjectScores($subjectId,$examAutoId,$performance_table_name)
    {
        $this->db->select('perfScore');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        // $this->db->where('perfMenteeId',$pid);
        $this->db->order_by('perfScore','DESC');//arrange from largest in position 1
        return $this->db->get()->result_array();//unique scores
    }

    //count of all scores per  subject in an exam
    public function getSubjectTotalScoreEntries($subjectId,$examAutoId,$performance_table_name)
    {
        $this->db->select('COUNT(perfScore) as totalEntries');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        $result = $this->db->get()->row()->totalEntries;
        if ($result > 0)
        {
            return '/'.$result;
        }else{
            return "-";
        }
    }

    // subject score
    public function getSubjectGrade($subjectId,$examAutoId,$pid,$performance_table_name)
    {
        $this->db->select('perfScore');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfMenteeId',$pid);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            $row = $query->row(); 
            $score= $row->perfScore;
            if($score >=81 && $score <=100)
            {
                return "A";
            }else if($score >=74 && $score <=80)
            {
                return "A⁻";
            }else if($score >=67 && $score <=73)
            {
                return "B⁺";
            }else if($score >=60 && $score <=66)
            {
                return "B";
            }else if($score >=53 && $score <=59)
            {
                return "B⁻";
            }else if($score >=46 && $score <=52)
            {
                return "C⁺";
            }else if($score >=39 && $score <=45)
            {
                return "C";
            }else if($score >=32 && $score <=38)
            {
                return "C⁻";
            }else if($score >=25 && $score <=31)
            {
                return "D⁺";
            }else if($score >=18 && $score <=24)
            {
                return "D";
            }else if($score >=11 && $score <=17)
            {
                return "D⁻";
            }else{
                return "E";
            }
        }else{
            return "-";
        }
        // return $this->db->get()->row();
    }


    //all active class 3 mentees
    public function activeF3Mentees()
    {
       $this->db->select('mentee.*,mship.*,mentor.mentorFname,mentor.mentorLname,sch.*,class.*');
        $this->db->from('mentees mentee');
        $this->db->join('mentorship mship', 'mentee.menteeAutoId=mship.mentorshipMenteeId','left');
        $this->db->join('schools  sch', 'sch.schoolAutoId=menteeSchoolId','left');
        $this->db->join('classes class', 'mentee.menteeAutoId=class.classMenteeId','left');
        $this->db->join('mentors mentor', 'mentor.mentorAutoId=mentee.menteeMentorId','left');
        $this->db->where('mentee.menteeActiveStatus',1);
        $this->db->order_by('mentee.menteeFname');

        $this->db->where('class.formCode',"FORM3");
        $this->db->where('mentee.menteeActiveStatus',1);
        $this->db->order_by('mentee.menteeFname');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //all active class 4 mentees
    public function activeF4Mentees()
    {
        $this->db->select('mentee.*,mship.*,mentor.mentorFname,mentor.mentorLname,sch.*,class.*');
        $this->db->from('mentees mentee');
        $this->db->join('mentorship mship', 'mentee.menteeAutoId=mship.mentorshipMenteeId','left');
        $this->db->join('schools  sch', 'sch.schoolAutoId=menteeSchoolId','left');
        $this->db->join('classes class', 'mentee.menteeAutoId=class.classMenteeId','left');
        $this->db->join('mentors mentor', 'mentor.mentorAutoId=mentee.menteeMentorId','left');
        $this->db->where('mentee.menteeActiveStatus',1);
        $this->db->order_by('mentee.menteeFname');

        $this->db->where('class.formCode',"FORM4");
        $this->db->where('mentee.menteeActiveStatus',1);
        $this->db->order_by('mentee.menteeFname');
        $result=$this->db->get()->result_array();
        return $result;
    }

    //list of all mentee/teacher schools
    public function schools()
    {
        $this->db->select('*');
        $this->db->from('schools');
        $result=$this->db->get()->result_array();
        return $result;
    }

    //Admin Registration
    public function registerAdmin($adminInfo)
    {
        if($this->db->insert('admins',$adminInfo))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }

    //Mentor Registration
    public function registerMentor($mentorInfo)
    {
        if($this->db->insert('mentors',$mentorInfo))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //Guardian Registration
    public function registerGuardian($guardianInfo)
    {
        if($this->db->insert('guardians',$guardianInfo))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //get specific parent
    public function getGuardian($guardianUID)
    {
        $this->db->select('guardian.*,COUNT(mentee.menteeGuardianId) as mentees');
        $this->db->from('guardians guardian');
        $this->db->join('mentees mentee', 'guardian.guardianAutoId=mentee.menteeGuardianId AND mentee.menteeActiveStatus=1', 'left');
        $this->db->where('guardianAutoId',$guardianUID);
        $this->db->group_by('guardian.guardianAutoId');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //Parent/Guardian update -No new profile photo uploaded or uploaded photo has errors
    public function updateGuardianNoPhoto($guardianUID,$guardianInfo)
    {
        $this->db->where('guardianAutoId',$guardianUID);
        $this->db->update('guardians',$guardianInfo);
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }
    }
    //parent/guardian update - new profile photo uploaded without errors 
    public function updateGuardian($guardianUID,$guardianInfo,$initFile)
    {
            $this->db->where('guardianAutoId',$guardianUID);
            $this->db->update('guardians',$guardianInfo);
            unlink("uploads/profile_photos/guardians/".$initFile);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
       //delete guardian 
    public function deleteGuardian($guardianUID)
    {
        
                
        //delete from class
        $this->db->where('guardianAutoId',$guardianUID);
        $query=$this->db->delete('guardians');
        $affected=$this->db->affected_rows();
        if($affected>0)
            {
                return true;

            }else
                {
                     return false;
                }
    }
     public function disableGuardian($guardianUID,$disableInfo)
    {
            $this->db->where('guardianAutoId',$guardianUID);
            $this->db->update('guardians',$disableInfo);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //mentee Registration
    public function registerMentee($menteeInfo)
    {
        if($this->db->insert('mentees',$menteeInfo))
            {
                $insert_id = $this->db->insert_id();
                return  $insert_id;//return id of last inserted record
            }
             else
                {
                    return false;
                }
    }
    //Add mentee to their respective classes. Follows success of the registerMentee($menteeInfo)
    public function insertMenteeToRespectiveForm($classInfo)
    {
            if($this->db->insert('classes',$classInfo))
            {
                return  true;
            }else
                {
                    return false;
                }
        
    }

    //mentee update -No new profile photo uploaded or uploaded photo has errors
    public function updateMenteeNoPhoto($menteeInfo,$menteeUID)
    {
            $this->db->where('menteeAutoId',$menteeUID);
            $this->db->update('mentees',$menteeInfo);
            // unlink("uploads/profile_photos/mentees/".$initFile);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }

    //mentee update - new profile photo uploaded without errors 
    public function updateMentee($menteeInfo,$menteeUID,$initFile)
    {
            $this->db->where('menteeAutoId',$menteeUID);
            $this->db->update('mentees',$menteeInfo);
            unlink("uploads/profile_photos/mentees/".$initFile);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //delete mentee 
    public function deleteMentee($menteeUID)
    {
        
                
        //delete from class
        $this->db->where('menteeAutoId',$menteeUID);
        $query=$this->db->delete('mentees');
        $affected=$this->db->affected_rows();
        if($affected>0)
            {
                return true;

            }else
                {
                     return false;
                }
    }
     public function disableMentee($menteeUID,$disableInfo)
    {
            $this->db->where('menteeAutoId',$menteeUID);
            $this->db->update('mentees',$disableInfo);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //get specific mentee
    public function getMentee($menteeUID)
    {
        $this->db->select('mentee.*,mship.*,sch.*,class.*,guardian.guardianFname,guardian.guardianLname,guardian.guardianPhone1,guardian.guardianPhone2,mentor.mentorFname,mentor.mentorLname');
        $this->db->from('mentees mentee');
        $this->db->join('mentorship mship', 'mentee.menteeAutoId=mship.mentorshipMenteeId','left');
        $this->db->join('schools  sch', 'sch.schoolAutoId=menteeSchoolId','left');
        $this->db->join('classes class', 'mentee.menteeAutoId=class.classMenteeId AND class.classYear='.date('Y'),'left');
        $this->db->join('mentors mentor', 'mentor.mentorAutoId=mentee.menteeMentorId','left');
        $this->db->join('guardians guardian','guardian.guardianAutoId=mentee.menteeGuardianId','left');
        $this->db->where('mentee.menteeAutoId',$menteeUID);
        // $this->db->where('classes.classYear',date('Y'));
        $result=$this->db->get()->result_array();
        return $result;
    }

    //mentee update to assign mentor
    public function assignMentor($menteeUID,$details)
    {
            $this->db->where('menteeAutoId',$menteeUID);
            $this->db->update('mentees',$details);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //mentee update to assign guardian
    public function assignGuardian($menteeUID,$details)
    {
            $this->db->where('menteeAutoId',$menteeUID);
            $this->db->update('mentees',$details);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }

    //intern Registration
    public function registerIntern($internInfo)
    {
        if($this->db->insert('interns',$internInfo))
            {
                return true;
            }else
                {
                    return false;
                }
    }
    //list of all active interns
    public function activeInterns()
    {
        $this->db->select('int.*,cs.*');
        $this->db->from('interns int');
        $this->db->join('courses cs','int.internCourseId=cs.courseAutoId','left');
        $this->db->where('int.internActiveStatus',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //list of all active interns
    public function activeInternsForAttendance()
    {
        //get Ids of mentors whose attendance date is equovalent today's 
            $this->db->select('attendance.attendanceInternId');
            $this->db->from('internattendance attendance');
            $this->db->where('attendance.attendanceDate ',date('Y-m-d'));
            $result=$this->db->get_compiled_select();

            $this->db->select('*');
            $this->db->from('interns');
            $this->db->where("`internAutoId` NOT IN ($result)", NULL, FALSE);//select only mentors whose ids dont appear on the above query
            $this->db->where('internActiveStatus',1);
            $this->db->order_by('internFname');
            $result2=$this->db->get()->result_array();
            return $result2;
    }
    //get specific intern
    public function getIntern($internUID)
    {
        $this->db->select('intern.*,course.*,inst.*');
        $this->db->from('interns intern');
        $this->db->join('institutions inst','intern.internInstitutionId=inst.institutionAutoId','left');
        $this->db->join('courses course','course.courseAutoId=intern.internCourseId','left');
        $this->db->where('intern.internAutoId',$internUID);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //Intern update -No new profile photo uploaded or uploaded photo has errors
    public function updateInternNoPhoto($internInfo,$internUID)
    {
        $this->db->where('internAutoId',$internUID);
        $this->db->update('interns',$internInfo);
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }
    }
    //Intern update - new profile photo uploaded without errors 
    public function updateIntern($internInfo,$internUID,$initFile)
    {
            $this->db->where('internAutoId',$internUID);
            $this->db->update('interns',$internInfo);
            unlink("uploads/profile_photos/interns/".$initFile);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
     //delete intern 
    public function deleteIntern($internUID)
    {
        
                
        //delete from class
        $this->db->where('internAutoId',$internUID);
        $query=$this->db->delete('interns');
        $affected=$this->db->affected_rows();
        if($affected>0)
            {
                return true;

            }else
                {
                     return false;
                }
    }
     public function disableIntern($internUID,$disableInfo)
    {
            $this->db->where('internAutoId',$internUID);
            $this->db->update('interns',$disableInfo);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //list of all registered courses
    public function courses()
    {
        $this->db->select('cs.*, inst.*');
        $this->db->from('courses cs, institutions inst');
        $this->db->where('cs.courseInstitutionId=inst.institutionAutoId');
        $result=$this->db->get()->result_array();
        return $result;
    }

    //Teacher Registration
    public function registerTeacher($teacherInfo)
    {
        if($this->db->insert('teachers',$teacherInfo))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //list of all active teachers
    public function activeTeachers()
    {
        $this->db->select('*');
        $this->db->from('teachers');
        $this->db->where('teacherActiveStatus',1);
        $result=$this->db->get()->result_array();
        return $result;
    }
    public function activeTeachersForAttendance()
    {
            //get Ids of teachers whose attendance date is equovalent today's 
            $this->db->select('attendance.attendanceTeacherId');
            $this->db->from('teacherattendance attendance');
            $this->db->where('attendance.attendanceDate ',date('Y-m-d'));
            $result=$this->db->get_compiled_select();

            $this->db->select('*');
            $this->db->from('teachers');
            $this->db->where("`teacherAutoId` NOT IN ($result)", NULL, FALSE);//select only mentors whose ids dont appear on the above query
            $this->db->where('teacherActiveStatus',1);
            $this->db->order_by('teacherFname');
            $result2=$this->db->get()->result_array();
            return $result2;
    }
    //get specific teacher
    public function getTeacher($teacherUID)
    {
        $this->db->select('teacher.*,school.*');
        $this->db->from('teachers teacher');
        $this->db->join('schools school','teacher.teacherSchoolId=school.schoolAutoId');
        $this->db->where('teacher.teacherAutoId',$teacherUID);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //Teacher update -No new profile photo uploaded or uploaded photo has errors
    public function updateTeacherNoPhoto($teacherInfo,$teacherUID)
    {
        $this->db->where('teacherAutoId',$teacherUID);
        $this->db->update('teachers',$teacherInfo);
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }
    }
    //teacher update - new profile photo uploaded without errors 
    public function updateTeacher($teacherInfo,$teacherUID,$initFile)
    {
            $this->db->where('teacherAutoId',$teacherUID);
            $this->db->update('teachers',$teacherInfo);
            unlink("uploads/profile_photos/teachers/".$initFile);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
     //delete teacher 
    public function deleteTeacher($teacherUID)
    {
        
                
        //delete from class
        $this->db->where('teacherAutoId',$teacherUID);
        $query=$this->db->delete('teachers');
        $affected=$this->db->affected_rows();
        if($affected>0)
            {
                return true;

            }else
                {
                     return false;
                }
    }
     public function disableTeacher($teacherUID,$disableInfo)
    {
            $this->db->where('teacherAutoId',$teacherUID);
            $this->db->update('teachers',$disableInfo);
            $affected=$this->db->affected_rows();
             if($affected>0)
                    {
                        return true;

                    }else
                        {
                            return false;
                        }
    }
    //list of all active parents/guardians
    public function activeGuardians()
    {
        $this->db->select('guardian.*,COUNT(mentee.menteeGuardianId) as mentees');
        $this->db->from('guardians guardian');
        $this->db->join('mentees mentee', 'guardian.guardianAutoId=mentee.menteeGuardianId AND mentee.menteeActiveStatus=1', 'left');
        $this->db->where('guardian.guardianActiveStatus',1);
        $this->db->group_by('guardian.guardianAutoId');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //list of all teacher institutions
    public function teacherInstitutions()
    {
        $this->db->select('*');
        $this->db->from('teacherinstitutions');
        $result=$this->db->get()->result_array();
        return $result;
    }

    //list of all general subjects
    public function generalSubjects()
    {
        $this->db->select('*');
        $this->db->from('generalsubjects');
        $this->db->where('subjectLocked',0);
        $result=$this->db->get()->result_array();
        return $result;
    }

    //list of all macheo subjects
    public function macheoSubjects()
    {
        $this->db->select('*');
        $this->db->from('macheosubjects');
        $this->db->where('subjectLocked',0);
        $result=$this->db->get()->result_array();
        return $result;
    } 

    //list of all general subjects selectable
    public function selectSubjects()
    {
        $this->db->select('*');
        $this->db->from('generalsubjects');
        $this->db->where('subjectCompulsory',0);
        $this->db->where('subjectLocked',0);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //list of all macheo subjects selectable
    public function selectMacheoSubjects()
    {
        $this->db->select('*');
        $this->db->from('macheosubjects');
        $this->db->where('subjectCompulsory',0);
        $this->db->where('subjectLocked',0);
        $result=$this->db->get()->result_array();
        return $result;
    }

    //list of all classes
    public function classes()
    {
        $this->db->select('*');
        $this->db->from('classes');
        $result=$this->db->get()->result_array();
        return $result;
    }

    //list of all macheo exams
    public function macheoExams()
    {
        $this->db->select('*');
        $this->db->from('macheoexams');
        $result=$this->db->get()->result_array();
        return $result;
    }
    public function registerMacheoExams($macheoexamInfo)
    {
        if($this->db->insert('macheoexams',$macheoexamInfo))
        {
            return true;
        }else
            {
                return false;
            }
    }
    //record new general exam scores 
    public function newMacheoExamScores($subjectScores)
    {
        if($this->db->insert_batch('macheo_exams_performance',$subjectScores))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //list of all general exams
    public function generalExams()
    {
        $this->db->select('*');
        $this->db->from('generalexams');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //general exams registration    
    public function registerGeneralExams($generalexamInfo)
    {
        if($this->db->insert('generalexams',$generalexamInfo))
        {
            return true;
        }else
            {
                return false;
            }
    }
    //record new general exam scores 
    public function newGeneralExamScores($subjectScores)
    {
        if($this->db->insert_batch('general_exams_performance',$subjectScores))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //save selected general subjects
    public function selectedGeneralSubjects($selectedSubjects)
    {
        if($this->db->insert_batch('selectedgeneralsubjects',$selectedSubjects))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //save selected macheo subjects
    public function selectedMacheoSubjects($selectedSubjects)
    {
        if($this->db->insert_batch('selectedmacheosubjects',$selectedSubjects))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //get list of macheo subjects taken by mentee
    public function getMacheoSubjects($pid)
    {
       $this->db->select('subs.*,ss.*');
        $this->db->from('selectedmacheosubjects ss');
        $this->db->join('macheosubjects subs', 'subs.subjectAutoId=ss.selectSubjectId');
        $this->db->where('ss.selectMenteeId',$pid);
        $this->db->order_by('subs.subjectAutoId');
        return $this->db->get()->result_array();
    }
    //get list of  subjects taken by mentee
    public function getSubjects($pid,$selected_subjects_table_name,$subjects_table)
    {
        $this->db->select('subs.*,ss.*');
        $this->db->from($selected_subjects_table_name.' ss');
        $this->db->join($subjects_table.' subs', 'subs.subjectAutoId=ss.selectSubjectId');
        $this->db->where('ss.selectMenteeId',$pid);
        $this->db->order_by('subs.subjectAutoId');
        return $this->db->get()->result_array();
    }
    //get list of  subjects taken by mentee
    public function getSubjectsPerMentee($pid,$examAutoId,$performance_table_name,$selected_subjects_table_name,$subjects_table)
    {
        //first, select mentee  subjects whose marks have been recorded for the specified exam
        $this->db->select('perf.perfSubjectId');
        $this->db->from($performance_table_name.' perf');
        $this->db->join($subjects_table.' subs', 'subs.subjectAutoId=perf.perfSubjectId');
        $this->db->where('perf.perfExamId',$examAutoId);
        $this->db->where('perf.perfMenteeId',$pid);
        $result1=$this->db->get_compiled_select();

        $this->db->select('subjects.*,ss.*');
        $this->db->from($selected_subjects_table_name.' ss');
        $this->db->join($subjects_table.' subjects', 'subjects.subjectAutoId=ss.selectSubjectId');
        $this->db->where("ss.selectSubjectId NOT IN ($result1)", NULL, FALSE);//select only subjects whose ids  do not appear on the above query
        $this->db->where('ss.selectMenteeId',$pid);
        $this->db->order_by('subjects.subjectAutoId');
        return $this->db->get()->result_array();
    }
    //get average per subject
    public function getSubjectAverage($subjectId,$examAutoId,$performance_table_name)
    {
        $this->db->select('AVG(perfScore) as average ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        return number_format((float)$this->db->get()->row()->average, 2, '.', '');//average formatted to 2 decimal places

    }
      //get average per subject per school
    public function getSubjectAveragePerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name)
    {
         //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolId);
        $result=$this->db->get_compiled_select();


        $this->db->select('AVG(perfScore) as average ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where("perfMenteeId IN ($result)", NULL, FALSE);//select only subjects whose ids appear on the above query
        $this->db->where('perfExamId',$examAutoId);
        return number_format((float)$this->db->get()->row()->average, 2, '.', '');//average formatted to 2 decimal places


    }
    //count number of A plains per subject
    public function getSubject_A_Plain($subjectId,$examAutoId,$performance_table_name)
    {
        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',81);
        $this->db->where('perfScore <=',100);
        return $this->db->get()->row()->total;
    }
    //count number of A plains per subject per school
    public function getSubject_A_PlainPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name)
    {
         //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolId);
        $result=$this->db->get_compiled_select();

        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where("perfMenteeId IN ($result)", NULL, FALSE);//select only subjects whose ids appear on the above query
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',81);
        $this->db->where('perfScore <=',100);
        return $this->db->get()->row()->total;
    }
    //count number of A Minus per subject
    public function getSubject_A_Minus($subjectId,$examAutoId,$performance_table_name)
    {
        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',74);
        $this->db->where('perfScore <=',80);
        return $this->db->get()->row()->total;
    }
     //count number of A Minus per subject Per School
    public function getSubject_A_MinusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name)
    {
         //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolId);
        $result=$this->db->get_compiled_select();

        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where("perfMenteeId IN ($result)", NULL, FALSE);//select only subjects whose ids appear on the above query
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',74);
        $this->db->where('perfScore <=',80);
        return $this->db->get()->row()->total;
    }
    //count number of B Plus per subject
    public function getSubject_B_Plus($subjectId,$examAutoId,$performance_table_name)
    {
        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',67);
        $this->db->where('perfScore <=',73);
        return $this->db->get()->row()->total;
    }
     //count number of B Plus per subject per school
    public function getSubject_B_PlusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name)
    {
        //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolId);
        $result=$this->db->get_compiled_select();

        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where("perfMenteeId IN ($result)", NULL, FALSE);//select only subjects whose ids appear on the above query
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',67);
        $this->db->where('perfScore <=',73);
        return $this->db->get()->row()->total;
    }
    //count number of B Plain per subject
    public function getSubject_B_Plain($subjectId,$examAutoId,$performance_table_name)
    {
        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',60);
        $this->db->where('perfScore <=',66);
        return $this->db->get()->row()->total;
    }
     //count number of B Plain per subject per school
    public function getSubject_B_PlainPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name)
    {
        //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolId);
        $result=$this->db->get_compiled_select();

        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where("perfMenteeId IN ($result)", NULL, FALSE);//select only subjects whose ids appear on the above query
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',60);
        $this->db->where('perfScore <=',66);
        return $this->db->get()->row()->total;
    }
    //count number of B Minus per subject
    public function getSubject_B_Minus($subjectId,$examAutoId,$performance_table_name)
    {
        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',53);
        $this->db->where('perfScore <=',59);
        return $this->db->get()->row()->total;
    }
     //count number of B Minus per subject per school
    public function getSubject_B_MinusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name)
    {
        //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolId);
        $result=$this->db->get_compiled_select();

        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where("perfMenteeId IN ($result)", NULL, FALSE);//select only subjects whose ids appear on the above query
         $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',53);
        $this->db->where('perfScore <=',59);
        return $this->db->get()->row()->total;
    }
    //count number of C Plus per subject
    public function getSubject_C_Plus($subjectId,$examAutoId,$performance_table_name)
    {
        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',46);
        $this->db->where('perfScore <=',52);
        return $this->db->get()->row()->total;
    }
     //count number of C Plus per subject per school
    public function getSubject_C_PlusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name)
    {
        //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolId);
        $result=$this->db->get_compiled_select();

        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where("perfMenteeId IN ($result)", NULL, FALSE);//select only subjects whose ids appear on the above query
         $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',46);
        $this->db->where('perfScore <=',52);
        return $this->db->get()->row()->total;
    }
    
    //count number of C Plain per subject
    public function getSubject_C_Plain($subjectId,$examAutoId,$performance_table_name)
    {
        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',39);
        $this->db->where('perfScore <=',45);
        return $this->db->get()->row()->total;
    }
    //count number of C Plain per subject per school
    public function getSubject_C_PlainPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name)
    {
        //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolId);
        $result=$this->db->get_compiled_select();

        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where("perfMenteeId IN ($result)", NULL, FALSE);//select only subjects whose ids appear on the above query
         $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',39);
        $this->db->where('perfScore <=',45);
        return $this->db->get()->row()->total;
    }
    //count number of C Minus per subject
    public function getSubject_C_Minus($subjectId,$examAutoId,$performance_table_name)
    {
        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',32);
        $this->db->where('perfScore <=',38);
        return $this->db->get()->row()->total;
    }
    //count number of C Minus per subject per school
    public function getSubject_C_MinusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name)
    {
        //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolId);
        $result=$this->db->get_compiled_select();

        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where("perfMenteeId IN ($result)", NULL, FALSE);//select only subjects whose ids appear on the above query
         $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',32);
        $this->db->where('perfScore <=',38);
        return $this->db->get()->row()->total;
    }
    //count number of D Plus per subject
    public function getSubject_D_Plus($subjectId,$examAutoId,$performance_table_name)
    {
        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',25);
        $this->db->where('perfScore <=',31);
        return $this->db->get()->row()->total;
    }
    //count number of D Plus per subject per school
    public function getSubject_D_PlusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name)
    {
        //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolId);
        $result=$this->db->get_compiled_select();

        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where("perfMenteeId IN ($result)", NULL, FALSE);//select only subjects whose ids appear on the above query
         $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',25);
        $this->db->where('perfScore <=',31);
        return $this->db->get()->row()->total;
    }
    //count number of D Plain per subject
    public function getSubject_D_Plain($subjectId,$examAutoId,$performance_table_name)
    {
        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',18);
        $this->db->where('perfScore <=',24);
        return $this->db->get()->row()->total;
    }
    //count number of D Plain per subject per school
    public function getSubject_D_PlainPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name)
    {
        //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolId);
        $result=$this->db->get_compiled_select();

        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where("perfMenteeId IN ($result)", NULL, FALSE);//select only subjects whose ids appear on the above query
         $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',18);
        $this->db->where('perfScore <=',24);
        return $this->db->get()->row()->total;
    }
    //count number of D Minus per subject
    public function getSubject_D_Minus($subjectId,$examAutoId,$performance_table_name)
    {
        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',11);
        $this->db->where('perfScore <=',17);
        return $this->db->get()->row()->total;
    }
     //count number of D Minus per subject per school
    public function getSubject_D_MinusPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name)
    {
        //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolId);
        $result=$this->db->get_compiled_select();

        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where("perfMenteeId IN ($result)", NULL, FALSE);//select only subjects whose ids appear on the above query
         $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',11);
        $this->db->where('perfScore <=',17);
        return $this->db->get()->row()->total;
    }
    //count number of E's per subject
    public function getSubject_E($subjectId,$examAutoId,$performance_table_name)
    {
        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',7);
        $this->db->where('perfScore <=',16);
        return $this->db->get()->row()->total;
    }
      //count number of E's per subject
    public function getSubject_EPerSchool($schoolId,$subjectId,$examAutoId,$performance_table_name)
    {
        //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolId);
        $result=$this->db->get_compiled_select();

        $this->db->select('COUNT(perfScore) as total ');
        $this->db->from($performance_table_name);
        $this->db->where('perfSubjectId',$subjectId);
        $this->db->where("perfMenteeId IN ($result)", NULL, FALSE);//select only subjects whose ids appear on the above query
         $this->db->where('perfExamId',$examAutoId);
        $this->db->where('perfScore >=',7);
        $this->db->where('perfScore <=',16);
        return $this->db->get()->row()->total;
    }

    //Institution registration
    public function registerInstitution($institutionInfo)
    {
        if($this->db->insert('institutions',$institutionInfo))
            {
                return true;
            }else
                {
                    return false;
                }
    }
    //get specific institution
    public function getInstitution($institutionUID)
    {
        $this->db->select('institution.*');
        $this->db->from('institutions institution');
        //$this->db->join('mentors mentor','institution.institutionAutoId=mentor.mentorInstitutionId');
        $this->db->where('institution.institutionAutoId',$institutionUID);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //update institution details
    public function updateInstitutionNoPhoto($institutionInfo,$institutionUID)
    {
        $this->db->where('institutionAutoId',$institutionUID);
        $this->db->update('institutions',$institutionInfo);
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }
    }
      //School registration
    public function registerSchool($schoolInfo)
    {
        if($this->db->insert('schools',$schoolInfo))
            {
                return true;
            }else
                {
                    return false;
                }
    }
    //
    public function schoolCategories()
    {
        $this->db->select('*');
        $this->db->from('schoolcategories');
        $result=$this->db->get()->result_array();
        return $result;
    }
        //get specific school
    public function getSchool($schoolUID)
    {
        $this->db->select('school.*,schcategory.*');
        $this->db->from('schools school');
        $this->db->join('schoolcategories schcategory','school.schoolCategoryId=schcategory.categoryAutoId');
        $this->db->where('school.schoolAutoId',$schoolUID);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //update school details
        public function updateSchoolNoPhoto($schoolInfo,$schoolUID)
    {
        $this->db->where('schoolAutoId',$schoolUID);
        $this->db->update('schools',$schoolInfo);
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }
    }
        //Course registration
        public function registerCourse($courseInfo)
    {
        if($this->db->insert('courses',$courseInfo))
            {
                return true;
            }else
                {
                    return false;
                }
    } 
        //Get specific course
     public function getCourse($courseUID)
    {
        $this->db->select('course.*,institution.institutionName');
        $this->db->from('courses course');
        $this->db->join('institutions institution','course.courseInstitutionId=institution.institutionAutoId');
        $this->db->where('course.courseAutoId',$courseUID);
        $result=$this->db->get()->result_array();
        return $result;
    }
    //update school details
        public function updateCourse($courseInfo,$courseUID)
    {
        $this->db->where('courseAutoId',$courseUID);
        $this->db->update('courses',$courseInfo);
        $affected=$this->db->affected_rows();
         if($affected>0)
                {
                    return true;

                }else
                    {
                        return false;
                    }
    }
       
    // admin dashboard counts

    //count mentees
    public function menteesCount()
    {
        $this->db->select('*');
        $this->db->from('mentees');
        $this->db->where('menteeActiveStatus',1);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }

    //count mentors
    public function mentorsCount()
    {
        $this->db->select('*');
        $this->db->from('mentors');
        $this->db->where('mentorActiveStatus',1);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count interns
    public function internsCount()
    {
        $this->db->select('*');
        $this->db->from('interns');
        $this->db->where('internActiveStatus',1);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count admins
    public function adminsCount()
    {
        $this->db->select('*');
        $this->db->from('admins');
        $this->db->where('adminActiveStatus',1);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count general exams
    public function teachersCount()
    {
        $this->db->select('*');
        $this->db->from('teachers');
        $this->db->where('teacherActiveStatus',1);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
           {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count macheo exams
    public function macheoExamsCount()
    {
        $this->db->select('*');
        $this->db->from('macheoexams');
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count general exams
    public function generalExamsCount()
    {
        $this->db->select('*');
        $this->db->from('generalexams');
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }

    //attendance records
    //save selected mentee attendance
    public function menteeAttendance($attendanceInfo)
    {
        if($this->db->insert_batch('menteeattendance',$attendanceInfo))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //count of all mentees marked present, absent or excused in a particular date
    public function countOfMenteesMarkedPresentAbsentOrExcused($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('menteeattendance');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
     //count of all mentees marked present, absent or excused in a particular date per school
    public function countOfMenteesMarkedPresentAbsentOrExcusedPerSchool($attendanceDate,$schoolUID)
    {
         //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolUID);
        $result=$this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('menteeattendance');
        $this->db->where('attendanceDate',$attendanceDate);
        $this->db->where("`attendanceMenteeId` IN ($result)", NULL, FALSE);//select only mentees whose ids appear on the above query
        $result2=$this->db->get();
        if ( $result2->num_rows() > 0 )
            {
                return $result2->num_rows();
            }else
                {
                    return 0;
                }
    }
    //list of all forms
    public function getAllForms()
    {
        $this->db->select('*');
        $this->db->from('forms');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //mentee attendance per form
    public function viewMenteeAttendance($formCode,$attendanceDate)
    {
        $this->db->select('mentee.menteeFname, mentee.menteeLname,mentee.menteeProfilePhoto,attendance.attendanceStatus,attendance.attendanceAutoId, school.schoolAlias');
        $this->db->from('menteeattendance attendance');
        $this->db->join('mentees mentee','mentee.menteeAutoId=attendance.attendanceMenteeId');
        $this->db->join('classes class','mentee.menteeAutoId=class.classMenteeId');
        $this->db->join('schools school','mentee.menteeSchoolId=school.schoolAutoId');
        $this->db->where('attendance.attendanceDate',$attendanceDate);
        $this->db->where('class.formCode',$formCode);
        $result=$this->db->get()->result_array();
        return $result;

    }
    //all mentee attendance per date
    public function allMenteeAttendance($attendanceDate)
    {
        $this->db->select('mentee.menteeFname, mentee.menteeLname,mentee.menteeProfilePhoto,attendance.attendanceStatus,attendance.attendanceAutoId, school.schoolAlias');
        $this->db->from('menteeattendance attendance');
        $this->db->join('mentees mentee','mentee.menteeAutoId=attendance.attendanceMenteeId');
        $this->db->join('classes class','mentee.menteeAutoId=class.classMenteeId');
        $this->db->join('schools school','mentee.menteeSchoolId=school.schoolAutoId');
        $this->db->where('attendance.attendanceDate',$attendanceDate);
        $result=$this->db->get()->result_array();
        return $result;

    }
    //get individual mentee attendance status
    public function getMenteeAttendance($recordId)
    {
         $this->db->select('mentee.menteeFname, mentee.menteeLname,attendance.attendanceStatus,attendance.attendanceAutoId');
        $this->db->from('menteeattendance attendance');
        $this->db->join('mentees mentee','mentee.menteeAutoId=attendance.attendanceMenteeId');
        $this->db->where('attendance.attendanceAutoId',$recordId);
        $result=$this->db->get()->result_array();
        return $result;

    }
    //delete mentee attendance
    public function deleteMenteeAttendanceRecord($recordId)
     {
        $this->db->where('attendanceAutoId',$recordId);
        $query=$this->db->delete('menteeattendance');
        
        $affected=$this->db->affected_rows();
            
        if($affected>0)
            {
                return true;

            }else
                {
                    return false;
                }
    }


    // $this->db->select('menteeAutoId');
    //     $this->db->from('mentees');
    //     $this->db->where('menteeSchoolId ',$schoolUID);
    //     $result=$this->db->get_compiled_select();

    //     $this->db->select('attendance.attendanceDate');
    //     $this->db->from('menteeattendance attendance');
    //     $this->db->join('mentees mentee','mentee.menteeAutoId=attendance.attendanceMenteeId');
    //     $this->db->join('classes class','mentee.menteeAutoId=class.classMenteeId');
    //     $this->db->where("attendance.attendanceMenteeId IN ($result)", NULL, FALSE);//select only mentees whose ids  appear on the above query
    //     $this->db->where('class.formCode',$formCode);
    //     $this->db->group_by('attendance.attendanceDate');
    //     $this->db->order_by('attendance.attendanceDate','DESC');
    //     $result2=$this->db->get()->result_array();
    //     return $result2;


    //count of all mentees per form marked present, absent or excused in a particular date
    public function countOfMenteesMarkedPresentAbsentOrExcusedPerForm($attendanceDate,$formCode)
    {
        $this->db->select('attendance.*');
        $this->db->from('menteeattendance attendance');
        $this->db->join('mentees mentee','mentee.menteeAutoId=attendance.attendanceMenteeId');
        $this->db->join('classes class','mentee.menteeAutoId=class.classMenteeId');
        $this->db->where('class.formCode',$formCode);
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count of all mentees per form marked present, absent or excused in a particular date per school
    public function countOfMenteesMarkedPresentAbsentOrExcusedPerFormPerSchool($attendanceDate,$formCode,$schoolUID)
    {
        $this->db->select('attendance.*');
        $this->db->from('menteeattendance attendance');
        $this->db->join('mentees mentee','mentee.menteeAutoId=attendance.attendanceMenteeId');
        $this->db->join('classes class','mentee.menteeAutoId=class.classMenteeId');
        $this->db->where('mentee.menteeSchoolId',$schoolUID);
        $this->db->where('class.formCode',$formCode);
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }

     //mentee attendance status
    public function menteesAttendanceStatusPerForm($attendanceDate,$formCode,$menteeUID)
    {
        $this->db->select('attendance.attendanceStatus');
        $this->db->from('menteeattendance attendance');
        $this->db->join('mentees mentee','attendance.attendanceMenteeId='.$menteeUID);
        $this->db->join('classes class','mentee.menteeAutoId=class.classMenteeId');
        $this->db->where('class.formCode',$formCode);
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                $row = $result->row(); 
                return $row->attendanceStatus;
            }else
                {
                    return null;
                }
    }
   
   //mentee attendance grouped by date per school
    public function menteeAttendanceGroupedPerDatePerSchool($schoolUID)
   {
        //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolUID);
        $result2=$this->db->get_compiled_select();

        $this->db->select('attendanceDate');
        $this->db->from('menteeattendance');
        $this->db->where("`attendanceMenteeId` IN ($result2)", NULL, FALSE);//select only mentees whose ids appear on the above query
        $this->db->group_by('attendanceDate');
        $this->db->order_by('attendanceDate','DESC');
        $result=$this->db->get()->result_array();
        return $result;
    }
// ->where("DATE_FORMAT(column_name,'%Y-%m')", $bulan)
// $this->db->group_by('MONTH(date), YEAR(date)');
    //mentee attendance grouped by month per mentee
    public function menteeAttendanceGroupedPerDatePerMentee($menteeUID)
    {
        //get Ids of mentees belonging to the particular school 
        $this->db->select('attendanceMenteeId');
        $this->db->from('menteeattendance');
        $this->db->where('attendanceMenteeId ',$menteeUID);
        $result1=$this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('menteeattendance');
        $this->db->where("`attendanceMenteeId` IN ($result1)", NULL, FALSE);//select only mentees whose ids appear on the above query
        $this->db->group_by('attendanceDate');
        $this->db->order_by('attendanceDate','DESC');
        $result2=$this->db->get()->result_array();
        return $result2;
    }


    //mentee attendance grouped by date
    public function menteeAttendanceGroupedPerDate()
    {
        $this->db->select('attendance.attendanceDate');
        $this->db->from('menteeattendance attendance');
        $this->db->group_by('attendance.attendanceDate');
        $this->db->order_by('attendance.attendanceDate','DESC');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //mentee attendance grouped by date per form
    public function menteeAttendanceGroupedPerDatePerForm($formCode)
    {
        $this->db->select('attendance.attendanceDate');
        $this->db->from('menteeattendance attendance');
        $this->db->join('mentees mentee','mentee.menteeAutoId=attendance.attendanceMenteeId');
        $this->db->join('classes class','mentee.menteeAutoId=class.classMenteeId');
        $this->db->where('class.formCode',$formCode);
        $this->db->group_by('attendance.attendanceDate');
        $this->db->order_by('attendance.attendanceDate','DESC');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //mentee attendance grouped by date per form
    public function menteeAttendanceGroupedPerDatePerFormPerMentee($formCode,$menteeUID)
    {
        $this->db->select('attendance.attendanceDate');
        $this->db->from('menteeattendance attendance');
        $this->db->join('mentees mentee','attendance.attendanceMenteeId=mentee.menteeAutoId');
        $this->db->join('classes class','mentee.menteeAutoId=class.classMenteeId');
        $this->db->where('class.formCode',$formCode);
        $this->db->where('mentee.menteeAutoId',$menteeUID);
        $this->db->group_by('attendance.attendanceDate');
        $this->db->order_by('attendance.attendanceDate','DESC');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //count all mentees marked present in a particular date
    public function countOfMenteesPresent($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('menteeattendance');
        $this->db->where('attendanceStatus','PRESENT');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
     //count all mentees marked present in a particular date per school
    public function countOfMenteesPresentPerSchool($attendanceDate,$schoolUID)
    {
        //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolUID);
        $result=$this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('menteeattendance');
        $this->db->where("`attendanceMenteeId` IN ($result)", NULL, FALSE);//select only mentees whose ids appear on the above query
        $this->db->where('attendanceStatus','PRESENT');
        $this->db->where('attendanceDate',$attendanceDate);
        $result2=$this->db->get();
        if ( $result2->num_rows() > 0 )
            {
                return $result2->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count all mentees marked absent in a particular date
    public function countOfMenteesAbsent($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('menteeattendance');
        $this->db->where('attendanceStatus','ABSENT');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count all mentees marked absent in a particular date per school
    public function countOfMenteesAbsentPerSchool($attendanceDate,$schoolUID)
    {
        //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolUID);
        $result=$this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('menteeattendance');
        $this->db->where("`attendanceMenteeId` IN ($result)", NULL, FALSE);//select only mentees whose ids appear on the above query
        $this->db->where('attendanceStatus','PRESENT');
        $this->db->where('attendanceDate',$attendanceDate);
        $result2=$this->db->get();
        if ( $result2->num_rows() > 0 )
            {
                return $result2->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count all mentees marked excused in a particular date
    public function countOfMenteesExcused($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('menteeattendance');
        $this->db->where('attendanceStatus','EXCUSED');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count all mentees marked excused in a particular date per school
    public function countOfMenteesExcusedPerSchool($attendanceDate,$schoolUID)
    {
        //get Ids of mentees belonging to the particular school 
        $this->db->select('menteeAutoId');
        $this->db->from('mentees');
        $this->db->where('menteeSchoolId ',$schoolUID);
        $result=$this->db->get_compiled_select();

        $this->db->select('*');
        $this->db->from('menteeattendance');
        $this->db->where("`attendanceMenteeId` IN ($result)", NULL, FALSE);//select only mentees whose ids appear on the above query
        $this->db->where('attendanceStatus','EXCUSED');
        $this->db->where('attendanceDate',$attendanceDate);
        $result2=$this->db->get();
        if ( $result2->num_rows() > 0 )
            {
                return $result2->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count all form mentees marked present in a particular date
    public function countOfMenteesPresentPerFormPerDate($attendanceDate,$formCode)
    {
        $this->db->select('attendance.*');
        $this->db->from('menteeattendance attendance');
        $this->db->join('mentees mentee','mentee.menteeAutoId=attendance.attendanceMenteeId');
        $this->db->join('classes class','mentee.menteeAutoId=class.classMenteeId');
        $this->db->where('class.formCode',$formCode);
        $this->db->where('attendanceStatus','PRESENT');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count all form mentees marked present in a particular date per school
    public function countOfMenteesPresentPerFormPerDatePerSchool($attendanceDate,$formCode,$schoolUID)
    {
        $this->db->select('attendance.*');
        $this->db->from('menteeattendance attendance');
        $this->db->join('mentees mentee','mentee.menteeAutoId=attendance.attendanceMenteeId');
        $this->db->join('classes class','mentee.menteeAutoId=class.classMenteeId');
        $this->db->where('mentee.menteeSchoolId',$schoolUID);
        $this->db->where('class.formCode',$formCode);
        $this->db->where('attendanceStatus','PRESENT');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count form mentees marked absent  in a particular date
    public function countOfMenteesAbsentPerFormPerDate($attendanceDate,$formCode)
    {
        $this->db->select('attendance.*');
        $this->db->from('menteeattendance attendance');
        $this->db->join('mentees mentee','mentee.menteeAutoId=attendance.attendanceMenteeId');
        $this->db->join('classes class','mentee.menteeAutoId=class.classMenteeId');
        $this->db->where('class.formCode',$formCode);
        $this->db->where('attendanceStatus','ABSENT');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count form mentees marked absent  in a particular date per school
    public function countOfMenteesAbsentPerFormPerDatePerSchool($attendanceDate,$formCode,$schoolUID)
    {
        $this->db->select('attendance.*');
        $this->db->from('menteeattendance attendance');
        $this->db->join('mentees mentee','mentee.menteeAutoId=attendance.attendanceMenteeId');
        $this->db->join('classes class','mentee.menteeAutoId=class.classMenteeId');
        $this->db->where('mentee.menteeSchoolId',$schoolUID);
        $this->db->where('class.formCode',$formCode);
        $this->db->where('attendanceStatus','ABSENT');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
   
   //count form mentees marked excused in a particular date
    public function countOfMenteesExcusedPerFormPerDate($attendanceDate,$formCode)
    {
        $this->db->select('attendance.*');
        $this->db->from('menteeattendance attendance');
        $this->db->join('mentees mentee','mentee.menteeAutoId=attendance.attendanceMenteeId');
        $this->db->join('classes class','mentee.menteeAutoId=class.classMenteeId');
        $this->db->where('class.formCode',$formCode);
        $this->db->where('attendanceStatus','EXCUSED');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count form mentees marked excused in a particular date per school
    public function countOfMenteesExcusedPerFormPerDatePerSchool($attendanceDate,$formCode,$schoolUID)
    {
        $this->db->select('attendance.*');
        $this->db->from('menteeattendance attendance');
        $this->db->join('mentees mentee','mentee.menteeAutoId=attendance.attendanceMenteeId');
        $this->db->join('classes class','mentee.menteeAutoId=class.classMenteeId');
        $this->db->where('mentee.menteeSchoolId',$schoolUID);
        $this->db->where('class.formCode',$formCode);
        $this->db->where('attendanceStatus','EXCUSED');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
   
  
    //save selected mentor attendance
    public function mentorAttendance($attendanceInfo)
    {
        if($this->db->insert_batch('mentorattendance',$attendanceInfo))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //mentor attendance grouped by date
    public function mentorAttendanceGroupedPerDate()
    {
        $this->db->select('attendance.attendanceDate');
        $this->db->from('mentorattendance attendance');
        $this->db->group_by('attendance.attendanceDate');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //count of all mentors marked present, absent or excused in a particular date
    public function countOfMentorsMarkedPresentAbsentOrExcused($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('mentorattendance');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count mentors marked present in a particular date
    public function countOfMentorsPresent($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('mentorattendance');
        $this->db->where('attendanceStatus','PRESENT');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count mentors marked absent in a particular date
    public function countOfMentorsAbsent($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('mentorattendance');
        $this->db->where('attendanceStatus','ABSENT');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count mentors marked excused in a particular date
    public function countOfMentorsExcused($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('mentorattendance');
        $this->db->where('attendanceStatus','EXCUSED');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //save selected intern attendance
    public function internAttendance($attendanceInfo)
    {
        if($this->db->insert_batch('internattendance',$attendanceInfo))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //count of all interns marked present, absent or excused in a particular date
    public function countOfInternsMarkedPresentAbsentOrExcused($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('internattendance');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //intern attendance grouped by date
    public function internAttendanceGroupedPerDate()
    {
        $this->db->select('attendance.attendanceDate');
        $this->db->from('internattendance attendance');
        $this->db->group_by('attendance.attendanceDate');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //count interns marked present in a particular date
    public function countOfInternsPresent($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('internattendance');
        $this->db->where('attendanceStatus','PRESENT');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count interns marked absent in a particular date
    public function countOfInternsAbsent($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('internattendance');
        $this->db->where('attendanceStatus','ABSENT');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count interns marked excused in a particular date
    public function countOfInternsExcused($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('internattendance');
        $this->db->where('attendanceStatus','EXCUSED');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //save selected teacher attendance
    public function teacherAttendance($attendanceInfo)
    {
        if($this->db->insert_batch('teacherattendance',$attendanceInfo))
            {
                return true;
            }
             else
                {
                    return false;
                }
    }
    //count of all mentors marked present, absent or excused in a particular date
    public function countOfTeachersMarkedPresentAbsentOrExcused($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('teacherattendance');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //teacher attendance grouped by date
    public function teacherAttendanceGroupedPerDate()
    {
        $this->db->select('attendance.attendanceDate');
        $this->db->from('teacherattendance attendance');
        $this->db->group_by('attendance.attendanceDate');
        $result=$this->db->get()->result_array();
        return $result;
    }
    //count teachers marked present in a particular date
    public function countOfTeachersPresent($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('teacherattendance');
        $this->db->where('attendanceStatus','PRESENT');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count teachers marked absent in a particular date
    public function countOfTeachersAbsent($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('teacherattendance');
        $this->db->where('attendanceStatus','ABSENT');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
    //count teachers marked excused in a particular date
    public function countOfTeachersExcused($attendanceDate)
    {
        $this->db->select('*');
        $this->db->from('teacherattendance');
        $this->db->where('attendanceStatus','EXCUSED');
        $this->db->where('attendanceDate',$attendanceDate);
        $result=$this->db->get();
        if ( $result->num_rows() > 0 )
            {
                return $result->num_rows();
            }else
                {
                    return 0;
                }
    }
}//close class MainModel
