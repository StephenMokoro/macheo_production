<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @author Mokoro Stephen
*/
class LoginModel extends CI_Model
{
	function __construct()
		{
		     parent::__construct();
		     $this->load->database();
		}

    //login admin
	public function validate_admin($username) 
        {
            $this->db->select('*');
            $this->db->from('admins');
            $this->db->where('adminUsername', $username);
            $this->db->where('adminActiveStatus', 1);
            $this->db->limit(1);
            $query = $this->db->get();
             if ($query->num_rows() == 1) 
                {
                    return $query->result(); 

                } else 
                        {
                            return false;
                        }
        }
     //login mentor
    public function validate_mentor($username, $password) 
        {
            $this->db->select('*');
            $this->db->from('mentors');
            $this->db->where('mentorEmail', $username);
            $this->db->where('mentorPassword',$password);
            // $this->db->where('mts.mentorInstitutionId=inst.institutionAutoId');
            $this->db->where('mentorActiveStatus', 1);
            $this->db->limit(1);
            $query = $this->db->get();
             if ($query->num_rows() == 1) 
                {
                    return $query->result(); 

                } else 
                        {
                            return false;
                        }
    }
   
}